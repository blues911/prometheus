<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->string('name');
			$table->enum('list', [
				'diamond',
				'platinum',
				'gold'
			]);
			$table->enum('type', [
				'agency',
				'online',
				'print',
				'tv',
				'radio'
			]);
			$table->enum('category', [
				'vertical',
				'business',
				'it',
				'general'
			]);
			$table->integer('reach')->nullable();
			$table->integer('region_id');
			$table->integer('country_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('media');
	}

}
