<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableMediaSubcategory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media_subcategory', function($table)
		{
			$table->integer('media_id')->unsigned();
			$table->integer('subcategory_id')->unsigned();
			$table->foreign('media_id')->references('id')->on('media')->onDelete('restrict');
			$table->foreign('subcategory_id')->references('id')->on('subcategory')->onDelete('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('media_subcategory');
	}

}
