<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('system_users', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();

			$table->string('password');
			$table->string('email');
			$table->string('remember_token')->nullable();

            $table->integer('region_id');
            $table->integer('country_id');
			$table->string('name')->nullable();
			$table->enum('role', [
				'administrator',
				'manager'
			]);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('system_users');
	}

}
