<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeKpiHeadersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE kpi_headers MODIFY COLUMN net_effect varchar (255) NOT NULL;');
		DB::statement('ALTER TABLE kpi_headers MODIFY COLUMN impact_index varchar (255) NOT NULL;');
		DB::statement('ALTER TABLE kpi_headers MODIFY COLUMN negative varchar (255) NOT NULL;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
