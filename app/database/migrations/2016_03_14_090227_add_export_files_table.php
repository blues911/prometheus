<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExportFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('export_files', function (Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->string('name');
			$table->string('filename');
			$table->integer('user_id')->unsigned();
			$table->timestamps();
		});

		DB::statement("ALTER TABLE `export_files`
			ADD FOREIGN KEY (`user_id`) REFERENCES `system_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
		");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('export_files');
	}

}
