<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Speaker;
use App\Models\Coverage;

class SpeakersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('speakers_coverage', function($table)
		{
			$table->integer('speaker_id')->unsigned();
			$table->integer('coverage_id')->unsigned();
			$table->foreign('speaker_id')->references('id')->on('speakers')->onDelete('restrict');
			$table->foreign('coverage_id')->references('id')->on('coverage')->onDelete('cascade');
		});

		$coverages = \DB::table('coverage')
			->select("id", "speakers_quoted")
			->whereRaw('LENGTH(speakers_quoted) > 0')
			->groupBy("id", "speakers_quoted")
			->get();

		foreach ($coverages as $coverage_speakers) {
			$speakers = explode(",", $coverage_speakers->speakers_quoted);
			foreach ($speakers as $speaker) {
				$speakerObject = Speaker::firstOrCreate(['name'=> trim($speaker)]);
				$coverage      = Coverage::find($coverage_speakers->id);
				if (!$coverage->hasSpeaker($speakerObject->id)) {
					$coverage->speakers()->attach($speakerObject->id);
				}
			}
		}

		DB::statement("ALTER TABLE `coverage`
			CHANGE `speakers_quoted` `obsolete_speakers_quoted` varchar(255) COLLATE 'utf8_unicode_ci' NOT NULL;
		");
		DB::statement("ALTER TABLE `coverage`
			CHANGE `obsolete_speakers_quoted` `obsolete_speakers_quoted` varchar(255) COLLATE 'utf8_unicode_ci' NULL AFTER `url`;
		");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
