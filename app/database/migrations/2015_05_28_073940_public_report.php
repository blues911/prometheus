<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PublicReport extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("TRUNCATE TABLE `save_reports`;");
		DB::statement("ALTER TABLE `save_reports`
			ADD `params` text NOT NULL DEFAULT '',
			ADD `hash` varchar(100) NOT NULL DEFAULT '' AFTER `params`;
		");
		DB::statement("ALTER TABLE `save_reports` ADD `tab` varchar(100) COLLATE 'utf8_unicode_ci' NULL;");
		DB::statement("ALTER TABLE `save_reports` ADD `user_id` int(10) unsigned NULL, ADD FOREIGN KEY (`user_id`) REFERENCES `system_users` (`id`);");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
