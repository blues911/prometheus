<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCoverageCodingProcess extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("TRUNCATE `coverage_coding_process`");
        DB::statement("ALTER TABLE `coverage_coding_process` CHANGE COLUMN `month` `year` INTEGER NOT NULL");
        DB::statement("ALTER TABLE `coverage_coding_process` CHANGE COLUMN `status` `months` TEXT DEFAULT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
