<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableCoverageSubcategory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('coverage_subcategory', function($table)
		{
			$table->integer('coverage_id')->unsigned();
			$table->integer('subcategory_id')->unsigned();
			$table->foreign('coverage_id')->references('id')->on('coverage')->onDelete('restrict');
			$table->foreign('subcategory_id')->references('id')->on('subcategory')->onDelete('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('coverage_subcategory');
	}

}
