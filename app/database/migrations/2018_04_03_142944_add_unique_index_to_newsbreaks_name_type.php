<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueIndexToNewsbreaksNameType extends Migration {

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
                DB::statement("ALTER TABLE `newsbreaks` DROP INDEX `name`");

		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
            DB::statement("ALTER TABLE `newsbreaks` ADD UNIQUE `name` (`name`)");
		}

}
