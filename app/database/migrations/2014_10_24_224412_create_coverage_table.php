<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoverageTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'coverage',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('owner_id');
                $table->integer('region_id');
                $table->integer('country_id');
                $table->timestamps();

                $table->string('name');
                $table->float('reach');
                $table->string('headline');
                $table->string('author');
                $table->string('url')->nullable();
                $table->enum(
                    'media_type',
                    [
                        'agency',
                        'online',
                        'print',
                        'radio',
                        'tv'
                    ]
                );
                $table->enum(
                    'media_category',
                    [
                        'vertical',
                        'business',
                        'it',
                        'general'
                    ]
                )->nullable();
                $table->enum(
                    'visibility',
                    [
                        0.4,
                        0.75,
                        1
                    ]
                );
                $table->string('speakers_quoted');
                $table->string('third_speakers_quoted');
                $table->enum('status', [
                        'draft',
                        'complete',
                        'approved'
                    ]);
                $table->enum('key_message_penetration', [1, 0.6, 0]);
                $table->enum('media_list', [1, 0.8, 0.6]);
                $table->enum('mention', [1, 0.9, 0.8, 0.5]);
                $table->enum('speaker_quote', [1, 0.8])->default('0.8');
                $table->enum('image', [1, 0.5]);
                $table->enum('header_tonality', [1, 0.85, 0.5]);
                $table->enum('main_tonality', [1, 0]);
                $table->enum('third_speakers_quotes', [1, 0])->default('0');
                $table->string('negative_explanation');
                $table->string('campaign');
                $table->string('event_name');
                $table->string('news_break');

                $table->boolean('desired_article');

                $table->float('impact_index');
                $table->float('net_effect');
                $table->dateTime('date')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('system_users');
    }

}
