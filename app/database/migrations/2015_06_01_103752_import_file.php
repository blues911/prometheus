<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportFile extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('import_files', function (Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->string('name');
			$table->timestamps();
		});

		DB::statement("ALTER TABLE `coverage` ADD `file_id` int unsigned NULL;");
		DB::statement("ALTER TABLE `coverage` ADD FOREIGN KEY (`file_id`) REFERENCES `import_files` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
