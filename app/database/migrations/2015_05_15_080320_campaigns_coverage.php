<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Campaign;
use App\Models\Coverage;

class CampaignsCoverage extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('campaigns_coverage', function($table)
		{
			$table->integer('campaign_id')->unsigned();
			$table->integer('coverage_id')->unsigned();
			$table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('restrict');
			$table->foreign('coverage_id')->references('id')->on('coverage')->onDelete('restrict');
		});

		$campaigns = \DB::table('coverage')
			->select("id", "campaign")
			->whereRaw('LENGTH(campaign) > 0')
			->groupBy("campaign")
			->get();


		foreach ($campaigns as $campaign) {
			$names = explode(",", $campaign->campaign);
			foreach ($names as $campaign_name) {
				$campaignObject = Campaign::firstOrCreate(['name'=> trim($campaign_name)]);
				$coverage       = Coverage::find($campaign->id);
				if (!$coverage->hasCampaign($campaignObject->id)) {
					$coverage->campaigns()->attach($campaignObject->id);
				}
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
