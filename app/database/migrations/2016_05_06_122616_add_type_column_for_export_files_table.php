<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeColumnForExportFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('export_files', function (Blueprint $table) {
			$table->enum('type', array('pdf', 'xls'))->default('xls');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('export_files', function (Blueprint $table) {
			$table->dropColumn('type');
		});
	}

}
