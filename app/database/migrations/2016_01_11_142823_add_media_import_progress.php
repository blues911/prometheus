<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMediaImportProgress extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("UPDATE notifications SET type = 'import_coverage' WHERE type = 'import'");
		DB::statement("ALTER TABLE `import_files` ADD `type` varchar(255) NULL AFTER `active`;");
		DB::statement("UPDATE import_files SET type = 'import_coverage' WHERE type IS NULL;");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
