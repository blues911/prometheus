<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\NewsBreak;
use App\Models\Coverage;

class NewsbreaksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('newsbreaks_coverage', function($table)
		{
			$table->integer('newsbreak_id')->unsigned();
			$table->integer('coverage_id')->unsigned();
			$table->foreign('newsbreak_id')->references('id')->on('newsbreaks')->onDelete('restrict');
			$table->foreign('coverage_id')->references('id')->on('coverage')->onDelete('cascade');
		});

		$coverages = \DB::table('coverage')
			->select("id", "news_break")
			->whereRaw('LENGTH(news_break) > 0')
			->get();

		foreach ($coverages as $coverage_newsbreak) {
			$newsbreakObject = NewsBreak::firstOrCreate(['name'=> trim($coverage_newsbreak->news_break)]);
			$coverage        = Coverage::find($coverage_newsbreak->id);
			if (!$coverage->hasNewsbreak($newsbreakObject->id)) {
				$coverage->newsbreaks()->attach($newsbreakObject->id);
			}
		}

		DB::statement("ALTER TABLE `coverage`
			CHANGE `campaign` `obsolete_campaign` varchar(255) COLLATE 'utf8_unicode_ci' NOT NULL AFTER `negative_explanation`,
			CHANGE `news_break` `obsolete_news_break` varchar(255) COLLATE 'utf8_unicode_ci' NOT NULL AFTER `event_name`;
		");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
