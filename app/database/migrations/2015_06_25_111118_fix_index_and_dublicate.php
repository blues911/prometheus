<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixIndexAndDublicate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("ALTER TABLE `coverage` DROP INDEX `name_date_headline_country_id_author`;");
		DB::statement("ALTER TABLE `coverage` CHANGE `headline` `headline` varchar(1000) COLLATE 'utf8_unicode_ci' NOT NULL AFTER `reach`;");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
