<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CheckDb extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("UPDATE countries SET region_id = 32 WHERE region_id = 29");
		DB::statement("ALTER TABLE `countries` ADD FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

		DB::statement("ALTER TABLE `media`
			CHANGE `region_id` `region_id` int(10) unsigned NOT NULL AFTER `reach`,
			CHANGE `country_id` `country_id` int(10) unsigned NOT NULL AFTER `region_id`;
		");
		DB::statement("ALTER TABLE `media` ADD FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
		DB::statement("ALTER TABLE `media` ADD FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

		DB::statement("ALTER TABLE `notifications` CHANGE `user_id` `user_id` int(10) unsigned NOT NULL AFTER `id`;");
		DB::statement("ALTER TABLE `notifications` ADD FOREIGN KEY (`user_id`) REFERENCES `system_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE");

		DB::statement("ALTER TABLE `coverage`
			CHANGE `owner_id` `owner_id` int(10) unsigned NOT NULL AFTER `id`,
			CHANGE `region_id` `region_id` int(10) unsigned NOT NULL AFTER `owner_id`,
			CHANGE `country_id` `country_id` int(10) unsigned NOT NULL AFTER `region_id`;
		");
		DB::statement("ALTER TABLE `coverage` ADD FOREIGN KEY (`owner_id`) REFERENCES `system_users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
		DB::statement("ALTER TABLE `coverage` ADD FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
		DB::statement("ALTER TABLE `coverage` ADD FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

		DB::statement("ALTER TABLE `products_coverage`
			CHANGE `product_id` `product_id` int(10) unsigned NOT NULL FIRST,
			CHANGE `coverage_id` `coverage_id` int(10) unsigned NOT NULL AFTER `product_id`;
		");
		DB::statement("ALTER TABLE `products_coverage` ADD FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE");
		DB::statement("ALTER TABLE `products_coverage` ADD FOREIGN KEY (`coverage_id`) REFERENCES `coverage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE");

		DB::statement("ALTER TABLE `system_users`
			CHANGE `region_id` `region_id` int(10) unsigned NOT NULL AFTER `role`,
			CHANGE `country_id` `country_id` int(10) unsigned NOT NULL AFTER `region_id`;
		");
		DB::statement("UPDATE `system_users` SET region_id = 26 WHERE region_id IN (1, 3, 5, 36);");
		DB::statement("UPDATE `system_users` SET country_id = 214 WHERE country_id IN (1, 3, 20);");
		DB::statement("ALTER TABLE `system_users` ADD FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
		DB::statement("ALTER TABLE `system_users` ADD FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

		DB::statement("ALTER TABLE `user_coverage`
			CHANGE `user_id` `user_id` int(10) unsigned NOT NULL FIRST,
			CHANGE `coverage_id` `coverage_id` int(10) unsigned NOT NULL AFTER `user_id`;
		");
		DB::statement("ALTER TABLE `user_coverage` ADD FOREIGN KEY (`user_id`) REFERENCES `system_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE");
		DB::statement("ALTER TABLE `user_coverage` ADD FOREIGN KEY (`coverage_id`) REFERENCES `coverage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE");

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
