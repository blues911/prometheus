<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKpiHeadersTableV2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kpi_headers_v2', function (Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->integer('region_id')->unsigned()->nullable();
			$table->string('net_effect_da');
			$table->string('impact_index');
			$table->string('net_effect_ua');
			$table->string('company_mindshare');
			$table->string('b2b_mindshare');
			$table->string('b2c_mindshare');
			$table->timestamps();

			$table->foreign('region_id')->references('id')->on('regions');
			
		});

		DB::statement('ALTER TABLE kpi_headers_v2 ADD year YEAR' );

		Schema::table('kpi_headers_v2', function($table) {
			$table->unique(['region_id', 'year']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kpi_headers_v2');
	}

}
