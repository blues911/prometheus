<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixRelation extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("ALTER TABLE `campaigns_coverage`
			DROP FOREIGN KEY `campaigns_coverage_campaign_id_foreign`,
			ADD FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
		");

		DB::statement("ALTER TABLE `campaigns_coverage`
			DROP FOREIGN KEY `campaigns_coverage_coverage_id_foreign`,
			ADD FOREIGN KEY (`coverage_id`) REFERENCES `coverage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
		");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
