<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPdfColumnToSaveReports extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('save_reports', function (Blueprint $table) {
			$table->enum('type', array('page', 'pdf'))->default('page');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('save_reports', function (Blueprint $table) {
			$table->dropColumn('type');
		});
	}

}
