<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeColumnForNewsbreaksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('newsbreaks', function (Blueprint $table) {
			$table->enum('type', array('global', 'local'))->default('global');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('newsbreaks', function (Blueprint $table) {
			$table->dropColumn('type');
		});
	}

}
