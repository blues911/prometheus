<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpFiles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('help_files', function(Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('format');
            $table->string('filename');

            $table->enum('type', [
                'doc',
                'pdf',
                'xls',
            ]);
            $table->timestamps();
        });

        $files = [
            [
                'name'     => 'Campaigns and product names with abbreviations',
                'format'   => 'docx',
                'filename' => '9867ba5cd775dfc088000f94087541d2',
                'type'     => 'doc'
            ],
            [
                'name'     => 'KL key messages',
                'format'   => 'xlsm',
                'filename' => '05a4518823ba52af410776801a944418',
                'type'     => 'xls'
            ],
            [
                'name'     => 'User manual',
                'format'   => 'docx',
                'filename' => '5ffddb105b97a94a0335a159e6d6edd8',
                'type'     => 'doc'
            ],
            [
                'name'     => 'User manual in PDF',
                'format'   => 'pdf',
                'filename' => '0c4ae4f46541a2840f74d2efc449806b',
                'type'     => 'pdf'
            ]
        ];

        if (!file_exists(Config::get('app.files_path'))) {
            mkdir(Config::get('app.files_path'), 0777, true);
        }

        foreach ($files as $file) {
            DB::statement("INSERT INTO `help_files` (`name`, `format`, `filename`, `type`, `created_at`, `updated_at`)
                VALUES ('".$file['name']."', '".$file['format']."', '".$file['filename']."', '".$file['type']."', NOW(), NOW())");
            copy(app('path').'/tmp_files/'.$file['filename'], app('path').'/storage/files/'.$file['filename']);
        }
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
