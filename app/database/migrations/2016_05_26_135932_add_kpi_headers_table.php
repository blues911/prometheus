<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKpiHeadersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kpi_headers', function (Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->integer('region_id')->unsigned()->nullable();
			$table->float('net_effect');
			$table->float('impact_index');
			$table->float('negative');
			$table->string('company_mindshare');
			$table->string('b2b_mindshare');
			$table->string('b2c_mindshare');
			$table->timestamps();

			$table->foreign('region_id')->references('id')->on('regions');
			
		});

		DB::statement('ALTER TABLE kpi_headers ADD year YEAR' );

		Schema::table('kpi_headers', function($table) {
			$table->unique(['region_id', 'year']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kpi_headers');
	}

}
