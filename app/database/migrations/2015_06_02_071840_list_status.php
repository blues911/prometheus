<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ListStatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("ALTER TABLE `events` ADD `status` enum('active','archived') COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT 'active' AFTER `name`;");
		DB::statement("ALTER TABLE `campaigns` ADD `status` enum('active','archived') COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT 'active' AFTER `name`;");
		DB::statement("ALTER TABLE `newsbreaks` ADD `status` enum('active','archived') COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT 'active' AFTER `name`;");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
