<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUserManual extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $file = \DB::table('help_files')->where('name', 'User manual')->first();
        if ($file) {
            $filePath = Config::get('app.files_path')."/".$file->filename;
            if (is_file($filePath)) {
                unlink($filePath);
            }
            \DB::table('help_files')->where('name', 'User manual')->delete();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      //
    }

}
