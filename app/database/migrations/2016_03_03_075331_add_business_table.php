<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Business;

class AddBusinessTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('business', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->enum('status', ['active', 'archived'])->default('active');
			$table->timestamps();
		});
		Schema::create('business_coverage', function($table)
		{
			$table->integer('business_id')->unsigned();
			$table->integer('coverage_id')->unsigned();
		});

		DB::statement("ALTER TABLE `business_coverage`
			ADD FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
		");

		DB::statement("ALTER TABLE `business_coverage`
			ADD FOREIGN KEY (`coverage_id`) REFERENCES `coverage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
		");

		Business::create(['name' => 'B2B']);
		Business::create(['name' => 'B2C']);
		Business::create(['name' => 'UA']);
		Business::create(['name' => 'VSB']);
		Business::create(['name' => 'Kaspersky Motorsport']);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('business_coverage');
		Schema::drop('business');
	}

}
