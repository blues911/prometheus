<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCoveregeSubcategory extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coverage_subcategory', function (Blueprint $table) {
            $table->dropForeign('coverage_subcategory_coverage_id_foreign');

        });
        Schema::table('coverage_subcategory', function (Blueprint $table) {

            $table->foreign('coverage_id')->references('id')->on('coverage')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coverage_subcategory', function (Blueprint $table) {
            //
        });
    }

}
