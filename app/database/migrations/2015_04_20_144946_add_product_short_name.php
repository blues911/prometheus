<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductShortName extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('products', function($table)
		{
			$table->string('short_name')->after('name')->nullable();
		});

		$products = [
			'Kaspersky Internet Security'					=> 'KIS',
			'Kaspersky Anti-Virus'							=> 'KAV',
			'Kaspersky Internet Security – Multi-device'	=> 'KIS - MD',
			'Kaspersky Total Security - Multi-device'		=> 'KTS - MD',
			'Kaspersky Internet Security for Android'		=> 'KISA',
			'Kaspersky Internet Security for Mac'			=> 'KIS for MAC',
			'Kaspersky Safe Kids'							=> 'KSK',
			'Kaspersky Small Office Security'				=> 'KSOS',
			'Kaspersky Password Manager'					=> 'KPM',
			'Kaspersky Endpoint Security for Business'		=> 'KESB',
			'Kaspersky  Security for Virtulisation'			=> 'KSV',
			'Kaspersky Fraud Prevention'					=> 'KFP',
			'Kaspersky DDoS protection'						=> 'KDP',
			'Kaspersky Security System'						=> 'KSS',
		];

		foreach ($products as $name => $short_name) {
			$product = App\Models\Product::where('name', '=', $name)->first();
			if (empty($product)) {
				$product		= new App\Models\Product();
				$product->name	= $name;
			}
			$product->short_name = $short_name;
			$product->save();
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
