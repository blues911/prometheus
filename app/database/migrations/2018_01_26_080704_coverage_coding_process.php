<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CoverageCodingProcess extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coverage_coding_process', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('month');
            $table->boolean('status')->default(true);
        });

        $months = [
            'january',
            'february',
            'march',
            'april',
            'may',
            'june',
            'july',
            'august',
            'september',
            'october',
            'november',
            'december',
        ];

        foreach ($months as $month) {
            DB::table('coverage_coding_process')->insert([
                'month' => $month
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coverage_coding_process');
    }

}
