<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RegionSeqnum extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    DB::statement("ALTER TABLE `regions` ADD `seqnum` int(10) COLLATE 'utf8_unicode_ci' AFTER `updated_at`;");
        $seqnums = ['APAC' => 10, 'EM' => 20, 'RU KZ' => 30, 'EU' => 40, 'NA' => 50,'JP' => 60];

        foreach ($seqnums as $region => $seqnum) {
            DB::statement("UPDATE `regions` SET `seqnum` = $seqnum WHERE `name` = '$region'");
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
