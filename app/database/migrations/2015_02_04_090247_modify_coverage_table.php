<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCoverageTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE coverage
                       MODIFY key_message_penetration ENUM('absent', 'counter', 'present', 'extent')");
        DB::statement("ALTER TABLE coverage
                       MODIFY main_tonality ENUM('negative', 'positive', 'neutral')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE coverage
                       MODIFY key_message_penetration ENUM('1', '0.6', '0')");
        DB::statement("ALTER TABLE coverage
                       MODIFY main_tonality ENUM('1', '0')");
    }

}
