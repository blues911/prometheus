<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Models\Country;
use \App\Models\Region;

class ImportRegions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$regions   = Region::all();
		foreach ($regions as $region) {
			$region->name = str_replace("_", " ", $region->name);
			$region->save();
		}

		$countries = Country::all();
		foreach ($countries as $country) {
			$country->name = str_replace("_", " ", $country->name);
			$country->save();
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
