<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueIndexToSpeakersName extends Migration {

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
				DB::statement("ALTER TABLE `speakers` ADD UNIQUE `name` (`name`)");
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
				DB::statement("ALTER TABLE `speakers` DROP INDEX `name`");
		}

}
