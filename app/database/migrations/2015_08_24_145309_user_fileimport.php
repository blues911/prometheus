<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserFileimport extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		mkdir(Config::get('app.imports_path'), 0755, true);

		DB::statement("
			ALTER TABLE `import_files`
			ADD `filename` varchar(255) COLLATE 'utf8_unicode_ci' NULL AFTER `name`,
			ADD `user_id` int(10) unsigned NULL AFTER `filename`,
			ADD FOREIGN KEY (`user_id`) REFERENCES `system_users` (`id`) ON DELETE CASCADE;
		");
		DB::statement("ALTER TABLE `import_files` ADD `active` tinyint unsigned NULL DEFAULT '1' AFTER `user_id`;");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
