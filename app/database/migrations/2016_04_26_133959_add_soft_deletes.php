<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeletes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('business', function($table)
		{
			$table->softDeletes();
		});
		Schema::table('campaigns', function($table)
		{
			$table->softDeletes();
		});
		Schema::table('countries', function($table)
		{
			$table->softDeletes();
		});
		Schema::table('events', function($table)
		{
			$table->softDeletes();
		});
		Schema::table('media', function($table)
		{
			$table->softDeletes();
		});
		Schema::table('newsbreaks', function($table)
		{
			$table->softDeletes();
		});
		Schema::table('products', function($table)
		{
			$table->softDeletes();
		});
		Schema::table('regions', function($table)
		{
			$table->softDeletes();
		});
		Schema::table('speakers', function($table)
		{
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('business', function($table)
		{
			$table->dropColumn('deleted_at');
		});
		Schema::table('campaigns', function($table)
		{
			$table->dropColumn('deleted_at');
		});
		Schema::table('countries', function($table)
		{
			$table->dropColumn('deleted_at');
		});
		Schema::table('events', function($table)
		{
			$table->dropColumn('deleted_at');
		});
		Schema::table('media', function($table)
		{
			$table->dropColumn('deleted_at');
		});
		Schema::table('newsbreaks', function($table)
		{
			$table->dropColumn('deleted_at');
		});
		Schema::table('products', function($table)
		{
			$table->dropColumn('deleted_at');
		});
		Schema::table('regions', function($table)
		{
			$table->dropColumn('deleted_at');
		});
		Schema::table('speakers', function($table)
		{
			$table->dropColumn('deleted_at');
		});
	}

}
