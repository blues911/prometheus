<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDefaultsCoverageTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE coverage
                       MODIFY image ENUM('1', '0.5') DEFAULT NULL");
        DB::statement("ALTER TABLE coverage
                       MODIFY mention ENUM('1', '0.9', '0.8', '0.5') DEFAULT NULL");
        DB::statement("ALTER TABLE coverage
                       MODIFY speaker_quote ENUM('1', '0.8') DEFAULT '0.8'");
        DB::statement("ALTER TABLE coverage
                       MODIFY third_speakers_quotes ENUM('1', '0') DEFAULT '0'");
        DB::statement("ALTER TABLE coverage
                       MODIFY header_tonality ENUM('1', '0.85', '0.5') DEFAULT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE coverage
                       MODIFY image ENUM('1', '0.5')");
        DB::statement("ALTER TABLE coverage
                       MODIFY mention ENUM('1', '0.9', '0.8', '0.5')");
        DB::statement("ALTER TABLE coverage
                       MODIFY speaker_quote ENUM('1', '0.8') DEFAULT '0.8'");
        DB::statement("ALTER TABLE coverage
                       MODIFY third_speakers_quotes ENUM('1', '0') DEFAULT '0'");
        DB::statement("ALTER TABLE coverage
                       MODIFY header_tonality ENUM('1', '0.85', '0.5')");
    }
}
