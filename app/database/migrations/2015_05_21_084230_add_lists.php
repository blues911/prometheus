<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLists extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$newsbreaks = [
			"BlueCoat's Content Analysis System",
			'1 in 8 users don’t believe in cyberthreats',
			'137,000 New Phishing Wildcards',
			'25% of Critical Infrastructure OrganizationsCritical Infrastructure',
			'38% DDoS',
			'60%Android attack/Interpol',
			'APT Logbook',
			'APT Predictions 2015',
			'Best Performance award',
			'BioNyfiken',
			'Boletos',
			'BYOD Security Awarenes among VSB',
			'Camera spying',
			'Carbanak',
			'Caught in a social net',
			'Children cause parent’s money or data loss',
			'Chthonic (Zeus)',
			'CIP',
			'COLP',
			'Companies under DDoS',
			'Connected cars',
			'Corporate security forecast: cloudy',
			'CRN 2014',
			'CSNG',
			'CSW',
			'Cyberbullying_launch',
			'Cybercrime in Brazil',
			'Cybercriminals earn money',
			'Cybersmart Guide',
			'Cyberthreats 2014',
			'Danger of online transactions',
			'Dark Hotel',
			'DDoS Countermeasures',
			'DDoS Protection',
			'Desert Falcons',
			'Differences of Virtual Security Solutions',
			'Encryption Recognition',
			'Encryptors',
			'Energetic Bear/Crouching Yeti',
			'Equation Group',
			'EquationDrug',
			'Few companies protect financial transactions on client endpoints',
			'Financial Attacks Tripled',
			'Financial cybercriminals',
			'Financial threats',
			'Forever lost',
			'GTT Alertness',
			'GTT Launch ',
			'GTT Winner',
			'Hacking a living room',
			'Hitachi NAS',
			'Ignoring IT strategy',
			'Infected ATMs/Interpol',
			'Intellectual Property',
			'INTERPOL/Europol',
			'Investment for VSB',
			'KDP launch Europe',
			'KESB2015',
			'Kids India',
			'KIS A Protection',
			'KIS in MRG Effitas',
			'KISA - Product of the Year',
			'KL - GBS partnership',
			'Koler',
			'KTS Launch',
			'Mac and PC at Risk',
			'Mac OS risks ',
			'Machete APT',
			'Managing Information in Virtual Network',
			'Miniduke',
			'Mobile threats',
			'Mobile threats/Interpol',
			'MRG Effitas Certification',
			'NetTraveler 10',
			'Number of the Year ',
			'One Dollar Lesson',
			'Onion malware',
			'Online payments',
			'OPSEC',
			'Parallels ',
			'Parental control KSN ',
			'Patent_efficient security scans',
			'Patents in Q3 ',
			'Patents Q4 2014',
			'Peter Hewett',
			'Phound launch',
			'PikeOS',
			'Predictions 2015',
			'Q2 Enterprise Anti-Virus Test',
			'Q2 threats report',
			'Q3 digest of products’ tests',
			'Q3 threats',
			'Regin',
			'Report Stolen Mobile Devices',
			'Reputation matters',
			'Schoolnet collaboration',
			'Security Startup Challenge',
			'Shvetsov_CTO',
			'Shylock',
			'SMB IT Strategy',
			'Spam in 2014',
			'Spam in August',
			'Spam in July',
			'Spam in June',
			'Spam in Q2 ',
			'Spam Q3',
			'Spam September',
			'Spam/France',
			'Special Spice for Mac Device',
			'Statistics 2014',
			'Stolen Banking Information',
			'Stuxnet victims',
			'Stuxnet vulterabilities',
			'Syrian malware',
			'The Epic Snake/Turla',
			'Tic-Tac-Toe',
			'Top concerns of financial companies',
			'Top Security Stories 2014',
			'Top Track 250',
			'TOP-3 2014',
			'Virtualization in Sensitive-Information Sectors',
			'Virtualization',
			'Virtualization_Limited Adoption',
			'VMware Ready - vCloud Air',
			'Wearable Connected Devices',
			'Windows vulnerabilities',
			'Women Are Generally Less Concerned',
			'World Cup 2014',
			'ZyXEL',
		];

		foreach ($newsbreaks as $name) {
			$newsbreak = App\Models\NewsBreak::where('name', '=', $name)->first();
			if (empty($newsbreak)) {
				$newsbreak       = new App\Models\NewsBreak();
				$newsbreak->name = $name;
				$newsbreak->save();
			}
		}

		$campaings = [
			'APT',
			'CIP',
			'Cyberbullying',
			'GTT',
			'Kaspersky Security Bulletin 2014',
			'KDP',
			'KESB',
			'KFP',
			'KFP, KESB, KDP',
			'KFP, KIS-MD, KTS-MD',
			'KIS-MD',
			'KIS-MD/KTS-MD',
			'KISA',
			'KSN report',
			'KSOS',
			'KSV',
			'KTS-MD',
			'SSC',
		];

		foreach ($campaings as $name) {
			$campaing = App\Models\Campaign::where('name', '=', $name)->first();
			if (empty($campaing)) {
				$campaing       = new App\Models\Campaign();
				$campaing->name = $name;
				$campaing->save();
			}
		}

		$events = [
			"CSNG",
			"CSW",
			"Kaspersky's visit to India",
			"KESB SP1",
			"MWC15",
			"SAS",
		];

		foreach ($events as $name) {
			$event = App\Models\Event::where('name', '=', $name)->first();
			if (empty($event)) {
				$event       = new App\Models\Event();
				$event->name = $name;
				$event->save();
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
