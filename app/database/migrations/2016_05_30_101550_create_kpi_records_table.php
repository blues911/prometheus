<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKpiRecordsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kpi_records', function (Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->integer('region_id')->unsigned()->nullable();
			$table->string('company_mindshare');
			$table->string('b2b_mindshare');
			$table->string('b2c_mindshare');
			$table->boolean('is_net_effect')->default(true);
			$table->boolean('is_impact_index')->default(true);
			$table->boolean('is_negative')->default(true);
			$table->boolean('is_company_mindshare')->default(true);
			$table->boolean('is_b2b_mindshare')->default(true);
			$table->boolean('is_b2c_mindshare')->default(true);
			$table->date('date');
			$table->enum('status', ['active', 'hidden'])->default('hidden');
			$table->timestamps();

			$table->foreign('region_id')->references('id')->on('regions');
			
		});

		Schema::table('kpi_records', function($table) {
			$table->unique(['region_id', 'date']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kpi_records');
	}

}
