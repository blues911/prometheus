<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubregionIdToCountries extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries', function($table)
        {
            $table->integer('subregion_id')->unsigned()->nullable();
            $table->foreign('subregion_id')->references('id')->on('subregions')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries', function($table) {
            $table->dropForeign('countries_subregion_id_foreign');
            $table->dropColumn('subregion_id');
        });
    }

}
