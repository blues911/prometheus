<?php

use App\Models\Subcategory;

class SubcategoryTableSeeder extends Seeder {

    public function run()
    {
        $data[] = [
            'slug' => 'lifestyle',
            'title' => 'Lifestyle'
        ];
        $data[] = [
            'slug' => 'smb',
            'title' => 'SMB media'
        ];
        $data[] = [
            'slug' => 'enterprise',
            'title' => 'Enterprise'
        ];
        $data[] = [
            'slug' => 'financial',
            'title' => 'Financial'
        ];
        $data[] = [
            'slug' => 'virtualization',
            'title' => 'Virtualization'
        ];
        $data[] = [
            'slug' => 'automotive',
            'title' => 'Automotive'
        ];
        $data[] = [
            'slug' => 'industrial',
            'title' => 'Industrial'
        ];
        $data[] = [
            'slug' => 'banking_and_insurance',
            'title' => 'Banking and Insurance'
        ];
        $data[] = [
            'slug' => 'travel',
            'title' => 'Travel'
        ];
        $data[] = [
            'slug' => 'parental_media',
            'title' => 'Parental media'
        ];
        $data[] = [
            'slug' => 'gaming',
            'title' => 'Gaming'
        ];
        $data[] = [
            'slug' => 'pet_media',
            'title' => 'Pet media'
        ];
        $data[] = [
            'slug' => 'motorsport',
            'title' => 'Motorsport'
        ];
        $data[] = [
            'slug' => 'healthcare',
            'title' => 'Healthcare'
        ];
        $data[] = [
            'slug' => 'government',
            'title' => 'Government'
        ];
        $data[] = [
            'slug' => 'msp_media',
            'title' => 'MSP media'
        ];
        $data[] = [
            'slug' => 'education',
            'title' => 'Education'
        ];

		Eloquent::unguard();

		//disable foreign key check for this connection before running seeders
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Subcategory::truncate();

        foreach ($data as $k => $v) {
            Subcategory::create(array(
                'slug' => $v['slug'],
                'title' => $v['title']
            ));
        }
        
		// supposed to only apply to a single connection and reset it's self
		// but I like to explicitly undo what I've done for clarity
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}