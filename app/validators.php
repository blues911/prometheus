<?php
/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at Fahrenheit 451
 *
 * Date: 01.09.14
 * Time: 15:15
 */
Validator::extend(
    'alpha_spaces',
    function ($attribute, $value) {
        return preg_match('/^[\pL\s0-9]+$/u', $value);
    }
);

Validator::extend(
    'speakers_quoted',
    function ($attribute, $value, $parameters) {
        $result = true;
        if (is_array($value)) {
            foreach ($value as $item) {
                if (!preg_match("/^[^_0-9!@#$%№;:?*.,^<>~\\[\\]_='\"\\\\|\\/{}()\\+]+$/i", $item)) {
                    $result = false;
                    break;
                }
            }
        } else {
            $result = preg_match("/^[^_0-9!@#$%№;:?*.,^<>~\\[\\]_='\"\\\\|\\/{}()\\+]+$/i", $value);
        }

        return (bool)$result;
    }
);

Validator::extend(
    'third_speakers_quoted',
    function ($attribute, $value, $parameters) {
        $result = true;
        if (is_array($value)) {
            foreach ($value as $item) {
                if (!preg_match("/^[^_0-9!@#$%№;:?*.^<>~\\[\\]\\-_='\"\\\\|\\/{}()\\+]+$/i", $item)) {
                    $result = false;
                    break;
                }
            }
        } else {
            $result = preg_match("/^[^_0-9!@#$%№;:?*.^<>~\\[\\]\\-_='\"\\\\|\\/{}()\\+]+$/i", $value);
        }

        return (bool)$result;
    }
);

Validator::extend(
    'media_subcategories',
    function ($attribute, $value, $parameters) {

        $result = true;

        $subcategories = \App\Models\Subcategory::all()->toArray();
        $media = [];

        foreach ($subcategories as $item) {
            $media[] = strtolower($item['title']);
        }

        if (!is_array($value)) {
            $value = explode(",", $value);
        }

        if (!empty($value)) {
            foreach ($value as $k => $v) {
                $v = trim($v);
                $v = preg_replace('/\s+/', ' ', $v);
                $v = strtolower($v);
                if (!in_array($v, $media)) {
                    $result = false;
                }
            }
        }

        return (bool)$result;
    }
);