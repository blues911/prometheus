<?php
namespace App\Command;

use App\Models\Country;
use App\Models\Region;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class ImportCountries
 */
class ImportCountries extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'prometheus:import-countries';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command imports countries and link it with regions.';

    /**
     * Create a new command instance.
     *
     * @return \App\Command\ImportCountries
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $file = $this->option('file');
        $counter = 0;

        if (!file_exists($file)) {
            $this->error('File not found: '.$file);
            exit(0);
        }

        $handle = fopen($file, 'r');

        if ($handle) {
            while (($buffer = fgets($handle)) !== false) {
                $row = explode(',', $buffer);
                if (count($row) !== 2) {
                    continue;
                }

                $region = Region::firstOrCreate([
                        'name' => $row[0]
                    ]);
                $country = Country::firstOrCreate([
                        'name' => trim($row[1])
                    ]);

                $country->region()->associate($region);
                $country->save();
                $counter++;
            }
        }
        $this->info("Countries imported with region relation: ".$counter);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['file', null, InputOption::VALUE_REQUIRED, 'CSV file with two columns: <region>,<country>.', null],
        ];
    }

}
