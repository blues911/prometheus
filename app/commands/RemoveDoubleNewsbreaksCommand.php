<?php

namespace App\Command;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RemoveDoubleNewsbreaksCommand extends Command
{

		/**
		 * The console command name.
		 *
		 * @var string
		 */
		protected $name = 'dev:rdn';

		/**
		 * The console command description.
		 *
		 * @var string
		 */
		protected $description = 'Union double newsbreaks';

		/**
		 * Create a new command instance.
		 *
		 * @return void
		 */
		public function __construct()
		{
				parent::__construct();
		}

		/**
		 * Execute the console command.
		 *
		 * @return mixed
		 */
		public function fire()
		{
				$items = \DB::select("SELECT dd.name as name
						FROM (SELECT id, name, COUNT(id) as count FROM newsbreaks GROUP BY name) dd
						WHERE dd.count > 1");

				foreach ($items as $item) {
						$newsbreaks = \DB::select("SELECT id, name FROM newsbreaks WHERE name = ?", [$item->name]);

						$row = [];
						foreach ($newsbreaks as $n) {
								$row[] = ['id'=>$n->id,'name'=>$n->name];
						}

						sort($row);
						$parent = array_shift($row);

						\DB::update("UPDATE newsbreaks SET status = 'active', deleted_at = NULL WHERE id = ?", [$parent['id']]);
						echo "ID: {$parent['id']}\n";
						echo "NAME: {$parent['name']}\n";
						echo "Doubles: ". count($row) ."\n";

						foreach ($row as $double) {
								echo "-------------------------\n";
								echo "Double ID: {$double['id']}\n";
                                echo "Double Name: {$double['name']}\n";
								$result = \DB::update("UPDATE newsbreaks_coverage SET newsbreak_id = ? WHERE newsbreak_id = ?", [$parent['id'], $double['id']]);
								echo "newsbreaks_coverage U: {$result}\n";
								$result = \DB::delete("DELETE FROM newsbreaks WHERE id = ?", [$double['id']]);
								echo "newsbreaks D: {$result}\n";
						}

						echo "-------------------------\n\n";

						unset($row);
				}

				echo "Total: " . count($items) . "\n\n";
		}

		/**
		 * Get the console command arguments.
		 *
		 * @return array
		 */
		protected function getArguments()
		{
				return [];
		}

		/**
		 * Get the console command options.
		 *
		 * @return array
		 */
		protected function getOptions()
		{
				return [];
		}

}
