<?php

namespace App\Command;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\SystemUser;
use App\Models\Coverage;
use App\Models\Country;
use App\Models\Region;
use Config;

class RemoveDoubleSpeakersCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'dev:rds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Union double speakers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $doubleSpeakersName = \DB::select("select  dd.name as name, dd.count  
                from (SELECT id , `name`, count(id) as count  FROM pr_kasp.speakers group by `name`) dd
                where dd.count>1;");
        foreach ($doubleSpeakersName as $item) {
            $speakers = \DB::select("select id  from speakers where name=?", [$item->name]);

            foreach ($speakers as $s) {
                $arr[] = $s->id;
            }
            sort($arr);
            $min=array_shift($arr);

            foreach ($arr as $id) {
                echo "Parent ID: $min  ";
                echo "Double ID: $id  ";
                $result=\DB::update("update speakers_coverage set speaker_id = ? where speaker_id = (?)",[$min,$id]);
                echo "U: $result  ";
                $result=\DB::delete("delete from speakers where id = (?)",[$id]);
                echo "D: $result ";
                $result=\DB::update("update  speakers set deleted_at = NULL where id = (?)",[$min]);
                echo "A: $result  \n";
            }
            unset($arr);
        }




    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
