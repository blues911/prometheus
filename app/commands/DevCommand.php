<?php
namespace App\Command;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\SystemUser;
use App\Models\Coverage;
use App\Models\Country;
use App\Models\Region;
use Config;

class DevCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'dev:export';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Dev command.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$data = [
			'filter' => [
				'from_date' => date("Y-m-d", strtotime("01.01.2015")),
				'to_date'   => date("Y-m-d", strtotime("29.02.2016 + 1 day")),
			],
			'user'      => SystemUser::find(20)
		];

		$exportData = Coverage::prepareDataForExport($data['filter'], $data['user']);

		$filename = 'Coverage_' . date('Y-m-d H:i:s');

		\Excel::create($filename, function($excel) use ($exportData) {
		    $excel->sheet('Coverage', function($sheet) use ($exportData) {
		        $sheet->setAutoSize(false);
		        $sheet->setColumnFormat(['D' => 'dd.mm.yyyy']);
		        $sheet->fromArray($exportData['item'], null, 'A1', true, false);
                /* Отмечаем дубли */
                foreach ($exportData['duplicate'] as   $duplicate) {

                    $sheet->row($duplicate['row'], function ($row) {
                        $row->setBackground('#FFFF00');
                    });
                    $sheet->cell('AI'.$duplicate['row'], function($cell) use ($duplicate) {
                        $cell->setValue($duplicate['message']);

                    });
                }
                /* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */
		    });
		})->store('xls', \Config::get('app.tmp_path'));
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];
	}

}
