<?php
namespace App\Command;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class ImportCoverage
 * @package App\Command
 */
class ImportCoverage extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'prometheus:import-coverage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import coverage data from Excel files.';

    /**
     * Create a new command instance.
     *
     * @return \App\Command\ImportCoverage
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        // Start time
        $debugExecTime = -microtime(true);

        $importer = new \App\Prometheus\ImportCoverage(storage_path('exports/coverage.xls'));

        if (!$importer->run()) {
            $errors = $importer->getErrors();
        } else {
            // @todo send success message to the queue
        }

        // End time
        $debugExecTime += microtime(true);
        $this->comment('Elapsed time: '.round($debugExecTime, 3));
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

}
