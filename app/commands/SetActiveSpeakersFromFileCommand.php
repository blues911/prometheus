<?php

namespace App\Command;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Speaker;
use PHPExcel_IOFactory;

class SetActiveSpeakersFromFileCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'dev:sas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks speakers.xlsx and sets active speakers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $file = public_path('tmp/speakers.xlsx');

        if (!is_file($file)) {
            echo "Error: File {$file} is not exists!\n\n";
            die;
        }

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($file);
        $worksheet = $objPHPExcel->getSheet(0);
        $lastRow = $worksheet->getHighestRow();

        $excelSpeakers = [];
        for ($row = 1; $row <= $lastRow; $row++) {
            $excelSpeakers[$row] = trim($worksheet->getCell('A'.$row)->getValue());
        }

        $dbSpeakers = Speaker::all();

        // Delete speaker if it not exists in Excel file
        echo "Speakers Deleted:\n";
        echo "-----------------\n";
        $i = 0;
        foreach ($dbSpeakers as $dbSpeaker) {
            if (!in_array($dbSpeaker->name, $excelSpeakers)) {
                $dbSpeaker->delete();
                echo "D: {$dbSpeaker->name}\n";
                $i++;
            }
        }
        echo "-----------------\n";
        echo "Total: {$i}\n\n";

        // Add new or update deleted speaker from Excel file
        echo "Speakers Created:\n";
        echo "-----------------\n";
        $j = 0;
        foreach ($excelSpeakers as $excelSpeaker) {
            $oldSpeaker = Speaker::withTrashed()->where('name', $excelSpeaker)->first();
            if ($oldSpeaker && $oldSpeaker->deleted_at != NULL) {
                $oldSpeaker->name = $excelSpeaker;
                $oldSpeaker->deleted_at = NULL;
                $oldSpeaker->save();
                echo "U: {$excelSpeaker}\n";
                $j++;
            }
            if (!$oldSpeaker) {
                $newSpeaker = new Speaker;
                $newSpeaker->name = $excelSpeaker;
                $newSpeaker->save();
                echo "C: {$excelSpeaker}\n";
                $j++;
            }
            // skip if speaker exists and not deleted
        }
        echo "-----------------\n";
        echo "Total: {$j}\n";
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
