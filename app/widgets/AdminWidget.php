<?php
/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at Fahrenheit 451
 *
 * Date: 30.10.14
 * Time: 0:53
 */
namespace Widgets;

use App\Models\Country;
use App\Models\Media;
use App\Models\Product;
use App\Models\Region;
use App\Models\Subregion;
use App\Models\Subcategory;
use App\Models\SystemUser;
use App\Models\Event;
use App\Models\Campaign;
use App\Models\NewsBreak;
use App\Models\Speaker;
use App\Models\Business;

/**
 * Class AdminWidget
 * @package Widgets
 */
class AdminWidget
{
    /**
     * @param \Illuminate\View\View $view
     */
    public function compose($view)
    {
        $view->with('views', [
            'users',
            'regions',
            'subregions',
            'countries',
            'products',
            'media',
            'events',
            'campaigns',
            'business',
            'newsbreaks',
            'speakers',
            'subcategories'
        ]);
        $view->with('subregions', Subregion::all()->sortBy('name'));
        $view->with('regions', Region::all()->sortBy('name'));
        $view->with('countries', Country::all()->sortBy('name'));
        $view->with('products', Product::all()->sortBy('name'));
        $view->with('users', SystemUser::paginate(SystemUser::USERS_PAGE_SIZE_DEFAULT));
        $view->with('media', Media::orderBy('name')->paginate(30));
        $view->with('events', Event::all()->sortBy('name'));
        $view->with('newsbreaks', NewsBreak::orderBy('name')->paginate(NewsBreak::NEWSBREAKS_PAGE_SIZE_DEFAULT));
        $view->with('newsbreaksNames', array_map(function($a){
                $row = [];
                $row = [
                    'id' => $a['id'],
                    "name" => $a['name']
                ];
                return $row;
            }, NewsBreak::all()->toArray()));
        $view->with('campaigns', Campaign::all()->sortBy('name'));
        $view->with('business', Business::all()->sortBy('name'));
        $view->with('speakers', Speaker::orderBy('name')->paginate(Speaker::SPEAKERS_PAGE_SIZE_DEFAULT));
        $view->with('speakersNames', array_map(function($a){
                $row = [];
                $row = [
                    'id' => $a['id'],
                    "name" => $a['name']
                ];
                return $row;
            }, Speaker::all()->toArray()));
        $view->with('subcategories', Subcategory::orderBy('title')->paginate(Subcategory::SUBCATEGORIES_PAGE_SIZE_DEFAULT));
    }
}
