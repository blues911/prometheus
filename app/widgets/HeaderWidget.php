<?php
/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at Fahrenheit 451
 *
 * Date: 30.10.14
 * Time: 0:53
 */

namespace Widgets;

use Auth;

/**
 * Class HeaderWidget
 * @package Widgets
 */
class HeaderWidget
{
    /**
     * @param \Illuminate\View\View $view
     */
    public function compose($view)
    {
        $view->with('currentUser', Auth::user());
    }
}
