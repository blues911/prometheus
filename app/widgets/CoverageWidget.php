<?php
/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at Fahrenheit 451
 *
 * Date: 28.11.14
 * Time: 18:49
 */

namespace Widgets;

use App\Models\Country;
use App\Models\Product;
use App\Models\Region;

/**
 * Class CoverageWidget
 * @package Widgets
 */
class CoverageWidget
{
    /**
     * @param \Illuminate\View\View $view
     */
    public function compose($view)
    {
        $view->with('regions', Region::all()->sortBy('name'));
        $view->with('products', Product::where("status", "=", Product::STATUS_ACTIVE)->get()->sortBy('name'));
    }
}
