<?php
/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at Fahrenheit 451
 *
 * Date: 29.10.14
 * Time: 2:07
 */

namespace Widgets;

use Illuminate\Support\ServiceProvider;

/**
 * Class WidgetsServiceProvider
 * @package Widgets
 */
class WidgetsServiceProvider extends ServiceProvider
{
    public function register()
    {
        \View::composer('admin.index', '\Widgets\AdminWidget');
        \View::composer('components.header', '\Widgets\HeaderWidget');
        \View::composer('add-data', '\Widgets\CoverageWidget');
    }
}
