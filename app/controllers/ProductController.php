<?php
namespace App\Controllers;

use App\Models\Product;
use App\Models\User;
use Response;
use Input;
use App\Models;
use Auth;
use View;

/**
 * Class ProductController
 */
class ProductController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        $name      = Input::get('name');
        $shortName = Input::get('short_name');
        $status    = Input::get('status');
        if (empty($name) || empty($shortName) || empty($status)) {
            Response::json(['error' => false]);
        }

        $product                           = Product::findOrNew(Input::get('id', 0));
        $product->name                     = $name;
        $product->short_name               = $shortName;
        $product->status                   = $status;
        $product->is_security_intelligence = Input::get('is_security_intelligence', false);
        $product->save();

        return Response::json($product->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $product = Product::find($id);
        if (empty($product)) {
            return Response::json(['error' => true]);
        }
        $product->status = Product::STATUS_ACTIVE;
        $product->update(['status']);

        return Response::json(['error' => false]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        Product::destroy($id);
        return Response::json(
            [
                'error' => false
            ]
        );
    }

    /**
     * Restore the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function restore($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        try {
            $product = Product::onlyTrashed()->find($id);
            if ($product) {
                $product->deleted_at = NULL;
                $product->save();
            }
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }

        return Response::json(['error' => false]);
    }

    /**
     * get Products
     *
     * @return mixed
     */
    public function getProducts()
    {
        $onlyDeleted = filter_var(Input::get('only_deleted'), FILTER_VALIDATE_BOOLEAN);
        $products = $onlyDeleted  ? Product::onlyTrashed()->orderBy('name') : Product::orderBy('name');
        $view = $onlyDeleted ? 'admin.lists.deleted._products_list' : 'admin.lists._products_list';

        return View::make($view, [
            'products' => $products->get(),
        ])->render();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @return Response
     */
    public function destroySomeRecords()
    {
        $ids = explode(',', Input::get('ids'));
        
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        Product::destroy($ids);
        
        return Response::json(['error' => false]);
    }

}