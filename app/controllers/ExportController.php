<?php
namespace App\Controllers;

use App\Models\ExportFile;
use Auth;
use Config;
use View;
use Input;
use Response;
use Redirect;

class ExportController extends BaseController
{

    /**
     * Export page
     *
     * @return $this
     */
    public function getIndex()
    {
        $defaultDateFrom = date("Y-m-d", strtotime('first day of this month'));
        $defaultDateTo   = date("Y-m-d", strtotime('last day of this month'));
        return View::make('root')
            ->nest('template', 'export', [
                'currentUser'     => Auth::user(),
                'dateFrom'        => Input::has("start") ? Input::get("start", null) : $defaultDateFrom,
                'dateTo'          => Input::has("end") ? Input::get("end", null) : $defaultDateTo,
                'defaultDateFrom' => $defaultDateFrom,
                'defaultDateTo'   => $defaultDateTo,
            ])
            ->nest('customAssets', 'assets.export');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDelete($id)
    {
        $exportFile = ExportFile::find($id);
        if (empty($exportFile) || $exportFile->user_id != Auth::user()->id) {
            return Response::json(['error' => true]);
        }
        if (unlink(Config::get('app.tmp_path') . '/' . $exportFile->filename)) {
            $exportFile->delete();
            return Response::json(['error' => false, 'id' => $exportFile->id]);
        }
    }

    /**
     * get file
     *
     * @return void
     */
    public function getFile($id = null)
    {
        $fileContentType = [
            'xls' => 'application/vnd.ms-excel',
            'pdf' => 'application/pdf',
        ];

        $exportFile = ExportFile::find($id);
        if (empty($exportFile) || $exportFile->user_id != Auth::user()->id) {
            return Redirect::back();
        }
        header('Content-Type: ' . $fileContentType[$exportFile->type]);
        header('Content-Disposition: attachment;filename="'. $exportFile->name .'"');
        header('Cache-Control: max-age=0');
        header('Pragma: no-cache');
        readfile(Config::get('app.tmp_path')."/".$exportFile->filename);
    }


    /**
     * Get export files
     *
     * @return void
     */
    public function getHistory()
    {
        $search            = Input::has('search') ? Input::get('search.value') : '';
        $filter['user_id'] = Auth::user()->id;
        $dateFrom          = Input::get('start_range', null);
        $dateTo            = Input::get('end_range', null);

        if (!empty($dateFrom)) {
            $filter['from_date'] = date("Y-m-d", strtotime($dateFrom));
        }
        if (!empty($dateTo)) {
            $filter['to_date']   = date("Y-m-d", strtotime($dateTo . "+ 1 day"));
        }
        if (!empty($dateTo)) {
            $filter['to_date']   = date("Y-m-d", strtotime($dateTo . "+ 1 day"));
        }

        $list              = ExportFile::getActiveUserExportFiles(Input::get('start'), Input::get('length'), $filter, $search);
        $total             = count(ExportFile::getActiveUserExportFiles(0, 0, ['user_id' => Auth::user()->id], null));
        if (count($filter) > 1 || !empty($search)) {
            $recordsFiltered = count(ExportFile::getActiveUserExportFiles(0, 0, $filter, $search));
        }
        else {
            $recordsFiltered = $total;
        }

        return Response::json(
            [
                "recordsTotal"     => $total,
                "recordsFiltered"  => $recordsFiltered,
                "data"             => $list,
                'debug'            => [
                    'skip'    => Input::get('start'),
                    'limit'   => Input::get('length'),
                    'request' => Input::all(),
                ]
            ]
        );
    }

}