<?php
namespace App\Controllers;

use App\Models\BaseValidator;
use App\Models\Country;
use App\Models\Region;
use App\Models\SystemUser;
use App\Models\User;
use Hash;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Response;
use Exception;
use Input;
use Mail;
use Config;
use View;
use Validator;
use Str;
use URL;

/**
 * Class UserController
 */
class UserController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @throws Exception
     * @return Response
     */
    public function store()
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        $user        = SystemUser::findOrNew(Input::get('id', 0));
        $isNewRecord = empty($user->id);
        $type        = $isNewRecord ? SystemUser::EVENT_USER_CREATE : null;

        if (!$user->validate(Input::all())) {
            throw new Exception('Invalid input data');
        }

        $region  = Region::find(Input::get('region'))->getModel();
        $country = Country::find(Input::get('country'))->getModel();

        if ($country->region_id != $region->id) {
            throw new Exception('Invalid input data');
        }

        $user->fill(Input::except(['password']));
        $user->region()->associate($region);
        $user->country()->associate($country);

        $oldPass = $user->password;
        if (Input::get('password') !== '' || !Input::get('id', 0)) {
            $user->password = Hash::make(Input::get('password'));
        }

        if (!$user->save()) {
            return Response::json(["user" => []]);
        }

        if ($oldPass != $user->password && !$isNewRecord) {
            $type = SystemUser::EVENT_USER_CHANGE_PASSWORD;
        }

        if (!empty($type)) {
            SystemUser::notify($user->email, $type, ['email' => $user->email, 'password' => Input::get('password')]);
        }
        return Response::json(["user" => $user->toArray()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        try {
            SystemUser::destroy($id);
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }

        return Response::json(['error' => false]);
    }

    /**
     * Restore the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function restore($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        try {
            $user = SystemUser::onlyTrashed()->find($id);
            if ($user) {
                $user->deleted_at = NULL;
                $user->save();
            }
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }

        return Response::json(['error' => false]);
    }

    /**
     * Send sign on email for complete form
     *
     * @return Response
     */
    public function sendMailWithPassword()
    {
        $v = BaseValidator::make(Input::all(), BaseValidator::ENTER_EMAIL);
        if ($v->fails()) {
            return Response::json(['status' => 'error']);
        }

        $email = Input::get('email');
        $user  = SystemUser::findByEmail($email);
        if (empty($user)) {
            return Response::json(['status' => 'ok']);
        }
        $user->generateSign();
        SystemUser::notify($user->email, SystemUser::EVENT_USER_NEW_PASSWORD, ['url' => URL::to('user/change-password?sign='. $user->sign)]);
        return Response::json(['status' => 'ok']);
    }

    /**
     * Send sign on email for random password
     *
     * @return Response
     */
    public function sendMailForConfirmPassword()
    {
        $v = BaseValidator::make(Input::all(), BaseValidator::ENTER_EMAIL);
        if ($v->fails()) {
            return Response::json(['status' => 'error']);
        }

        $email = Input::get('email');
        $user  = SystemUser::findByEmail($email);
        if (empty($user)) {
            return Response::json(['status' => 'ok']);
        }
        $user->generateSign();
        SystemUser::notify($user->email, SystemUser::EVENT_USER_RANDOM_PASSWORD, ['url' => URL::to('user/set-random-password?sign='. $user->sign)]);
        return Response::json(['status' => 'ok']);
    }

    /**
     * form for change password
     *
     * @return mixed
     */
    public function changePassword()
    {
        $sign = Input::get('sign');
        $user = SystemUser::findBySign($sign);
        if (!empty($user)) {
            return View::make('root')->nest('template', 'change-password',  ['sign' => $sign])->nest('customAssets', 'assets.change-password');
        }
        return Redirect::guest('/');
    }

    /**
     * change password
     *
     * @return Response
     */
    public function sendNewPassword()
    {
        $sign     = Input::get('sign');
        $password = Input::get('password');
        $v        = BaseValidator::make(Input::all(), BaseValidator::CHANGE_PASSWORD);

        if ($v->fails()) {
            return Response::json(['status' => 'error', 'message' => $v->errors()->first()]);
        }

        $user = SystemUser::findBySign($sign);
        if (!empty($user)) {
            $user->changePassword($password);
        }
        return Response::json(['status' => 'ok']);
    }

    /**
     * set random password
     *
     * @return Response
     */
    public function setRandomPassword()
    {
        $sign = Input::get('sign');
        $user = SystemUser::findBySign($sign);
        if (empty($user)) {
            return Redirect::guest('/');
        }
        $password = Str::random(SystemUser::LENGTH_PASSWORD);
        $user->changePassword($password);
        return View::make('root', ['publicReport' => true])->nest('template', 'show-password', ['password' => $password]);
    }

    /**
     * get Users
     *
     * @return mixed
     */
    public function getUsers()
    {
        $limit = Input::get('limit', SystemUser::USERS_PAGE_SIZE_DEFAULT);
        $onlyDeleted = filter_var(Input::get('only_deleted'), FILTER_VALIDATE_BOOLEAN);
        $users = $onlyDeleted  ? SystemUser::onlyTrashed()->orderBy('name') : SystemUser::orderBy('name');
        $view = $onlyDeleted ? 'admin.lists.deleted._users_list' : 'admin.lists._users_list';

        if ($limit !== 'all') {
            $users = $users->paginate((int)$limit);
        } else {
            $users = $users->get();
        }

        return View::make($view, [
          'users' => $users,
          'limit' => $limit
        ])->render();
    }

}
