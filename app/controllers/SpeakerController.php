<?php
namespace App\Controllers;

use App\Models\Speaker;
use App\Models\User;
use Response;
use Input;
use App\Models;
use Auth;
use View;

/**
 * Class SpeakerController
 */
class SpeakerController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        $name = Input::get('name');
        if (empty($name)) {
            return Response::json(['error' => true, 'Name can\'t be empty']);
        }
        if (true !== Speaker::validate(['name' => $name])) {
            return Response::json(['error' => true, 'Speaker name must match "Firstname Lastname"']);
        }

        $speaker       = Speaker::findOrNew(Input::get('id', 0));
        $speaker->name = $name;
        $speaker->save();

        return Response::json($speaker->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        Speaker::destroy($id);
        return Response::json(['error' => false]);
    }

    /**
     * Restore the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function restore($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        try {
            $speacker = Speaker::onlyTrashed()->find($id);
            if ($speacker) {
                $speacker->deleted_at = NULL;
                $speacker->save();
            }
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }


        return Response::json(['error' => false]);
    }

    /**
     * get Speakers
     *
     * @return mixed
     */
    public function getSpeakers()
    {
        $limit = Input::get('limit', Speaker::SPEAKERS_PAGE_SIZE_DEFAULT);
        $onlyDeleted = filter_var(Input::get('only_deleted'), FILTER_VALIDATE_BOOLEAN);
        $speakers = $onlyDeleted  ? Speaker::onlyTrashed()->orderBy('name') : Speaker::orderBy('name');
        $view = $onlyDeleted ? 'admin.lists.deleted._speakers_list' : 'admin.lists._speakers_list';

        if ($limit !== 'all') {
            $speakers = $speakers->paginate((int)$limit);
        } else {
            $speakers = $speakers->get();
        }

        return View::make($view, [
            'speakers' => $speakers,
            'limit' => $limit
        ])->render();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @return Response
     */
    public function destroySomeRecords()
    {
        $ids = explode(',', Input::get('ids'));
        
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        
        Speaker::destroy($ids);
        
        return Response::json(['error' => false]);
    }

}