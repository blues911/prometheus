<?php
namespace App\Controllers;

use App\Models\Region;
use App\Models\Subregion;
use App\Models\User;
use Input;
use Response;
use App\Models\Country;
use Auth;
use View;

/**
 * Class CountryController
 */
class CountryController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Response::json([
            'countries' => Country::where('region_id', '=', Input::get('region_id'))->get(),
            'debug'     => [
                'request' => Input::all(),
                'query'   => \DB::getQueryLog()
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        $country           = Country::findOrNew(Input::get('id', 0));
        $country->name     = Input::get('name');
        $country->name     = str_replace("_", " ", $country->name);
        $country->iso_code = Input::get('iso');

        if (Input::get('region') !== '') {
            $country->region()->associate(Region::find(Input::get('region'))->getModel());
        }

        $subregion = Subregion::find(Input::get('subregion'));
        if ($subregion && $subregion->region_id == Input::get('region')) {
            $country->subregion()->associate($subregion->getModel());
        } else {
            $country->subregion()->dissociate();
        }

        $country->save();
        return Response::json($country->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        try {
            Country::destroy($id);
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }

        return Response::json(['error' => false]);
    }

    /**
     * Restore the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function restore($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        try {
            $country = Country::onlyTrashed()->find($id);
            if ($country) {
                $country->deleted_at = NULL;
                $country->save();
            }
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }

        return Response::json(['error' => false]);
    }

    /**
     * get Countries
     *
     * @return mixed
     */
    public function getCountries()
    {
        $onlyDeleted = filter_var(Input::get('only_deleted'), FILTER_VALIDATE_BOOLEAN);
        $countries = $onlyDeleted  ? Country::onlyTrashed()->orderBy('name') : Country::orderBy('name');
        $view = $onlyDeleted ? 'admin.lists.deleted._countries_list' : 'admin.lists._countries_list';

        return View::make($view, [
            'countries' => $countries->get(),
        ])->render();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @return Response
     */
    public function destroySomeRecords()
    {
        $ids = explode(',', Input::get('ids'));
        
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        try {
            Country::destroy($ids);
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }
        
        return Response::json(['error' => false]);
    }

}