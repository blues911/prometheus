<?php
namespace App\Controllers;

use App\Models\Business;
use App\Models\User;
use Response;
use Input;
use App\Models;
use Auth;
use View;

/**
 * Class BusinessController
 */
class BusinessController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        $name   = Input::get('name');
        $status = Input::get('status');
        if (empty($name) || empty($status) || !in_array($status, [Business::STATUS_ACTIVE, Business::STATUS_ARCHIVED])) {
            Response::json(['error' => false]);
        }

        $business         = Business::findOrNew(Input::get('id', 0));
        $business->name   = $name;
        $business->status = $status;
        $business->save();

        return Response::json($business->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        $business = Business::find($id);
        if (empty($business)) {
            return Response::json(['error' => true]);
        }
        $business->status = Business::STATUS_ACTIVE;
        $business->update(['status']);

        return Response::json(['error' => false]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        Business::destroy($id);
        return Response::json(['error' => false]);
    }

    /**
     * Restore the specified resource.
     *
     * @param  int $id
     * @return Response
     */

    public function restore($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        try {
            $business = Business::onlyTrashed()->find($id);
            if ($business) {
                $business->deleted_at = NULL;
                $business->save();
            }
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }

        return Response::json(['error' => false]);
    }

    /**
     * get Business
     *
     * @return mixed
     */
    public function getBusiness()
    {
        $onlyDeleted = filter_var(Input::get('only_deleted'), FILTER_VALIDATE_BOOLEAN);
        $business = $onlyDeleted  ? Business::onlyTrashed()->orderBy('name') : Business::orderBy('name');
        $view = $onlyDeleted ? 'admin.lists.deleted._business_list' : 'admin.lists._business_list';

        return View::make($view, [
            'business' => $business->get(),
        ])->render();
    }


    /**
     * Remove the specified resources from storage.
     *
     * @return Response
     */
    public function destroySomeRecords()
    {
        $ids = explode(',', Input::get('ids'));
        
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        
        Business::destroy($ids);
        
        return Response::json(['error' => false]);
    }

}