<?php
namespace App\Controllers;

use App\Models\NewsBreak;
use App\Models\User;
use Response;
use Input;
use App\Models;
use Auth;
use View;

/**
 * Class NewsbreakController
 */
class NewsbreakController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        $name   = Input::get('name');
        $status = Input::get('status');
        $type   = Input::get('type', NewsBreak::TYPE_GLOBAL);
        $complete   = Input::get('complete');
        if (empty($name) || empty($status) || empty($type) || 
            !in_array($status, [NewsBreak::STATUS_ACTIVE, NewsBreak::STATUS_ARCHIVED]) ||
            !in_array($type, [NewsBreak::TYPE_GLOBAL, NewsBreak::TYPE_LOCAL])) {
            Response::json(['error' => false]);
        }

        $newsbreak         = NewsBreak::findOrNew(Input::get('id', 0));
        $newsbreak->name   = $name;
        $newsbreak->status = $status;
        $newsbreak->type   = $type;
        $newsbreak->complete = ($complete) ? 1 : 0;
        $newsbreak->save();

        return Response::json($newsbreak->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        $newsbreak = NewsBreak::find($id);
        if (empty($newsbreak)) {
            return Response::json(['error' => true]);
        }
        $newsbreak->status = NewsBreak::STATUS_ACTIVE;
        $newsbreak->update(['status']);

        return Response::json(['error' => false]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        NewsBreak::destroy($id);
        return Response::json(['error' => false]);
    }

    /**
     * Restore the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function restore($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        try {
            $newsbreak = NewsBreak::onlyTrashed()->find($id);
            if ($newsbreak) {
                $newsbreak->deleted_at = NULL;
                $newsbreak->save();
            }
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }

        return Response::json(['error' => false]);
    }

    /**
     * get Newsbreaks
     *
     * @return mixed
     */
    public function getNewsbreaks()
    {
        $limit = Input::get('limit', NewsBreak::NEWSBREAKS_PAGE_SIZE_DEFAULT);
        $onlyDeleted = filter_var(Input::get('only_deleted'), FILTER_VALIDATE_BOOLEAN);
        $newsbreaks = $onlyDeleted  ? NewsBreak::onlyTrashed()->orderBy('name') : NewsBreak::orderBy('name');
        $view = $onlyDeleted ? 'admin.lists.deleted._newsbreaks_list' : 'admin.lists._newsbreaks_list';

        if ($limit !== 'all') {
            $newsbreaks = $newsbreaks->paginate((int)$limit);
        } else {
            $newsbreaks = $newsbreaks->get();
        }

        return View::make($view, [
          'newsbreaks' => $newsbreaks,
          'limit' => $limit
        ])->render();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @return Response
     */
    public function destroySomeRecords()
    {
        $ids = explode(',', Input::get('ids'));
        
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        NewsBreak::destroy($ids);
        
        return Response::json(['error' => false]);
    }

}