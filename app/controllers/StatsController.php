<?php

namespace App\Controllers;

use App\Helpers\ArrayHelper;
use App\Models\BaseValidator;
use App\Models\Country;
use \App\Models\SaveReport;
use \App\Models\Coverage;
use App\Models\Subcategory;
use \App\Models\User;
use App\Models\NewsBreak;
use App\Models\Campaign;
use App\Models\Product;
use Excel;
use Exception\APIException;
use Exception;
use Illuminate\Support\Facades\App;
use Input;
use Auth;
use Redirect;

/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at Fahrenheit 451
 *
 * Date: 01.12.14
 * Time: 18:56
 */
class StatsController extends BaseController
{

    const RANGE_DEPTH_LIMIT = 7257600; // 3 month (84 days)
    const DATE_FILTER_MONTH = "month";
    const FILTER_BY_NUMBERS = "numbers";
    const FILTER_BY_NET_EFFECT = "net_effect";
    const DYNAMIC_BY_NET_EFFECT = "net_effect";
    const DYNAMIC_BY_IMPACT_INDEX = "impact_index";
    const DYNAMIC_BY_REACH = "reach";
    const COUNT_TOP_RATING = 10;
    const TOP_STORIE_MIN_IMPACT_INDEX = 0.75;
    const SKIP_SPEAKERS_IN_RATING = [
        'Kaspersky Lab',
        'KL',
        'researcher',
        'statement',
        'speaker',
        'blog',
        'person',
        'expert'
    ];
    /**
     * @var array Date ranges for data
     */
    private $range = [];

    /**
     * @var bool Flag for determine weekly grouping
     */
    private $weekly = false;
    private $report = null;

    private $select_total_by = null;
    private $filter_by = null;

    private function validateFilter()
    {
        $validator = BaseValidator::make(Input::all(), BaseValidator::FILTER_BY_DATE_AND_REGION);

        if ($validator->fails()) {
            throw new APIException('Date range is incorrect');
        }
    }

    /**
     * get range
     *
     * @return array
     */
    private function getRange()
    {
        $start = Input::has('start') ? Input::get("start") : date('Y-m-d', strtotime('-50 year'));
        $end = Input::has('end') ? date('Y-m-d', strtotime(Input::get("end") . "+1 day")) : date('Y-m-d', strtotime("+1 day"));

        return compact('start', 'end');
    }

    /**
     * Validate filter parameters and set the date range for queries
     */
    public function __construct()
    {
        if (Auth::check() && !in_array(Auth::user()->role, [User::ROLE_ADMINISTRATOR, User::ROLE_MANAGER]) && !Input::has('token')) {
            throw new Exception("Access denied");
        }
        if (!Input::has('token') && !Auth::check()) {
            throw new Exception("Error Processing Request");
        }
        if (Input::has('token')) {
            $this->report = SaveReport::getByHash(Input::get('token'));
            if (empty($this->report)) {
                throw new Exception("Error Processing Request");
            }
        }
        $this->select_total_by = \DB::raw('SUM(coverage.net_effect) as total');
        $this->filter_by = Input::get('filter_by', self::FILTER_BY_NET_EFFECT);
        if ($this->filter_by == self::FILTER_BY_NUMBERS) {
            $this->select_total_by = \DB::raw('count(1) as total');
        }
        $this->validateFilter();
        $this->range = $this->getRange();

        $dateFilter = Input::get('date_filter', self::DATE_FILTER_MONTH);
        $this->weekly = $dateFilter != self::DATE_FILTER_MONTH;
    }


    /**
     * Add base filters for query
     *
     * @param object $builder
     * @param array $joinSkip
     *
     * @return object
     */
    public function builderFilters($builder, $joinSkip = [], $limitSkip = true)
    {
        $builder->where('coverage.status', '=', Coverage::STATUS_APPROVED)
            ->where('coverage.date', '>=', $this->range['start'])
            ->where('coverage.date', '<', $this->range['end']);

        $regionId = Input::get('region_id');
        if (!empty($regionId)) {
            $builder->where('coverage.region_id', '=', $regionId);
        }

        $subregionId = Input::get('subregion_id');
        if (!empty($subregionId)) {
            $builder->join('countries as cs', 'cs.id', '=', 'coverage.country_id');
            $builder->where('cs.subregion_id', '=', $subregionId);
        }

        $event = Input::get('event');
        if (!empty($event)) {
            $builder->where('coverage.event_name', 'LIKE', $event . "%");
        }

        $mediaList = Input::get('media_list');
        if (!empty($mediaList)) {
            $builder->where('coverage.media_list', '=', $mediaList);
        }

        $mediaCategory = Input::get('media_category');
        if (!empty($mediaCategory)) {
            $builder->where('coverage.media_category', '=', $mediaCategory);
        }

        $mediaSubCategory = Input::get('media_subcategory');
        if (!empty($mediaSubCategory)) {
            $builder->join('coverage_subcategory', 'coverage.id', '=', 'coverage_subcategory.coverage_id');
            $builder->where('coverage_subcategory.subcategory_id', '=', $mediaSubCategory);
        }

        $countryId = Input::get('country_id');
        if (!empty($countryId)) {
            $builder->where('coverage.country_id', '=', $countryId);
        }

        $newsbreakId = Input::get('newsbreak_id');
        if (!empty($newsbreakId)) {
            $builder->join('newsbreaks_coverage as nc1', 'coverage.id', '=', 'nc1.coverage_id');
            $builder->where('nc1.newsbreak_id', '=', $newsbreakId);
        }

        $productId = Input::get('product_id');
        if (!empty($productId) && !in_array('products_coverage', $joinSkip)) {
            $builder->join('products_coverage', 'coverage.id', '=', 'products_coverage.coverage_id');
            $builder->where('products_coverage.product_id', '=', $productId);
        }

        $campaignId = Input::get('campaign_id');
        if (!empty($campaignId)) {
            $builder->join('campaigns_coverage', 'coverage.id', '=', 'campaigns_coverage.coverage_id');
            $builder->where('campaigns_coverage.campaign_id', '=', $campaignId);
        }

        $businessId = Input::get('business');
        if (!empty($businessId)) {
            $builder->join('business_coverage', 'coverage.id', '=', 'business_coverage.coverage_id');
            $builder->where('business_coverage.business_id', '=', $businessId);
        }

        $speakerId = Input::get('speaker_id');
        if (!empty($speakerId)) {
            if (!in_array('speakers_coverage', $joinSkip)) {
                $builder->join('speakers_coverage', 'coverage.id', '=', 'speakers_coverage.coverage_id');
            }
            $builder->where('speakers_coverage.speaker_id', '=', $speakerId);
        }

        if ($limitSkip === false) {
            $top = Input::get('top');
            if (!empty($top)) {
                $builder->limit($top);
            } else {
                $builder->limit(self::COUNT_TOP_RATING);
            }
        }


        return $builder;
    }

    protected
    static function getComparisonParams()
    {

        $comparison = is_string(Input::get('comparison')) ? json_decode(Input::get('comparison'), true) : Input::get('comparison');

        if (!empty($comparison)) {
            return [
                'campaigns' => [
                    'table' => 'campaigns',
                    'alias' => 'c',
                    'join_field' => 'campaign_id',
                    'list' => $comparison['campaigns'] ? explode(",", $comparison['campaigns']) : [],
                    'seqnum' => 'seqnum_1',
                ],
                'newsbreaks' => [
                    'table' => 'newsbreaks',
                    'alias' => 'n',
                    'join_field' => 'newsbreak_id',
                    'list' => $comparison['newsbreaks'] ? explode(",", $comparison['newsbreaks']) : [],
                    'seqnum' => 'seqnum_2',
                ],
                'products' => [
                    'table' => 'products',
                    'alias' => 'p',
                    'join_field' => 'product_id',
                    'list' => $comparison['products'] ? explode(",", $comparison['products']) : [],
                    'seqnum' => 'seqnum_3',
                ],
                'speakers' => [
                    'table' => 'speakers',
                    'alias' => 's',
                    'join_field' => 'speaker_id',
                    'list' => $comparison['speakers'] ? explode(",", $comparison['speakers']) : [],
                    'seqnum' => 'seqnum_4',
                ],
                'events' => [
                    'table' => 'events',
                    'alias' => 'coverage',
                    'join_field' => null,
                    'field' => 'event_name',
                    'list' => $comparison['events'] ? explode(",", $comparison['events']) : [],
                    'seqnum' => 'seqnum_5',
                ],
            ];
        }

        return [];
    }

    /**
     * Add comparison filters for query
     *
     * @param object $builder
     *
     * @return object
     */
    public
    function builderComparisonFilters($builder)
    {
        $comparisonParams = self::getComparisonParams();

        if ($comparisonParams) {
            $builder->where(function ($query) use ($comparisonParams) {
                foreach ($comparisonParams as $group) {
                    if (empty($group['list'])) {
                        continue;
                    }
                    if (!empty($group['join_field'])) {
                        $query = $query->whereExists(function ($q) use ($group) {
                            $q->select(\DB::raw(1))
                                ->from("{$group['table']}_coverage as {$group['alias']}_c")
                                ->whereRaw("{$group['alias']}_c.coverage_id=coverage.id")
                                ->whereIn("{$group['alias']}_c.{$group['join_field']}", $group['list']);
                        }, 'or');
                    } else {
                        $query = $query->whereIn("coverage.{$group['field']}", $group['list'], 'or');
                    }
                }
            });
        }

        return $builder;
    }

    /**
     * @param array $data
     * @param int $precision
     *
     * @return mixed
     */
    protected
    function roundNumbers($data, $precision = 2)
    {
        if (empty($data)) {
            return [];
        }
        foreach ($data as &$item) {
            foreach ($item as &$value) {
                if (is_numeric($value) && is_float((float)$value)) {
                    $value = round($value, $precision);
                }
            }
        }
        return $data;
    }

    /**
     * Get average Impact Index by all filters
     *
     * @return float
     */
    protected
    function getAvgImpactIndex($comparisonFilters = false)
    {
        $builder = \DB::table('coverage')
            ->select(\DB::raw('ROUND(avg(coverage.impact_index), 2) as impact_index'));
        if ($comparisonFilters) {
            $builder = $this->builderComparisonFilters($builder);
        }
        $result = $this->builderFilters($builder)->first();

        return empty($result->impact_index) ? 0 : $result->impact_index;
    }

    /**
     * Prepare total counter stat for sharts by filter
     *
     * @param object $data
     * @param string $field
     *
     * @return array
     */
    public
    function prepareTotalResult($data, $field)
    {

        $result = [];
        foreach ($data as $item) {

            $item->total = (float)$item->total;
            $tmp = [
                'total' => $item->total,
            ];
            if (is_array($field)) {
                foreach ($field as $value) {
                    $tmp[$value] = $item->$value;
                }
            } else {
                $tmp[$field] = $item->$field;
            }
            $result[] = $tmp;
        }

        return $this->roundNumbers($result);
    }

    /**
     * Fetching coverage items count grouped by publication region. Chart "NUMBERS OF ARTICLES"
     *
     * @return array|static[]
     */
    public
    function getCoverageByRegion()
    {
        $regionId = Input::get('region_id');
        $subregionId = Input::get('subregion_id');
        $countryId = Input::get('country_id');

        $builder = \DB::table('coverage');

        if (empty($regionId) && empty($subregionId) && empty($countryId)) {
            $groupBy = 'regions.id';
            $builder->select('coverage.region_id', \DB::raw('count(1) as total'), 'regions.name')
                ->join('regions', 'regions.id', '=', 'coverage.region_id');
        } else {
            $groupBy = 'countries.id';
            $builder->select('coverage.country_id', 'coverage.country_id', \DB::raw('count(1) as total'), 'countries.name')
                ->join('countries', 'countries.id', '=', 'coverage.country_id');
            if (!empty($subregionId) && empty($countryId)) {
                $builder->where('countries.subregion_id', '=', $subregionId);
            }
        }

        $builder->groupBy($groupBy);

        return $this->builderFilters($builder)->get();
    }

    /**
     * Get total coverage count
     *
     * @return array|static[]
     */
    public
    function getCoverageTotals()
    {
        $result = ['coverage_count' => 0, 'impact_index' => 0, 'net_effect' => 0, 'media_outreach' => 0];
        $builder = \DB::table('coverage')
            ->select(
                \DB::raw('count(1) as coverage_count'),
                \DB::raw('sum(coverage.net_effect) as net_effect'),
                \DB::raw('sum(coverage.reach) as media_outreach')
            )
            ->join('regions', 'regions.id', '=', 'coverage.region_id')
            ->groupBy('regions.id');


        $builder = $this->builderComparisonFilters($builder);
        $data = $this->builderFilters($builder)->get();

        foreach ($data as $media) {
            $result['coverage_count'] += $media->coverage_count;
            $result['net_effect'] += empty($media->net_effect) ? 0 : $media->net_effect;
            $result['media_outreach'] += empty($media->media_outreach) ? 0 : $media->media_outreach;
        }
        $result['impact_index'] = $this->getAvgImpactIndex(true);
        $result['net_effect'] = $result['net_effect'] >= 1000000 ? number_format($result['net_effect'] / 1000000, 1, '.', '') . 'M' : '< 1M';
        $result['media_outreach'] = $result['media_outreach'] >= 1000000 ? number_format($result['media_outreach'] / 1000000, 1, '.', '') . 'M' : '< 1M';
        return $result;
    }

    /**
     * Get key messages
     *
     * @return array|static[]
     */
    public
    function getKeyMessages()
    {
        $regionId = Input::get('region_id');
        $subregionId = Input::get('subregion_id');
        $countryId = Input::get('country_id');

        $builder = \DB::table('coverage');
        // $value         = 'numbers' === $this->filter_by ? '1' : 'net_effect';
        // Касперский попросили отключить на этом графике разбиение на NE и количество
        // Пока просто комментрируем код отвечающий за это.
        $value = 1;
        $selectWith = \DB::raw("SUM(CASE WHEN coverage.key_message_penetration IN ('present', 'extent') THEN " . $value . " ELSE 0 END) as with_messages");
        $selectWithout = \DB::raw("SUM(CASE WHEN coverage.key_message_penetration IN ('absent', 'counter') THEN " . $value . " ELSE 0 END) as without_messages");

        if (empty($regionId) && empty($subregionId) && empty($countryId)) {
            $groupBy = 'regions.id';
            $builder->select('coverage.region_id', $selectWithout, $selectWith, 'regions.name')
                ->join('regions', 'regions.id', '=', 'coverage.region_id');
        } else {
            $groupBy = 'countries.id';
            $builder->select('coverage.country_id', $selectWithout, $selectWith, 'countries.name')
                ->join('countries', 'countries.id', '=', 'coverage.country_id');
            if (!empty($subregionId) && empty($countryId)) {
                $builder->where('countries.subregion_id', '=', $subregionId);
            }
        }

        $builder->whereNotNull('coverage.key_message_penetration')
            ->groupBy($groupBy);

        return $this->roundNumbers($this->builderFilters($builder)->get());
    }

    /**
     * Get key messages
     *
     * @return array|static[]
     */
    public
    function getSecurityIntelligence()
    {
        // Отключили график на фронте в GENERAL INFORMATION
        $builder = \DB::table('coverage');
        $regionId = Input::get('region_id');
        $countryId = Input::get('country_id');

        $countProduct = Coverage::countProduct('coverage');
        $countSecurityIntelligence = Coverage::countSecurityIntelligence('coverage');

        $selectWith = \DB::raw("SUM(CASE WHEN 0 < ({$countSecurityIntelligence->toSql()}) THEN 1 ELSE 0 END) as with_security_intelligence");
        $selectWithout = \DB::raw("SUM(CASE WHEN (0 < ({$countProduct->toSql()}) AND 0 = ({$countSecurityIntelligence->toSql()})) THEN 1 ELSE 0 END) as without_security_intelligence");
        if (!empty($regionId) || !empty($countryId)) {
            $groupBy = 'coverage.country_id';
            $builder->select(
                'coverage.country_id',
                $selectWithout,
                $selectWith,
                'countries.name'
            )
                ->join('countries', 'countries.id', '=', 'coverage.country_id');
        } else {
            $groupBy = 'region_id';
            $builder->select(
                'coverage.region_id',
                $selectWithout,
                $selectWith,
                'regions.name'
            )
                ->join('regions', 'regions.id', '=', 'coverage.region_id');
        }
        $builder->mergeBindings($countSecurityIntelligence)
            ->mergeBindings($countProduct)
            ->groupBy($groupBy);

        return $this->roundNumbers($this->builderFilters($builder)->get());
    }

    /**
     * Get key messages
     *
     * @return array|static[]
     */
    public
    function getTotalKeyMessages()
    {
        $builder = \DB::table('coverage')
            ->select('coverage.key_message_penetration AS penetration', \DB::raw("count(coverage.key_message_penetration) AS count"))
            ->whereNotNull('coverage.key_message_penetration')
            ->groupBy('coverage.key_message_penetration');

        $data = $this->builderFilters($builder)->get();
        $result = [];
        foreach ($data as $value) {
            $result[] = [
                'title' => Coverage::getKeyMessagePenetrationTitle($value->penetration),
                'message_penetration' => $value->penetration,
                'count' => $value->count
            ];
        }
        return $result;
    }

    /**
     * @param null $baseBuilder
     *
     * @return mixed
     */
    protected
    function getLocationLabels($baseBuilder = null)
    {
        $regionId = Input::get('region_id');
        $subregionId = Input::get('subregion_id');
        $countryId = Input::get('country_id');

        $builder = is_null($baseBuilder) ? \DB::table('coverage') : $baseBuilder;

        if (empty($regionId) && empty($subregionId) && empty($countryId)) {
            $builder
                ->select("r.name", "r.id")
                ->join('regions AS r', 'r.id', '=', 'coverage.region_id')
                ->join('countries AS c', 'c.region_id', '=', 'coverage.region_id')
                ->orderBy("r.seqnum")
                ->groupBy("r.name");
        } else {
            $builder
                ->select("c.name", "c.id")
                ->join('regions AS r', 'r.id', '=', 'coverage.region_id')
                ->join('countries AS c', 'c.region_id', '=', 'coverage.region_id')
                ->groupBy("c.name");
        }

        if (!empty($regionId)) {
            $builder->where('r.id', '=', $regionId);
        }

        if (!empty($subregionId) && empty($countryId)) {
            $builder->where('c.subregion_id', '=', $subregionId);
        }

        if (!empty($countryId)) {
            $builder->where('c.id', '=', $countryId);
        }

        return $this->builderFilters($builder)->get();
    }

    /**
     * @param $baseBuilder
     * @param $alias
     * @param $groupBy
     *
     * @return array
     */
    protected
    function getMedia($baseBuilder, $alias, $groupBy)
    {
        $result = [];
        $builder = clone $baseBuilder;
        $builder->select("{$alias}.name", "{$alias}.id", "coverage.media_category", \DB::raw('count(coverage.id) AS count'))
            ->whereNotNull('coverage.media_category')
            ->groupBy($groupBy, 'coverage.media_category');

        $mediaCategory = $this->builderFilters($builder)->get();
        foreach ($mediaCategory as $media) {
            $result['media_category'][$media->id][strtolower($media->media_category)] = $media->count;
            if (!isset($result['media_category']['total'][strtolower($media->media_category)])) {
                $result['media_category']['total'][strtolower($media->media_category)] = 0;
            }
            $result['media_category']['total'][strtolower($media->media_category)] += $media->count;
        }

        $mediaSubCategory = $this->prepareMediaSubcategories($alias, $baseBuilder);

        foreach ($mediaSubCategory as $subcategory) {
            if ($alias == 'r') {
                $key = 'region_id';
            } elseif ($alias == 's') {
                $key = 'subregion_id';
            } elseif ($alias == 'c') {
                $key = 'country_id';
            }
            $result['media_subcategory'][$subcategory->$key][strtolower($subcategory->title)] = $subcategory->count;
            if (!isset($result['media_subcategory']['total'][strtolower($subcategory->title)])) {
                $result['media_subcategory']['total'][strtolower($subcategory->title)] = 0;
            }
            $result['media_subcategory']['total'][strtolower($subcategory->title)] += $subcategory->count;
        }

        $list = Coverage::getMediaList();
        $builder = clone $baseBuilder;
        $builder->select("{$alias}.name", "{$alias}.id", "coverage.media_list", \DB::raw('count(coverage.id) AS count'))
            ->whereNotNull('coverage.media_list')
            ->groupBy($groupBy, 'coverage.media_list');

        $mediaList = $this->builderFilters($builder)->get();
        $result['media_list']['total'] = [];

        foreach ($mediaList as $media) {
            if (empty($media->media_list) || empty($list[$media->media_list])) {
                continue;
            }
            $result['media_list'][$media->id][$list[$media->media_list]] = $media->count;
            if (!isset($result['media_list']['total'][$list[$media->media_list]])) {
                $result['media_list']['total'][$list[$media->media_list]] = 0;
            }
            $result['media_list']['total'][$list[$media->media_list]] += $media->count;
        }

        $list = Coverage::getMainTonalityes();
        $builder = clone $baseBuilder;
        $builder->select("{$alias}.name", "{$alias}.id", "coverage.main_tonality", \DB::raw('count(coverage.id) AS count'))
            ->whereNotNull('coverage.main_tonality')
            ->groupBy($groupBy, 'coverage.main_tonality');

        $records = $this->builderFilters($builder)->get();
        $result['main_tonality']['total'] = [];

        foreach ($records as $record) {
            if (empty($record->main_tonality) || empty($list[$record->main_tonality])) {
                continue;
            }
            $result['main_tonality'][$record->id][$list[$record->main_tonality]] = $record->count;
            if (!isset($result['main_tonality']['total'][$list[$record->main_tonality]])) {
                $result['main_tonality']['total'][$list[$record->main_tonality]] = 0;
            }
            $result['main_tonality']['total'][$list[$record->main_tonality]] += $record->count;
        }
        return $result;
    }

    /**
     * Get regional KPIs
     *
     * @return array|static[]
     */
    public
    function getRegionalKpi()
    {
        $result = [
            'articles' => [],
            'media_outreach' => [],
            'labels' => [['name' => 'Total', 'id' => 'total']]
        ];

        $regionId = Input::get('region_id');
        $subregionId = Input::get('subregion_id');
        $countryId = Input::get('country_id');

        $baseBuilder = \DB::table('coverage');

        if (empty($regionId) && empty($subregionId) && empty($countryId)) {
            $groupBy = 'r.id';
            $alias = 'r';
            $baseBuilder->join('regions AS r', 'r.id', '=', 'coverage.region_id');
        } else {
            $groupBy = 'c.id';
            $alias = 'c';
            $baseBuilder->join('countries AS c', 'c.id', '=', 'coverage.country_id');
            if (!empty($subregionId) && empty($countryId)) {
                $baseBuilder->where('c.subregion_id', '=', $subregionId);
            }
        }

        $result['labels'] = array_merge($result['labels'], $this->getLocationLabels());

        $builder = clone $baseBuilder;
        $builder->select("{$alias}.name", "{$alias}.id", \DB::raw('count(coverage.id) as count'))
            ->groupBy($groupBy);


        $articles = $this->builderFilters($builder)->get();
        $total = 0;
        foreach ($articles as $article) {

            $result['articles'][$article->id] = $article->count;
            $total += $article->count;
        }
        $result['articles']['total'] = $total;

        $builder = clone $baseBuilder;
        $builder->select(
            "{$alias}.name",
            "{$alias}.id",
            \DB::raw('sum(coverage.reach) as reach'),
            \DB::raw('sum(coverage.net_effect) as net_effect'),
            \DB::raw('avg(coverage.impact_index) as impact_index')
        )
            ->groupBy($groupBy);


        $stats = $this->builderFilters($builder)->get();
        $totalMedia = 0;
        $totalImpact = 0;
        $totalNetEffect = 0;
        if (0 == count($stats)) {
            $result['show_total'] = empty($countryId);
            $result['net_effect']['total'] = 0;
            $result['media_outreach']['total'] = 0;
            $result['impact_index']['total'] = 0;
            return $result;
        }
        foreach ($stats as $media) {
            $index = $media->id;
            $result['media_outreach'][$index] = $media->reach >= 1000000 ? round($media->reach / 1000000) . 'M' : '< 1M';
            $result['impact_index'][$index] = round($media->impact_index, 2);
            $result['net_effect'][$index] = $media->net_effect >= 1000000 ? round($media->net_effect / 1000000) . 'M' : '< 1M';
            $totalMedia += $media->reach;
            $totalNetEffect += $media->net_effect;
            $totalImpact += $media->impact_index;
        }
        $result['impact_index']['total'] = $this->getAvgImpactIndex();
        $result['media_outreach']['total'] = $totalMedia >= 1000000 ? round($totalMedia / 1000000) . 'M' : '< 1M';
        $result['net_effect']['total'] = $totalNetEffect >= 1000000 ? round($totalNetEffect / 1000000) . 'M' : '< 1M';

        $result = array_merge($result, $this->getMedia($baseBuilder, $alias, $groupBy));
        $result['show_total'] = empty($countryId);

        return $result;
    }

    /**
     * @return array
     */
    public
    function getRegionalProductMentions()
    {
        $result = [
            'labels' => [['name' => 'Total', 'id' => 'total']]
        ];

        $regionId = Input::get('region_id');
        $subregionId = Input::get('subregion_id');
        $countryId = Input::get('country_id');

        $baseBuilder = \DB::table('coverage')
            // coverage to products has relationship "one to many"
            ->join(\DB::raw('(SELECT coverage_id FROM products_coverage GROUP BY coverage_id) as pc'), 'coverage.id', '=', 'pc.coverage_id');

        $result['labels'] = array_merge($result['labels'], $this->getLocationLabels(clone $baseBuilder));

        if (empty($regionId) && empty($subregionId) && empty($countryId)) {
            $groupBy = 'r.id';
            $alias = 'r';
            $baseBuilder->join('regions AS r', 'r.id', '=', 'coverage.region_id');
        } else {
            $groupBy = 'c.id';
            $alias = 'c';
            $baseBuilder->join('countries AS c', 'c.id', '=', 'coverage.country_id');
            if (!empty($subregionId) && empty($countryId)) {
                $baseBuilder->where('c.subregion_id', '=', $subregionId);
            }
        }

        $builder = clone $baseBuilder;
        $builder->select(
            "{$alias}.name",
            "{$alias}.id",
            \DB::raw('count(1) as count'),
            \DB::raw('round(avg(impact_index), 2) as impact_index'),
            \DB::raw('sum(coverage.net_effect) as net_effect')
        )
            ->groupBy($groupBy);

        $articles = $this->builderFilters($builder)->get();
        $totalArticles = 0;
        $totalNetEffect = 0;
        $totalsBuilder = \DB::table('coverage')->select(\DB::raw('count(1) as count'), \DB::raw('round(avg(impact_index), 2) as impact_index'));
        $totals = $this->builderFilters($totalsBuilder)->first();

        if (0 == count($articles)) {
            $articleId = 'total';
            if (!empty($countryId)) {
                $articleId = Country::getCountryById($countryId);
                $result['labels'][] = ['name' => $articleId, 'id' => $countryId];
                $result['show_total'] = false;
            }
            $result['percent'][$articleId] = 0;
            $result['articles'][$articleId] = 0;
            $result['net_effect'][$articleId] = 0;
            $result['impact_index'][$articleId] = 0;
            return $result;
        }
        foreach ($articles as $article) {
            $result['articles'][$article->id] = $article->count;
            $result['impact_index'][$article->id] = $article->impact_index;
            $result['net_effect'][$article->id] = $article->net_effect >= 1000000 ? round($article->net_effect / 1000000) . 'M' : '< 1M';
            $totalArticles += $article->count;
            $totalNetEffect += $article->net_effect;
        }
        $result['articles']['total'] = $totalArticles;
        $result['net_effect']['total'] = $totalNetEffect >= 1000000 ? round($totalNetEffect / 1000000) . 'M' : '< 1M';
        $result['impact_index']['total'] = $totals->impact_index;
        foreach ($articles as $article) {
            $totalArticlesCount = empty($countryId) ? $totalArticles : $totals->count;
            $result['percent'][$article->id] = number_format(($article->count / $totalArticlesCount) * 100, 1, '.', '') . '%';
        }
        $result['percent']['total'] = number_format(($totalArticles / $totals->count) * 100, 1, '.', '') . '%';

        $result = array_merge($result, $this->getMedia($baseBuilder, $alias, $groupBy));
        $result['show_total'] = empty($countryId);

        return $result;
    }

    /**
     * Fetching coverage items count grouped by media list type. Chart "MEDIA LISTS"
     *
     * @return array|static[]
     */
    public
    function getCoverageByList()
    {
        $builder = \DB::table('coverage')
            ->select('media_list', $this->select_total_by)
            ->whereRaw('LENGTH(media_list) > 0')
            ->groupBy('media_list');

        $data = $this->builderFilters($builder)->get();
        return $this->prepareTotalResult($data, 'media_list');
    }

    /**
     * Fetching coverage items count grouped by media type. Chart "MEDIA TYPES"
     *
     * @return array|static[]
     */
    public
    function getCoverageByType()
    {
        $builder = \DB::table('coverage')
            ->select('media_type', $this->select_total_by)
            ->whereRaw('LENGTH(media_type) > 0')
            ->groupBy('media_type');

        $data = $this->builderFilters($builder)->get();
        return $this->prepareTotalResult($data, 'media_type');
    }

    /**
     * Fetching coverage items count grouped by media category. Chart "MEDIA CATEGORIES"
     *
     * @return array|static[]
     */
    public
    function getCoverageByCategories()
    {
        $builder = \DB::table('coverage')
            ->select('media_category', $this->select_total_by)
            ->whereRaw('LENGTH(media_category) > 0')
            ->groupBy('media_category');

        $data = $this->builderFilters($builder)->get();
        return $this->prepareTotalResult($data, 'media_category');
    }

    /**
     * Fetching coverage items count grouped by country
     *
     * @return array|static[]
     */
    public
    function getCoverageByCountries()
    {
        $builder = \DB::table('coverage')
            ->select('coverage.country_id', \DB::raw('count(1) as total'), 'countries.name')
            ->join('countries', 'countries.id', '=', 'coverage.country_id')
            ->groupBy('country_id');

        return $this->builderFilters($builder)->get();
    }

    /**
     * Fetching coverage items count grouped by key message penetration
     *
     * @return array|static[]
     */
    public
    function getCoverageByMessagePenetration()
    {
        $builder = \DB::table('coverage')
            ->select('key_message_penetration as penetration', \DB::raw('count(1) as total'))
            ->groupBy('key_message_penetration');

        return $this->builderFilters($builder)->get();
    }

    /**
     * Fetching coverage items count grouped by main text tonality. Chart "MAIN TONALITY"
     *
     * @return array|static[]
     */
    public
    function getCoverageByMainTonality()
    {

        /**
         *
         $builder = \DB::table('coverage')
        ->select('main_tonality', $this->select_total_by)
        ->where('main_tonality', '!=', '')
        ->groupBy('main_tonality');

        $data = $this->builderFilters($builder)->get();
        return $this->prepareTotalResult($data, 'main_tonality');
         *
         */
        foreach (['positive', 'neutral', 'negative'] as $tonality) {
            $builder = \DB::table('coverage')
                ->select("main_tonality", $this->select_total_by)
                ->join('regions', 'regions.id', '=', 'coverage.region_id')
                ->where('main_tonality', '=', $tonality);
            $count = $this->builderFilters($builder)->get();

            $obj = new \stdClass();
            $obj->total=$count[0]->total;
            $obj->main_tonality=$tonality;

            $data[]=$obj;

        }

            return $this->prepareTotalResult($data, 'main_tonality');
        }

        /**
         * Fetching products by mentions count. Chart "PRODUCTS RATING"
         *
         * @return array|static[]
         */
        public
        function getTopProducts()
        {
            $builder = \DB::table('products_coverage as pc')
                ->select('pc.coverage_id', $this->select_total_by, \DB::raw('COALESCE(products.short_name, products.name) AS name'), 'pc.product_id')
                ->join('products', 'products.id', '=', 'pc.product_id')
                ->join('coverage', 'coverage.id', '=', 'pc.coverage_id')
                ->groupBy('pc.product_id')
                ->orderBy('total', 'desc');

            $data = $this->builderFilters($builder, ['products_coverage'], false)->get();
            return $this->prepareTotalResult($data, ['coverage_id', 'name', 'product_id']);
        }


        /**
         * Fetching newsbreaks by mentions count. Chart "NEWSBREAK RATING"
         *
         * @return array|static[]
         */
        public
        function getTopNewsbreaks()
        {
            $builder = \DB::table('newsbreaks_coverage as nс')
                ->select('newsbreaks.name', $this->select_total_by, \DB::raw('newsbreaks.id as newsbreak_id'))
                ->join('newsbreaks', 'newsbreaks.id', '=', 'nс.newsbreak_id')
                ->join('coverage', 'coverage.id', '=', 'nс.coverage_id')
                ->groupBy('newsbreaks.name')
                ->orderBy('total', 'desc');

            $data = $this->builderFilters($builder, [], false)->get();
            return $this->prepareTotalResult($data, ['name', 'newsbreak_id']);
        }

        /**
         * Fetching events by mentions count. Chart "EVENTS RATING"
         *
         * @return array|static[]
         */
        public
        function getTopEvents()
        {
            $builder = \DB::table('coverage')
                ->select('coverage.event_name as name', $this->select_total_by)
                ->whereRaw('LENGTH(event_name) > 0')
                ->groupBy('coverage.event_name')
                ->orderBy('total', 'desc')
                ->limit(self::COUNT_TOP_RATING);

            $data = $this->builderFilters($builder)->get();
            return $this->prepareTotalResult($data, 'name');
        }

        /**
         * Fetching campaigns by mentions count. Chart "CAMPAIGNS RATING"
         *
         * @return array|static[]
         */
        public
        function getTopCampaigns()
        {
            $builder = \DB::table('campaigns_coverage as сс')
                ->select('campaigns.name', $this->select_total_by, \DB::raw('campaigns.id as campaign_id'))
                ->join('campaigns', 'campaigns.id', '=', 'сс.campaign_id')
                ->join('coverage', 'coverage.id', '=', 'сс.coverage_id')
                ->groupBy('campaigns.name')
                ->orderBy('total', 'desc');

            $data = $this->builderFilters($builder, [], false)->get();
            return $this->prepareTotalResult($data, ['name', 'campaign_id']);
        }

        /**
         * Fetching desired articles. Chart "DESIRED ARTICLES"
         *
         * @return array
         */
        public
        function getDesiredArticles()
        {
            $builder = \DB::table('coverage')
                ->select(
                    $this->select_total_by,
                    \DB::raw('(CASE WHEN coverage.desired_article = 1 THEN "Desired" ELSE "Undesired" END) AS name')
                )
                ->groupBy('coverage.desired_article');

            $data = $this->builderFilters($builder)->get();
            return $this->prepareTotalResult($data, 'name');
        }

        /**
         * Fetching speakers rating
         *
         * @return array
         */
        public
        function getSpeakersRating()
        {
            $builder = \DB::table('coverage')
                ->select(
                    \DB::raw('SUM((CASE WHEN speakers_coverage.coverage_id IS NULL THEN 1 ELSE 0 END)) AS without_speakers'),
                    \DB::raw('COUNT(DISTINCT coverage.id) AS all_speakers')
                )
                ->leftJoin('speakers_coverage', 'coverage.id', '=', 'speakers_coverage.coverage_id');

            $speakers = $this->builderFilters($builder, ['speakers_coverage'])->get()[0];

            if (!empty($speakers->without_speakers) && !empty($speakers->all_speakers)) {
                return [
                    [
                        'name' => 'Without speakers',
                        'count' => (float)$speakers->without_speakers,
                    ],
                    [
                        'name' => 'With speakers',
                        'count' => (float)($speakers->all_speakers - $speakers->without_speakers),
                    ]
                ];
            }
            return [];
        }

        /**
         * Fetching products by mentions count. Chart "SPEAKER’S RATING"
         *
         * @return array|static[]
         */
        public
        function getTopSpeakersRating()
        {
            $filter_by = Input::get('filter_by', self::FILTER_BY_NET_EFFECT);
            $select_total_by = \DB::raw('SUM(coverage.net_effect) as total, speakers.id as speaker_id');
            if ($filter_by == self::FILTER_BY_NUMBERS) {
                $select_total_by = \DB::raw('count(distinct(coverage.id)) as total, speakers.id as speaker_id');
            }

            $builder = \DB::table('coverage')
                ->select('speakers.name', \DB::raw($select_total_by))
                ->join('speakers_coverage', 'coverage.id', '=', 'speakers_coverage.coverage_id')
                ->join('speakers', 'speakers.id', '=', 'speakers_coverage.speaker_id')
                ->whereNotIn('speakers.name', self::SKIP_SPEAKERS_IN_RATING)
                ->whereNull('speakers.deleted_at')
                ->groupBy('speakers.id')
                ->orderBy('total', 'desc');
            $data = $this->builderFilters($builder, ['speakers_coverage'], false)->get();
            return $this->prepareTotalResult($data, ['name', 'speaker_id']);
        }

        /**
         * Chart "PRODUCTS’ MENTIONS IN ARTICLE"
         *
         * @return array
         */
        public
        function getMentionProductRelation()
        {
            $builder = \DB::table('coverage')
                ->select(
                    $this->select_total_by,
                    \DB::raw('ROUND(AVG(coverage.impact_index), 2) as avg_index')
                )
                // coverage to products has relationship "one to many"
                ->join(\DB::raw('(SELECT coverage_id FROM products_coverage GROUP BY coverage_id) as pc'), 'coverage.id', '=', 'pc.coverage_id');

            $withMention = $this->builderFilters($builder, ['products_coverage'])->get();
            $withMention = $this->prepareTotalResult($withMention, 'avg_index');
            $withMention = array_shift($withMention);

            $builder = \DB::table('coverage')
                ->select(
                    $this->select_total_by,
                    \DB::raw('ROUND(AVG(impact_index), 2) as avg_index')
                )
                ->whereRaw('coverage.id not in (select coverage_id from products_coverage)');

            $withoutMentions = $this->builderFilters($builder, ['products_coverage'])->get();
            $withoutMentions = $this->prepareTotalResult($withoutMentions, 'avg_index');
            $withoutMentions = array_shift($withoutMentions);
            if (!empty($withoutMentions['total']) && !empty($withMention['total'])) {
                $result = [
                    [
                        'name' => 'With Product',
                        'total' => (float)$withMention['total'],
                        'avg_index' => (float)$withMention['avg_index'],
                    ], [
                        'name' => 'Without Product',
                        'total' => (float)$withoutMentions['total'],
                        'avg_index' => (float)$withoutMentions['avg_index'],
                    ]
                ];
                return $result;
            }
            return [];
        }

        /**
         * Fetching percent of speaker quoted. Chart "PERCENT OF SPEAKERS QUOTED"
         *
         * @return array|static[]
         */
        public
        function getSpeakerQuote()
        {
            $builder = \DB::table('coverage')
                ->select('speaker_quote', $this->select_total_by)
                ->whereNotNull('speaker_quote')
                ->whereRaw('LENGTH(speaker_quote) > 0')
                ->groupBy('speaker_quote');

            $data = $this->builderFilters($builder)->get();
            $result = $this->prepareTotalResult($data, 'speaker_quote');
            foreach ($result as &$r) {
                $r['name'] = $r['speaker_quote'] == 1 ? 'Present' : 'Absent';
                unset($r['speaker_quote']);
            }

            return $result;
        }

        /**
         * Fetching percent of third-party speakers quoted. Chart "Percent of third-party speakers quoted"
         *
         * @return array|static[]
         */
        public
        function getThirdSpeakerQuote()
        {
            $builder = \DB::table('coverage')
                ->select('third_speakers_quotes', $this->select_total_by)
                ->whereNotNull('third_speakers_quotes')
                ->whereRaw('LENGTH(third_speakers_quotes) > 0')
                ->groupBy('third_speakers_quotes');

            $data = $this->builderFilters($builder)->get();
            $result = $this->prepareTotalResult($data, 'third_speakers_quotes');
            foreach ($result as &$r) {
                $r['name'] = $r['third_speakers_quotes'] == "1" ? 'Present' : 'Absent';
                unset($r['third_speakers_quotes']);
            }

            return $result;
        }

        /**
         * Chart "MEDIA OUTREACH"
         *
         * @return array|static[]
         */
        public
        function getReachByRegion()
        {
            $regionId = Input::get('region_id');
            $subregionId = Input::get('subregion_id');
            $countryId = Input::get('country_id');

            $builder = \DB::table('coverage');

            if (empty($regionId) && empty($subregionId) && empty($countryId)) {
                $groupBy = 'regions.id';
                $builder->select('coverage.region_id', \DB::raw('SUM(coverage.reach) as reach'), 'regions.name')
                    ->join('regions', 'regions.id', '=', 'coverage.region_id');
            } else {
                $groupBy = 'countries.id';
                $builder->select('coverage.country_id', \DB::raw('SUM(coverage.reach) as reach'), 'countries.name')
                    ->join('countries', 'countries.id', '=', 'coverage.country_id');
                if (!empty($subregionId) && empty($countryId)) {
                    $builder->where('countries.subregion_id', '=', $subregionId);
                }
            }

            $builder->groupBy($groupBy);
            $builder->orderBy('reach', 'desc');
            $result = $this->builderFilters($builder)->get();
            foreach ($result as &$item) {
                $item->reach = (float)$item->reach;
            }

            return $result;
        }

        /**
         * get Avg-index
         *
         * @return array|static[]
         */
        public
        function getAvgIndexByRegion()
        {
            $regionId = Input::get('region_id');
            $subregionId = Input::get('subregion_id');
            $countryId = Input::get('country_id');

            $builder = \DB::table('coverage');

            if (empty($regionId) && empty($subregionId) && empty($countryId)) {
                $groupBy = 'regions.id';
                $builder->select('coverage.region_id', \DB::raw('ROUND(AVG(coverage.impact_index), 2) as avg_index'), 'regions.name')
                    ->join('regions', 'regions.id', '=', 'coverage.region_id');
            } else {
                $groupBy = 'countries.id';
                $builder->select('coverage.country_id', \DB::raw('ROUND(AVG(coverage.impact_index), 2) as avg_index'), 'countries.name')
                    ->join('countries', 'countries.id', '=', 'coverage.country_id');
                // by subregion
                if (!empty($subregionId) && empty($countryId)) {
                    $builder->where('countries.subregion_id', '=', $subregionId);
                }
            }

            $builder->groupBy($groupBy);
            $builder->orderBy("avg_index", 'desc');

            return [
                'by_region' => $this->builderFilters($builder)->get(),
                'common' => $this->getAvgImpactIndex()
            ];
        }

        /**
         * Chart "NET EFFECT"
         *
         * @return array|static[]
         */
        public
        function getNetByRegion()
        {
            $regionId = Input::get('region_id');
            $subregionId = Input::get('subregion_id');
            $countryId = Input::get('country_id');

            $builder = \DB::table('coverage');

            if (empty($regionId) && empty($subregionId) && empty($countryId)) {
                $groupBy = 'regions.id';
                $builder->select('coverage.region_id', \DB::raw('SUM(coverage.net_effect) as net'), 'regions.name')
                    ->join('regions', 'regions.id', '=', 'coverage.region_id');
            } else {
                $groupBy = 'countries.id';
                $builder->select('coverage.country_id', \DB::raw('SUM(coverage.net_effect) as net'), 'countries.name')
                    ->join('countries', 'countries.id', '=', 'coverage.country_id');
                if (!empty($subregionId) && empty($countryId)) {
                    $builder->where('countries.subregion_id', '=', $subregionId);
                }
            }

            $builder->groupBy($groupBy);

            $result = $this->builderFilters($builder)->get();
            foreach ($result as &$item) {
                $item->net = (float)$item->net;
            }

            return $this->roundNumbers($result);
        }

        /**
         * get Dynamic Newsbreak
         *
         * @return array
         */
        protected
        function getTopDynamicNewsbreak($selectBy, $groupBy, $type = null)
        {
            $selectsTop = [
                self::DYNAMIC_BY_IMPACT_INDEX => \DB::raw('count(coverage.id) as monthly_impact_index'),
                self::DYNAMIC_BY_REACH => \DB::raw('ROUND(SUM(reach), 2) as monthly_reach'),
                self::DYNAMIC_BY_NET_EFFECT => \DB::raw('ROUND(SUM(net_effect), 2) as monthly_net_effect'),
            ];
            $builder = \DB::table('coverage')
                ->join('newsbreaks_coverage as nc2', 'nc2.coverage_id', '=', 'coverage.id')
                ->join('newsbreaks', 'newsbreaks.id', '=', 'nc2.newsbreak_id')
                ->groupBy($groupBy)
                ->groupBy('newsbreaks.name')
                ->orderBy('date');

            if (!empty($type) && in_array($type, [self::DYNAMIC_BY_NET_EFFECT, self::DYNAMIC_BY_IMPACT_INDEX, self::DYNAMIC_BY_REACH])) {
                $builder->select(
                    $selectBy,
                    $selectsTop[$type],
                    \DB::raw('ROUND(SUM(net_effect), 2) as net_effect'),
                    \DB::raw('count(coverage.id) as total'), 'newsbreaks.name'
                )
                    ->whereNotNull("coverage.{$type}")
                    ->orderBy("monthly_{$type}", 'desc')
                    ->orderBy('total', 'desc');
            } else {
                $builder->select($selectBy, \DB::raw('count(coverage.id) as total'), 'newsbreaks.name', \DB::raw('ROUND(SUM(net_effect), 2) as net_effect'))
                    ->orderBy('total', 'desc');
            }

            $result = $this->builderFilters($builder)->get();
            $tmpDate = -1;
            $newsbreaks = [];
            foreach ($result as $index => $newsbreak) {
                if ($tmpDate == $newsbreak->date) {
                    continue;
                }
                $tmpDate = $newsbreak->date;
                $newsbreak->net_effect = round($newsbreak->net_effect / 1000000, 2);
                $newsbreaks[$newsbreak->date] = $newsbreak;
            }
            return $newsbreaks;
        }

        /**
         * @return array
         */
        public
        function getCoverageDynamic()
        {
            $this->validateFilter();
            if ($this->weekly) {
                $selectBy = \DB::raw("str_to_date(concat(yearweek(`date`, 1), ' monday'), '%X%V %W') as date");
                $groupBy = \DB::raw("yearweek(`date`, 1)");
            } else {
                $selectBy = \DB::raw("DATE_FORMAT(`date`, '%Y-%m') as date");
                $groupBy = \DB::raw("DATE_FORMAT(`date`, '%Y-%m')");
            }

            $builder = \DB::table('coverage')
                ->select($selectBy, \DB::raw('COUNT(1) as count'))
                ->groupBy($groupBy)
                ->orderBy('date');

            $result = $this->builderFilters($builder)->get();
            $newsbreaks = $this->getTopDynamicNewsbreak($selectBy, $groupBy);
            foreach ($result as $index => $coverage) {
                $result[$index]->total = !empty($newsbreaks[$coverage->date]) ? $newsbreaks[$coverage->date]->total : 0;
                $result[$index]->name = !empty($newsbreaks[$coverage->date]) ? $newsbreaks[$coverage->date]->name : null;
                $result[$index]->net_effect = !empty($newsbreaks[$coverage->date]) ? $newsbreaks[$coverage->date]->net_effect : null;
                $result[$index]->date = strtotime($coverage->date) * 1000;
            }
            return $result;
        }

        /**
         * @return array
         */
        public
        function getImpactDynamic()
        {
            $this->validateFilter();
            if ($this->weekly) {
                $selectBy = \DB::raw("str_to_date(concat(yearweek(`date`, 1), ' monday'), '%X%V %W') as date");
                $groupBy = \DB::raw("yearweek(`date`, 1)");
            } else {
                $selectBy = \DB::raw("DATE_FORMAT(`date`, '%Y-%m') as date");
                $groupBy = \DB::raw("DATE_FORMAT(`date`, '%Y-%m')");
            }

            $builder = \DB::table('coverage')
                ->select($selectBy, \DB::raw('ROUND(AVG(impact_index), 2) as monthly_impact_index'))
                ->whereNotNull('coverage.impact_index')
                ->groupBy($groupBy)
                ->orderBy('date');

            $result = $this->builderFilters($builder)->get();
            $newsbreaks = $this->getTopDynamicNewsbreak($selectBy, $groupBy, self::DYNAMIC_BY_IMPACT_INDEX);
            foreach ($result as $index => $impactDinamic) {
                $result[$index]->total = !empty($newsbreaks[$impactDinamic->date]) ? $newsbreaks[$impactDinamic->date]->total : 0;
                $result[$index]->name = !empty($newsbreaks[$impactDinamic->date]) ? $newsbreaks[$impactDinamic->date]->name : null;
                $result[$index]->net_effect = !empty($newsbreaks[$impactDinamic->date]) ? $newsbreaks[$impactDinamic->date]->net_effect : null;
                $result[$index]->date = strtotime($impactDinamic->date) * 1000;
            }
            return $result;
        }

        /**
         * @return array
         */
        public
        function getReachDynamic()
        {
            $this->validateFilter();
            if ($this->weekly) {
                $selectBy = \DB::raw("str_to_date(concat(yearweek(`date`, 1), ' monday'), '%X%V %W') as date");
                $groupBy = \DB::raw("yearweek(`date`, 1)");
            } else {
                $selectBy = \DB::raw("DATE_FORMAT(`date`, '%Y-%m') as date");
                $groupBy = \DB::raw("DATE_FORMAT(`date`, '%Y-%m')");
            }

            $builder = \DB::table('coverage')
                ->select($selectBy, \DB::raw('ROUND(SUM(reach), 2) as monthly_reach'))
                ->whereNotNull('coverage.reach')
                ->groupBy($groupBy)
                ->orderBy('date');

            $result = $this->builderFilters($builder)->get();
            $newsbreaks = $this->getTopDynamicNewsbreak($selectBy, $groupBy, self::DYNAMIC_BY_REACH);
            foreach ($result as $index => $reachDinamic) {
                $result[$index]->total = !empty($newsbreaks[$reachDinamic->date]) ? $newsbreaks[$reachDinamic->date]->total : 0;
                $result[$index]->name = !empty($newsbreaks[$reachDinamic->date]) ? $newsbreaks[$reachDinamic->date]->name : null;
                $result[$index]->net_effect = !empty($newsbreaks[$reachDinamic->date]) ? $newsbreaks[$reachDinamic->date]->net_effect : null;
                $result[$index]->date = strtotime($reachDinamic->date) * 1000;
            }
            return $result;
        }

        /**
         * @return array
         */
        public
        function getNetDynamic()
        {
            $this->validateFilter();
            if ($this->weekly) {
                $selectBy = \DB::raw("str_to_date(concat(yearweek(`date`, 1), ' monday'), '%X%V %W') as date");
                $groupBy = \DB::raw("yearweek(`date`, 1)");
            } else {
                $selectBy = \DB::raw("DATE_FORMAT(`date`, '%Y-%m') as date");
                $groupBy = \DB::raw("DATE_FORMAT(`date`, '%Y-%m')");
            }

            $builder = \DB::table('coverage')
                ->select($selectBy, \DB::raw('ROUND(SUM(net_effect), 2) as monthly_net'))
                ->whereNotNull('coverage.net_effect')
                ->groupBy($groupBy)
                ->orderBy('date');

            $result = $this->builderFilters($builder)->get();
            $newsbreaks = $this->getTopDynamicNewsbreak($selectBy, $groupBy, self::DYNAMIC_BY_NET_EFFECT);
            foreach ($result as $index => $netDinamic) {
                $result[$index]->total = !empty($newsbreaks[$netDinamic->date]) ? $newsbreaks[$netDinamic->date]->total : 0;
                $result[$index]->name = !empty($newsbreaks[$netDinamic->date]) ? $newsbreaks[$netDinamic->date]->name : null;
                $result[$index]->net_effect = !empty($newsbreaks[$netDinamic->date]) ? $newsbreaks[$netDinamic->date]->net_effect : null;
                $result[$index]->date = strtotime($netDinamic->date) * 1000;
            }

            return $result;
        }

        /**
         * @return array
         */
        public
        function getTopStories()
        {
            $builder = \DB::table('coverage')
                ->select(
                    "coverage.headline",
                    "countries.name as country",
                    "coverage.date",
                    "coverage.url",
                    \DB::raw('ROUND(coverage.impact_index, 2) as impact_index'),
                    "coverage.net_effect"
                )
                ->join('countries', 'countries.id', '=', 'coverage.country_id')
                ->where('coverage.impact_index', '>=', self::TOP_STORIE_MIN_IMPACT_INDEX)
                ->whereNotNull('coverage.name')
                ->orderBy('coverage.impact_index', 'desc')
                ->orderBy('coverage.net_effect', 'desc')
                ->orderBy('coverage.name');

            $result = $this->builderFilters($builder)->get();
            $top = [];
            $tmpImpactIndex = -1;
            foreach ($result as $coverage) {
                if ($tmpImpactIndex == $coverage->impact_index) {
                    continue;
                }
                $coverage->date = date("Y-m-d", strtotime($coverage->date));
                $top[] = $coverage;
                $tmpImpactIndex = $coverage->impact_index;
            }
            return $top;
        }

        /**
         * @return array
         */
        public
        function getIndexesComparison()
        {
            $regionId = Input::get('region_id');
            $subregionId = Input::get('subregion_id');
            $countryId = Input::get('country_id');
            
            $builder = \DB::table('coverage');

            if (empty($regionId) && empty($subregionId) && empty($countryId)) {
                $groupBy = 'regions.id';
                $builder->select(
                    "coverage.region_id",
                    "regions.name",
                    \DB::raw('sum(coverage.reach) as media_outreach'),
                    \DB::raw('ROUND(AVG(coverage.impact_index), 2) as impact_index'),
                    \DB::raw('ROUND(sum(coverage.net_effect), 2) as net_effect')
                )
                ->join('regions', 'regions.id', '=', 'coverage.region_id');
            } else {
                $groupBy = 'countries.id';
                $builder->select(
                    "coverage.country_id",
                    "countries.name",
                    \DB::raw('sum(coverage.reach) as media_outreach'),
                    \DB::raw('ROUND(AVG(coverage.impact_index), 2) as impact_index'),
                    \DB::raw('ROUND(sum(coverage.net_effect), 2) as net_effect')
                )
                ->join('countries', 'countries.id', '=', 'coverage.country_id');
                if (!empty($subregionId) && empty($countryId)) {
                    $builder->where('countries.subregion_id', '=', $subregionId);
                }
            }

            $builder->groupBy($groupBy);

            return $this->builderFilters($builder)->get();
        }

        public
        function getSubcategories()
        {
            $filter_by = Input::get('filter_by', self::FILTER_BY_NET_EFFECT);
            $select_total_by = \DB::raw('SUM(coverage.net_effect) as coverage_cnt');
            if ($filter_by == self::FILTER_BY_NUMBERS) {
                $select_total_by = \DB::raw('count(distinct(coverage.id)) coverage_cnt');
            }


            $builder = \DB::table('coverage_subcategory as t')
                ->select(
                    'subcategory.id',
                    'subcategory.title',
                    \DB::raw($select_total_by)
                )
                ->leftJoin('subcategory', 'subcategory.id', '=', 't.subcategory_id')
                ->leftJoin('coverage', 'coverage.id', '=', 't.coverage_id')
                ->groupBy('t.subcategory_id');

            $mediaSubCategory = Input::get('media_subcategory');
            if (!empty($mediaSubCategory)) {
                $builder->where('t.subcategory_id', '=', $mediaSubCategory);
            }

            return $this->builderFilters($builder)->get();
        }

        public
        function prepareMediaSubcategories($alias, $baseBuilder)
        {
            $builder = clone $baseBuilder;

            if ($alias == 'r') {
                $builder->select(
                    'subcategory.id',
                    'subcategory.title',
                    'regions.id as region_id',
                    \DB::raw('count(t.coverage_id) as count')
                )
                ->leftJoin('coverage_subcategory AS t', 't.coverage_id', '=', 'coverage.id')
                ->leftJoin('subcategory', 'subcategory.id', '=', 't.subcategory_id')
                ->leftJoin('regions', 'regions.id', '=', 'coverage.region_id')
                ->groupBy('regions.id', 't.subcategory_id');
            } elseif ($alias == 's') {
                $builder->select(
                    'subcategory.id',
                    'subcategory.title',
                    'subregions.id as subregion_id',
                    \DB::raw('count(t.coverage_id) as count')
                )
                ->leftJoin('coverage_subcategory AS t', 't.coverage_id', '=', 'coverage.id')
                ->leftJoin('subcategory', 'subcategory.id', '=', 't.subcategory_id')
                ->leftJoin('countries', 'countries.id', '=', 'coverage.country_id')
                ->leftJoin('subregions', 'subregions.id', '=', 'countries.subregion_id')
                ->groupBy('subregions.id', 't.subcategory_id');
            } elseif ($alias == 'c') {
                $builder->select(
                    'subcategory.id',
                    'subcategory.title',
                    'countries.id as country_id',
                    \DB::raw('count(t.coverage_id) as count')
                )
                ->leftJoin('coverage_subcategory AS t', 't.coverage_id', '=', 'coverage.id')
                ->leftJoin('subcategory', 'subcategory.id', '=', 't.subcategory_id')
                ->leftJoin('countries', 'countries.id', '=', 'coverage.country_id')
                ->groupBy('countries.id', 't.subcategory_id');
            }

            $mediaSubCategory = Input::get('media_subcategory');
            if (!empty($mediaSubCategory)) {
                $builder->where('t.subcategory_id', '=', $mediaSubCategory);
            }

            return $this->builderFilters($builder)->get();
        }

        /**
         * Fetching comparison campaigns, newsbreaks, products, speakers and events
         *
         * @return array|static[]
         */
        public
        function getComparison()
        {
            $result = [
                'articles' => [],
                'media_outreach' => [],
                'labels' => [['name' => 'Total', 'id' => 'total']]
            ];
            $regionId = Input::get('region_id');
            $subregionId = Input::get('subregion_id');
            $countryId = Input::get('country_id');
            $businessId = Input::get('business');
            $mediaList = Input::get('media_list');
            $mediaCategory = Input::get('media_category');
            $mediaSubcategory = Input::get('media_subcategory');

            $grouping = self::getComparisonParams();

            $baseBuilder = Coverage::where('coverage.status', '=', Coverage::STATUS_APPROVED)
                ->where('coverage.date', '>=', $this->range['start'])
                ->where('coverage.date', '<', $this->range['end']);

            if (!empty($regionId)) {
                $baseBuilder->where('coverage.region_id', '=', (int)$regionId);
            }
            if (!empty($subregionId)) {
                $baseBuilder->join('countries', 'countries.id', '=', 'coverage.country_id')
                    ->join('subregions', 'subregions.id', '=', 'countries.subregion_id')
                    ->where('subregions.id', '=', (int)$subregionId);
            }
            if (!empty($countryId)) {
                $baseBuilder->where('coverage.country_id', '=', (int)$countryId);
            }
            if (!empty($mediaList)) {
                $baseBuilder->where('coverage.media_list', '=', $mediaList);
            }
            if (!empty($mediaCategory)) {
                $baseBuilder->where('coverage.media_category', '=', $mediaCategory);
            }
            if (!empty($mediaSubcategory)) {
                $baseBuilder = $baseBuilder->join('coverage_subcategory', 'coverage.id', '=', 'coverage_subcategory.coverage_id')
                    ->where('coverage_subcategory.coverage_id', '=', $mediaSubcategory);
            }
            if (!empty($businessId)) {
                $baseBuilder = $baseBuilder->join('business_coverage', 'coverage.id', '=', 'business_coverage.coverage_id')
                    ->where('business_coverage.business_id', '=', (int)$businessId);
            }

            // articles, media_outreach, net_effect
            $data = [];
            foreach ($grouping as $group) {
                if (empty($group['list'])) {
                    continue;
                }
                $builder = clone $baseBuilder;

                if (!empty($group['join_field'])) {
                    $builder
                        ->select(
                            "{$group['alias']}.name",
                            "{$group['alias']}.id",
                            \DB::raw('count(coverage.id) as count'),
                            \DB::raw('sum(coverage.reach) as reach'),
                            \DB::raw('sum(coverage.net_effect) as net_effect'),
                            \DB::raw('avg(coverage.impact_index) as impact_index')
                        )
                        ->join("{$group['table']}_coverage as {$group['alias']}_c", "{$group['alias']}_c.coverage_id", '=', 'coverage.id')
                        ->join("{$group['table']} as {$group['alias']}", "{$group['alias']}.id", '=', "{$group['alias']}_c.{$group['join_field']}")
                        ->whereIn("{$group['alias']}.id", $group['list'])
                        ->groupBy("{$group['alias']}.id");
                } else {
                    $builder
                        ->select(
                            "{$group['alias']}.{$group['field']} as name",
                            "{$group['alias']}.{$group['field']} as id",
                            \DB::raw('count(coverage.id) as count'),
                            \DB::raw('sum(coverage.reach) as reach'),
                            \DB::raw('sum(coverage.net_effect) as net_effect'),
                            \DB::raw('avg(coverage.impact_index) as impact_index')
                        )
                        ->whereIn("{$group['alias']}.{$group['field']}", $group['list'])
                        ->whereRaw('LENGTH(event_name) > 0')
                        ->groupBy("{$group['alias']}.{$group['field']}");
                }

                $data[$group['table']] = $builder->get()->toArray();
            }

            $total = [
                'articles' => 0,
                'media_outreach' => 0,
                'net_effect' => 0,
            ];
            foreach ($data as $group => $groupValues) {
                foreach ($groupValues as $value) {
                    $id = $grouping[$group]['seqnum'] . "_" . str_replace(" ", "_", $value['id']);
                    $result['articles'][$id] = $value['count'];

                    $result['labels'][] = [
                        'id' => $id,
                        'name' => $value['name'],
                        'filter_by' => $group,
                        'filter_id' => $value['id']
                    ];

                    $result['media_outreach'][$id] = $value['reach'] >= 1000000 ? round($value['reach'] / 1000000) . 'M' : '< 1M';
                    $result['impact_index'][$id] = round($value['impact_index'], 2);
                    $result['net_effect'][$id] = $value['net_effect'] >= 1000000 ? round($value['net_effect'] / 1000000) . 'M' : '< 1M';
                }
            }

            if (empty($data)) {
                return $data;
            }
            $builder = clone $baseBuilder;
            $builder->select(\DB::raw('COUNT(coverage.id) as articles, SUM(coverage.reach) as media_outreach, SUM(coverage.net_effect) as net_effect'));

            $builder->where(function ($query) use ($grouping) {
                foreach ($grouping as $group) {
                    if (empty($group['list'])) {
                        continue;
                    }
                    if (!empty($group['join_field'])) {
                        $query = $query->whereExists(function ($q) use ($group) {
                            $q->select(\DB::raw(1))
                                ->from("{$group['table']}_coverage as {$group['alias']}_c")
                                ->whereRaw("{$group['alias']}_c.coverage_id=coverage.id")
                                ->whereIn("{$group['alias']}_c.{$group['join_field']}", $group['list']);
                        }, 'or');
                    } else {
                        $query = $query->whereIn("coverage.{$group['field']}", $group['list'], 'or');
                    }
                }
            });

            $data = $builder->get()->first();

            $result['impact_index']['total'] = array_sum($result['impact_index']);
            $result['articles']['total'] = $data->articles;
            $result['media_outreach']['total'] = $data->media_outreach >= 1000000 ? round($data->media_outreach / 1000000) . 'M' : '< 1M';
            $result['net_effect']['total'] = $data->net_effect >= 1000000 ? round($data->net_effect / 1000000) . 'M' : '< 1M';

            // media_category, media_list, main_tonality
            $mediaList = [
                'media_category' => [],
                'media_list' => Coverage::getMediaList(),
                'main_tonality' => Coverage::getMainTonalityes(),
            ];

            foreach ($mediaList as $media => $value) {
                foreach ($grouping as $key => $group) {
                    $builder = clone $baseBuilder;

                    if (!empty($group['join_field'])) {
                        $builder
                            ->select(
                                "{$group['alias']}.name",
                                "{$group['alias']}.id",
                                \DB::raw('count(coverage.id) as count'),
                                "coverage.{$media} as media"
                            )
                            ->join("{$group['table']}_coverage as {$group['alias']}_c", "{$group['alias']}_c.coverage_id", '=', 'coverage.id')
                            ->join("{$group['table']} as {$group['alias']}", "{$group['alias']}.id", '=', "{$group['alias']}_c.{$group['join_field']}")
                            ->whereNotNull("coverage.{$media}")
                            ->whereIn("{$group['alias']}.id", $group['list'])
                            ->groupBy("{$group['alias']}.id", "coverage.{$media}");
                    } else {
                        $builder
                            ->select(
                                "{$group['alias']}.{$group['field']} as name",
                                "{$group['alias']}.{$group['field']} as id",
                                \DB::raw('count(coverage.id) as count'),
                                "coverage.{$media} as media"
                            )
                            ->whereNotNull("coverage.{$media}")
                            ->whereIn("{$group['alias']}.{$group['field']}", $group['list'])
                            ->groupBy("{$group['alias']}.{$group['field']}", "coverage.{$media}");
                    }

                    $data = $builder->get();

                    foreach ($data as $stats) {
                        $id = $grouping[$key]['seqnum'] . "_" . str_replace(" ", "_", $stats->id);

                        if (empty($stats->media) || (!empty($value) && empty($value[$stats->media]))) {
                            continue;
                        }
                        $valueKey = !empty($value) ? $value[$stats->media] : strtolower($stats->media);
                        $result[$media][$id][$valueKey] = $stats->count;
                    }
                }

                $builder = clone $baseBuilder;
                $builder->select(\DB::raw("coverage.$media as media, COUNT(coverage.id) as count"))
                    ->leftJoin("campaigns_coverage as c_c", "c_c.coverage_id", '=', 'coverage.id')
                    ->leftJoin("newsbreaks_coverage as n_c", "n_c.coverage_id", '=', 'coverage.id')
                    ->leftJoin("products_coverage as p_c", "p_c.coverage_id", '=', 'coverage.id')
                    ->leftJoin("speakers_coverage as s_c", "s_c.coverage_id", '=', 'coverage.id')
                    ->where(function ($query) use ($grouping) {
                        $query->whereIn("c_c.campaign_id", $grouping['campaigns']['list'], 'or')
                            ->whereIn("n_c.newsbreak_id", $grouping['newsbreaks']['list'], 'or')
                            ->whereIn("p_c.product_id", $grouping['products']['list'], 'or')
                            ->whereIn("s_c.speaker_id", $grouping['speakers']['list'], 'or')
                            ->whereIn("coverage.event_name", $grouping['events']['list'], 'or');
                    })
                    ->whereNotNull("coverage.{$media}")
                    ->groupBy("coverage.{$media}");
                $data = $builder->get();

                foreach ($data as $stats) {
                    $valueKey = !empty($value) && !empty($value[$stats->media]) ? $value[$stats->media] : strtolower($stats->media);
                    if (!isset($result[$media]['total'][$valueKey])) {
                        $result[$media]['total'][$valueKey] = 0;
                    }
                    $result[$media]['total'][$valueKey] = $stats->count;
                }
                
            }

            // media_subcategories
            foreach ($grouping as $key => $group) {

                $builder = clone $baseBuilder;

                if (!empty($group['join_field'])) {
                    $builder
                        ->select(
                            "{$group['alias']}.name",
                            "{$group['alias']}.id",
                            \DB::raw('count(coverage.id) as count'),
                            "subcat.title as subcategory"
                        )
                        ->join("{$group['table']}_coverage as {$group['alias']}_c", "{$group['alias']}_c.coverage_id", '=', 'coverage.id')
                        ->join("{$group['table']} as {$group['alias']}", "{$group['alias']}.id", '=', "{$group['alias']}_c.{$group['join_field']}")
                        ->join("coverage_subcategory as c_subcat", "c_subcat.coverage_id", '=', 'coverage.id')
                        ->join("subcategory as subcat", "subcat.id", '=', "c_subcat.subcategory_id")
                        ->whereIn("{$group['alias']}.id", $group['list'])
                        ->groupBy("{$group['alias']}.id", "subcat.title");
                } else {
                    $builder
                        ->select(
                            "{$group['alias']}.{$group['field']} as name",
                            "{$group['alias']}.{$group['field']} as id",
                            \DB::raw('count(coverage.id) as count'),
                            "subcat.title as subcategory"
                        )
                        ->join("coverage_subcategory as c_subcat", "c_subcat.coverage_id", '=', 'coverage.id')
                        ->join("subcategory as subcat", "subcat.id", '=', "c_subcat.subcategory_id")
                        ->whereIn("{$group['alias']}.{$group['field']}", $group['list'])
                        ->groupBy("{$group['alias']}.{$group['field']}", "subcat.title");
                }

                $data = $builder->get();

                foreach ($data as $stats) {
                    $id = $grouping[$key]['seqnum'] . "_" . str_replace(" ", "_", $stats->id);
                    if (isset($stats->subcategory)) {
                        $valueKey = strtolower($stats->subcategory);
                        $result['media_subcategory'][$id][$valueKey] = $stats->count;
                    }
                }

                $builder = clone $baseBuilder;

                $builder->select(\DB::raw("subcat.title as subcategory, COUNT(coverage.id) as count"))
                    ->join("coverage_subcategory as c_subcat", "c_subcat.coverage_id", '=', 'coverage.id')
                    ->join("subcategory as subcat", "subcat.id", '=', "c_subcat.subcategory_id")
                    ->leftJoin("campaigns_coverage as c_c", "c_c.coverage_id", '=', 'coverage.id')
                    ->leftJoin("newsbreaks_coverage as n_c", "n_c.coverage_id", '=', 'coverage.id')
                    ->leftJoin("products_coverage as p_c", "p_c.coverage_id", '=', 'coverage.id')
                    ->leftJoin("speakers_coverage as s_c", "s_c.coverage_id", '=', 'coverage.id')
                    ->where(function ($query) use ($grouping) {
                        $query->whereIn("c_c.campaign_id", $grouping['campaigns']['list'], 'or')
                            ->whereIn("n_c.newsbreak_id", $grouping['newsbreaks']['list'], 'or')
                            ->whereIn("p_c.product_id", $grouping['products']['list'], 'or')
                            ->whereIn("s_c.speaker_id", $grouping['speakers']['list'], 'or')
                            ->whereIn("coverage.event_name", $grouping['events']['list'], 'or');
                    })->groupBy("subcat.title");

                $data = $builder->get();

                foreach ($data as $stats) {
                    $valueKey = strtolower($stats->subcategory);
                    if (!isset($result['media_subcategory']['total'][$valueKey])) {
                      $result['media_subcategory']['total'][$valueKey] = 0;
                    }
                    $result['media_subcategory']['total'][$valueKey] = $stats->count;
                }
            }

            return $result;
        }

        /**
         * @param $name
         *
         * @return mixed
         */
        public
        function export($name)
        {
            $fileName = Input::get("filename");
            $name = $this->parseMethod($name);
            $getMethodName = 'get' . $name;
            $prepareMethodName = 'prepare' . $name . 'ToExport';
            if (method_exists($this, $getMethodName) && is_callable('Coverage', $prepareMethodName)) {
                $data = $this->$getMethodName();
                $exportData = Coverage::$prepareMethodName($data);
                Excel::create($fileName, function ($excel) use ($exportData, $name) {
                    $excel->sheet($name, function ($sheet) use ($exportData) {
                        $sheet->setAutoSize(true);
                        $sheet->setFontSize(10);
                        $sheet->fromArray($exportData, null, 'A1', true, false);
                    });
                })->export('xls');
            }
            App::abort(404);
        }

        /**
         * @param $name
         *
         * @return string
         */
        protected
        function parseMethod($name)
        {
            $parts = explode('-', $name);
            $result = '';
            foreach ($parts as $part) {
                $result = $result . ucfirst($part);
            }
            return $result;
        }

        public function getComparisonTopFilter()
        {
            $params = [];
            $result = [];
            $filter = Input::get('filter');

            $params['limit'] = 10;
            if (Input::has('date_start') && Input::has('date_end')) {
                $params['date']['start'] = Input::get('date_start');
                $params['date']['end'] = Input::get('date_end');
            }

            switch ($filter) {
                case 'newsbreaks':
                    $result = ArrayHelper::getFieldListFromArray(NewsBreak::getTop($params));
                    break;
                case 'campaigns':
                    $result = ArrayHelper::getFieldListFromArray(Campaign::getTop($params));
                    break;
                case 'products':
                    $result = ArrayHelper::getFieldListFromArray(Product::getTop($params));
                    break;
                case 'speakers':
                    $result = ArrayHelper::getFieldListFromArray(Coverage::getTopSpeakers($params));
                    break;
                case 'events':
                    $result = ArrayHelper::getFieldListFromArray(Coverage::getTopEvents($params), 'name');
                    break;
            }

            return $result;
        }
    }