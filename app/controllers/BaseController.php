<?php
namespace App\Controllers;

use View;

/**
 * Class BaseController
 */
class BaseController extends \Controller
{

    const SEMANTIC_UI_VERSION_OLD = 'old';
    const SEMANTIC_UI_VERSION_NEW = 'new';

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

}