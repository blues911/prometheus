<?php
namespace App\Controllers;

use App\Models\Campaign;
use App\Models\User;
use Response;
use Input;
use App\Models;
use Auth;
use View;

/**
 * Class CampaignController
 */
class CampaignController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        $name   = Input::get('name');
        $status = Input::get('status');
        if (empty($name) || empty($status) || !in_array($status, [Campaign::STATUS_ACTIVE, Campaign::STATUS_ARCHIVED])) {
            Response::json(['error' => false]);
        }

        $campaign         = Campaign::findOrNew(Input::get('id', 0));
        $campaign->name   = $name;
        $campaign->status = $status;
        $campaign->save();

        return Response::json($campaign->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        $campaign = Campaign::find($id);
        if (empty($campaign)) {
            return Response::json(['error' => true]);
        }
        $campaign->status = Campaign::STATUS_ACTIVE;
        $campaign->update(['status']);

        return Response::json(['error' => false]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        Campaign::destroy($id);
        return Response::json(['error' => false]);
    }

    /**
     * Restore the specified resource.
     *
     * @param  int $id
     * @return Response
     */

    public function restore($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        try {
            $campaign = Campaign::onlyTrashed()->find($id);
            if ($campaign) {
                $campaign->deleted_at = NULL;
                $campaign->save();
            }
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }

        return Response::json(['error' => false]);
    }

    /**
     * get Campaigns
     *
     * @return mixed
     */
    public function getCampaigns()
    {
        $onlyDeleted = filter_var(Input::get('only_deleted'), FILTER_VALIDATE_BOOLEAN);
        $campaigns = $onlyDeleted  ? Campaign::onlyTrashed()->orderBy('name') : Campaign::orderBy('name');
        $view = $onlyDeleted ? 'admin.lists.deleted._campaigns_list' : 'admin.lists._campaigns_list';

        return View::make($view, [
            'campaigns' => $campaigns->get(),
        ])->render();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @return Response
     */
    public function destroySomeRecords()
    {
        $ids = explode(',', Input::get('ids'));
        
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        Campaign::destroy($ids);
        
        return Response::json(['error' => false]);
    }

}