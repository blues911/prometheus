<?php
namespace App\Controllers;

use App\Models\Event;
use App\Models\User;
use Response;
use Input;
use App\Models;
use Auth;
use View;

/**
 * Class EventController
 */
class EventController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        $name   = Input::get('name');
        $status = Input::get('status');
        if (empty($name) || empty($status) || !in_array($status, [Event::STATUS_ACTIVE, Event::STATUS_ARCHIVED])) {
            Response::json(['error' => false]);
        }

        $event         = Event::findOrNew(Input::get('id', 0));
        $event->name   = $name;
        $event->status = $status;
        $event->save();

        return Response::json($event->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        $event = Event::find($id);
        if (empty($event)) {
            return Response::json(['error' => true]);
        }
        $event->status = Event::STATUS_ACTIVE;
        $event->update(['status']);

        return Response::json(['error' => false]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        Event::destroy($id);
        return Response::json(['error' => false]);
    }

    /**
     * Restore the specified resource.
     *
     * @param  int $id
     * @return Response
     */

    public function restore($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        try {
            $event= Event::onlyTrashed()->find($id);
            if ($event) {
                $event->deleted_at = NULL;
                $event->save();
            }
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }

        return Response::json(['error' => false]);
    }

    /**
     * get Events
     *
     * @return mixed
     */
    public function getEvents()
    {
        $onlyDeleted = filter_var(Input::get('only_deleted'), FILTER_VALIDATE_BOOLEAN);
        $events = $onlyDeleted  ? Event::onlyTrashed()->orderBy('name') : Event::orderBy('name');
        $view = $onlyDeleted ? 'admin.lists.deleted._events_list' : 'admin.lists._events_list';

        return View::make($view, [
            'events' => $events->get(),
        ])->render();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @return Response
     */
    public function destroySomeRecords()
    {
        $ids = explode(',', Input::get('ids'));
        
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        Event::destroy($ids);
        
        return Response::json(['error' => false]);
    }

}