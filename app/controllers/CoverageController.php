<?php

namespace App\Controllers;

use App\Models\BaseValidator;
use App\Models\Country;
use App\Models\Region;
use App\Models\SystemUser;
use App\Models\Event;
use App\Models\NewsBreak;
use App\Models\Campaign;
use App\Models\Speaker;
use App\Models\Coverage;
use App\Models\Business;
use App\Models\Media;
use App\Models\User;
use App\Models\Product;
use App\Models\Subcategory;
use Response;
use Redirect;
use Request;
use View;
use Auth;
use Input;
use File;
use Config;
use Exception;
use Cookie;

/**
 * Class CoverageController
 */
class CoverageController extends BaseController
{
    protected $monthsList = [
        '01' => 'january',
        '02' => 'february',
        '03' => 'march',
        '04' => 'april',
        '05' => 'may',
        '06' => 'june',
        '07' => 'july',
        '08' => 'august',
        '09' => 'september',
        '10' => 'october',
        '11' => 'november',
        '12' => 'december',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (Request::ajax()) {
            $filter = [];
            $sort = [];
            $search = Input::has('search') ? Input::get('search.value') : '';
            $regionId = (int)Input::get('region_id');
            $userId = (int)Input::get('user_id');
            $dateFrom = Input::get('start_range', null);
            $dateTo = Input::get('end_range', null);
            $sort['direction'] = Input::get('order.0.dir');
            $sort['column'] = Input::get('columns')[Input::get('order.0.column')]['data'];
            $otherFilter = json_decode(Input::get('filter', "[]"), true);
            $subcategories = trim(Input::get('media_subcategory'),'[]');

            if (!empty($otherFilter)) {
                $filter['status'] = Coverage::STATUS_APPROVED;
            }
            if (is_array($otherFilter)) {
                $filter = array_merge($filter, $otherFilter);
            }
            if (!empty($filter['file_id']) && $filter['status'] == Coverage::STATUS_APPROVED) {
                unset($filter['status']);
            }
            if ($regionId) {
                $filter['region_id'] = $regionId;
            }
            if ($userId) {
                $filter['owner_id'] = $userId;
            }
            if (!empty($dateFrom)) {
                $filter['from_date'] = date("Y-m-d", strtotime($dateFrom));
            }
            if (!empty($dateTo)) {
                $filter['to_date'] = date("Y-m-d", strtotime($dateTo . "+ 1 day"));
            }
            if (!empty($subcategories)) {
                $filter['media_subcategory'] = $subcategories;
            }


            $list = Coverage::getListForView(Input::get('start'), Input::get('length'), $filter, $sort, $search);
            $total = Coverage::count();
            if (count($filter)) {
                $recordsFiltered = count(Coverage::getList(0, 0, $filter, $sort, $search, false, true));
            } else {
                $recordsFiltered = $total;
            }
            return Response::json(
                [
                    "recordsTotal" => $total,
                    "recordsFiltered" => $recordsFiltered,
                    "recordsForDelete" => Coverage::getCountCoverageForDelete($filter, $search),
                    "data" => $list,
                    'debug' => [
                        'skip' => Input::get('start'),
                        'limit' => Input::get('length'),
                        'request' => Input::all(),
                        'regionId' => $regionId,
                        'sort' => $sort
                    ]
                ]
            );
        }

        $filter = Input::all();
        $isFiltered = !empty($filter);
        if ($isFiltered && is_array($filter)) {
            $filter['status'] = Coverage::STATUS_APPROVED;
        }
        unset($filter["start"], $filter["end"], $filter["region_id"]);
        $defaultDateFrom = date("Y-m-d", strtotime('first day of this month'));
        $defaultDateTo = date("Y-m-d", strtotime('last day of this month'));
        return View::make('root', [
            'jsOnly' => true,
            'semanticVersion' => self::SEMANTIC_UI_VERSION_NEW
        ])
            ->nest('template', 'coverage-table', [
                'regions' => Region::all()->sortBy('name'),
                'users' => SystemUser::all()->sortBy('name'),
                'dateFrom' => $isFiltered || Input::has("start") ? Input::get("start", null) : $defaultDateFrom,
                'dateTo' => $isFiltered || Input::has("end") ? Input::get("end", null) : $defaultDateTo,
                'defaultDateFrom' => $defaultDateFrom,
                'defaultDateTo' => $defaultDateTo,
                'regionId' => Input::get("region_id", 0),
                'filter' => $filter,
                'mode' => Cookie::get('coverage-mode', Coverage::MODE_COMPACT),
                'codingClosedMonths' => $this->prepareCodingClosedMonths()
            ])
            ->nest('customAssets', 'assets/coverage-table');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $date = date('Y-m');
        $closedMonths = $this->prepareCodingClosedMonths();
        if (Auth::user()->role != User::ROLE_ADMINISTRATOR && in_array($date, $closedMonths)) {
            return Redirect::to('/');
        }

        /** @var $user \App\Models\SystemUser */
        $user = SystemUser::find(Auth::user()->id);
        $coverage = new Coverage;
        $coverage->status = Coverage::STATUS_DRAFT;
        // $coverage->date = date('Y-m-d H:i:s');

        if (empty($user->region_id) || empty($user->country_id)) {
            return Redirect::to('/');
        }

        if ($user->region) {
            $coverage->region()->associate($user->region);
        }

        if ($user->country) {
            $coverage->country()->associate($user->country);
        }

        $coverage->owner()->associate($user->getModel());
        $coverage->save();

        return View::make('root', [
            'jsOnly' => true,
            'semanticVersion' => self::SEMANTIC_UI_VERSION_NEW
        ])->nest(
            'template',
            'add-data',
            [
                'coverage' => $coverage,
                'attachedProducts' => [],
                'attachedCampaigns' => [],
                'attachedBusiness' => [],
                'newsBreaks' => [],
                'speakersQuoted' => [],
                'access_complete' => Coverage::access(Coverage::ACCESS_COMPLETE_COVERAGE, $coverage),
                'countries' => Country::where('region_id', '=', $user->region->id)->get()->sortBy('name'),
                'new' => true,
                'eventData' => Event::getList(),
                'newsData' => NewsBreak::getList(),
                'campaigns' => Campaign::getActiveCampaigns(),
                'business' => Business::getActiveBusiness(),
                'speakersData' => Speaker::getList(),
                'access_edit' => true,
                'media' => Media::where('region_id', '=', $user->region->id)
                    ->where('country_id', '=', $user->country->id)
                    ->orderBy('name')
                    ->get(),
                'subcategories' => Subcategory::orderBy('title')->get(),
                'codingClosedMonths' => $this->prepareCodingClosedMonths()
            ]
        )->nest('customAssets', 'assets/add-data');
    }

    /**
     * Approve coverage
     *
     * @return Response
     */
    public function approve()
    {
        $coverage = Coverage::find(Input::get("id", null));
        if (empty($coverage)) {
            return Response::json(["error" => true, 'Not found']);
        }
        if (!Coverage::access(Coverage::ACCESS_PUBLISH_COVERAGE, $coverage)) {
            return Response::json(["error" => true, 'Access denied']);
        }
        return Response::json(["error" => !$coverage->approve()]);
    }

    /**
     * Return to draft coverage
     *
     * @return Response
     */
    public function returnToDraft()
    {
        $coverage = Coverage::find(Input::get("id", null));
        if (empty($coverage)) {
            return Response::json(["error" => true, 'Not found']);
        }
        if (!Coverage::access(Coverage::ACCESS_RETURN_TO_DRAFT, $coverage)) {
            return Response::json(["error" => true, 'Access denied']);
        }
        return Response::json(["error" => !$coverage->toDraft()]);
    }

    /**
     * Approve coverage
     *
     * @return Response
     */
    public function approveAll()
    {
        if (Auth::user()->role != User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        $filter = [];
        $regionId = Input::get("region", null);
        $userId = Input::get("user", null);
        $dateFrom = Input::get('start_range', null);
        $dateTo = Input::get('end_range', null);
        $otherFilter = json_decode(Input::get('filter', "[]"), true);
        if (is_array($otherFilter)) {
            $filter = array_merge($filter, $otherFilter);
        }

        if ($regionId) {
            $filter['region_id'] = $regionId;
        }
        if ($userId) {
            $filter['owner_id'] = $userId;
        }
        if (!empty($dateFrom)) {
            $filter['from_date'] = date("Y-m-d", strtotime($dateFrom));
        }
        if (!empty($dateTo)) {
            $filter['to_date'] = date("Y-m-d", strtotime($dateTo . "+ 1 day"));
        }
        $filter['status'] = Coverage::STATUS_COMPLETE;

        return Response::json(["error" => !Coverage::approveByFilter($filter)]);
    }

    /**
     * Approve coverage
     *
     * @return Response
     */
    public function deleteAll()
    {
        $filter = [];
        $regionId = Input::get("region_id", null);
        $userId = Input::get("user_id", null);
        $dateFrom = Input::get('start_range', null);
        $dateTo = Input::get('end_range', null);
        $search = Input::has('search') ? Input::get('search') : '';
        $otherFilter = json_decode(Input::get('filter', "[]"), true);
        if (is_array($otherFilter)) {
            $filter = array_merge($filter, $otherFilter);
        }

        if ($regionId) {
            $filter['region_id'] = $regionId;
        }
        if ($userId) {
            $filter['owner_id'] = $userId;
        }
        if (!empty($dateFrom)) {
            $filter['from_date'] = date("Y-m-d", strtotime($dateFrom));
        }
        if (!empty($dateTo)) {
            $filter['to_date'] = date("Y-m-d", strtotime($dateTo . "+ 1 day"));
        }
        if (!empty($filter['file_id']) && $filter['status'] == Coverage::STATUS_APPROVED) {
            unset($filter['status']);
        }
        return Response::json(["error" => !Coverage::deleteByFilter($filter, $search)]);
    }

    /**
     * Complete coverage
     *
     * @return Response
     */
    public function complete()
    {
        $coverage = Coverage::find(Input::get("id", null));
        // get filters
        $params = Input::all();
        unset($params['id']);
        if (empty($coverage)) {
            return Response::json(["error" => true, 'Not found']);
        }
        if (!Coverage::access(Coverage::ACCESS_COMPLETE_COVERAGE, $coverage)) {
            return Response::json(["error" => true, 'Access denied']);
        }
        $result = $coverage->complete();
        if (Request::ajax()) {
            return Response::json(["error" => !$result]);
        }
        return Redirect::route('coverage.index', $params);
    }

    /**
     * Set coverage table mode
     *
     * @return Response
     */
    public function setTableMode()
    {
        $mode = Input::get("mode", Coverage::MODE_FULL);
        if (!in_array($mode, [Coverage::MODE_FULL, Coverage::MODE_COMPACT])) {
            $mode = Coverage::MODE_FULL;
        }
        $cookie = Cookie::forever('coverage-mode', $mode);
        return Response::json(["error" => false])->withCookie($cookie);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $coverage = new Coverage;
        $coverage->fill(Input::all());
        if ($coverage->validate(Input::all())) {
            $coverage->save();
            return Response::json(
                [
                    "error" => false,
                    "id" => $coverage->id,
                    'debug' => $coverage
                ]
            );
        } else {
            return Response::json(
                [
                    "error" => true,
                    "message" => "Validation errors",
                    "errors" => $coverage->errors(),
                    'debug' => Input::all()
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $coverage = Coverage::findOrFail($id);
        $result = $coverage->toArray();
        $result['error'] = false;
        return Response::json($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $user = Auth::getUser();
        $coverage = Coverage::findOrNew($id);
        if (!Coverage::access(Coverage::ACCESS_EDIT_COVERAGE, $coverage) && !Coverage::access(Coverage::ACCESS_VIEW_COVERAGE, $coverage)) {
            return Redirect::to('/');
        }

        // prepare available subcategories
        $subcategories = $coverage->subcategories;
        $activeSubcategories = [];
        if (!empty($subcategories)) {
            $ids = [];
            foreach ($subcategories as $subcategory) {
                $activeSubcategories['list'][] = [
                    'id' => $subcategory->id,
                    'title' => $subcategory->title
                ];
                $ids[] = $subcategory->id;
            }
            $activeSubcategories['ids'] = implode(',', $ids);
        }

        return View::make('root', [
            'jsOnly' => true,
            'semanticVersion' => self::SEMANTIC_UI_VERSION_NEW
        ])->nest(
            'template',
            'add-data',
            array_merge([
                'eventData' => Event::getList(),
                'newsData' => NewsBreak::getList(),
                'campaigns' => Campaign::getActiveCampaigns(),
                'business' => Business::getActiveBusiness(),
                'speakersData' => Speaker::getList(),
                'access_complete' => Coverage::access(Coverage::ACCESS_COMPLETE_COVERAGE, $coverage),
                'access_edit' => Coverage::access(Coverage::ACCESS_EDIT_COVERAGE, $coverage),
                'subcategories' => Subcategory::orderBy('title')->get(),
                'active_subcategories' => $activeSubcategories,
                'codingClosedMonths' => $this->prepareCodingClosedMonths()
            ], $coverage->getDataForEdit())
        )->nest('customAssets', 'assets/add-data');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        /** @var $coverage Coverage */

        $coverage = Coverage::find($id);
        $coverage->fill(Input::all());
        if ($coverage->validate(Input::all())) {

            if (Input::has('media_subcategories')) {
                if (Input::get('media_subcategories') == '0') {
                    $coverage->subcategories()->detach();
                } else {

                    $subcategories = explode(',',
                        trim(
                            Input::get('media_subcategories')
                            , '[]'
                        )
                    );
                     $coverage->subcategories()->sync($subcategories);
                }
            }

            if (Input::has('product_id') && Input::has('state')) {
                $productId = (int)Input::get('product_id');

                if ((bool)Input::get('state') && !$coverage->hasProduct($productId)) {
                    $coverage->products()->attach($productId);
                } elseif (!(bool)Input::get('state')) {
                    $coverage->products()->detach($productId);
                }
            }

            if (Input::has('products') || !(bool)Input::get('products_state', true)) {
                $coverage->products()->detach();
                foreach (Input::get('products', []) as $productId) {
                    $coverage->products()->attach($productId);
                }
            }

            if (Input::has('campaign_id') && Input::has('state')) {
                $campaignId = (int)Input::get('campaign_id');

                if ((bool)Input::get('state') && !$coverage->hasCampaign($campaignId)) {
                    $coverage->campaigns()->attach($campaignId);
                } elseif (!(bool)Input::get('state')) {
                    $coverage->campaigns()->detach($campaignId);
                }
            }

            if (Input::has('business_id') && Input::has('state')) {
                $businessId = (int)Input::get('business_id');

                if ((bool)Input::get('state') && !$coverage->hasBusiness($businessId)) {
                    $coverage->business()->attach($businessId);
                } elseif (!(bool)Input::get('state')) {
                    $coverage->business()->detach($businessId);
                }
            }

            if (Input::has('campaigns') || !(bool)Input::get('campaigns_state', true)) {
                $coverage->campaigns()->detach();
                foreach (Input::get('campaigns', []) as $campaignId) {
                    $coverage->campaigns()->attach($campaignId);
                }
            }

            if (Input::has('business') || !(bool)Input::get('business_state', true)) {
                $coverage->business()->detach();
                foreach (Input::get('business', []) as $businessId) {
                    $coverage->business()->attach($businessId);
                }
            }

            if (Input::has('news_break')) {
                $newsBreakesInput = json_decode(Input::get('news_break'), true);
                $newsBreakesInput = is_array($newsBreakesInput) ? $newsBreakesInput : [];
                $newsBreakesSave = [];
                // remove old data
                $coverageNewsbreaks = $coverage->newsbreaks()->withTrashed()->get();
                foreach ($coverageNewsbreaks as $newsbreak) {
                    $coverage->newsbreaks()->detach($newsbreak->newsbreak_id);
                }
                // prepare data for save
                foreach ($newsBreakesInput as $p) {
                    $newsbreak = NewsBreak::withTrashed()->where(['name' => trim($p)])->first();
                    if ($newsbreak && $newsbreak->deleted_at != NULL) {
                        $newsbreak->status = 'active';
                        $newsbreak->complete = 1;
                        $newsbreak->deleted_at = NULL;
                        $newsbreak->save();
                    } else if (!$newsbreak) {
                        $newsbreak = NewsBreak::create([
                            'name' => trim($p),
                            'status' => 'active',
                            'type' => 'global',
                            'complete' => 1
                        ]);
                    }
                    $newsBreakesSave[] = $newsbreak;
                }
                // save new data
                $coverage->newsbreaks()->saveMany($newsBreakesSave);
            }

            if (Input::has('speakers_quoted')) {
                $speakersInput = json_decode(Input::get('speakers_quoted'), true);
                $speakersInput = is_array($speakersInput) ? $speakersInput : [];
                $speakersSave = [];
                // remove old data
                $coverageSpeakers = $coverage->speakers()->get();
                foreach ($coverageSpeakers as $speaker) {
                    $coverage->speakers()->detach($speaker->speaker_id);
                }
                // prepare data for save
                foreach ($speakersInput as $p) {
                    $tmp = trim(strtolower($p));
                    if (in_array($tmp, BaseValidator::$rules[BaseValidator::NOT_IN_FOR_SPEAKERS]) || !$p ||
                        true !== Speaker::validate(['name' => trim($p)])) {
                        continue;
                    }
                    $speakersSave[] = Speaker::firstOrCreate(['name' => trim($p)]);
                }
                // save new data
                $coverage->speakers()->saveMany($speakersSave);
            }

            if (Input::has('speaker_quote')) {
                $coverage->speaker_quote = Input::get('speaker_quote_state');
            }

            if (Input::has('third_speakers_quotes')) {
                $coverage->third_speakers_quotes = Input::get('third_speakers_quotes_state');
            }

            if (Input::has('comments')) {
                $coverage->comments = Input::get('comments');
            }

            if (Input::has('third_speakers_quoted')) {
                $tmp = trim(strtolower(Input::get('third_speakers_quoted')));
                if (in_array($tmp, BaseValidator::$rules[BaseValidator::NOT_IN_FOR_SPEAKERS]) ||
                    true !== Coverage::validateThirdSpeakers(['third_speakers' => $tmp])) {
                    $coverage->third_speakers_quoted = null;
                }
            }

            $coverage->calc();
            $coverage->save();

            return Response::json(
                [
                    "error" => false,
                    "id" => $coverage->id,
                    'debug' => Input::all(),
                    'debug_data' => $coverage
                ]
            );
        } else {
            return Response::json(
                [
                    "error" => true,
                    "message" => "Validation errors",
                    "errors" => $coverage->errors(),
                    'debug' => Input::all(),
                    'debug_data' => $coverage
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @throws Exception
     * @return Response
     */
    public function destroy($id)
    {
        $coverage = Coverage::findOrFail($id);
        if (!Coverage::access(Coverage::ACCESS_DELETE_COVERAGE, $coverage)) {
            return Response::json(["error" => true, 'Access denied']);
        }

        if ($coverage->subcategoriesIds()) {
            $coverage->subcategories()->detach($coverage->subcategoriesIds());
        }

        Coverage::destroy($id);

        return Response::json(["error" => false, "id" => $id]);
    }

    /**
     * Make CSV file with coverage data
     *
     * @return void
     */
    public function export()
    {
        $filter = [];
        $regionId = Input::get("region", null);
        $userId = Input::get("user", null);
        $dateFrom = Input::get('start_range', null);
        $dateTo = Input::get('end_range', null);
        $otherFilter = json_decode(Input::get('filter', "[]"), true);
        $search = Input::has('search') ? Input::get('search') : '';

        if (!empty($otherFilter)) {
            $filter['status'] = Coverage::STATUS_APPROVED;
        }
        if (is_array($otherFilter)) {
            $filter = array_merge($filter, $otherFilter);
        }
        if ($regionId) {
            $filter['region_id'] = $regionId;
        }
        if ($userId) {
            $filter['owner_id'] = $userId;
        }
        if (!empty($dateFrom)) {
            $filter['from_date'] = date("Y-m-d", strtotime($dateFrom));
        }
        if (!empty($dateTo)) {
            $filter['to_date'] = date("Y-m-d", strtotime($dateTo . "+ 1 day"));
        }
        if (!empty($filter['file_id']) && $filter['status'] == Coverage::STATUS_APPROVED) {
            unset($filter['status']);
        }

        $filename = 'Coverage';
        $filename .= $regionId ? '_' . Region::getRegionById($regionId) : '';
        $filename .= $dateFrom ? '_from' . date("Ymd", strtotime($dateFrom)) : '';
        $filename .= $dateTo ? '_to' . date("Ymd", strtotime($dateTo)) : '';

        if (Request::ajax()) {
            \Queue::push('ExportBackground', ['filter' => $filter, 'user' => Auth::user(), 'search' => $search, 'filename' => $filename], 'export_coverage');
            return;
        }

        $exportData = Coverage::prepareDataForExport($filter, [], $search);

        \Excel::create($filename, function ($excel) use ($exportData) {
            $excel->sheet('Coverage', function ($sheet) use ($exportData) {
                $sheet->setAutoSize(false);
                $sheet->setColumnFormat(['D' => 'dd.mm.yyyy']);
                $sheet->fromArray($exportData['item'], null, 'A1', true, false);
                /* Отмечаем дубли */
                foreach ($exportData['duplicate'] as   $duplicate) {

                    $sheet->row($duplicate['row'], function ($row) {
                        $row->setBackground('#FFFF00');
                    });
                    $sheet->cell('AI'.$duplicate['row'], function($cell) use ($duplicate) {
                        $cell->setValue($duplicate['message']);

                    });
                }
                /* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */
            });
        })->export('xls');
    }

    /**
     * Return content and data for quick edit coverage in modal
     *
     * @return void
     */
    public function getEditModal()
    {
        $id = Input::get("id", null);
        $user = Auth::getUser();
        $coverage = Coverage::findOrNew($id);
        if (!Coverage::access(Coverage::ACCESS_EDIT_COVERAGE, $coverage)) {
            return Response::json(["error" => true, 'Access denied']);
        }

        // prepare available subcategories
        $subcategories = $coverage->subcategories;
        $activeSubcategories = [];
        if (!empty($subcategories)) {
            $ids = [];
            foreach ($subcategories as $subcategory) {
                $activeSubcategories['list'][] = [
                    'id' => $subcategory->id,
                    'title' => $subcategory->title
                ];
                $ids[] = $subcategory->id;
            }
            $activeSubcategories['ids'] = implode(',', $ids);
        }

        $data = $coverage->getDataForEdit();
        $data = array_merge([
            'eventData' => Event::getList(),
            'newsData' => NewsBreak::getList(),
            'campaigns' => Campaign::getActiveCampaigns(),
            'business' => Business::getActiveBusiness(),
            'speakersData' => Speaker::getList(),
            'regions' => Region::all()->sortBy('name'),
            'type' => Input::get("type"),
            'products' => Product::where("status", "=", Product::STATUS_ACTIVE)->get()->sortBy('name'),
            'subcategories' => Subcategory::orderBy('title')->get(),
            'active_subcategories' => $activeSubcategories
        ], $data);

        return Response::json(["error" => false, "html" => View::make('blocks.quick-coverage-edit', $data)->render()]);
    }

    public function checkCodingProcess()
    {
        $result["error"] = false;
        $result["month"] = '';

        $method = Input::get('method');
        $coverageId = Input::get('coverage_id');
        $date = date('Y-m');
        $closedMonths = $this->prepareCodingClosedMonths();

        if ($method == 'create') {
            if (Auth::user()->role != User::ROLE_ADMINISTRATOR && in_array($date, $closedMonths)) {
                $result["error"] = true;
                $result["month"] = date("F", strtotime($date));
            }
        }
        if ($method == 'edit') {
            $coverage = Coverage::find($coverageId);
            $date = date('Y-m', strtotime($coverage->date));
            if (Auth::user()->role != User::ROLE_ADMINISTRATOR && in_array($date, $closedMonths)) {
                $result["error"] = true;
                $result["month"] = date("F", strtotime($date));
            }
        }

        return Response::json($result);
    }

    public function changeCodingProcess()
    {
        $inputData = Input::all();
        $deletedYears = json_decode($inputData['deleted_years']);
        $codingData = (isset($inputData['coding_data'])) ? $inputData['coding_data'] : null;

        if (!empty($deletedYears)) {
            foreach ($deletedYears as $dY) {
                \DB::table('coverage_coding_process')
                    ->where('year', $dY)
                    ->delete();
            }
        }

        if (!empty($codingData)) {
            foreach ($codingData as $cD) {
                $record = \DB::table('coverage_coding_process')->where('year', $cD['year'])->first();
                $codingMonths = (isset($cD['months'])) ? $cD['months'] : [];
                $months = $this->prepareCodingMonths($codingMonths);
                if ($record) {
                    \DB::table('coverage_coding_process')
                        ->where('year', $cD['year'])
                        ->update(['months' => $months]);
                } else {
                    \DB::table('coverage_coding_process')->insert([
                        'year' => $cD['year'],
                        'months' => $months
                    ]);
                }
            }
        }

        return Response::json([
            "errors" => false
        ]);
    }

    public function getCodingProcessModal()
    {
        $data = \DB::table('coverage_coding_process')->orderBy('year')->get();
        $activeYears = array_map(function($item){
            return trim((string)$item->year);
        }, $data);

        return Response::json([
            "error" => false,
            "html" => View::make('blocks.coding-process', [
                'activeYears' => json_encode($activeYears),
                'data' => $data
            ])->render()
        ]);
    }

    protected function prepareCodingMonths($months)
    {
        $list = [];

        if ($months) {
            foreach ($this->monthsList as $num => $name) {
                if (array_key_exists($name, $months)) {
                    $list[$name] = 1;
                } else {
                    $list[$name] = 0;
                }
            }
        } else {
            foreach ($this->monthsList as $num => $name) {
                $list[$name] = 0;
            }
        }

        return json_encode($list);
    }

    protected function prepareCodingClosedMonths()
    {
        $codingProcess = \DB::table('coverage_coding_process')->get();
        $result = [];

        foreach ($codingProcess as $cP) {
            if ($cP->months) {
                $months = json_decode($cP->months);
                foreach ($months as $month => $status) {
                    if ($status == 0) {
                      $result[] = $cP->year.'-'.array_search($month, $this->monthsList);
                    }
                }
            }
        }

        return $result;
    }
}