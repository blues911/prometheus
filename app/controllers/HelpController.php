<?php
namespace App\Controllers;

use App\Models\BaseValidator;
use App\Models\HelpFiles;
use App\Models\User;
use Auth;
use Illuminate\Support\Facades\Response;
use View;
use Redirect;
use Input;
use Config;

class HelpController extends BaseController
{

    /**
     * Show files
     *
     * @return mixed
     */
    public function index()
    {
        return View::make('root')
            ->nest('template', 'help-files', ['files' => HelpFiles::getList()])
            ->nest('customAssets', 'assets.help-files');
    }

    /**
     * Download File By Id
     *
     * @return void
     */
    public function download($id = null)
    {
        $file = HelpFiles::find($id);
        if (empty($file) || !Auth::check()) {
            return Redirect::back();
        }

        header($file->getContentType());
        header('Content-Disposition: attachment;filename="'. $file->name . '.' . $file->format . '"');
        header('Cache-Control: max-age=0');
        header('Pragma: no-cache');
        readfile(Config::get('app.files_path')."/".$file->filename);
    }

    /**
     * Import file
     *
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function import($id = null)
    {
        if (Auth::check() && Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        $file = Input::file('file');
        if ($id == null || !isset($file)) {
            return Response::json(['error' => true, 'errors' => 'File not found']);
        }
        $helpFile = HelpFiles::find($id);
        if (!isset($helpFile)) {
            return Response::json(['error' => true, 'errors' => 'File not found']);
        }

        $validator = BaseValidator::make(Input::file(), $helpFile->getRule());
        if ($validator->fails()) {
            return Response::json(['error' => true, 'validationError' => true, 'validationErrors' => $validator->errors(), 'errors' => '']);
        }

        $newName = md5(time());
        $file    = $file->move(storage_path('files'), $newName);
        if (!$helpFile->updateFileName($newName, Input::file('file')->getClientOriginalName())) {
            return Response::json(['error' => true, 'errors' => 'File not update']);
        }
        return Response::json(["error" => false]);
    }

}