<?php
namespace App\Controllers;

use App\Helpers\ArrayHelper;
use App\Helpers\CurlHelper;
use App\Models\Campaign;
use App\Models\Business;
use App\Models\Country;
use App\Models\Coverage;
use App\Models\Region;
use App\Models\Subregion;
use App\Models\SaveReport;
use App\Models\Product;
use App\Models\Event;
use App\Models\NewsBreak;
use App\Models\Speaker;
use App\Models\User;
use App\Models\Subcategory;
use Auth;
use View;
use Redirect;
use Input;
use Response;
use URL;
use Session;

/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at Fahrenheit 451
 *
 * Date: 26.10.14
 * Time: 20:31
 */
class IndexController extends BaseController
{

    protected $filters   = [
        'start'               => null,
        'end'                 => null,
        'region_id'           => null,
        'subregion_id'        => null,
        'event'               => null,
        'newsbreak'           => '',
        'newsbreak_id'        => null,
        'product'             => '',
        'product_id'          => null,
        'country'             => '',
        'country_id'          => null,
        'campaign'            => '',
        'campaign_id'         => null,
        'business'            => '',
        'business_id'         => null,
        'speaker'             => '',
        'speaker_id'          => null,
        'media_list'          => '',
        'media_list_show'     => '',
        'media_category'      => '',
        'media_category_show' => '',
        'media_subcategory'      => '',
        'media_subcategory_show' => '',
    ];

    /**
     * go home
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function home()
    {
        if (Auth::check()) {
            return Redirect::action('App\Controllers\CoverageController@index');
        }
        return View::make('root')->nest('template', 'login')->nest('customAssets', 'assets.login');
    }

    /**
     * show charts
     *
     * @return $this
     */
    public function charts()
    {
        if (!in_array(Auth::user()->role, [User::ROLE_ADMINISTRATOR, User::ROLE_MANAGER])) {
            return Redirect::to("/");
        }

        $products      = Product::orderBy('name')->get();
        $productData   = ArrayHelper::prepareDropDownList($products, 'short_name', 'name');

        // $newsbreaks    = NewsBreak::where('complete', '=', 1)->orderBy('name')->get();
        $newsbreaks    = \DB::table('newsbreaks')
            ->join('newsbreaks_coverage', 'newsbreaks_coverage.newsbreak_id', '=', 'newsbreaks.id')
            ->select('newsbreaks.*')
            ->where('newsbreaks.complete', '=', 1)
            ->whereNull('newsbreaks.deleted_at')
            ->groupBy('newsbreaks.name')
            ->get();
        $newsbreakData = ArrayHelper::prepareDropDownList($newsbreaks);

        $countries     = Country::orderBy('name')->get();
        $countryData   = ArrayHelper::prepareDropDownList($countries);

        $campaigns     = Campaign::orderBy('name')->get();
        $campaignData  = ArrayHelper::prepareDropDownList($campaigns);

        $business      = Business::orderBy('name')->get();
        $businessData  = ArrayHelper::prepareDropDownList($business);

        // $speakers      = Speaker::orderBy('name')->get();
        $speakers      = \DB::table('speakers')
            ->join('speakers_coverage', 'speakers_coverage.speaker_id', '=', 'speakers.id')
            ->select('speakers.*')
            ->whereNull('speakers.deleted_at')
            ->groupBy('speakers.name')
            ->get();
        $speakerData   = ArrayHelper::prepareDropDownList($speakers);

        $subcategories     = Subcategory::orderBy('title')->get();
        $subcategoriesData   = ArrayHelper::prepareDropDownList($subcategories, 'title');

        return View::make('root', ['semanticVersion' => self::SEMANTIC_UI_VERSION_NEW])
            ->nest('template', 'charts', [
                'regions'           => Region::all()->sortBy('name'),
                'subregions'        => Subregion::orderBy('name')->get(),
                'filters'           => $this->filters,
                'eventData'         => Coverage::getAllEvents(),
                'newsData'          => $newsbreakData,
                'productData'       => $productData,
                'countryData'       => $countryData,
                'countries'         => $countries,
                'campaignData'      => $campaignData,
                'businessData'      => $businessData,
                'speakerData'       => $speakerData,
                'reports'           => SaveReport::getReportsList(),
                'subcategories'     => $subcategories,
                'subcategoriesData' => $subcategoriesData
            ])
            ->nest('customAssets', 'assets.charts');
    }

    /**
     * show reports
     *
     * @return $this
     */
    public function reports()
    {
        $saveReport = SaveReport::getByHash(Input::get('t', ""));
        if (!Input::has('t') || empty($saveReport)) {
            return Redirect::to("/");
        }

        $filters              = array_merge($this->filters, json_decode($saveReport->params, true));
        $filters['region']    = Region::getRegionById($filters['region_id']);
        $filters['business']  = Business::getBusinessById($filters['business']);
        $filters['country']   = Country::getCountryById($filters['country_id']);
        $filters['newsbreak'] = NewsBreak::getNewsbreakById($filters['newsbreak_id']);
        $map                  = Coverage::getListForViewMap();

        if (isset ($map['media_list'][$filters['media_list']])) {
            $filters['media_list_show']     = $map['media_list'][$filters['media_list']];
        }
        if (isset ($map['media_category'][$filters['media_category']])) {
            $filters['media_category_show'] = $map['media_category'][$filters['media_category']];
        }

        $top = [
            'newsbreaks' => !empty($filters['newsbreaks']) ? explode(",", $filters['newsbreaks']) : [],
            'campaigns'  => !empty($filters['campaigns'])  ? explode(",", $filters['campaigns'])  : [],
            'products'   => !empty($filters['products'])   ? explode(",", $filters['products'])   : [],
            'speakers'   => !empty($filters['speakers'])   ? explode(",", $filters['speakers'])   : [],
            'events'     => !empty($filters['events'])     ? explode(",", $filters['events'])     : [],
        ];
        $topName = [
            'newsbreaks' => NewsBreak::find($top['newsbreaks'])->lists('name'),
            'campaigns'  => Campaign::find($top['campaigns'])->lists('name'),
            'products'   => Product::find($top['products'])->lists('name'),
            'speakers'   => Speaker::find($top['speakers'])->lists('name'),
            'events'     => $top['events'],
        ];

        return View::make('root', ['publicReport' => true, 'semanticVersion' => self::SEMANTIC_UI_VERSION_NEW])
            ->nest('template', 'charts', [
                'regions'           => Region::all()->sortBy('name'),
                'filters'           => $filters,
                'eventData'         => [],
                'newsData'          => [],
                'productData'       => [],
                'countryData'       => [],
                'campaignData'      => [],
                'businessData'      => [],
                'speakerData'       => [],
                //'subcategoriesData' => [],
                'reports'           => [],
                'publicReport'      => true,
                'hash'              => $saveReport->hash,
                'top'               => $top,
                'topName'          => $topName,
            ])
            ->nest('customAssets', 'assets.charts', ['publicReport' => true]);
    }

    /**
     * generate html for creating pdf
     *
     * @return $this
     */
    public function pdfReports()
    {
        $saveReport = SaveReport::getByHash(Input::get('t', ""));
        if (!Input::has('t') || empty($saveReport)) {
            return Redirect::to("/");
        }

        $filters              = array_merge($this->filters, json_decode($saveReport->params, true));
        $filters['region']    = Region::getRegionById($filters['region_id']);
        $filters['business']  = Business::getBusinessById($filters['business']);
        $filters['country']   = Country::getCountryById($filters['country_id']);
        $filters['newsbreak'] = NewsBreak::getNewsbreakById($filters['newsbreak_id']);
        $filters['token']     = $saveReport->hash;
        $filters['charts']    = $filters['charts'] ? explode(',', $filters['charts']) : [];
        $map                  = Coverage::getListForViewMap();

        if (isset ($map['media_list'][$filters['media_list']])) {
            $filters['media_list_show']     = $map['media_list'][$filters['media_list']];
        }
        if (isset ($map['media_category'][$filters['media_category']])) {
            $filters['media_category_show'] = $map['media_category'][$filters['media_category']];
        }

        $top = [
            'newsbreaks' => !empty($filters['newsbreaks']) ? explode(",", $filters['newsbreaks']) : [],
            'campaigns'  => !empty($filters['campaigns'])  ? explode(",", $filters['campaigns'])  : [],
            'products'   => !empty($filters['products'])   ? explode(",", $filters['products'])   : [],
            'speakers'   => !empty($filters['speakers'])   ? explode(",", $filters['speakers'])   : [],
            'events'     => !empty($filters['events'])     ? explode(",", $filters['events'])     : [],
        ];

        $regionalKpi = CurlHelper::curl(URL::to('/stats/regional-kpi') . '?' . http_build_query($filters));
        $regionalKpi['labels'] = !empty($regionalKpi['labels']) ? array_chunk($regionalKpi['labels'], 9) : [];

        $regionalProductMentions = CurlHelper::curl(URL::to('/stats/regional-product-mentions') . '?' . http_build_query($filters));
        $regionalProductMentions['labels'] = !empty($regionalProductMentions['labels']) ? array_chunk($regionalProductMentions['labels'], 9) : [];

        $comparison = CurlHelper::curl(URL::to('/stats/comparison') . '?' . http_build_query($filters));
        $comparison['labels'] = !empty($comparison['labels']) ? array_chunk($comparison['labels'], 9) : [];
        
        return View::make('pdf', ['publicReport' => true, 'semanticVersion' => self::SEMANTIC_UI_VERSION_NEW])
            ->nest('template', 'components/charts_pdf', [
                'regions'                   => Region::all()->sortBy('name'),
                'filters'                   => $filters,
                'eventData'                 => [],
                'newsData'                  => [],
                'productData'               => [],
                'countryData'               => [],
                'campaignData'              => [],
                'businessData'              => [],
                'speakerData'               => [],
                'reports'                   => [],
                'publicReport'              => true,
                'top'                       => $top,
                'hash'                      => $saveReport->hash,
                'regionalKpi'               => $regionalKpi,
                'regionalProductMentions'   => $regionalProductMentions,
                'comparison'                => $comparison,
            ]);
    }

    /**
     * page for admin
     *
     * @return $this
     */
    public function admin()
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Redirect::to("/");
        }

        return View::make('root', ['jsOnly' => true,'semanticVersion' => self::SEMANTIC_UI_VERSION_NEW])
            ->nest('template', 'admin.index', ['subcategories2' => Subcategory::orderBy('title')->get()])
            ->nest('customAssets', 'assets.admin');
    }

    /**
     * Get progress import file
     *
     * @return void
     */
    public function getReportList()
    {
        $count_report_list = count(SaveReport::getReportsList(0, 0));
        return Response::json(
            [
                "recordsTotal"     => $count_report_list,
                "recordsFiltered"  => $count_report_list,
                "data"             => SaveReport::getReportsList(Input::get('start'), Input::get('length')),
                'debug'            => [
                    'skip'    => Input::get('start'),
                    'limit'   => Input::get('length'),
                    'request' => Input::all(),
                 ]
            ]
        );
    }

    /**
     * Queues to save the report page in the pdf format
     * 
     * @return $this
     */
    public function savePdf()
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(['error' => true, 'message' => 'Access denied']);
        }
        $report = SaveReport::add('pdf', Input::get('params', json_encode([])), Input::get('tab', null), SaveReport::PDF_TYPE);
        
        $params = json_decode(Input::get('params', json_encode([])), true);

        $filename = 'Report';
        if (!empty($params['country_id'])) {
            $filename .= '_' . Country::getCountryById($params['country_id']);
        } elseif (!empty($params['region_id'])) {
            $filename .= '_' . Region::getRegionById($params['region_id']);
        }
        $filename .= !empty($params['start']) ? '_from' . date("Ymd", strtotime($params['start'])) : '';
        $filename .= !empty($params['end']) ? '_to' . date("Ymd", strtotime($params['end'])) : '';

        if (!empty($report)) {
            \Queue::push('ExportChartsBackground', ['report' => $report, 'user' => Auth::user(), 'filename' => $filename], 'export_charts');
        }
    }



}