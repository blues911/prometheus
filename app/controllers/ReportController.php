<?php
namespace App\Controllers;

use App\Models\SaveReport;
use App\Models\User;
use Response;
use Exception;
use Input;
use Auth;

/**
 * Class ReportController
 */
class ReportController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(['error' => true, 'message' => 'Access denied']);
        }
        $report = SaveReport::add(Input::get('name', null), Input::get('params', json_encode([])), Input::get('tab', null), SaveReport::PAGE_TYPE);
        if (!empty($report)) {
            return Response::json([
                'error' => false,
                'url'   => SaveReport::getUrl($report->url, $report->hash, $report->tab),
                'id'    => $report->id,
                'name'  => $report->name
            ]);
        }
        return Response::json(['error' => true]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @throws Exception
     * @return Response
     */
    public function store()
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(['error' => true, 'message' => 'Access denied']);
        }
        $name = Input::get('name');
        if (empty($name)) {
            return Response::json([]);
        }
        $report       = SaveReport::findOrNew(Input::get('id', 0));
        $report->name = $name;
        $report->save();

        return Response::json($report->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(['error' => true, 'message' => 'Access denied']);
        }
        SaveReport::destroy($id);
        return Response::json(['error' => false]);
    }

    /**
     * Return content and data for edit report in modal
     *
     * @return void
     */
    public function getEditModal()
    {
        $id        = Input::get("id", null);
        $report  = SaveReport::findOrNew($id);
        $data = $report->toArray();

        return Response::json(["error" => false, "html" => \View::make('blocks.quick-report-edit', $data)->render()]);
    }


}