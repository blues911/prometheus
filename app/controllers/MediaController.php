<?php
namespace App\Controllers;

use App\Models\Country;
use App\Models\Media;
use App\Models\Region;
use App\Models\User;
use Response;
use Exception;
use Input;
use Auth;
use View;

/**
 * Class MediaController
 */
class MediaController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $media = Media::with('subcategories')->where('country_id', '=', Input::get('country_id'))->orderBy('name')->get();
        
        return Response::json([
            'media' => $media,
            'debug' => [
                'request' => Input::all(),
                'query'   => \DB::getQueryLog()
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @throws Exception
     * @return Response
     */
    public function store()
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        if (!Media::validate(Input::all())) {
            throw new Exception('Invalid input data');
        }

        $media = Media::findOrNew(Input::get('id', 0));
        $media->fill(Input::except(['region', 'country']));

        $region  = Region::find(Input::get('region'))->getModel();
        $country = Country::find(Input::get('country'))->getModel();

        if ($country->region_id != $region->id) {
            throw new Exception('Invalid input data');
        }

        if (Input::has('subcategories')) {
            $subcategories = explode(',', Input::get('subcategories'));
            $media->subcategories()->sync($subcategories);
        } else {
            $media->subcategories()->detach();
        }

        $media->region()->associate($region);
        $media->country()->associate($country);
        $media->save();

        return Response::json(
            [
                'media' => $media->toArray()
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        Media::destroy($id);
        return Response::json(
            [
                'error' => false
            ]
        );
    }

    /**
     * Restore the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function restore($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        try {
            $media = Media::onlyTrashed()->find($id);
            if ($media) {
                $media->deleted_at = NULL;
                $media->save();
            }
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }

        return Response::json(['error' => false]);
    }

    /**
     * get Media
     *
     * @return mixed
     */
    public function getMedia()
    {
        $media = Media::getFiltered(Input::all());
        $onlyDeleted = filter_var(Input::get('only_deleted'), FILTER_VALIDATE_BOOLEAN);
        $media = $onlyDeleted  ? $media->onlyTrashed()->paginate(Media::MEDIA_PAGE_SIZE) : $media->paginate(Media::MEDIA_PAGE_SIZE);
        $view = $onlyDeleted ? 'admin.lists.deleted._media_list' : 'admin.lists._media_list';

        return View::make($view, ['media' => $media])->render();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @return Response
     */
    public function destroySomeRecords()
    {
        $ids = explode(',', Input::get('ids'));
        
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        Media::destroy($ids);
        
        return Response::json(['error' => false]);
    }

}