<?php
namespace App\Controllers;

use App\Models\BaseValidator;
use App\Models\Country;
use App\Models\Coverage;
use App\Models\Media;
use App\Models\Notification;
use App\Models\Product;
use App\Models\Region;
use App\Models\ImportFile;
use App\Models\User;
use Auth;
use Config;
use View;
use Input;
use Response;
use Redirect;
use Cache;
use Request;

/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at Fahrenheit 451
 *
 * Date: 24.11.14
 * Time: 15:24
 */
class ImportController extends BaseController
{
    public function __construct()
    {
        if (!in_array(Auth::user()->role, [User::ROLE_ADMINISTRATOR, User::ROLE_MANAGER])) {
            if (Request::ajax()) {
                die(Response::json(["error" => true, 'Access denied']));
            } else {
                return Redirect::to('/')->send();
            } 
        }
    }

    /**
     * Import page
     *
     * @return $this
     */
    public function getIndex()
    {

        $defaultDateFrom = date("Y-m-d", strtotime('first day of this month'));
        $defaultDateTo   = date("Y-m-d", strtotime('last day of this month'));
        return View::make('root')
            ->nest('template', 'import', [
                'currentUser'     => Auth::user(),
                'dateFrom'        => Input::has("start") ? Input::get("start", null) : $defaultDateFrom,
                'dateTo'          => Input::has("end") ? Input::get("end", null) : $defaultDateTo,
                'defaultDateFrom' => $defaultDateFrom,
                'defaultDateTo'   => $defaultDateTo,
            ])
            ->nest('customAssets', 'assets.import');
    }

    /**
     * push queue
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCoverage()
    {
        $file = Input::file('file');
        $file->move(storage_path('exports'), $file->getFilename());
        $approve = Input::get("approve", "no");

        \Queue::push('ImportHandler', [
            'file'              => storage_path('exports/'.$file->getFilename()),
            'original_filename' => $file->getClientOriginalName(),
            'file_name'         => $file->getFilename(),
            'user_id'           => Auth::user()->id,
            'need_approve'      => $approve == "on",
            'file_id'           => null,
            'type'              => Notification::IMPORT_COVERAGE,
        ]);

        return Response::json([
            'status'  => 'queued',
            'message' => 'File added to the processing queue',
            'file'    => $file->getFilename()
        ]);
    }

    /**
     * post Media
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postMedia()
    {
        $filename = time() + '_media.xls';
        $file     = Input::file('file');
        $file->move(storage_path('exports'), $filename);

        \Queue::push('ImportHandler', [
            'file'              => storage_path('exports/'. $filename),
            'original_filename' => $file->getClientOriginalName(),
            'file_name'         => $file->getFilename(),
            'user_id'           => Auth::user()->id,
            'file_id'           => null,
            'type'              => Notification::IMPORT_MEDIA
        ]);

        return Response::json([
            'status'  => 'queued',
            'message' => 'File added to the processing queue',
            'file'    => $file->getFilename()
        ]);
    }

    /**
     * Update notification status to 'read'
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postReadMessage()
    {
        $n         = Notification::find(Input::get('id'));
        $n->status = 'read';
        $n->save();

        return Response::json([
                'error'        => false,
                'unread_count' => count(\App\Models\SystemUser::getUnreadNotifications(Auth::user()->id))
            ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDelete($id = null)
    {
        $importFile = ImportFile::find($id);
        if (empty($importFile) || $importFile->user_id != Auth::user()->id) {
            return Response::json(['error' => true]);
        }
        $importFile->active = 0;
        return Response::json(['error' => !$importFile->save(), 'id' => $importFile->id]);
    }

    /**
     * Post Repeat
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postRepeat($id = null)
    {
        $importFile = ImportFile::find($id);
        if (empty($importFile) || $importFile->user_id != Auth::user()->id) {
            return Response::json(['error' => false]);
        }

        $filename = md5(time());
        \Queue::push('ImportHandler', [
            'file'              => Config::get('app.imports_path')."/".$importFile->filename,
            'original_filename' => $importFile->name,
            'file_name'         => $filename,
            'user_id'           => Auth::user()->id,
            'need_approve'      => Auth::user()->role == User::ROLE_ADMINISTRATOR,
            'file_id'           => $importFile->id,
            'type'              => $importFile->type
        ]);

        return Response::json([
            'status'  => 'queued',
            'message' => 'File added to the processing queue',
            'file'    => $filename,
            'name'    => $importFile->name,
            'type'    => $importFile->type
        ]);
    }

    /**
     * get file
     *
     * @return void
     */
    public function getFile($id = null)
    {
        $importFile = ImportFile::find($id);
        if (empty($importFile) || $importFile->user_id != Auth::user()->id) {
            return Redirect::back();
        }
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $importFile->name .'"');
        header('Cache-Control: max-age=0');
        header('Pragma: no-cache');
        readfile(Config::get('app.imports_path')."/".$importFile->filename);
    }


    /**
     * Get progress import file
     *
     * @return void
     */
    public function checkProgress()
    {
        $file = Input::get("file", "");


        return Response::json([
            'progress'      => Cache::get($file.'_queue:progress', 0),
            'status'        => Cache::get($file.'_queue:status', 0),
            'error_file'    => Cache::get($file.'_queue:error_file', 0),
            'errors'    => Cache::get($file.'_queue:errors', 0)
        ]);
    }


    /**
     * @param $file
     */
    public function getFileError($file)
    {
        if (file_exists(Config::get('app.imports_path') . "/errors/" . $file)) {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $file . '"');
            header('Cache-Control: max-age=0');
            header('Pragma: no-cache');
            readfile(Config::get('app.imports_path') . "/errors/" . $file);
        }
        else{
            return Response::json(["error" => true, 'Notd found']);
        }
    }
    /**
     * Get progress import file
     *
     * @return void
     */
    public function getHistory()
    {
        $search            = Input::has('search') ? Input::get('search.value') : '';
        $filter['user_id'] = Auth::user()->id;
        $dateFrom          = Input::get('start_range', null);
        $dateTo            = Input::get('end_range', null);

        if (!empty($dateFrom)) {
            $filter['from_date'] = date("Y-m-d", strtotime($dateFrom));
        }
        if (!empty($dateTo)) {
            $filter['to_date']   = date("Y-m-d", strtotime($dateTo . "+ 1 day"));
        }
        if (!empty($dateTo)) {
            $filter['to_date']   = date("Y-m-d", strtotime($dateTo . "+ 1 day"));
        }
        if (Input::has('type')) {
            $filter['type'] = Input::get('type');
        }

        $list              = ImportFile::getActiveUserImportFiles(Input::get('start'), Input::get('length'), $filter, $search);
        $total             = count(ImportFile::getActiveUserImportFiles(0, 0, ['user_id' => Auth::user()->id], null));
        if (count($filter) > 1 || !empty($search)) {
            $recordsFiltered = count(ImportFile::getActiveUserImportFiles(0, 0, $filter, $search));
        }
        else {
            $recordsFiltered = $total;
        }

        return Response::json(
            [
                "recordsTotal"     => $total,
                "recordsFiltered"  => $recordsFiltered,
                "data"             => $list,
                'debug'            => [
                    'skip'    => Input::get('start'),
                    'limit'   => Input::get('length'),
                    'request' => Input::all(),
                ]
            ]
        );
    }

}