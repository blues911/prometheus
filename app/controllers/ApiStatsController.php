<?php
/**
 * Created by PhpStorm.
 * User: shakinm@gmail.com
 * Date: 09.10.2017
 * Time: 20:43
 */

namespace App\Controllers;


use App\Models\Coverage;
use App\Models\KpiRecord;
use App\Models\Region;
use Aws\CloudFront\Exception\Exception;
use Exception\APIException;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Validator;
use Input;

/**
 * Class ApiStatsController
 * @package App\Controllers
 */
class ApiStatsController extends BaseController
{

    private $filters = [];
    private $rules = [];
    private $range = [];
    private $region;
    private $limit;

    CONST LIMIT = 10;

    /**
     * ApiStatsController constructor.
     */
    public function __construct(Request $request)
    {

        if (Input::has('filter') && json_decode(Input::get('filter')) != null) {

            $this->filters = json_decode(Input::get('filter'), true);


            $regions = $this->getAviableRegions();


            if ($regions) {
                foreach ($regions as $region) {
                    $arrRegion[] = $region['name'];
                }
                $strRegion = implode(",", $arrRegion);

                $this->rules['region'] = "in:$strRegion";
            }

            $this->rules['start'] = "date_format:Y-m-d";
            $this->rules['end'] = "date_format:Y-m-d";
            $this->rules['month'] = "date_format:Y-m";
            $this->validateFilter($request);

        } elseif (Input::has('filter')) {
            throw new APIException('Неверный формат json');
        }


        $this->range = $this->getRange();
        $this->limit = $this->getLimit();
        $this->region = $this->getRegion();

    }

    /**
     * @return int|mixed
     */
    private function getRegion()
    {
        return $this->region = isset($this->filters['region']) ? $this->filters['region'] : null;
    }

    /**
     * @return int|mixed
     */
    private function getLimit()
    {
        return $this->limit = isset($this->filters['limit']) ? $this->filters['limit'] : self::LIMIT;
    }

    /**
     * @return array
     */
    private function getRange()
    {
        $start = isset($this->filters['date']['start']) ? $this->filters['date']['start'] : date('Y-m-d', strtotime('-50 year'));
        $end = isset($this->filters['date']['end']) ? date('Y-m-d', strtotime($this->filters['date']['end'] . "+1 day")) : date('Y-m-d', strtotime("+1 day"));


        return compact('start', 'end');
    }

    /**
     *
     */
    private function validateFilter($request)
    {
        try {

            if (isset ($this->filters) && is_array($this->filters)) {

                foreach ($this->filters as $k => $v) {

                    if (!in_array($k,
                        $request->getRequestUri() == '/api/stats/mindshare' ? ['month', 'region'] : [
                            'date',
                            'region',
                            'speakers_name',
                            'speakers_ids'
                        ])) {
                        throw new APIException("Неверный параметр запроса: $k");
                    }
                }
            }
            if (isset ($this->filters['date']) && is_array($this->filters['date'])) {

                foreach ($this->filters['date'] as $k => $v) {
                    if (!in_array($k, ['start', 'end'])) {
                        throw new APIException("Неверный параметр запроса: $k");
                    }
                }
            } elseif (isset ($this->filters['date']) && !is_array($this->filters['date'])) {

                throw new APIException("Неверный параметр запроса: date");
            }

            if (isset ($this->filters['date']['start'])) {
                $validator = Validator::make(
                    ['start' => $this->filters['date']['start']], $this->rules

                );
                if ($validator->fails()) {
                    throw new APIException('Неверный формат даты (Y-m-d): start');
                }
            }

            if (isset ($this->filters['date']['end'])) {
                $validator = Validator::make(['end' => $this->filters['date']['end']], $this->rules);
                if ($validator->fails()) {
                    throw new APIException('Неверный формат даты (Y-m-d): end');
                }
            }
            if (isset ($this->filters['month'])) {
                $validator = Validator::make(['month' => $this->filters['month']],
                    $this->rules);

                if ($validator->fails()) {
                    throw new APIException('Неверный формат даты (Y-m): month');
                }
            }

            if (isset ($this->filters['region'])) {

                $validator = Validator::make(['region' => $this->filters['region']],
                    $this->rules);

                if ($validator->fails()) {
                    throw new APIException('Неверный параметр запроса: region');
                }
            }
        } catch (Exception $e) {
            throw new APIException($e->getMessage());
        }


    }

    /**
     * @param $builder
     * @return mixed
     * @throws APIException
     */
    public function builderFilters($builder, $noRegion = false)
    {
        try {
            $builder->where('coverage.status', '=', Coverage::STATUS_APPROVED)
                ->where('coverage.date', '>=', $this->range['start'])
                ->where('coverage.date', '<', $this->range['end']);


            if (isset($this->filters['speakers_ids']) && !is_array($this->filters['speakers_ids'])) {
                $ids = array_map('trim', explode(",", $this->filters['speakers_ids']));
                $builder->whereIn('speakers_coverage.speaker_id', $ids);

            } elseif (isset($this->filters['speakers_name']) && !is_array($this->filters['speakers_name'])) {
                $names = array_map('trim', explode(",", $this->filters['speakers_name']));
                $builder->where(function ($query) use ($names) {
                    foreach ($names as $name) {
                        $query->orWhere("speakers.name", "LIKE", "%$name%");
                    }
                    return $query;
                });
            }
            if ($noRegion == false && isset($this->filters['region']) && !is_array($this->filters['region'])) {
                $regionName = trim($this->filters['region']);
                $builder->where(function ($query) use ($regionName) {
                    $query->orWhere("regions.name", "=", $regionName);
                    return $query;
                });
            }
            return $builder;
        } catch (\Exception $e) {
            throw new APIException('Wrong incoming filter params');
        }
    }


    /**
     * @param $builder
     * @param bool $mindshare
     * @return mixed
     * @throws APIException
     */
    public function builderFiltersForKpi($builder, $mindshare = false, $noRegion = false)
    {
        try {
            $KPItable="kpi_records_v2";
            if (isset($this->filters['month'])) {
                $firstDayYear = new \DateTime($this->filters['month']);
                $firstDayYear->modify('first day of January this year')
                    ->format('Y-m');

                if ($firstDayYear->format('Y') < "2018") {
                    $KPItable="kpi_records";
                }
                $start = new \DateTime($this->filters['month']);
                $start->modify('first day of this month')
                    ->format('Y-m');
                $end = new \DateTime($this->filters['month']);
                $end->modify('last day of this month')
                    ->format('Y-m');

                if ($mindshare) {
                    $builder->where($KPItable.'.date', '=', $start);
                } else {
                    $builder->where('coverage.status', '=', Coverage::STATUS_APPROVED)
                        ->where('coverage.date', '>=', $firstDayYear)
                        ->where('coverage.date', '<', $end);
                }

            } else {

                $builder->where($KPItable.'.status', '=', KpiRecord::STATUS_ACTIVE);


            }


            if ($noRegion == false && isset($this->filters['region']) && !is_array($this->filters['region'])) {
                $regionName = trim($this->filters['region']);
                $builder->where(function ($query) use ($regionName) {

                    $query->orWhere("regions.name", "=", $regionName);
                    return $query;
                });
            }
            return $builder;
        } catch (\Exception $e) {
            throw new APIException($e->getMessage());
        }
    }


    public function getKpi()
    {
        $result = [];


        $month = date('n', strtotime($this->range['end']));

        $articles = $this->getRegionArticles();;
        $net_effect = $this->getRegionNetEffect();
        $net_effect_da = $this->getRegionNetEffectDA();
        $net_effect_ua = $this->getRegionNetEffectUA();
        $impact_index = $this->getRegionImpactIndex();
        $global_index = $this->getGlobalImpactIndex();
        $sumNeteffect = $sumNeteffectDA = $sumNeteffectUA = $sumArticles = 0;
        //    $mindshare = $this->getMindShare();
        $tonality = $this->getRegionCoverageTonality();
        $global_tonality = $this->getGlobalCoverageTonality();

        if ($articles) {
            foreach ($articles as $k => $article) {
                $result[$k] = [
                    'articles' => $articles[$k],
                    'impactindex' => isset($impact_index[$k]) ? $impact_index[$k] : null,
                    'neteffect' => isset($net_effect[$k]) ? round($net_effect[$k], 2) : null,
                    'neteffect_desired_articles' => isset($net_effect_da[$k]) ? round($net_effect_da[$k], 2) : null,
                    'neteffect_undesired_articles' => isset($net_effect_ua[$k]) ? round($net_effect_ua[$k], 2) : null,
                    'tonality' => $tonality[$k] ? $tonality[$k] : []
                ];

                $sumArticles += $articles[$k];
                $sumNeteffect += isset($net_effect[$k]) ? round($net_effect[$k], 2) : 0;
                $sumNeteffectDA += isset($net_effect_da[$k]) ? round($net_effect_da[$k], 2) : 0;
                $sumNeteffectUA += isset($net_effect_ua[$k]) ? round($net_effect_ua[$k], 2) : 0;

            }

            if (!isset($this->filters['region'])) {
                $result['GLOBAL'] = [
                    'articles' => $sumArticles,
                    'impactindex' => $global_index,
                    'neteffect' => $sumNeteffect,
                    'neteffect_desired_articles' => $sumNeteffectDA,
                    'neteffect_undesired_articles' => $sumNeteffectUA,
                    'tonality' => $global_tonality
                ];
            }
        }


        return $result;
    }

    /**
     * @return array
     */
    public function getRegionIndexes()
    {
        $result = [];


        $articles = $this->getRegionArticles();;
        $net_effect = $this->getRegionNetEffect();
        $impact_index = $this->getRegionImpactIndex();

        if ($articles) {
            foreach ($articles as $k => $article) {
                $result[$k] = [
                    'articles' => $articles[$k],
                    'impact_index' => isset($impact_index[$k]) ? round($impact_index[$k], 2) : null,
                    'net_effect' => isset($net_effect[$k]) ? round($net_effect[$k], 2) : null
                ];


            }
        }
        if (!isset($this->filters['region'])) {
            $result['total'] = [
                'articles' => round($this->getCoverageSumArticles(), 2),
                'impact_index' => round($this->getCoverageAvgImpactIndex(), 2),
                'net_effect' => round($this->getCoverageAvgNetEffect(), 2)
            ];
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getSpeakers()
    {

        $result = [];

        $articles = $this->getSpeakersArticles();
        $impact_index = $this->getSpeakersImpactIndex();
        $net_effect = $this->getSpeakersNetEffect();

        if ($articles) {
            foreach ($articles as $k => $article) {
                $result[$k] = [
                    'articles' => $articles[$k],
                    'impact_index' => $impact_index[$k],
                    'net_effect' => $net_effect[$k],
                ];


            }
        }

        return $result;
    }

    /**
     * @return array Region|null
     */
    private function getAviableRegions()
    {
        $regions = Region::whereNull('deleted_at')->get()->toArray();

        return $regions;

    }

    /**
     * @return array
     * @throws APIException
     */
    private function getSpeakersArticles()
    {
        $result = [];

        try {
            $builder = \DB::table('coverage')
                ->select('speakers.name', \DB::raw('count(distinct(coverage.id)) as value'))
                ->join('speakers_coverage', 'coverage.id', '=', 'speakers_coverage.coverage_id')
                ->join('speakers', 'speakers.id', '=', 'speakers_coverage.speaker_id')
                ->whereNull('speakers.deleted_at')
                ->groupBy('speakers.name')
                ->orderBy('value', 'desc');
            $data = $this->builderFilters($builder)->get();

            if ($data) {
                foreach ($data as $d) {
                    $result[$d->name] = $d->value;
                }
            }
            return $result;
        } catch (\Exception $e) {
            throw new APIException($e->getMessage());
        }
    }

    /**
     * @return array
     * @throws APIException
     */
    private function getSpeakersNetEffect()
    {
        $result = [];

        try {
            $builder = \DB::table('coverage')
                ->select('speakers.name', \DB::raw('sum(coverage.net_effect) as value'))
                ->join('speakers_coverage', 'coverage.id', '=', 'speakers_coverage.coverage_id')
                ->join('speakers', 'speakers.id', '=', 'speakers_coverage.speaker_id')
                ->whereNull('speakers.deleted_at')
                ->groupBy('speakers.name');

            $data = $this->builderFilters($builder)->get();
            if ($data) {
                foreach ($data as $d) {
                    $result[$d->name] = $d->value;
                }
            }
            return $result;
        } catch (\Exception $e) {
            throw new APIException($e->getMessage());
        }
    }

    /**
     * @return array
     * @throws APIException
     */
    private function getSpeakersImpactIndex()
    {
        $result = [];

        try {
            $builder = \DB::table('coverage')
                ->select('speakers.name', \DB::raw('avg(coverage.impact_index) as value'))
                ->join('speakers_coverage', 'coverage.id', '=', 'speakers_coverage.coverage_id')
                ->join('speakers', 'speakers.id', '=', 'speakers_coverage.speaker_id')
                ->whereNull('speakers.deleted_at')
                ->groupBy('speakers.name');

            $data = $this->builderFilters($builder)->get();

            if ($data) {
                foreach ($data as $d) {
                    $result[$d->name] = $d->value;
                }
            }

            return $result;
        } catch (\Exception $e) {
            throw new APIException('Error.');
        }
    }

    private function getCoverageSumArticles()
    {
        $result = null;

        try {
            $builder = \DB::table('coverage')
                ->select(\DB::raw('count(distinct(coverage.id)) as value'));
            $data = $this->builderFilters($builder)->get();

            if (isset($data[0])) {
                $result = $data[0]->value;
            }


            return $result;
        } catch (\Exception $e) {
            throw new APIException('Error.');
        }
    }

    private function getCoverageAvgNetEffect()
    {
        $result = null;

        try {
            $builder = \DB::table('coverage')
                ->select(\DB::raw('avg(coverage.net_effect) as value'));
            $data = $this->builderFilters($builder)->get();

            if (isset($data[0])) {
                $result = $data[0]->value;
            }


            return $result;
        } catch (\Exception $e) {
            throw new APIException('Error.');
        }
    }

    private function getCoverageAvgImpactIndex()
    {
        $result = null;

        try {
            $builder = \DB::table('coverage')
                ->select(\DB::raw('avg(coverage.impact_index) as value'));
            $data = $this->builderFilters($builder)->get();

            if (isset($data[0])) {
                $result = $data[0]->value;
            }


            return $result;
        } catch (\Exception $e) {
            throw new APIException('Error.');
        }
    }

    /**
     * @return array
     * @throws APIException
     */
    private function getRegionArticles()
    {


        $result = [];

        try {
            $builder = \DB::table('coverage')
                ->select('regions.name', \DB::raw('count(distinct(coverage.id)) as value'))
                ->join('regions', 'regions.id', '=', 'coverage.region_id')
                ->whereNull('regions.deleted_at')
                ->groupBy('regions.name');

            $data = $this->builderFilters($builder)->get();

            if ($data) {
                foreach ($data as $d) {
                    $result[$d->name] = $d->value;
                }
            }

            return $result;
        } catch (\Exception $e) {
            throw new APIException('Error.');
        }

    }

    private function getRegionCoverageTonality()
    {


        $result = $allArr = [];

        try {

            $builder = \DB::table('coverage')
                ->select('regions.name', \DB::raw('count(distinct(coverage.id)) as value'))
                ->join('regions', 'regions.id', '=', 'coverage.region_id')
                ->whereNull('regions.deleted_at')
                ->groupBy('regions.name');

            $all = $this->builderFilters($builder)->get();


            if ($all) {
                foreach ($all as $a) {
                    $allArr[$a->name] = $a->value;
                }
            } else {
                return null;
            }


            foreach (['positive', 'neutral', 'negative'] as $tonality) {
                $builder = \DB::table('coverage')
                    ->select('regions.name', \DB::raw('count(distinct(coverage.id)) as value'))
                    ->join('regions', 'regions.id', '=', 'coverage.region_id')
                    ->where('main_tonality', '=', $tonality)
                    ->whereNull('regions.deleted_at')
                    ->groupBy('regions.name');

                $dataBuilder = clone    $builder;
                $data = $this->builderFilters($dataBuilder)->get();


                if ($data) {
                    foreach ($data as $d) {
                        $result[$d->name][$tonality] = round((100 * $d->value) / $allArr[$d->name], 2);
                        $result[$d->name] = array_merge(['positive' => 0, 'neutral' => 0, 'negative' => 0], $result[$d->name]);
                    }

                }


            }


            return $result;
        } catch (\Exception $e) {
            throw new APIException('Error.');
        }

    }

    private function getGlobalCoverageTonality()
    {


        $result = $allArr = [];

        try {

            $countCoverage = 0;

            $builder = \DB::table('coverage')
                ->select(\DB::raw('count(distinct(coverage.id)) as value'));
            $all = $this->builderFilters($builder, true)->get();


            if ($all) {
                $countCoverage = $all[0]->value;
            } else {
                return null;
            }


            foreach (['positive', 'neutral', 'negative'] as $tonality) {
                $builder = \DB::table('coverage')
                    ->select(\DB::raw('count(distinct(coverage.id)) as value'))
                    ->where('main_tonality', '=', $tonality);

                $dataBuilder = clone    $builder;
                $data = $this->builderFilters($dataBuilder, true)->get();


                if ($data) {
                    foreach ($data as $d) {
                        $result[$tonality] = round((100 * $d->value) / $countCoverage, 2);
                        $result = array_merge(['positive' => 0, 'neutral' => 0, 'negative' => 0], $result);
                    }

                }


            }


            return $result;
        } catch (\Exception $e) {
            throw new APIException('Error.');
        }

    }

    /**
     * @return array
     * @throws APIException
     */
    private
    function getRegionImpactIndex()
    {

        $result = [];

        try {
            $builder = \DB::table('coverage')
                ->select('regions.name', \DB::raw('avg(coverage.impact_index) as value'))
                ->join('regions', 'regions.id', '=', 'coverage.region_id')
                ->whereNull('regions.deleted_at')
                ->groupBy('regions.name');

            $data = $this->builderFilters($builder)->get();

            if ($data) {
                foreach ($data as $d) {
                    $result[$d->name] = $d->value;
                }
            }

            return $result;
        } catch (\Exception $e) {
            throw new APIException('Error.');
        }
    }

    private
    function getGlobalImpactIndex()
    {

        $result = [];

        try {
            $builder = \DB::table('coverage')
                ->select(\DB::raw('avg(coverage.impact_index) as value'));

            $data = $this->builderFilters($builder, true)->get();

            $result = $data[0]->value;

            return $result;
        } catch (\Exception $e) {
            throw new APIException('Error.');
        }
    }

    /**
     * @return array
     * @throws APIException
     */
    private
    function getRegionNetEffect()
    {

        $result = [];

        try {
            $builder = \DB::table('coverage')
                ->select('regions.name', \DB::raw('sum(coverage.net_effect) as value'))
                ->join('regions', 'regions.id', '=', 'coverage.region_id')
                ->whereNull('regions.deleted_at')
                ->groupBy('regions.name');

            $data = $this->builderFilters($builder)->get();

            if ($data) {
                foreach ($data as $d) {
                    $result[$d->name] = $d->value;
                }
            }

            return $result;
        } catch (\Exception $e) {
            throw new APIException('Error.');
        }
    }


    /**
     * @return array
     * @throws APIException
     */
    private
    function getRegionNetEffectDA()
    {

        $result = [];

//        try {
        $builder = \DB::table('coverage')
            ->select('regions.name', \DB::raw('sum(coverage.net_effect) as value'))
            ->join('regions', 'regions.id', '=', 'coverage.region_id')
            ->where('coverage.desired_article', '=', 1)
            ->whereNull('regions.deleted_at')
            ->groupBy('regions.name');

        $data = $this->builderFilters($builder)->get();

        if ($data) {
            foreach ($data as $d) {
                $result[$d->name] = $d->value;
            }
        }

        return $result;
//        } catch (\Exception $e) {
//            throw new APIException('Error.');
//        }
    }


    /**
     * @return array
     * @throws APIException
     */
    private
    function getRegionNetEffectUA()
    {

        $result = [];

        try {
            $builder = \DB::table('coverage')
                ->select('regions.name', \DB::raw('sum(net_effect) as value'))
                ->join('regions', 'regions.id', '=', 'coverage.region_id')
                ->where('coverage.desired_article', '=', "")
                ->whereNull('regions.deleted_at')
                ->groupBy('regions.name');

            $data = $this->builderFilters($builder)->get();

            if ($data) {
                foreach ($data as $d) {
                    $result[$d->name] = $d->value;
                }
            }

            return $result;
        } catch (\Exception $e) {
            throw new APIException('Error.');
        }
    }

    public
    function getMindShare()
    {
        $result = [];

        $KPItable="kpi_records_v2";

        if (isset($this->filters['month'])) {
            $firstDayYear = new \DateTime($this->filters['month']);


            if ($firstDayYear->format('Y') < "2018") {
                $KPItable = "kpi_records";
            }
        }
        $builder = \DB::table($KPItable)
            ->select(\DB::raw("regions.name, 
              {$KPItable}.company_mindshare as company,
              {$KPItable}.b2b_mindshare as b2b,
              {$KPItable}.b2c_mindshare as b2c "))
            ->leftJoin('regions', 'regions.id', '=', "{$KPItable}.region_id")
            ->whereNull('regions.deleted_at');
        $data = $this->builderFiltersForKpi($builder, true)->get();

        $re = '/^([[0-9]|[0-9][^D])[.|[\s]?+([\w]+[\s]+[\w]+|[\w]+)[\D]+?([[\d]+.[\d]+]|[\d]+)/';
        
        if ($data) {
            foreach ($data as $d) {
                $arr = [];
                foreach (['company', 'b2b', 'b2c'] as $v) {
                    try {
                        if ($d->{$v}) {

                            foreach (explode("\n", trim($d->{$v})) as $line) {
                                $str = $line; //'1 Symantec  - 23.81%';

                                preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);

                                //  if (!isset($matches[0])) dd($line, $d->{$v}, $matches);

                                $arr[$v][] = [
                                    'title' => $matches[0][2],
                                    'rank' => trim($matches[0][1], '.'),
                                    'share' => $matches[0][3] / 100,
                                ];
                            }

                        } else {
                            throw new \Exception('Parse Error');
                        }

                    } catch (\Exception $e) {
                        $arr[$v] = ['title' => null, 'rank' => null, 'share' => null];
                    }
                }

                if (!empty($d->name)) {
                    $result[$d->name] = $arr;
                } else {
                    $result['GLOBAL'] = $arr;
                }
            }
        }

        return $result;
    }
}