<?php
namespace App\Controllers;

use App\Models\Subregion;
use App\Models\Region;
use App\Models\User;
use Response;
use Input;
use Auth;
use View;

/**
 * Class SubregionController
 */
class SubregionController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Response::json([
            'subregions' => Subregions::all(),
            'debug'   => [
                'request' => Input::all()
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        $subregion = Subregion::findOrNew(Input::get('id', 0));
        $subregion->name = Input::get('name');

        if (Input::get('region') !== '') {
            $subregion->region()->associate(Region::find(Input::get('region'))->getModel());
        }

        $subregion->save();
        return Response::json($subregion->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        try {
            Subregion::destroy($id);
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }

        return Response::json(['error' => false]);
    }

    /**
     * Restore the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function restore($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        try {
            $subregion = Subregion::onlyTrashed()->find($id);
            if ($subregion) {
                $subregion->deleted_at = NULL;
                $subregion->save();
            }
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }

        return Response::json(['error' => false]);
    }

    /**
     * get Regions
     *
     * @return mixed
     */
    public function getSubregions()
    {
        $onlyDeleted = filter_var(Input::get('only_deleted'), FILTER_VALIDATE_BOOLEAN);
        $subregions = $onlyDeleted  ? Subregion::onlyTrashed()->orderBy('name') : Subregion::orderBy('name');
        $view = $onlyDeleted ? 'admin.lists.deleted._subregions_list' : 'admin.lists._subregions_list';

        return View::make($view, [
            'subregions' => $subregions->get(),
        ])->render();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @return Response
     */
    public function destroySomeRecords()
    {
        $ids = explode(',', Input::get('ids'));
        
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        try {
            Subregion::destroy($ids);
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }
        
        return Response::json(['error' => false]);
    }
}