<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 02.06.2017
 * Time: 11:17
 */

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Prometheus\TopRanking;
use App\Prometheus\ImportCoverage;

class TestController extends BaseController
{

    public function index()
    {
        $data = ['file' => 123,
            'user_id' => 22,
            'file_name' => 'name',
            'original_filename' => 'original_name',
            'need_approve' => true,
            'file_id' => 1231,];

        $importer = new ImportCoverage($data['file'],
            $data['user_id'],
            $data['file_name'],
            $data['original_filename'],
            $data['need_approve'],
            $data['file_id']);

        $importer->run();
        $importer->addNotification($data);
        echo 'YES';
    }

}