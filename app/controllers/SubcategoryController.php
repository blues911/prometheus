<?php
namespace App\Controllers;

use App\Models\Subcategory;
use App\Models\User;
use Response;
use Input;
use Auth;
use View;

/**
 * Class SubcategoryController
 */
class SubcategoryController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        $title = Input::get('title');
        if (empty($title)) {
            return Response::json(['error' => true, 'Title can\'t be empty']);
        }


        $subcategory = Subcategory::findOrNew(Input::get('id', 0));
        $subcategory->title = $title;
        $subcategory->slug=$this->createSlug($title);
        $subcategory->save();

        return Response::json($subcategory->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        Subcategory::destroy($id);
        return Response::json(['error' => false]);
    }

    /**
     * Restore the specified resource.
     *
     * @param  int $id
     * @return Response
     */

    public function restore($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        try {
            $subcategory = Subcategory::onlyTrashed()->find($id);
            if ($subcategory) {
                $subcategory->deleted_at = NULL;
                $subcategory->save();
            }
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }

        return Response::json(['error' => false]);
    }

    /**
     * get Subcategories
     *
     * @return mixed
     */
    public function getSubcategories()
    {
        $onlyDeleted = filter_var(Input::get('only_deleted'), FILTER_VALIDATE_BOOLEAN);
        $subcategories = $onlyDeleted  ? Subcategory::onlyTrashed()->orderBy('title') : Subcategory::orderBy('title');
        $view = $onlyDeleted ? 'admin.lists.deleted._subcategories_list' : 'admin.lists._subcategories_list';

        return View::make($view, [
            'subcategories' => $subcategories->get(),
        ])->render();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @return Response
     */
    public function destroySomeRecords()
    {
        $ids = explode(',', Input::get('ids'));
        
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        
        Subcategory::destroy($ids);
        
        return Response::json(['error' => false]);
    }

    public static function createSlug($str, $delimiter = '_'){

        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;

    }
}