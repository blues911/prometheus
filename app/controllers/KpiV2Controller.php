<?php
namespace App\Controllers;


use Auth;
use View;
use Redirect;
use Input;
use Response;
use URL;
use App\Models\KpiHeaderV2;
use App\Models\KpiRecordV2;
use App\Models\Region;
use App\Models\Coverage;

class KpiV2Controller extends BaseController
{

    public function index()
    {
        $year = Input::exists('year') ? intval(Input::get('year')) : 2018;
        $regions = Region::all();
        $currentMonth = $year < intval(date('Y')) ? 12 : date('n');

        $data = [];
        $data['global']['headers'] = KpiHeaderV2::firstOrCreate(['region_id' => null, 'year' => date('Y')])->toArray();
        foreach ($regions as $region) {
            $data[$region->name]['headers'] = KpiHeaderV2::firstOrCreate(['region_id' => $region->id, 'year' => date('Y')])->toArray();
        }

        for($i = 1; $i <= $currentMonth; $i++) {
            $date = date($year.'-' . $i . '-01');
            $kpi = KpiRecordV2::firstOrCreate([
                'region_id' => null,
                'date' => $date
            ])->toArray();
            $data['global']['records'][$i] = array_merge($kpi, Coverage::kpiDataV2ByRegion($date));

            foreach ($regions as $region) {
                $kpiByRegion = KpiRecordV2::firstOrCreate([
                    'region_id' => $region->id,
                    'date' => $date
                ])->toArray();
                $data[$region->name]['records'][$i] =
                    array_merge($kpiByRegion, Coverage::kpiDataV2ByRegion($date, $region->id));

            }
        }

        return View::make('root', ['semanticVersion' => self::SEMANTIC_UI_VERSION_NEW])
            ->nest('template', 'kpiV2', [
                'data' 			=> $data,
                'currentUser' 	=> Auth::user(),
                'regions'		=> $regions
            ])
            ->nest('customAssets', 'assets.kpiV2');
    }

    public function saveHeaders()
    {
        if (!Auth::user()->is_superadmin) {
            return Response::json(["error" => true, 'Access denied']);
        }

        $region_id = Input::get('region_id');
        $kpi = KpiHeaderV2::firstOrNew(['region_id' => $region_id ?: null, 'year' => date('Y')]);
        $kpi->net_effect_da =  Input::get('net_effect_da', '');
        $kpi->impact_index = Input::get('impact_index', '');
        $kpi->net_effect_ua = Input::get('net_effect_ua', '');
        $kpi->company_mindshare = Input::get('company_mindshare', '');
        $kpi->b2b_mindshare = Input::get('b2b_mindshare', '');
        $kpi->b2c_mindshare = Input::get('b2c_mindshare', '');

        if (!$kpi->save()) {
            return Response::json(['error' => true]);
        }
    }

    public function getHeadersByRegion()
    {
        if (!Auth::user()->is_superadmin) {
            return Response::json(["error" => true, 'Access denied']);
        }

        $id = Input::get('id');
        $data = KpiHeaderV2::where('region_id', '=', $id ?: null)->where('year', '=', date('Y'))->first();

        return json_encode($data);
    }

    public function saveRecords()
    {
        if (!Auth::user()->is_superadmin) {
            return Response::json(["error" => true, 'Access denied']);
        }

        $region_id = Input::get('region_id');
        $date = Input::get('date');
        $kpi = KpiRecordV2::firstOrNew(['region_id' => $region_id ?: null, 'date' => $date]);
        $kpi->company_mindshare 	= Input::get('company_mindshare', '');
        $kpi->b2b_mindshare 		= Input::get('b2b_mindshare', '');
        $kpi->b2c_mindshare 		= Input::get('b2c_mindshare', '');
        $kpi->status 				= Input::get('status') ? KpiRecordV2::STATUS_ACTIVE : KpiRecordV2::STATUS_HIDDEN;
        $kpi->is_net_effect_da 		= (bool)Input::get('is_net_effect_da');
        $kpi->is_impact_index 		= (bool)Input::get('is_impact_index');
        $kpi->is_net_effect_ua 			= (bool)Input::get('is_net_effect_ua');
        $kpi->is_company_mindshare  = (bool)Input::get('is_company_mindshare');
        $kpi->is_b2b_mindshare 		= (bool)Input::get('is_b2b_mindshare');
        $kpi->is_b2c_mindshare 		= (bool)Input::get('is_b2c_mindshare');


        if (!$kpi->save()) {
            return Response::json(['error' => true]);
        }
    }

    public function getRecordsByRegion()
    {
        if (!Auth::user()->is_superadmin) {
            return Response::json(["error" => true, 'Access denied']);
        }

        $id = Input::get('id');
        $date = Input::get('date');
        $data = KpiRecordV2::where('region_id', '=', $id ?: null)->where('date', '=', $date)->first();

        return Response::json($data);
    }

}