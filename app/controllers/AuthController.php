<?php
namespace App\Controllers;

use App\Models\BaseValidator;
use Validator;
use Input;
use Auth;
use Log;
use Redirect;
use Session;

/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at Fahrenheit 451
 *
 * Date: 27.10.14
 * Time: 1:09
 */
class AuthController extends BaseController
{
    /**
     * signin in system
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function signin()
    {
        $v = BaseValidator::make(Input::all(), BaseValidator::SIGNIN);
        if ($v->failed()) {
            return Redirect::guest('/')->with('message', 'Invalid credentials');
        }

        $credentials = [
            'email'    => Input::get('email'),
            'password' => Input::get('password')
        ];

        if (Auth::attempt($credentials, true)) {
            Session::put('time-offset', (int)Input::get('time-offset', 0));
            return Redirect::to('coverage');
        } else {
            $message = "Authentication attempt failed";
            Log::warning($message, ['email' => Input::get('email')]);
            return Redirect::guest('/')->with('message', $message);
        }
    }

    /**
     * logout of system
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        Auth::logout();
        return Redirect::guest('/');
    }

}