<?php
namespace App\Controllers;

use Auth;
use View;
use Redirect;
use Input;
use Response;
use URL;
use App\Models\KpiHeader;
use App\Models\KpiRecord;
use App\Models\Region;
use App\Models\Coverage;

class KpiController extends BaseController
{
    //@todo Убрать говно
	public function index()
    {
        $year = Input::exists('year') ? intval(Input::get('year')) : 2016;
    	$regions = Region::all();
    	$currentMonth = $year < intval(date('Y')) ? 12 : date('n');

    	$data = [];
    	$data['global']['headers'] = KpiHeader::firstOrCreate(['region_id' => null, 'year' => $year]);
        foreach ($regions as $region) {
    		$data[$region->name]['headers'] = KpiHeader::firstOrCreate(['region_id' => $region->id, 'year' => $year]);
    	}


    	for($i = 1; $i <= $currentMonth; $i++) {
    		$date = date($year.'-' . $i . '-01');
    		$kpi = KpiRecord::firstOrCreate([
    			'region_id' => null, 
    			'date' => $date
    		])->toArray();
    		$data['global']['records'][$i] = array_merge($kpi, Coverage::kpiDataByRegion($date));

    		foreach ($regions as $region) {
	    		$kpiByRegion = KpiRecord::firstOrCreate([
	    			'region_id' => $region->id, 
	    			'date' => $date
	    		])->toArray();
	    		$data[$region->name]['records'][$i] = 
	    			array_merge($kpiByRegion, Coverage::kpiDataByRegion($date, $region->id));
	    	}
    	}

        return View::make('root', ['semanticVersion' => self::SEMANTIC_UI_VERSION_NEW])
        	->nest('template', 'kpi', [
        		'data' 			=> $data,
        		'currentUser' 	=> Auth::user(),
        		'regions'		=> $regions
        	])
        	->nest('customAssets', 'assets.kpi');
    }

    public function saveHeaders()
    {
    	if (!Auth::user()->is_superadmin) {
            return Response::json(["error" => true, 'Access denied']);
        }

    	$region_id = Input::get('region_id');
    	$kpi = KpiHeader::firstOrNew(['region_id' => $region_id ?: null, 'year' => date('Y')]);
    	$kpi->net_effect =  Input::get('net_effect', '');
    	$kpi->impact_index = Input::get('impact_index', '');
    	$kpi->negative = Input::get('negative', '');
    	$kpi->company_mindshare = Input::get('company_mindshare', '');
    	$kpi->b2b_mindshare = Input::get('b2b_mindshare', '');
    	$kpi->b2c_mindshare = Input::get('b2c_mindshare', '');

    	if (!$kpi->save()) {
    		return Response::json(['error' => true]);
    	}
    }

    public function getHeadersByRegion()
    {
    	if (!Auth::user()->is_superadmin) {
            return Response::json(["error" => true, 'Access denied']);
        }

    	$id = Input::get('id');
    	$data = KpiHeader::where('region_id', '=', $id ?: null)->where('year', '=', date('Y'))->first();

    	return json_encode($data);
    }

    public function saveRecords()
    {
    	if (!Auth::user()->is_superadmin) {
            return Response::json(["error" => true, 'Access denied']);
        }

    	$region_id = Input::get('region_id');
    	$date = Input::get('date');
    	$kpi = KpiRecord::firstOrNew(['region_id' => $region_id ?: null, 'date' => $date]);
    	$kpi->company_mindshare 	= Input::get('company_mindshare', '');
    	$kpi->b2b_mindshare 		= Input::get('b2b_mindshare', '');
    	$kpi->b2c_mindshare 		= Input::get('b2c_mindshare', '');
    	$kpi->status 				= Input::get('status') ? KpiRecord::STATUS_ACTIVE : KpiRecord::STATUS_HIDDEN;
    	$kpi->is_net_effect 		= (bool)Input::get('is_net_effect');
    	$kpi->is_impact_index 		= (bool)Input::get('is_impact_index');
    	$kpi->is_negative 			= (bool)Input::get('is_negative');
    	$kpi->is_company_mindshare  = (bool)Input::get('is_company_mindshare');
    	$kpi->is_b2b_mindshare 		= (bool)Input::get('is_b2b_mindshare');
    	$kpi->is_b2c_mindshare 		= (bool)Input::get('is_b2c_mindshare');


    	if (!$kpi->save()) {
    		return Response::json(['error' => true]);
    	}
    }

    public function getRecordsByRegion()
    {
    	if (!Auth::user()->is_superadmin) {
            return Response::json(["error" => true, 'Access denied']);
        }

    	$id = Input::get('id');
    	$date = Input::get('date');
    	$data = KpiRecord::where('region_id', '=', $id ?: null)->where('date', '=', $date)->first();

    	return Response::json($data);
    }

}