<?php
namespace App\Controllers;

use App\Models\Region;
use App\Models\User;
use Response;
use Input;
use App\Models;
use Auth;
use View;

/**
 * Class RegionController
 */
class RegionController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Response::json([
            'regions' => Region::all(),
            'debug'   => [
                'request' => Input::all()
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        $name = Input::get('name');
        if (empty($name)) {
            return Response::json([]);
        }
        $region       = Region::findOrNew(Input::get('id', 0));
        $region->name = str_replace("_", " ", $name);
        $region->save();
        return Response::json($region->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }
        try {
            Region::destroy($id);
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }

        return Response::json(['error' => false]);
    }

    /**
     * Restore the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function restore($id)
    {
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        try {
            $region = Region::onlyTrashed()->find($id);
            if ($region) {
                $region->deleted_at = NULL;
                $region->save();
            }
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }

        return Response::json(['error' => false]);
    }

    /**
     * get Regions
     *
     * @return mixed
     */
    public function getRegions()
    {
        $onlyDeleted = filter_var(Input::get('only_deleted'), FILTER_VALIDATE_BOOLEAN);
        $regions = $onlyDeleted  ? Region::onlyTrashed()->orderBy('name') : Region::orderBy('name');
        $view = $onlyDeleted ? 'admin.lists.deleted._regions_list' : 'admin.lists._regions_list';

        return View::make($view, [
            'regions' => $regions->get(),
        ])->render();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @return Response
     */
    public function destroySomeRecords()
    {
        $ids = explode(',', Input::get('ids'));
        
        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            return Response::json(["error" => true, 'Access denied']);
        }

        try {
            Region::destroy($ids);
        } catch (\Exception $e) {
            return Response::json(['error' => true]);
        }
        
        return Response::json(['error' => false]);
    }

}