<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <title>Prometheus: PR Measurement System</title>

    <link rel="stylesheet" type="text/css" href="/css/styles.css">
    <link rel="stylesheet" type="text/css" href="/css/additional.css">
    <link rel="stylesheet" type="text/css" href="/css/new-styles.css">
    <link rel="stylesheet" type="text/css" href="/vendor/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="/vendor/DataTables/extensions/FixedColumns/css/dataTables.fixedColumns.css">
    <link rel="stylesheet" type="text/css" href="/vendor/DataTables/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="/css/admin-form.css">
    <link rel="stylesheet" type="text/css" href="/css/charts-pdf.css">

    <script src="/vendor/jquery.js"></script>
    <script src="/vendor/stickyfloat.js"></script>
    <script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/vendor/jquery.shorten.js"></script>
    <? if (empty($semanticVersion) || $semanticVersion == App\Controllers\BaseController::SEMANTIC_UI_VERSION_OLD): ?>
        <script src="/vendor/semantic-ui-stable/semantic.min.js"></script>
    <? else: ?>
        <script src="/vendor/semantic-ui-stable-2/semantic.min.js"></script>
    <? endif; ?>
    <script src="/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="/vendor/jquery-address/jQuery.address.js"></script>
    <script type="text/javascript" src="/js/classie.js"></script>
    <?= isset($customAssets) ? $customAssets : '' ?>
    <script type="text/javascript" src="/js/site.js"></script>
    <script src="/vendor/highcharts/highcharts.js"></script>
    <!--Fix canvas-tool.js error "Cannot read property 'prototype' of undefined"-->
    <script>
        if (Highcharts != null && Highcharts.CanVGRenderer == null) {
            Highcharts.CanVGRenderer = {};
        }
    </script>
    <script src="/vendor/highcharts/highcharts-more.js"></script>
    <script src="/vendor/highcharts/no-data-to-display.js"></script>
    <script src="/vendor/highcharts/modules/export/canvas-tools.js"></script>
    <script src="/vendor/highcharts/modules/export/exporting.js"></script>
    <script src="/vendor/highcharts/modules/export/export-csv.js"></script>
    <script src="/vendor/highcharts/modules/export/highcharts-export-clientside.js"></script>
    <script src="/vendor/highcharts/modules/data.js"></script>
    <script src="/vendor/d3.min.js"></script>
    <script src="/vendor/d3.tip.js"></script>
    <script src="/vendor/angular.js"></script>
    <script src="/js/clipboard.js"></script>
    <? if (empty($publicReport) || !$publicReport): ?>
        <script src="/js/ZeroClipboard/ZeroClipboard.js"></script>
        <script src="/js/highcharts-init.js"></script>
    <? else: ?>
        <script src="/js/highcharts-init-public.js"></script>
    <? endif; ?>
    <script src="/vendor/DataTables/js/jquery.dataTables.js"></script>
    <script src="/vendor/DataTables/extensions/FixedColumns/js/dataTables.fixedColumns.js"></script>
    <script src="/js/charts-app.js"></script>
    <script src="/js/api-factory.js"></script>
    <script src="/js/directive.js"></script>
    <script src="/js/numeral.min.js"></script>
</head>
<body id="page-body">
    <img src="/img/pr-horizontal-logo.png" alt="Prometheus | PR Measurement System"/>
    <div>
        <?= isset($template) ? $template : '' ?>
    </div>
</body>
</html>