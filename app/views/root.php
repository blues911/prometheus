<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="is_admin" content="<?= (Auth::user() && Auth::user()->role == App\Models\User::ROLE_ADMINISTRATOR) ? 'yes' : 'no' ?>">
    <title>Prometheus: PR Measurement System</title>

    <link
        href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=cyrillic-ext,latin-ext,latin,cyrillic'
        rel='stylesheet' type='text/css'>

    <? if (empty($semanticVersion) || (!empty($jsOnly)) || $semanticVersion == App\Controllers\BaseController::SEMANTIC_UI_VERSION_OLD): ?>
        <link rel="stylesheet" type="text/css" href="/vendor/semantic-ui-stable/semantic.min.css">
    <? else: ?>
        <link rel="stylesheet" type="text/css" href="/vendor/semantic-ui-stable-2/semantic.min.css">
    <? endif; ?>
    <link rel="stylesheet" type="text/css" href="/css/styles.css">
    <link rel="stylesheet" type="text/css" href="/css/additional.css">
    <link rel="stylesheet" type="text/css" href="/css/new-styles.css">
    <link rel="stylesheet" type="text/css" href="/vendor/bootstrap-datepicker/css/datepicker3.css">
    <link rel="stylesheet" type="text/css" href="/vendor/jquery-ui/jquery-ui.min.css">

    <script src="/vendor/jquery.js"></script>
    <script src="/vendor/underscore-min.js"></script>
    <script src="/vendor/stickyfloat.js"></script>
    <script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/vendor/jquery.shorten.js"></script>
    <? if (empty($semanticVersion) || $semanticVersion == App\Controllers\BaseController::SEMANTIC_UI_VERSION_OLD): ?>
        <script src="/vendor/semantic-ui-stable/semantic.min.js"></script>
    <? else: ?>
        <script src="/vendor/semantic-ui-stable-2/semantic.min.js"></script>
    <? endif; ?>
    <script src="/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="/vendor/jquery-address/jQuery.address.js"></script>

    <script type="text/javascript" src="/js/classie.js"></script>
    <?= isset($customAssets) ? $customAssets : '' ?>
    <script type="text/javascript" src="/js/site.js"></script>
</head>
<body id="page-body">
<?php echo View::make('components.notification'); ?>
<div class="ui padded grid">
    <?php if (Auth::user() || !empty($publicReport)) {
        $options = !empty($publicReport) ? ['hideMenu' => $publicReport] : [];
        echo View::make('components.header', $options);
    } ?>
    <div class="sixteen wide column content-column">
        <?= isset($template) ? $template : '' ?>
    </div>
</div>
</body>
</html>
