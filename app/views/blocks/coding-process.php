<form class="ui equal width form" id="coding-process-form" data-active-years='<?=$activeYears?>'>
    <div class="ui three column grid no-margin-bottom">
        <div class="field column no-margin-bottom">
            <div class="ui right icon input coding-year">
                <input type="text" id="coding-year" name="year" value="<?=date("Y")?>">
                <i class="calendar icon add-on"></i>
            </div>
        </div>
        <div class="field column no-margin-bottom">
            <a href="#" class="ui green button icon add-coding-year" title="Add"><i class="checkmark icon"></i></a>
        </div>
    </div>
    <div id="coding-years-list">
        <?php if ($data) : ?>
            <?php foreach ($data as $item) :
                $year = $item->year;
                $months = json_decode($item->months);
            ?>
                <div class="ui form coding-year-form" data-year="<?=$year?>">
                    <div class="ui three column grid no-margin-bottom">
                        <div class="field column no-margin-bottom">
                            <div class="ui right icon input coding-date">
                                <input type="text" name="coding_data[<?=$year?>][year]" value="<?=$year?>" readonly>
                            </div>
                        </div>
                        <div class="field column no-margin-bottom">
                            <a href="#" class="ui button icon blue handle-coding-year" title="Settings"><i class="setting icon"></i></a>
                            <a href="#" class="ui button icon red delete-coding-year" title="Delete"><i class="remove icon"></i></a>
                        </div>
                    </div>
                    <div class="ui equal width form coding-months" style="display: none;">
                        <div class="grouped fields">
                            <?php foreach ($months as $month => $status) : ?>
                                <div class="inline field">
                                    <div class="ui checkbox">
                                        <input type="checkbox" id="<?=$year?>_<?=$month?>" name="coding_data[<?=$year?>][months][<?=$month?>]" <?= ($status == 1) ? 'checked' : '' ?>>
                                        <label for="<?=$year?>_<?=$month?>"><?=ucfirst($month)?></label>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <br/>
                    </div>
                </div>
          <?php endforeach; ?>
      <?php endif; ?>
    </div>
    <input type="hidden" name="deleted_years" value="[]">
</form>