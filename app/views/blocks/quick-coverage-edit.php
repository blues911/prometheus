<script type="text/javascript">
    var speakers        = <?=json_encode($speakersData)?>;
    var speakers_quoted = <?=json_encode($speakersQuoted)?>;
    var news            = <?=json_encode($newsData)?>;
    var news_breaks     = <?=json_encode($newsBreaks)?>;
</script>

<form class="ui equal width form">
    <input type="hidden" disabled="disabled" name="id" id="id" value="<?= $coverage->id ?>">

    <? if ($type == 'edit-analytics'): ?>
        <div class="ui three column grid">
            <div class="field column">
                <label>Visibility</label>

                <div class="ui selection dropdown">
                    <input type="hidden" name="visibility" value="<?= $coverage->visibility ?>">

                    <div class="default text"><?= $coverage->visibility ?></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <? foreach (\App\Models\Coverage::getFieldValues("visibility") as $key => $value): ?>
                            <div class="item" data-value="<?= $key ?>"><?= $value ?></div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="field column">
                <label>Desired Article</label>

                <div class="ui selection dropdown">
                    <input type="hidden" name="desired_article" value="<?= (int)$coverage->desired_article ?>">

                    <div class="default text"></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <? foreach (\App\Models\Coverage::getFieldValues("desired_article") as $key => $value): ?>
                            <div class="item" data-value="<?= $key ?>"><?= $value ?></div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="field column">
                <label>Image</label>

                <div class="ui selection dropdown">
                    <input type="hidden" name="image" value="<?= $coverage->image ?>">

                    <div class="default text"><?= $coverage->image ?></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <? foreach (\App\Models\Coverage::getFieldValues("image") as $key => $value): ?>
                            <div class="item" data-value="<?= $key ?>"><?= $value ?></div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="field">
            <label>URL (if available)</label>
            <input type="text" name="url" placeholder="http://" value="<?= $coverage->url ?>">
        </div>
    <? endif; ?>

    <? if ($type == 'edit-general'): ?>
        <div class="ui one column grid no-margin-bottom">
            <div class="field column edit-general-form">
                <input type="hidden" name="media_list"     value="<?= $coverage->media_list ?>">
                <input type="hidden" name="media_category" value="<?= $coverage->media_category ?>">
                <input type="hidden" name="media_type"     value="<?= $coverage->media_type ?>">
                <input type="hidden" name="reach"          value="<?= $coverage->reach ?>">
                <label>Publication Name</label>
                <div class="ui selection dropdown media">
                    <input type="hidden" name="name" value="<?= $coverage->name ?>">

                    <div class="default text"><?= $coverage->name ?></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <?php foreach ($media as $item): ?>
                            <div class="item"
                                 data-value="<?= $item->name ?>"
                                 data-media='{"name": "<?= $item->name ?>", "list": "<?= $item->list ?>", "type": "<?= $item->type ?>", "category": "<?= $item->category ?>", "reach": "<?= $item->reach ?>"}'
                                 data-subcategories="<?= $item->subcategoriesIds() ?>"><?= $item->name ?></div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="ui one column grid no-margin-bottom">
            <div class="field column edit-general-form">
                <label>Media Subcategories</label>
                <div class="ui selection disabled multiple dropdown media-subcategories">
                    <?php if (isset($active_subcategories['list'])) : ?>
                        <input type="hidden" name="media_subcategories" value="<?= $active_subcategories['ids'] ?>">
                        <?php foreach ($active_subcategories['list'] as $subcategory) : ?>
                            <a class="ui label" data-value="<?= $subcategory['id'] ?>"><?= $subcategory['title'] ?><i class="delete icon"></i></a>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <input type="hidden" name="media_subcategories" value="">
                    <?php endif; ?>

                    <div class="default text"></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <?php foreach ($subcategories as $subcategory): ?>
                            <div class="item" data-value="<?= $subcategory->id ?>"><?= $subcategory->title ?></div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>        
        <div class="ui one column grid">
            <div class="field column">
                <label>Comment</label>
                <textarea name="comments"><?= $coverage->comments ?></textarea>
            </div>
        </div>
    <? endif; ?>

    <? if ($type == 'edit-publication'): ?>
        <div class="ui three column grid no-margin-bottom">
            <div class="field column no-margin-bottom">
                <label for="date">Date</label>

                <div class="ui right labeled icon input date">
                    <input type="text" id="date" name="date" value="<?= date('Y-m-d', strtotime($coverage->date)) ?>">
                    <i class="calendar icon add-on"></i>
                </div>
            </div>

            <div class="field column no-margin-bottom">
                <label>Region</label>

                <div class="ui selection dropdown region-list <?= (Auth::user()->role === App\Models\User::ROLE_USER ? 'disabled' : '' )?>">
                    <input type="hidden" name="<?= (Auth::user()->role === App\Models\User::ROLE_USER ? '' : 'region_id' )?>" value="<?= $coverage->region->id ?>">
                    <div class="default text"><?= $coverage->region->name ?></div>
                    <i class="dropdown icon"></i>
                    <div class="menu">
                        <?php foreach ($regions as $region): ?>
                            <div class="item" data-value="<?= $region->id ?>"><?= $region->name ?></div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

            <div class="field column no-margin-bottom">
                <label>Country</label>

                <div class="ui selection dropdown countries-list">
                    <input type="hidden" name="country_id" value="<?= $coverage->country->id ?>">

                    <div class="default text"><?= $coverage->country->name ?></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <?php foreach ($countries as $country): ?>
                            <div class="item" data-value="<?= $country->id ?>"><?= $country->name ?></div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="ui two column grid">
            <div class="field column no-margin-bottom">
                <label for="author">Author</label>
                <input type="text" name="author" id="author" value="<?= $coverage->author ?>">
            </div>
            <div class="field column no-margin-bottom">
                <label for="headline">Headline</label>
                <input type="text" name="headline" id="headline" value="<?= $coverage->headline ?>">
            </div>
        </div>
    <? endif; ?>

    <? if ($type == 'edit-mentioned'): ?>
        <div class="ui one column grid">
            <div class="field column speakers_quoted_block no-padding-bottom no-margin-bottom">
                <label for="speakers_quoted_control">Speakers quoted (Format: Firstname Lastname)</label>
                <ul name="speakers_quoted_control" class="seven wide" id="speakers_quoted_control"></ul>
                <input type="hidden" name="speakers_quoted" id="speakers_quoted" value="">
            </div>
            <div class="field column no-padding-top no-margin-bottom">
                <div class="ui pointing label">For example: Alexander Gostev. 
                    If you need to add several names please use comma to separate them 
                    (for example: Alexander Gostev, Eugene Kaspersky).</div>
            </div>

            <div class="field column no-padding-bottom no-margin-bottom no-padding-top">
                <label for="third_speakers_quoted">3rd party speakers quoted</label>
                <input type="text" name="third_speakers_quoted" id="third_speakers_quoted"
                       value="<?= $coverage->third_speakers_quoted ?>">
            </div>
            
            <div class="ui two column grid">
                <div class="field column">
                    <label>Brand or product mention</label>

                    <div class="ui selection dropdown">
                        <input type="hidden" name="mention" value="<?= $coverage->mention ?>">

                        <div class="default text"><?= $coverage->mention ?></div>
                        <i class="dropdown icon"></i>

                        <div class="menu">
                            <div class="item" data-value="1">Headline</div>
                            <div class="item" data-value="0.9">Lead</div>
                            <div class="item" data-value="0.8">Body</div>
                            <div class="item" data-value="0.5">Context</div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="field column products no-padding-top no-margin-top">
                <label>Brand or product mention</label>
                <div class="grouped fields">
                    <?php foreach ($products as $product): ?>
                        <div class="inline field">
                            <div class="ui checkbox <?= in_array($product->id, $attachedProducts) ? 'checked' : '' ?>">
                                <input type="checkbox" id="ch-<?= $product->id ?>" name="products[<?= $product->id ?>]" data-model="products" value="<?= $product->id ?>"
                                <?= in_array($product->id, $attachedProducts) ? 'checked' : '' ?> >
                                <label for="ch-<?= $product->id ?>"><?= $product->name ?></label>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <? endif; ?>

    <? if ($type == 'edit-events'): ?>
        <div class="ui one column grid">
            <div class="field column no-padding-bottom">
                <label for="event_name">Event Name</label>
                <input type="text" name="event_name" id="event_name" value="<?= $coverage->event_name ?>">
            </div>

            <div class="field column no-padding-top no-margin-bottom no-padding-bottom">
                <label for="news_break">Newsbreak</label>
                <ul name="news_break_control" class="seven wide" id="news_break_control"></ul>
                <input type="hidden" name="news_break" id="news_break" value="">
            </div>

            <div class="field column products no-padding-top no-margin-bottom no-padding-bottom no-margin-top">
                <label>Themes / Campaigns</label>
                <div class="grouped fields">
                    <? foreach ($campaigns as $campaign): ?>
                        <div class="inline field">
                            <div class="ui checkbox <?= in_array($campaign->id, $attachedCampaigns) ? 'checked' : '' ?>">
                                <input type="checkbox" id="cam-<?= $campaign->id ?>" name="campaigns[<?= $campaign->id ?>]" data-model="campaigns" value="<?= $campaign->id ?>"
                                <?= in_array($campaign->id, $attachedCampaigns) ? 'checked' : '' ?> >
                                <label for="cam-<?= $campaign->id ?>"><?= $campaign->name ?></label>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
            <div class="field column business no-margin-bottom no-padding-bottom">
                <label>Business segment</label>
                <div class="grouped fields">
                    <? foreach ($business as $business_row): ?>
                        <div class="inline field">
                            <div class="ui checkbox <?= in_array($business_row->id, $attachedBusiness) ? 'checked' : '' ?>">
                                <input type="checkbox" id="bus-<?= $business_row->id ?>" name="business[<?= $business_row->id ?>]" data-model="business" value="<?= $business_row->id ?>"
                                    <?= in_array($business_row->id, $attachedBusiness) ? 'checked' : '' ?>>
                                <label for="bus-<?= $business_row->id ?>"><?= $business_row->name ?></label>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
                <div class="ui red pointing prompt label transition no-margin-top hidden">Business segment is requared</div>
            </div>
        </div>
    <? endif; ?>

    <? if ($type == 'edit-tonality'): ?>
        <div class="edit-tonality">
            <div class="ui one column grid no-margin-bottom">
                <div class="field column">
                    <label>Key message penetration</label>

                    <div class="ui selection dropdown">
                        <input type="hidden" name="key_message_penetration"
                               value="<?= $coverage->key_message_penetration ?>">

                        <div class="default text"><?= $coverage->key_message_penetration ?></div>
                        <i class="dropdown icon"></i>

                        <div class="menu">
                            <div class="item" data-value="present"><i class="custom-icon icon-positive" title="Positive"></i> Clearly present</div>
                            <div class="item" data-value="extent"><i class="custom-icon icon-neutral" title="Neutral"></i> Present to some extent</div>
                            <div class="item" data-value="absent"><i class="custom-icon icon-negative" title="Absent"></i> Absent</div>
                            <div class="item" data-value="counter"><i class="custom-icon icon-dead" title="Counter-message"></i> Counter message</div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="ui two column grid no-margin-top">
                <div class="field column no-padding-top no-margin-bottom">
                    <label>Header Tonality</label>

                    <div class="ui selection dropdown">
                        <input type="hidden" name="header_tonality" value="<?= $coverage->header_tonality ?>">

                        <div class="default text"><?= $coverage->header_tonality ?></div>
                        <i class="dropdown icon"></i>

                        <div class="menu">
                            <div class="item" data-value="0.5"><i class="custom-icon icon-negative" title="Negative"></i> Negative</div>
                            <div class="item" data-value="0.85"><i class="custom-icon icon-neutral" title="Neutral"></i> Neutral</div>
                            <div class="item" data-value="1"><i class="custom-icon icon-positive header_tonality" title="Positive"></i> Positive</div>
                        </div>
                    </div>
                </div>

                <div class="field column no-padding-top no-margin-bottom">
                    <label>Main Tonality</label>

                    <div class="ui selection dropdown">
                        <input type="hidden" name="main_tonality" value="<?= $coverage->main_tonality ?>">

                        <div class="default text"><?= $coverage->main_tonality ?></div>
                        <i class="dropdown icon"></i>

                        <div class="menu">
                            <div class="item" data-value="negative"><i class="custom-icon icon-negative" title="Negative"></i> Negative</div>
                            <div class="item" data-value="neutral"><i class="custom-icon icon-neutral" title="Neutral"></i> Neutral</div>
                            <div class="item" data-value="positive"><i class="custom-icon icon-positive header_tonality" title="Positive"></i> Positive</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ui one column grid no-margin-top comment">
                <div class="field column">
                    <label for="negative_explanation">Explanation of Negative Tonality</label>
                    <textarea rows="3" name="negative_explanation" id="negative_explanation"><?= trim($coverage->negative_explanation) ?></textarea>
                </div>
            </div>
        </div>
    <? endif; ?>
</form>