<div class="ui form">
    <form action="/user" method="post">
        <input type="hidden" name="id" value="">
        <div class="field">
            <div class="ui input">
                <input placeholder="User e-mail" type="text" name="email">
                <div class="ui corner label">
                    <i class="icon asterisk"></i>
                </div>
            </div>
        </div>
        <div class="field">
            <div class="ui input">
                <input placeholder="User password" type="password" class="password" name="password">
                <div class="ui corner label">
                    <i class="icon asterisk"></i>
                </div>
            </div>
        </div>
        <div class="field">
            <div class="ui checkbox">
                <input type="checkbox" class="show-password" name="show-password"/>
                <label>Show Password</label>
            </div>
        </div>
        <div class="field">
            <div class="ui input">
                <input placeholder="Full Name" type="text" name="name">

                <div class="ui corner label">
                    <i class="icon asterisk"></i>
                </div>
            </div>
        </div>
        <div class="field">
            <div class="ui fluid selection dropdown role">
                <div class="text" data-default="Select Role">Select Role</div>
                <i class="dropdown icon"></i>
                <input type="hidden" name="role">

                <div class="menu">
                    <div class="item" data-value="<?=App\Models\User::ROLE_ADMINISTRATOR?>">Admin</div>
                    <div class="item" data-value="<?=App\Models\User::ROLE_MANAGER?>">Manager</div>
                    <div class="item" data-value="<?=App\Models\User::ROLE_USER?>">User</div>
                </div>
            </div>
        </div>
        <div class="field">
            <div class="ui fluid selection dropdown region">
                <div class="text" data-default="Select Region">Select Region</div>
                <i class="dropdown icon"></i>
                <input type="hidden" name="region">

                <div class="menu">
                    <?php foreach ($regions as $region): ?>
                        <div class="item" data-value="<?= $region->id ?>"><?= $region->name ?></div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="field">
            <div class="ui fluid selection dropdown country">
                <div class="text" data-default="Select Country">Select Country</div>
                <i class="dropdown icon"></i>
                <input type="hidden" name="country">

                <div class="menu">
                    <?php foreach ($countries as $country): ?>
                        <div class="item" data-value="<?= $country->id ?>"><?= $country->name ?></div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="ui green submit button" id="save-user">Save</div>
        <div class="ui red cancel button">Cancel</div>
        <div class="ui error message"></div>
    </form>
</div>