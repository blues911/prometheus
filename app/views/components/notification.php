<div class="ui page dimmer" id="notification">
    <div class="content">
        <div class="center">
            <h2 class="ui inverted icon header">
                <i class="icon circular inverted emphasized green checkmark"></i>
                <span></span>
                <div class="sub header"></div>
            </h2>
        </div>
    </div>
</div>