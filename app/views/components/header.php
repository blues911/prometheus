<div class="ui row header-row">
    <div class="left floated four wide column">
        <h1 class="ui header">
            <a href="/"><img src="/img/pr-horizontal-logo.png" alt="Prometheus | PR Measurement System"/></a>
        </h1>
    </div>
    <? if (empty($hideMenu) || !$hideMenu || Auth::check()): ?>
        <div class="right floated right aligned twelve wide column">
            <?php if (Auth::check()): ?>
                <div style="color: white;">
                    <div class="ui dropdown settings-user">
                        You are logged in as&nbsp; <?= Auth::user()->name ?> <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item">
                                <div id="change-password"> Send a password </div>
                            </div>
                        </div>
                    </div>
                </div>
            <? endif; ?>
            <div class="ui compact menu main">
                <a class="item" href="/coverage">
                    <i class="icon table"></i> Coverage
                </a>
                <?php if (in_array($currentUser->role, [\App\Models\User::ROLE_ADMINISTRATOR, \App\Models\User::ROLE_MANAGER])): ?>
                <a class="item" href="/import">
                    <i class="icon upload"></i> Import
                    <div class="floating ui red label unread">
                        <?= count(\App\Models\SystemUser::getUnreadNotifications($currentUser->id)) ?>
                    </div>
                </a>
                <?php endif; ?>
                <a class="item" href="/export">
                    <i class="icon download"></i> Export
                </a>
                <?php if (in_array($currentUser->role, [\App\Models\User::ROLE_ADMINISTRATOR, \App\Models\User::ROLE_MANAGER])): ?>
                    <a class="item" href="/charts#executive_summary">
                        <i class="icon dashboard"></i> Reports
                    </a>
                <?php endif; ?>
                <a class="item" href="/kpi-dashboard">
                    <i class="block layout icon"></i> KPIs Dashboard
                </a>
                <?php if ($currentUser->role == \App\Models\User::ROLE_ADMINISTRATOR): ?>
                    <a class="item" href="/admin">
                        <i class="icon setting"></i> Manage
                    </a>
                <?php endif; ?>
                <a class="item" href="/help">
                    <i class="icon circle help"></i> Help
                </a>
                <a class="item red" href="/logout">
                    <i class="icon sign out"></i> Logout
                </a>
            </div>
        </div>

        <div class="ui modal email-modal-random-password">
            <i class="close icon"></i>
            <div class="header">Send a password</div>
            <div class="content">
                <div class="ui form">
                    <div class="field first-random-password">
                        <input name="email" type="text" id="input-email-random-password" disabled value="<?= Auth::check() ? Auth::user()->email : '' ?>">
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                    <div class="field second-random-password" style="display:none">
                        <p>We've sent a new password reset link to your email. Please, check it</p>
                    </div>
                </div>
            </div>
            <div class="actions">
                <div class="ui button first-random-passwordl">Cancel</div>
                <div class="positive ui button approve first-random-password" id="send-mail-random-password">Send</div>
                <div class="ui positive right labeled icon button second-random-password" style="display:none">
                    Ok<i class="checkmark icon"></i>
                </div>
            </div>
        </div>

    <? endif; ?>
</div>


