<? $isPrivateReport = (boolean) (empty($publicReport) || !$publicReport); ?>
<script type="text/javascript">
    var region          = "<?=$filters['region_id']?>";
    var events          = <?=json_encode($eventData)?>;
    var news            = <?=json_encode($newsData)?>;
    var hash            = <?=json_encode(!empty($hash) ? $hash : "")?>;
    var products        = <?=json_encode($productData)?>;
    var regions         = <?=json_encode($regions)?>;
    var filters         = <?=json_encode($filters)?>;
    var countries       = <?=json_encode($countryData)?>;
    var campaigns       = <?=json_encode($campaignData)?>;
    var business        = <?=json_encode($businessData)?>;
    var speakers        = <?=json_encode($speakerData)?>;
    var reports         = <?=json_encode($reports)?>;
    var newsbreaks_list = <?=json_encode(implode(',', $top['newsbreaks']))?>;
    var campaigns_list  = <?=json_encode(implode(',', $top['campaigns']))?>;
    var products_list   = <?=json_encode(implode(',', $top['products']))?>;
    var speakers_list   = <?=json_encode(implode(',', $top['speakers']))?>;
    var events_list     = <?=json_encode(implode(',', $top['events']))?>;
    var isPrivateReport = <?=(int)$isPrivateReport?>;
    var is_pdf          = true;
</script>

<div ng-app="Reports" id="reports-page">
    <div class="ui top attached tabular menu charts ui one column centered grid" style="display: none;">
        <a class="active item" data-tab="executive_summary">Executive Summary</a>
        <a class="item" data-tab="general_information">General Information</a>
        <a class="item" data-tab="comparison">Comparison</a>
        <a class="item" data-tab="dynamics">Dynamics</a>
    </div>

    <div id="reports">
        <div data-tab="executive_summary" style="page-break-before:always;"
            class="ui bottom attached tab segment charts regional <?= App\Helpers\ArrayHelper::isPdfTabEmpty('executive_summary', $filters['charts']) ? '' : 'hide'?>">
            <h2>Executive Summary</h2>

            <div class="ui one column centered grid charts-nav-buttons">
                <div class="ui basic button teal"><?= $filters['filtered_by'] == 'net_effect' ? 'Net Effect' : 'Number of articles'?></div>
            </div>

            <div class="ui one column centered grid main-totals-menu">
                <span class="main-totals">Net Effect:&nbsp;<strong><span class="net_effect">-</span></strong></span>
                <span class="main-totals">Impact Index:&nbsp;<strong><span class="impact_index">-</span></strong></span>
                <span class="main-totals">Media Outreach:&nbsp;<strong><span class="media_outreach">-</span></strong></span>
                <span class="main-totals">Number of Articles:&nbsp;<strong><span class="coverage_count">-</span></strong></span>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-net-effect', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Net Effect</div>
                <center><div id="chart-net-effect" class="chart-nest pie"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('impact-index', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Impact index</div>
                <center><div id="impact-index" class="chart-nest pie"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-reach', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Media Outreach</div>
                <center><div id="chart-reach" class="chart-nest pie"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-articles', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Number of articles</div>
                <center><div id="chart-articles" class="chart-nest pie"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-main-tonality', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Main tonality</div>
                <center><div id="chart-main-tonality" class="chart-nest pie"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('key_meesages', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">KEY MESSAGES</div>
                <center><div id="key_meesages"></div></center>
                <center><div class="column hide" id="key_message_penetration">
                    <p class="no-bottom-margin font-weight-normal">Key message penetration</p>
                    <table id="message_penetration_table">
                        <thead></thead>
                        <tbody><tr></tr></tbody>
                    </table>
                </div></center>
            </div>

            <div class="ui one column centered grid <?= (empty($filters['charts']) || in_array('chart-indexes-comparison', $filters['charts'])) ? '' : 'hide'?>" 
                style="page-break-before:always;">
                <div class="column centered">
                    <div class="fifteen wide column chart-holder" id="chart-indexes-comparison">
                        <div class="ui pointing below label header">3 Indexes comparison</div>
                    </div>
                </div>
            </div>
            
            <div class="ui one column centered grid <?= (empty($filters['charts']) || in_array('chart-regional-statistics', $filters['charts'])) ? '' : 'hide'?>" 
                style="page-break-before:always;">
                <div class="column chart-holder">
                    <div class="ui pointing below label header">Kaspersky Lab Regional Statistics</div>
                    <?php for ($i = 0; $i < count($regionalKpi['labels']); $i++): ?>
                    <table id="kpi_table" <?= ($i != 0 && $i%2 == 0) ? 'style="page-break-before:always;"' : '' ?>>
                        <thead>
                            <tr>
                                <th style="min-width: 120px; max-width: 120px;"></th>
                                <?php foreach ($regionalKpi['labels'][$i] as $label): ?>
                                    <th><?= $label['name'] ?></th>
                                <?php endforeach; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Articles</td>
                                <?php foreach ($regionalKpi['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalKpi['articles'][$label['id']]) ? '-' : $regionalKpi['articles'][$label['id']] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Media Outreach</td>
                                <?php foreach ($regionalKpi['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalKpi['media_outreach'][$label['id']]) ? '-' : $regionalKpi['media_outreach'][$label['id']] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td >Impact Index</td>
                                <?php foreach ($regionalKpi['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalKpi['impact_index'][$label['id']]) ? '-' : round($regionalKpi['impact_index'][$label['id']], 2) ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Net Effect</td>
                                <?php foreach ($regionalKpi['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalKpi['net_effect'][$label['id']]) ? '-' : $regionalKpi['net_effect'][$label['id']] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr><td colspan="10">Media Category</td></tr>
                            <tr>
                                <td>General Interest</td>
                                <?php foreach ($regionalKpi['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalKpi['media_category'][$label['id']]['general']) ? '-' : $regionalKpi['media_category'][$label['id']]['general'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>IT</td>
                                <?php foreach ($regionalKpi['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalKpi['media_category'][$label['id']]['it']) ? '-' : $regionalKpi['media_category'][$label['id']]['it'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Business</td>
                                <?php foreach ($regionalKpi['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalKpi['media_category'][$label['id']]['business']) ? '-' : $regionalKpi['media_category'][$label['id']]['business'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Vertical Media</td>
                                <?php foreach ($regionalKpi['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalKpi['media_category'][$label['id']]['vertical']) ? '-' : $regionalKpi['media_category'][$label['id']]['vertical'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr><td colspan="10">Media List</td></tr>
                            <tr>
                                <td>Diamond</td>
                                <?php foreach ($regionalKpi['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalKpi['media_list'][$label['id']]['diamond']) ? '-' : $regionalKpi['media_list'][$label['id']]['diamond'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Platinum</td>
                                <?php foreach ($regionalKpi['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalKpi['media_list'][$label['id']]['platinum']) ? '-' : $regionalKpi['media_list'][$label['id']]['platinum'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Gold</td>
                                <?php foreach ($regionalKpi['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalKpi['media_list'][$label['id']]['gold']) ? '-' : $regionalKpi['media_list'][$label['id']]['gold'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr><td colspan="10">Main Tonality</td></tr>
                            <tr>
                                <td>Positive</td>
                                <?php foreach ($regionalKpi['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalKpi['main_tonality'][$label['id']]['positive']) ? '-' : $regionalKpi['main_tonality'][$label['id']]['positive'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Neutral</td>
                                <?php foreach ($regionalKpi['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalKpi['main_tonality'][$label['id']]['neutral']) ? '-' : $regionalKpi['main_tonality'][$label['id']]['neutral'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Negative</td>
                                <?php foreach ($regionalKpi['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalKpi['main_tonality'][$label['id']]['negative']) ? '-' : $regionalKpi['main_tonality'][$label['id']]['negative'] ?></td>
                                <?php endforeach; ?>
                            </tr>

                        </tbody>
                    </table>
                    <?php endfor; ?>
                </div>
            </div>

            <div class="ui one column centered grid <?= (empty($filters['charts']) || in_array('chart-product-mentions', $filters['charts'])) ? '' : 'hide'?>" 
                style="page-break-before:always;">
                <div class="column chart-holder">
                    <div class="ui pointing below label header">Articles with Product Mentions</div>
                    <?php for ($i = 0; $i < count($regionalProductMentions['labels']); $i++): ?>
                    <table id="product_mentions_table"  <?= ($i != 0 && $i%2 == 0) ? 'style="page-break-before:always;"' : '' ?>>
                        <thead>
                            <tr>
                                <th style="min-width: 120px; max-width: 120px;"></th>
                                <?php foreach ($regionalProductMentions['labels'][$i] as $label): ?>
                                    <th><?= $label['name'] ?></th>
                                <?php endforeach; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Number</td>
                                <?php foreach ($regionalProductMentions['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalProductMentions['articles'][$label['id']]) ? '-' : $regionalProductMentions['articles'][$label['id']] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Percent</td>
                                <?php foreach ($regionalProductMentions['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalProductMentions['percent'][$label['id']]) ? '-' : $regionalProductMentions['percent'][$label['id']] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Impact Index</td>
                                <?php foreach ($regionalProductMentions['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalProductMentions['impact_index'][$label['id']]) ? '-' : round($regionalProductMentions['impact_index'][$label['id']], 2) ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Net Effect</td>
                                <?php foreach ($regionalProductMentions['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalProductMentions['net_effect'][$label['id']]) ? '-' : $regionalProductMentions['net_effect'][$label['id']] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr><td colspan="10">Media Category</td></tr>
                            <tr>
                                <td>General Interest</td>
                                <?php foreach ($regionalProductMentions['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalProductMentions['media_category'][$label['id']]['general']) ? '-' : $regionalProductMentions['media_category'][$label['id']]['general'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>IT</td>
                                <?php foreach ($regionalProductMentions['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalProductMentions['media_category'][$label['id']]['it']) ? '-' : $regionalProductMentions['media_category'][$label['id']]['it'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Business</td>
                                <?php foreach ($regionalProductMentions['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalProductMentions['media_category'][$label['id']]['business']) ? '-' : $regionalProductMentions['media_category'][$label['id']]['business'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Vertical Media</td>
                                <?php foreach ($regionalProductMentions['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalProductMentions['media_category'][$label['id']]['vertical']) ? '-' : $regionalProductMentions['media_category'][$label['id']]['vertical'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr><td colspan="10">Media List</td></tr>
                            <tr>
                                <td>Diamond</td>
                                <?php foreach ($regionalProductMentions['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalProductMentions['media_list'][$label['id']]['diamond']) ? '-' : $regionalProductMentions['media_list'][$label['id']]['diamond'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Platinum</td>
                                <?php foreach ($regionalProductMentions['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalProductMentions['media_list'][$label['id']]['platinum']) ? '-' : $regionalProductMentions['media_list'][$label['id']]['platinum'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Gold</td>
                                <?php foreach ($regionalProductMentions['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalProductMentions['media_list'][$label['id']]['gold']) ? '-' : $regionalProductMentions['media_list'][$label['id']]['gold'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr><td colspan="10">Main Tonality</td></tr>
                            <tr>
                                <td>Positive</td>
                                <?php foreach ($regionalProductMentions['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalProductMentions['main_tonality'][$label['id']]['positive']) ? '-' : $regionalProductMentions['main_tonality'][$label['id']]['positive'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Neutral</td>
                                <?php foreach ($regionalProductMentions['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalProductMentions['main_tonality'][$label['id']]['neutral']) ? '-' : $regionalProductMentions['main_tonality'][$label['id']]['neutral'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Negative</td>
                                <?php foreach ($regionalProductMentions['labels'][$i] as $label): ?>
                                    <td><?= empty($regionalProductMentions['main_tonality'][$label['id']]['negative']) ? '-' : $regionalProductMentions['main_tonality'][$label['id']]['negative'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                        </tbody>
                    </table>
                    <?php endfor; ?>
                </div>
            </div>

        </div>

        <div data-tab="general_information" style="page-break-before:always;"
            class="ui bottom attached tab segment charts general <?= App\Helpers\ArrayHelper::isPdfTabEmpty('general_information', $filters['charts']) ? '' : 'hide'?>">
            <h2>General Information</h2>
            <input type="hidden" id="general_information_filter" value="<?= $filters['filtered_by'] ?>" />
            
            <div class="ui one column centered grid charts-nav-buttons">
                <div class="ui basic button teal"><?= $filters['filtered_by'] == 'net_effect' ? 'Net Effect' : 'Number of articles'?></div>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-media-type', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Media types</div>
                <center><div id="chart-media-type" class="chart-nest pie"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-media-list', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Media lists</div>
                <center><div id="chart-media-list" class="chart-nest pie"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-media-category', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Media Categories</div>
                <center><div id="chart-media-category" class="chart-nest pie"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-subcategories', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Media Subcategories</div>
                <center><div id="chart-subcategories" class="chart-nest pie"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-mention-relation', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Products mentions</div>
                <center><div id="chart-mention-relation" class="chart-nest pie"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-speaker-quote', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Speakers quoted</div>
                <center><div id="chart-speaker-quote" class="chart-nest pie"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-third-speaker-quote', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">3rd party Speakers</div>
                <center><div id="chart-third-speaker-quote" class="chart-nest pie"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-products', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Products rating</div>
                <center><div id="chart-products" class="chart-nest bar"></div></center>
            </div>

            <div class="column <?= (empty($filters['charts']) || in_array('security_intelligence', $filters['charts'])) ? '' : 'hide'?>" style="display: none;">
                <div class="ui pointing below label header">Security Intelligence</div>
                <center><div id="security_intelligence" class="chart-nest bar"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-speakers-count', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Speakers rating</div>
                <center><div id="chart-speakers-count" class="chart-nest bar"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-newsbreaks', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Newsbreaks Rating</div>
                <center><div id="chart-newsbreaks" class="chart-nest bar"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-campaign', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Themes/Campaigns rating</div>
                <center><div id="chart-campaign" class="chart-nest bar"></div></center>
            </div>

            <div class="auto-break column chart-holder <?= (empty($filters['charts']) || in_array('chart-desired-article', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Desired/Undesired articles</div>
                <center><div id="chart-desired-article" class="chart-nest pie"></div></center>
            </div>

            <div class="column <?= (empty($filters['charts']) || in_array('chart-events', $filters['charts'])) ? '' : 'hide'?>"
                style="page-break-before:always;">
                <div class="ui pointing below label header">Events rating</div>
                <center><div id="chart-events" class="chart-nest bar"></div></center>
            </div>

            <div class="column <?= (empty($filters['charts']) || in_array('chart-top-story', $filters['charts'])) ? '' : 'hide'?>" 
                style="page-break-before:always;">
                <div class="ui pointing below label column header chart-holder">A Top Story &mdash; publication with Impact Index &ge; 0,75{{test}}</div>
                <div class="column top_story">
                    <ol class="nine wide column centered">
                        
                    </ol>
                </div>
            </div>
            
        </div>

        <div data-tab="dynamics" style="page-break-before:always;"
            class="ui bottom attached tab segment charts timeline <?= App\Helpers\ArrayHelper::isPdfTabEmpty('dynamics', $filters['charts']) ? '' : 'hide'?>">
            <input type="hidden" id="timeline_filter" value="<?= $filters['date_filter'] ?>" />
            <h2>Dynamics</h2>

            <div class="ui one column centered grid charts-nav-buttons">
                <div class="ui basic button teal"><?= $filters['date_filter'] == 'week' ? 'Weeks' : 'Months'?></div>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-net-dynamic', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Net Effect</div>
                <center><div id="chart-net-dynamic" class="chart-nest line"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-impact-dynamic', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Impact Index</div>
                <center><div id="chart-impact-dynamic" class="chart-nest line"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-reach-dynamic', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Media Outreach</div>
                <center><div id="chart-reach-dynamic" class="chart-nest line"></div></center>
            </div>

            <div class="auto-break column <?= (empty($filters['charts']) || in_array('chart-articles-dynamic', $filters['charts'])) ? '' : 'hide'?>">
                <div class="ui pointing below label header">Number of Articles</div>
                <center><div id="chart-articles-dynamic" class="chart-nest line"></div></center>
            </div>

        </div>

        <div class="ui bottom attached tab segment charts comparison <?= App\Helpers\ArrayHelper::isPdfTabEmpty('comparison', $filters['charts']) ? '' : 'hide'?>" 
            id="comparison-tab" data-tab="comparison" style="padding-top: 0px;">
            <h2 style="page-break-before:always;">Comparison</h2>

            <div class="ui one column centered grid">
                <div class="column chart-holder">
                    <div class="ui pointing below label header">Comparison ratings</div>
                    <?php for ($i = 0; $i < count($comparison['labels']); $i++): ?>
                    <table id="comparison_table" <?= ($i != 0 && $i%2 == 0) ? 'style="page-break-before:always;"' : '' ?>>
                        <thead>
                            <tr>
                                <th style="min-width: 120px; max-width: 120px;"></th>
                                <?php foreach ($comparison['labels'][$i] as $label): ?>
                                    <th title="<?= $label['name'] ?>"><?= $label['name'] ?></th>
                                <?php endforeach; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Articles</td>
                                <?php foreach ($comparison['labels'][$i] as $label): ?>
                                    <td><?= empty($comparison['articles'][$label['id']]) ? '-' : $comparison['articles'][$label['id']] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Media Outreach</td>
                                <?php foreach ($comparison['labels'][$i] as $label): ?>
                                    <td><?= empty($comparison['media_outreach'][$label['id']]) ? '-' : $comparison['media_outreach'][$label['id']] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td >Impact Index</td>
                                <?php foreach ($comparison['labels'][$i] as $label): ?>
                                    <td><?= empty($comparison['impact_index'][$label['id']]) ? '-' : round($comparison['impact_index'][$label['id']], 2) ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Net Effect</td>
                                <?php foreach ($comparison['labels'][$i] as $label): ?>
                                    <td><?= empty($comparison['net_effect'][$label['id']]) ? '-' : $comparison['net_effect'][$label['id']] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr><td colspan="10">Media Category</td></tr>
                            <tr>
                                <td>General Interest</td>
                                <?php foreach ($comparison['labels'][$i] as $label): ?>
                                    <td><?= empty($comparison['media_category'][$label['id']]['general']) ? '-' : $comparison['media_category'][$label['id']]['general'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>IT</td>
                                <?php foreach ($comparison['labels'][$i] as $label): ?>
                                    <td><?= empty($comparison['media_category'][$label['id']]['it']) ? '-' : $comparison['media_category'][$label['id']]['it'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Business</td>
                                <?php foreach ($comparison['labels'][$i] as $label): ?>
                                    <td><?= empty($comparison['media_category'][$label['id']]['business']) ? '-' : $comparison['media_category'][$label['id']]['business'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Vertical Media</td>
                                <?php foreach ($comparison['labels'][$i] as $label): ?>
                                    <td><?= empty($comparison['media_category'][$label['id']]['vertical']) ? '-' : $comparison['media_category'][$label['id']]['vertical'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr><td colspan="10">Media List</td></tr>
                            <tr>
                                <td>Diamond</td>
                                <?php foreach ($comparison['labels'][$i] as $label): ?>
                                    <td><?= empty($comparison['media_list'][$label['id']]['diamond']) ? '-' : $comparison['media_list'][$label['id']]['diamond'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Platinum</td>
                                <?php foreach ($comparison['labels'][$i] as $label): ?>
                                    <td><?= empty($comparison['media_list'][$label['id']]['platinum']) ? '-' : $comparison['media_list'][$label['id']]['platinum'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Gold</td>
                                <?php foreach ($comparison['labels'][$i] as $label): ?>
                                    <td><?= empty($comparison['media_list'][$label['id']]['gold']) ? '-' : $comparison['media_list'][$label['id']]['gold'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr><td colspan="10">Main Tonality</td></tr>
                            <tr>
                                <td>Positive</td>
                                <?php foreach ($comparison['labels'][$i] as $label): ?>
                                    <td><?= empty($comparison['main_tonality'][$label['id']]['positive']) ? '-' : $comparison['main_tonality'][$label['id']]['positive'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Neutral</td>
                                <?php foreach ($comparison['labels'][$i] as $label): ?>
                                    <td><?= empty($comparison['main_tonality'][$label['id']]['neutral']) ? '-' : $comparison['main_tonality'][$label['id']]['neutral'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td>Negative</td>
                                <?php foreach ($comparison['labels'][$i] as $label): ?>
                                    <td><?= empty($comparison['main_tonality'][$label['id']]['negative']) ? '-' : $comparison['main_tonality'][$label['id']]['negative'] ?></td>
                                <?php endforeach; ?>
                            </tr>
                        </tbody>
                    </table>
                    <?php endfor; ?>
                </div>
            </div>
        </div>

    </div>
</div>