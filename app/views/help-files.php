<div class="ui page help-files">
    <div class="ui segment">
        <div class="ui grid">

            <?php if (Auth::user()->role === App\Models\User::ROLE_ADMINISTRATOR):?>
                <div class="help-row row" style="display: none;">
                    <div class="five wide column ui message">
                        <i class="close-title close icon"></i>
                        <p id="message-text"></p>
                    </div>
                    <div class="div-hidden" id="import-files">
                        <input type="file" name="file">
                    </div>
                </div>
            <?php endif; ?>

            <div class="row">
                <div class="ten wide column">
                    <?php foreach ($files as $file): ?>
                        <h2>
                            <div class="img-file">
                                <img src="img/<?=$file->type?>.png">
                            </div>
                            <div class="content-file">
                                <?= $file->name ?> <br/>
                                <a href="/help/download/id/<?=$file->id?>" class="ui teal button download"> Download </a>
                                <?php if (Auth::user()->role === App\Models\User::ROLE_ADMINISTRATOR):?>
                                    <div class="ui purple button import-file" file-id="<?=$file->id?>"> Import </div>
                                <?php endif; ?>
                            </div>
                        </h2>
                    <?php endforeach; ?>
                </div>
            </div>

        </div>
    </div>
</div>