<div class="ui page admin" id="export-page">

    <div class="ui tab segment bottom active" data-tab="import_history">
        <div class="ui one column relaxed grid export-history">
            <div class="column table-actions ui form">
                <div class="ui form">
                    <div class="fields">
                        <div id="datepicker-start" class="two wide field input-daterange" style="width: 135px !important;">
                            <div class="ui right labeled icon input first-block" title="Start date">
                                <input type="text" name="start" value="<?=$dateFrom?>" id="start_range">
                                <i class="calendar icon add-on start"></i>
                            </div>
                        </div>
                        <div id="datepicker-end" class="two wide field input-daterange second-block" style="width: 135px !important;">
                            <div class="ui right labeled icon input" title="End date">
                                <input type="text" name="end" value="<?=$dateTo?>" id="end_range">
                                <i class="calendar icon add-on end"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column" style="padding-top: 0px; margin-top: 35px;">
                <table class="ui fixed single line celled table" id="export-history-table" width="100%">
                    <thead>
                        <th class="col-file-type">Icon</th>
                        <th class="col-filename">Filename</th>
                        <th class="col-date">Date</th>
                        <th class="col-actions"></th>
                    </thead>
                </table>
            </div>
        </div>
    </div>

</div>
