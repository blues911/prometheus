<div class="ui two column relaxed grid">
    <div class="column">
        <div class="ui checkbox visible-archived">
            <input type="checkbox">
            <label>Show archive</label>
        </div>
        <h2>Business Segment List:</h2>
        <div id="business-list">
            <?= View::make('admin.lists._business_list', ['business' => $business])->render() ?>
        </div>
    </div>
    <div class="column add_business_block">
        <div class="ui labeled icon buttons toggle">
            <div class="ui teal button add">
                <i class="add sign box icon"></i>
                ADD BUSINESS SEGMENT
            </div>
        </div>
        <div class="ui labeled icon buttons">
            <div class="ui red button delete-selected" data-href="/business/some_remove">
                <i class="trash icon"></i>
                DELETE SELECTED
            </div>
        </div>
        <h2 class="toggle-header invisible">Add New Business segment:</h2>

        <div class="ui form invisible">
            <form action="/business" method="post">
                <input type="hidden" name="id" value=""/>
                <div class="field">
                    <div class="ui input">
                        <input placeholder="Business segment name" type="text" name="name">

                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui selection dropdown business-status">
                        <div class="default text" data-default="Select List">Select status</div>
                        <i class="dropdown icon"></i>
                        <input type="hidden" name="status" value="">

                        <div class="menu">
                            <div class="item" data-value="<?=App\Models\Business::STATUS_ACTIVE?>">Active</div>
                            <div class="item" data-value="<?=App\Models\Business::STATUS_ARCHIVED?>">Archived</div>
                        </div>
                    </div>
                </div>
                <div class="ui green submit button">Save</div>
                <div class="ui red cancel button">Cancel</div>
                <div class="ui error message"></div>
            </form>
        </div>
    </div>
</div>