<div class="ui two column relaxed grid">
    <div class="column">
        <h2>Speakers List:</h2>
        <div id="speakers-list" data-deleted="false">
            <?= View::make('admin.lists._speakers_list', ['speakers' => $speakers])->render() ?>
        </div>
    </div>
    <div class="column add_speaker_block">
        <div class="ui labeled icon buttons toggle">
            <div class="ui teal button add">
                <i class="add sign box icon"></i>
                ADD SPEAKER
            </div>
        </div>
        <div class="ui labeled icon buttons">
            <div class="ui red button delete-selected" data-href="/speaker/some_remove">
                <i class="trash icon"></i>
                DELETE SELECTED
            </div>
        </div>
        <h2 class="toggle-header invisible">Add New Speaker:</h2>

        <div class="ui form invisible">
            <form id="speaker-form" action="/speaker" method="post">
                <input type="hidden" name="id" value=""/>
                <div class="field">
                    <div class="ui input">
                        <input placeholder="Speaker name" type="text" name="name">

                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
                <div class="ui green submit button">Save</div>
                <div class="ui red cancel button">Cancel</div>
                <div class="ui error message"></div>
            </form>
        </div>
    </div>
</div>