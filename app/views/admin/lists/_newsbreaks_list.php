<?php
    if (!isset($limit)) {
        $limit = App\Models\NewsBreak::NEWSBREAKS_PAGE_SIZE_DEFAULT;
    }
?>
<div class="ui checkbox select-all"><input type="checkbox"></div>

<div class="ui circular labels">
  <a class="ui label <?= $limit == 25 ? 'teal' : '' ?>" id="teal-25">25</a>
  <a class="ui label <?= $limit == 50 ? 'teal' : '' ?>" id="teal-50">50</a>
  <a class="ui label <?= $limit == 100 ? 'teal' : '' ?>" id="teal-100">100</a>
  <a class="ui label <?= $limit == 'all' ? 'teal' : '' ?>" id="teal-all">all</a>
</div>
<div class="ui divided list newsbreaks-list">
    <?php foreach ($newsbreaks as $newsbreak) : ?>
        <div class="item <?= $newsbreak->status == \App\Models\NewsBreak::STATUS_ARCHIVED ? 'archived hidden-archived' : ''?>">
            <div class="left floated">
                <div class="ui checkbox">
                    <input type="checkbox">
                </div>
            </div>
            <div class="right floated">
                <a href="#" class="right floated ui delete button red" title="Delete newsbreak"><i class="delete icon"></i></a>
                    <span class="approve">
                        <a href="/newsbreak/<?= $newsbreak->id ?>" class="right floated ui yes button green">Yes</a>
                        <a href="#" class="right floated ui no button red">No</a>
                    </span>
            </div>
            <div class="content data"
                 data-id="<?= $newsbreak->id ?>"
                 data-status="<?= $newsbreak->status ?>"
                 data-name="<?= htmlspecialchars($newsbreak->name, ENT_QUOTES | ENT_SUBSTITUTE) ?>"
                 data-type="<?= $newsbreak->type ?>"
                 data-complete="<?= $newsbreak->complete ?>">
                <div class="header">
                    <?php if ($newsbreak->status == \App\Models\NewsBreak::STATUS_ARCHIVED): ?>
                        <i class="hide icon" title="Archive" style="font-size: 18px;"></i>
                    <?php endif; ?>
                    <a href="#" class="name"><?= htmlspecialchars($newsbreak->name, ENT_QUOTES | ENT_SUBSTITUTE) ?></a>
                    <?php if ($newsbreak->type == \App\Models\NewsBreak::TYPE_GLOBAL): ?>
                        <i title="Global" class="world icon"></i>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?php
    if ($limit !== 'all') {
        $newsbreaks->setBaseUrl('/newsbreaks/get-newsbreaks');
        echo $newsbreaks->links(); 
    }
?>