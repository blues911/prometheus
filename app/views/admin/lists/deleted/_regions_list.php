<div class="ui divided list regions-list-deleted">
    <?php foreach ($regions as $region) : ?>
        <div class="item">
            <div class="right floated">
                <a href="#" class="right floated ui restore button green" title="Restore region"><i class="refresh icon"></i></a>
                <span class="approve-restore">
                    <a href="/region/restore/<?= $region->id ?>" class="right floated ui yes button green">Yes</a>
                    <a href="#" class="right floated ui no button red">No</a>
                </span>
            </div>
            <div class="content data"
                 data-id="<?= $region->id ?>"
                 data-name="<?= $region->name ?>">
                <div class="header">
                    <a href="#" class="name"><?= $region->name ?></a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>