<div class="ui divided list products-list-deleted">
    <?php foreach ($products as $product) : ?>
        <div class="item <?= $product->status == \App\Models\Product::STATUS_ARCHIVED ? 'archived hidden-archived' : ''?>">
            <div class="left floated">
                <div class="ui checkbox">
                    <input type="checkbox">
                </div>
            </div>
            <div class="right floated">
                <a href="#" class="right floated ui restore button green" title="Restore product"><i class="refresh icon"></i></a>
                <span class="approve-restore">
                    <a href="/product/restore/<?= $product->id ?>" class="right floated ui yes button green">Yes</a>
                    <a href="#" class="right floated ui no button red">No</a>
                </span>
            </div>
            <div class="content data"
                 data-id="<?= $product->id ?>"
                 data-name="<?= $product->name ?>"
                 data-status="<?= $product->status ?>"
                 data-short-name="<?= $product->short_name ?>"
                 data-security-intelligence="<?= $product->is_security_intelligence ?>">
                <div class="header">
                    <?php if ($product->status == \App\Models\Product::STATUS_ARCHIVED): ?>
                        <i class="hide icon" title="Archive" style="font-size: 18px;"></i>
                    <?php endif; ?>
                    <a href="#" class="name"><?= $product->name ?> </a>
                    <?= $product->is_security_intelligence ? '<i title="Security Intelligence" class="protect icon"></i>' : '' ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>