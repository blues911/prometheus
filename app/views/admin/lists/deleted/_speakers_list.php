<?php
    if (!isset($limit)) {
        $limit = App\Models\Speaker::SPEAKERS_PAGE_SIZE_DEFAULT;
    }
?>
<div class="ui circular labels">
  <a class="ui label <?= $limit == 25 ? 'teal' : '' ?>" id="teal-25">25</a>
  <a class="ui label <?= $limit == 50 ? 'teal' : '' ?>" id="teal-50">50</a>
  <a class="ui label <?= $limit == 100 ? 'teal' : '' ?>" id="teal-100">100</a>
  <a class="ui label <?= $limit == 'all' ? 'teal' : '' ?>" id="teal-all">all</a>
</div>
<div class="ui divided list speakers-list-deleted">
    <?php foreach ($speakers as $speaker) : ?>
        <div class="item">
            <div class="right floated">
                <a href="#" class="right floated ui restore button green" title="Restore speaker"><i class="refresh icon"></i></a>
                <span class="approve-restore">
                    <a href="/speaker/restore/<?= $speaker->id ?>" class="right floated ui yes button green">Yes</a>
                    <a href="#" class="right floated ui no button red">No</a>
                </span>
            </div>
            <div class="content data"
                 data-id="<?= $speaker->id ?>"
                 data-name="<?= htmlspecialchars($speaker->name, ENT_QUOTES | ENT_SUBSTITUTE) ?>">
                <div class="header">
                    <a href="#" class="name">
                        <?= htmlspecialchars($speaker->name, ENT_QUOTES | ENT_SUBSTITUTE) ?>
                    </a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?php
    if ($limit !== 'all') {
        $speakers->setBaseUrl('/speakers/get-speakers');
        echo $speakers->fragment('speakers')->links(); 
    }
?>