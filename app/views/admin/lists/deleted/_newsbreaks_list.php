<?php
    if (!isset($limit)) {
        $limit = App\Models\NewsBreak::NEWSBREAKS_PAGE_SIZE_DEFAULT;
    }
?>
<div class="ui circular labels">
  <a class="ui label <?= $limit == 25 ? 'teal' : '' ?>" id="teal-25">25</a>
  <a class="ui label <?= $limit == 50 ? 'teal' : '' ?>" id="teal-50">50</a>
  <a class="ui label <?= $limit == 100 ? 'teal' : '' ?>" id="teal-100">100</a>
  <a class="ui label <?= $limit == 'all' ? 'teal' : '' ?>" id="teal-all">all</a>
</div>
<div class="ui divided list newsbreaks-list-deleted">
    <?php foreach ($newsbreaks as $newsbreak) : ?>
        <div class="item <?= $newsbreak->status == \App\Models\NewsBreak::STATUS_ARCHIVED ? 'archived hidden-archived' : ''?>">
            <div class="right floated">
                <a href="#" class="right floated ui restore button green" title="Restore newsbreak"><i class="refresh icon"></i></a>
                    <span class="approve-restore">
                        <a href="/newsbreak/restore/<?= $newsbreak->id ?>" class="right floated ui yes button green">Yes</a>
                        <a href="#" class="right floated ui no button red">No</a>
                    </span>
            </div>
            <div class="content data"
                 data-id="<?= $newsbreak->id ?>"
                 data-status="<?= $newsbreak->status ?>"
                 data-name="<?= htmlspecialchars($newsbreak->name, ENT_QUOTES | ENT_SUBSTITUTE) ?>"
                 data-type="<?= $newsbreak->type ?>"
                 data-complete="<?= $newsbreak->complete ?>">
                <div class="header">
                    <?php if ($newsbreak->status == \App\Models\NewsBreak::STATUS_ARCHIVED): ?>
                        <i class="hide icon" title="Archive" style="font-size: 18px;"></i>
                    <?php endif; ?>
                    <a href="#" class="name"><?= htmlspecialchars($newsbreak->name, ENT_QUOTES | ENT_SUBSTITUTE) ?></a>
                    <?php if ($newsbreak->type == \App\Models\NewsBreak::TYPE_GLOBAL): ?>
                        <i title="Global" class="world icon"></i>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?php
    if ($limit !== 'all') {
        $newsbreaks->setBaseUrl('/newsbreaks/get-newsbreaks');
        echo $newsbreaks->links(); 
    }
?>