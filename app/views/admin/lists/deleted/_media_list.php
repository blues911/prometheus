<div class="ui divided list media-list-deleted">
    <?php foreach ($media as $item) : ?>
        <div class="item">
            <div class="content data left floated"
                 data-id="<?= $item->id ?>"
                 data-name="<?= $item->name ?>"
                 data-reach="<?= $item->reach ?>"
                 data-region="<?= $item->region ? $item->region->id : '' ?>"
                 data-country="<?= $item->country ? $item->country->id : '' ?>"
                 data-list="<?= $item->list ?>"
                 data-type="<?= $item->type ?>"
                 data-category="<?= $item->category ?>"
                 data-subcategories="<?= $item->subcategoriesIds() ?>">
                <div class="header">
                    <a href="#" class="name media-list-item"><?= $item->name ?></a>
                </div>
            </div>
            <div class="right floated">
                <a href="#" class="right floated ui restore button green" title="Restore media"><i class="refresh icon"></i></a>
                <span class="approve-restore">
                    <a href="/media/restore/<?= $item->id ?>" class="right floated ui yes button green">Yes</a>
                    <a href="#" class="right floated ui no button red">No</a>
                </span>
            </div>
            <div class="content left floated region-country">
                <div class="left floated region"><?= $item->region->name ?></div>
                <div class="left floated country"><?= $item->country->name ?></div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?php
    $media->setBaseUrl('/media/get-media');
    echo $media->fragment('media')->links();
?>