<div class="ui divided list business-list-deleted">
    <?php foreach ($business as $business_row) : ?>
        <div class="item <?= $business_row->status == \App\Models\Business::STATUS_ARCHIVED ? 'archived hidden-archived' : ''?>">
            <div class="right floated">
                <a href="#" class="right floated ui restore button green" title="Restore business segment"><i class="refresh icon"></i></a>
                    <span class="approve-restore">
                        <a href="/business/restore/<?= $business_row->id ?>" class="right floated ui yes button green">Yes</a>
                        <a href="#" class="right floated ui no button red">No</a>
                    </span>
            </div>
            <div class="content data"
                  data-id="<?= $business_row->id ?>"
                  data-status="<?= $business_row->status ?>"
                  data-name="<?= $business_row->name ?>">
                <div class="header">
                    <?php if ($business_row->status == \App\Models\Business::STATUS_ARCHIVED): ?>
                        <i class="hide icon" title="Archive" style="font-size: 18px;"></i>
                    <?php endif; ?>
                    <a href="#" class="name"><?= $business_row->name ?></a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>