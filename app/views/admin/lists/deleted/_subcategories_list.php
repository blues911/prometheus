<div class="ui divided list subcategories-list-deleted">
    <?php foreach ($subcategories as $subcategory) : ?>
        <div class="item">
            <div class="right floated">
                <a href="#" class="right floated ui restore button green" title="Restore subcategory"><i class="refresh icon"></i></a>
                <span class="approve-restore">
                    <a href="/subcategory/restore/<?= $subcategory->id ?>" class="right floated ui yes button green">Yes</a>
                    <a href="#" class="right floated ui no button red">No</a>
                </span>
            </div>
            <div class="content data"
                 data-id="<?= $subcategory->id ?>"
                 data-name="<?= htmlspecialchars($subcategory->title, ENT_QUOTES | ENT_SUBSTITUTE) ?>">
                <div class="header">
                    <a href="#" class="name">
                        <?= htmlspecialchars($subcategory->title, ENT_QUOTES | ENT_SUBSTITUTE) ?>
                    </a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>