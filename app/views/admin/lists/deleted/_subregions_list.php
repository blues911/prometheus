<div class="ui divided list subregions-list-deleted">
    <?php foreach ($subregions as $subregion) : ?>
        <div class="item">
            <div class="right floated">
                <a href="#" class="right floated ui restore button green" title="Restore subregion"><i class="refresh icon"></i></a>
                <span class="approve-restore">
                    <a href="/subregion/restore/<?= $subregion->id ?>" class="right floated ui yes button green">Yes</a>
                    <a href="#" class="right floated ui no button red">No</a>
                </span>
            </div>
            <div class="content data"
                 data-id="<?= $subregion->id ?>"
                 data-name="<?= $subregion->name ?>"
                 data-region="<?= $subregion->region ? $subregion->region->id : '' ?>">
                <div class="header">
                    <a href="#" class="name"><?= $subregion->name ?></a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>