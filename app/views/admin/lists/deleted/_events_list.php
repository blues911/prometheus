<div class="ui divided list events-list-deleted">
    <?php foreach ($events as $event) : ?>
        <div class="item <?= $event->status == \App\Models\Event::STATUS_ARCHIVED ? 'archived hidden-archived' : ''?>">
            <div class="right floated">
                <a href="#" class="right floated ui restore button green" title="Restore event"><i class="refresh icon"></i></a>
                    <span class="approve-restore">
                        <a href="/event/restore/<?= $event->id ?>" class="right floated ui yes button green">Yes</a>
                        <a href="#" class="right floated ui no button red">No</a>
                    </span>
            </div>
            <div class="content data"
                 data-id="<?= $event->id ?>"
                 data-status="<?= $event->status ?>"
                 data-name="<?= $event->name ?>">
                <div class="header">
                    <?php if ($event->status == \App\Models\Event::STATUS_ARCHIVED): ?>
                        <i class="hide icon" title="Archive" style="font-size: 18px;"></i>
                    <?php endif; ?>
                    <a href="#" class="name"><?= $event->name ?></a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>