<div class="ui divided list campaigns-list-deleted">
    <?php foreach ($campaigns as $campaign) : ?>
        <div class="item <?= $campaign->status == \App\Models\Campaign::STATUS_ARCHIVED ? 'archived hidden-archived' : ''?>">
            <div class="left floated">
                <div class="ui checkbox">
                    <input type="checkbox">
                </div>
            </div>
            <div class="right floated">
                <a href="#" class="right floated ui restore button green" title="Restore campaign"><i class="refresh icon"></i></a>
                    <span class="approve-restore">
                        <a href="/campaign/restore/<?= $campaign->id ?>" class="right floated ui yes button green">Yes</a>
                        <a href="#" class="right floated ui no button red">No</a>
                    </span>
            </div>
            <div class="content data"
                 data-id="<?= $campaign->id ?>"
                 data-status="<?= $campaign->status ?>"
                 data-name="<?= $campaign->name ?>">
                <div class="header">
                    <?php if ($campaign->status == \App\Models\Campaign::STATUS_ARCHIVED): ?>
                        <i class="hide icon" title="Archive" style="font-size: 18px;"></i>
                    <?php endif; ?>
                    <a href="#" class="name"><?= $campaign->name ?></a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>