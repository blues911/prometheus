<div class="ui divided list countries-list-deleted">
    <?php foreach ($countries as $country) : ?>
        <div class="item">
            <div class="right floated">
                <a href="#" class="right floated ui restore button green" title="Restore country"><i class="refresh icon"></i></a>
                <span class="approve-restore">
                    <a href="/country/restore/<?= $country->id ?>" class="right floated ui yes button green">Yes</a>
                    <a href="#" class="right floated ui no button red">No</a>
                </span>
            </div>
            <div class="content data"
                 data-id="<?= $country->id ?>"
                 data-name="<?= $country->name ?>"
                 data-iso="<?= $country->iso_code ?>"
                 data-region="<?= $country->region ? $country->region->id : '' ?>"
                 data-subregion="<?= $country->subregion ? $country->subregion->id : 0 ?>">
                <div class="header">
                    <a href="#" class="name"><?= $country->name ?></a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>