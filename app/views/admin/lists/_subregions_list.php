<div class="ui checkbox select-all"><input type="checkbox"></div>

<div class="ui divided list subregions-list">
    <?php foreach ($subregions as $subregion) : ?>
        <div class="item">
            <div class="left floated">
                <div class="ui checkbox">
                    <input type="checkbox">
                </div>
            </div>
            <div class="right floated">
                <a href="#" class="right floated ui delete button red" title="Delete subregion"><i class="delete icon"></i></a>
                <span class="approve">
                    <a href="/subregion/<?= $subregion->id ?>" class="right floated ui yes button green">Yes</a>
                    <a href="#" class="right floated ui no button red">No</a>
                </span>
            </div>
            <div class="content data"
                 data-id="<?= $subregion->id ?>"
                 data-name="<?= $subregion->name ?>"
                 data-region="<?= $subregion->region ? $subregion->region->id : '' ?>">
                <div class="header">
                    <a href="#" class="name"><?= $subregion->name ?></a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>