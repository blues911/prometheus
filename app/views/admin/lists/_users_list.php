<?php
    if (!isset($limit)) {
        $limit = App\Models\SystemUser::USERS_PAGE_SIZE_DEFAULT;
    }
?>
<div class="ui circular labels">
  <a class="ui label <?= $limit == 25 ? 'teal' : '' ?>" id="teal-25">25</a>
  <a class="ui label <?= $limit == 50 ? 'teal' : '' ?>" id="teal-50">50</a>
  <a class="ui label <?= $limit == 100 ? 'teal' : '' ?>" id="teal-100">100</a>
  <a class="ui label <?= $limit == 'all' ? 'teal' : '' ?>" id="teal-all">all</a>
</div>
<div class="ui basic accordion users-list">
    <?php foreach ($users as $user) : ?>
        <div class="item">
            <div class="title">
                <i class="dropdown icon"></i>
                <a href="#" class="name"><?= $user->name ?></a>
                <a href="#" class="right floated ui delete button red" title="Delete user"><i class="delete icon"></i></a>
                <span class="approve">
                    <a href="/user/<?= $user->id ?>" class="right floated ui yes button green">Yes</a>
                    <a href="#" class="right floated ui no button red">No</a>
                </span>
                <div class="email"><?= $user->email ?></div>
            </div>
            <div class="content data"
                 data-id="<?= $user->id ?>"
                 data-name="<?= $user->name ?>"
                 data-email="<?= $user->email ?>"
                 data-role="<?= $user->role ?>"
                 data-region="<?= $user->region ? $user->region->id : '0' ?>"
                 data-country="<?= $user->country ? $user->country->id : '' ?>">
                <div class="ui label"><i class="user icon"></i><?= $user->role ?></div>
                <div class="ui label"><i
                        class="globe icon"></i><?= $user->region ? $user->region->name : 'Not assigned' ?>
                    /<?= $user->country ? $user->country->name : 'Not assigned' ?></div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?php
    if ($limit !== 'all') {
        $users->setBaseUrl('/users/get-users');
        echo $users->links(); 
    }
?>