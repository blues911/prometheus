<div class="ui checkbox select-all"><input type="checkbox"></div>

<div class="ui divided list subcategories-list">
    <?php foreach ($subcategories as $subcategory) : ?>
        <div class="item">
            <div class="left floated">
                <div class="ui checkbox">
                    <input type="checkbox">
                </div>
            </div>
            <div class="right floated">
                <a href="#" class="right floated ui delete button red" title="Delete subcategory"><i class="delete icon"></i></a>
                    <span class="approve">
                        <a href="/subcategory/<?= $subcategory->id ?>" class="right floated ui yes button green">Yes</a>
                        <a href="#" class="right floated ui no button red">No</a>
                    </span>
            </div>
            <div class="content data"
                 data-id="<?= $subcategory->id ?>"
                 data-name="<?= htmlspecialchars($subcategory->title, ENT_QUOTES | ENT_SUBSTITUTE) ?>">
                <div class="header">
                    <a href="#" class="name">
                        <?= htmlspecialchars($subcategory->title, ENT_QUOTES | ENT_SUBSTITUTE) ?>
                    </a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>