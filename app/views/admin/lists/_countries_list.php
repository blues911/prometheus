<div class="ui checkbox select-all"><input type="checkbox"></div>

<div class="ui divided list countries-list">
    <?php foreach ($countries as $country) : ?>
        <div class="item">
            <div class="left floated">
                <div class="ui checkbox">
                    <input type="checkbox">
                </div>
            </div>
            <div class="right floated">
                <a href="#" class="right floated ui delete button red" title="Delete country"><i class="delete icon"></i></a>
                <span class="approve">
                    <a href="/country/<?= $country->id ?>" class="right floated ui yes button green">Yes</a>
                    <a href="#" class="right floated ui no button red">No</a>
                </span>
            </div>
            <div class="content data"
                 data-id="<?= $country->id ?>"
                 data-name="<?= $country->name ?>"
                 data-iso="<?= $country->iso_code ?>"
                 data-region="<?= $country->region ? $country->region->id : '' ?>"
                 data-subregion="<?= $country->subregion ? $country->subregion->id : 0 ?>">
                <div class="header">
                    <a href="#" class="name"><?= $country->name ?></a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>