<div class="ui checkbox select-all"><input type="checkbox"></div>

<div class="ui divided list regions-list">
    <?php foreach ($regions as $region) : ?>
        <div class="item">
            <div class="left floated">
                <div class="ui checkbox">
                    <input type="checkbox">
                </div>
            </div>
            <div class="right floated">
                <a href="#" class="right floated ui delete button red" title="Delete region"><i class="delete icon"></i></a>
                <span class="approve">
                    <a href="/region/<?= $region->id ?>" class="right floated ui yes button green">Yes</a>
                    <a href="#" class="right floated ui no button red">No</a>
                </span>
            </div>
            <div class="content data"
                 data-id="<?= $region->id ?>"
                 data-name="<?= $region->name ?>">
                <div class="header">
                    <a href="#" class="name"><?= $region->name ?></a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>