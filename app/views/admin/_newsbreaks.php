<div class="ui two column relaxed grid">
    <div class="column">
        <div class="ui checkbox visible-archived">
            <input type="checkbox">
            <label>Show archive</label>
        </div>
        <h2>Newsbreaks List:</h2>
        <div id="newsbreaks-list">
            <?= View::make('admin.lists._newsbreaks_list', ['newsbreaks' => $newsbreaks])->render() ?>
        </div>
    </div>
    <div class="column add_newsbreak_block">
        <div class="ui labeled icon buttons toggle">
            <div class="ui teal button add">
                <i class="add sign box icon"></i>
                ADD NEWSBREAK
            </div>
        </div>
        <div class="ui labeled icon buttons">
            <div class="ui red button delete-selected" data-href="/newsbreak/some_remove">
                <i class="trash icon"></i>
                DELETE SELECTED
            </div>
        </div>
        <h2 class="toggle-header invisible">Add New Newsbreak:</h2>

        <div class="ui form invisible">
            <form id="newsbreak-form" action="/newsbreak" method="post">
                <input type="hidden" name="id" value=""/>
                <div class="field">
                    <div class="ui input">
                        <input placeholder="Newsbreak name" type="text" name="name">
                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui selection dropdown newsbreak-status">
                        <div class="default text" data-default="Select List">Select status</div>
                        <i class="dropdown icon"></i>
                        <input type="hidden" name="status" value="">
                        <div class="menu">
                            <div class="item" data-value="<?=App\Models\NewsBreak::STATUS_ACTIVE?>">Active</div>
                            <div class="item" data-value="<?=App\Models\NewsBreak::STATUS_ARCHIVED?>">Archived</div>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui selection dropdown newsbreak-type">
                        <div class="default text" data-default="Select List">Select type</div>
                        <i class="dropdown icon"></i>
                        <input type="hidden" name="type" value="">
                        <div class="menu">
                            <div class="item" data-value="<?=App\Models\NewsBreak::TYPE_GLOBAL?>">Global</div>
                            <div class="item" data-value="<?=App\Models\NewsBreak::TYPE_LOCAL?>">Local</div>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="inline field">
                        <div class="ui checkbox">
                            <input type="checkbox" id="complete" name="complete">
                            <label for="complete">Complete</label>
                        </div>
                    </div>
                </div>
                <div class="ui green submit button">Save</div>
                <div class="ui red cancel button">Cancel</div>
                <div class="ui error message"></div>
            </form>
        </div>
    </div>
</div>