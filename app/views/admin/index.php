<script type="text/javascript">
    var countries = <?=json_encode($countries)?>;
    var subregions = <?=json_encode($subregions)?>;
    var newsbreaksNames = <?=json_encode($newsbreaksNames)?>;
    var speakersNames = <?=json_encode($speakersNames)?>;
</script>

<div class="ui page admin">

    <div class="ui pointing secondary tabular menu" id="admin-tabs">
        <a class="item" data-tab="users">Users</a>
        <a class="item" data-tab="regions">Regions</a>
        <a class="item" data-tab="subregions">Subregions</a>
        <a class="item" data-tab="countries">Countries</a>
        <a class="item" data-tab="products">Products</a>
        <a class="item" data-tab="media">Media Lists</a>
        <a class="item" data-tab="events">Events</a>
        <a class="item" data-tab="campaigns">Themes/Campaigns</a>
        <a class="item" data-tab="business">Business Segment</a>
        <a class="item" data-tab="newsbreaks">Newsbreaks</a>
        <a class="item" data-tab="speakers">Speakers</a>
        <a class="item" data-tab="subcategories">Subcategories</a>
    </div>

    <?php foreach ($views as $view) : ?>
        <div class="ui tab segment manage <?= $view ?> loading" data-tab="<?= $view ?>">
            <div class="ui two column relaxed grid">
                <div class="column">
                    <div class="ui checkbox deleted-items" data-tab="<?= $view ?>">
                        <input type="checkbox" tabindex="0" class="hidden" value="0">
                        <label>Show only deleted</label>
                    </div>
                </div>
            </div>
            <?php echo View::make('admin._'.$view, $__data)->render(); ?>
        </div>
    <?php endforeach; ?>

    <?php echo View::make('components.notification'); ?>

    <div class="ui modal some-delete">
        <i class="close icon"></i>
        <div class="header">DELETE SOME ITEMS</div>
        <div class="content">
            <span class="selected-items-count"></span> item(s) will be permanently deleted and cannot be recovered. Are you sure?
        </div>
        <div class="actions">
            <div class="ui button cancel">Cancel</div>
            <div class="ui button approve">OK</div>
        </div>
    </div>

</div>