<?php use App\Models\Media; ?>
    <div class="ui form visible filter-form grid two column equal width" id="media_filter_form">
        <div class="fifteen wide column" id="media_filter_inputs">
            <div class="ui grid four column">
                <div class="four column field">
                    <label>Region</label>

                    <div class="ui selection dropdown" id="region_filter_dropdown" title="Filter by region">
                        <input type="hidden" name="region_id" id="region_id" value="">

                        <div class="default text">Choose region...</div>
                        <i class="dropdown icon"></i>

                        <div class="menu">
                            <div class="item" data-value="">All</div>
                            <?php foreach ($regions as $region): ?>
                                <div class="item" data-value="<?= $region->id ?>"><?= $region->name ?></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="four column field" id="country_filter_div">
                    <label>Country</label>

                    <div class="ui right labeled icon input" title="Filter by country">
                        <input type="hidden" id="country_id" value="">
                        <input type="text" name="country" id="country_filter" value="">
                    </div>
                </div>
                <div class="four column field" id="name_filter_div">
                    <label>Name</label>

                    <div class="ui right labeled icon input" title="Filter by name">
                        <input type="text" name="name" id="name_filter" value="">
                    </div>
                </div>
                <div class="four column field">
                    <label>Order by</label>

                    <div class="ui selection dropdown" id="order_by_dropdown" title="Sort by ...">
                        <input type="hidden" name="order_by" id="order_by_filter" value="">

                        <div class="default text">Choose field...</div>
                        <i class="dropdown icon"></i>

                        <div class="menu">
                            <?php foreach (Media::$orderList as $value => $name): ?>
                                <div class="item" data-value="<?= $value ?>"><?= $name ?></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="one wide column field buttons-container">
            <a class="ui teal button icon filter-button wide send-button" title="Apply media lists filter">
                <i class="filter icon"></i>
            </a>
        </div>
</div>
<div class="ui two column relaxed grid">
    <div class="column">
        <h2>Media List:</h2>

        <div id="media-list">
            <?= View::make('admin.lists._media_list', ['media' => $media])->render() ?>
        </div>
    </div>
    <div class="column add_media_block">
        <div class="ui labeled icon buttons toggle">
            <div class="ui teal button add">
                <i class="add sign box icon"></i>
                ADD MEDIA
            </div>
        </div>
        <div class="ui labeled icon buttons">
            <div class="ui red button delete-selected" data-href="/media/some_remove">
                <i class="trash icon"></i>
                DELETE SELECTED
            </div>
        </div>
        <h2 class="toggle-header invisible">Add New Media:</h2>

        <div class="ui form invisible">
            <form action="/media" method="post">
                <input type="hidden" name="id" value=""/>
                <div class="field">
                    <div class="ui input">
                        <input placeholder="Publication name" type="text" name="name">

                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input placeholder="Reach" type="text" name="reach">
                    </div>
                </div>
                <div class="field">
                    <div class="ui fluid selection dropdown region">
                        <div class="text" data-default="Select Region">Select Region</div>
                        <i class="dropdown icon"></i>
                        <input type="hidden" name="region">

                        <div class="menu">
                            <?php foreach ($regions as $region): ?>
                                <div class="item" data-value="<?= $region->id ?>"><?= $region->name ?></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui fluid selection dropdown country">
                        <div class="text" data-default="Select Country">Select Country</div>
                        <i class="dropdown icon"></i>
                        <input type="hidden" name="country">

                        <div class="menu">
                            <?php foreach ($countries as $country): ?>
                                <div class="item" data-value="<?= $country->id ?>"><?= $country->name ?></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui selection dropdown list-status">
                        <div class="default text" data-default="Select List">Select List</div>
                        <i class="dropdown icon"></i>
                        <input type="hidden" name="list" value="">

                        <div class="menu">
                            <div class="item" data-value="0.6">Gold</div>
                            <div class="item" data-value="0.8">Platinum</div>
                            <div class="item" data-value="1">Diamond</div>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui selection dropdown type">
                        <div class="default text" data-default="Select Type">Select Type</div>
                        <i class="dropdown icon"></i>
                        <input type="hidden" name="type" value="">

                        <div class="menu">
                            <div class="item" data-value="agency">Agency</div>
                            <div class="item" data-value="online">Online</div>
                            <div class="item" data-value="print">Print</div>
                            <div class="item" data-value="radio">Radio</div>
                            <div class="item" data-value="tv">TV</div>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui selection dropdown media-category">
                        <div class="default text" data-default="Select Category">Select Category</div>
                        <i class="dropdown icon"></i>
                        <input type="hidden" name="category" value="">

                        <div class="menu">
                            <div class="item" data-value="vertical">Vertical</div>
                            <div class="item" data-value="business">Business Media</div>
                            <div class="item" data-value="it">IT Media</div>
                            <div class="item" data-value="general">General Media</div>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui selection dropdown multiple media-subcategories">
                        <div class="default text" data-default="Select Subcategories">Select Subcategories</div>
                        <i class="dropdown icon"></i>
                        <input type="hidden" name="subcategories" value="">

                        <div class="menu">
                            <?php foreach ($subcategories as $subcategory): ?>
                                <div class="item" data-value="<?= $subcategory->id ?>"><?= $subcategory->title ?></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="ui green submit button">Save</div>
                <div class="ui red cancel button">Cancel</div>
                <div class="ui error message"></div>
            </form>
        </div>
    </div>
</div>