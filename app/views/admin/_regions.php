<div class="ui two column relaxed grid">
    <div class="column">
        <h2>Regions List:</h2>
        <div id="regions-list" data-deleted="false">
            <?= View::make('admin.lists._regions_list', ['regions' => $regions])->render() ?>
        </div>
    </div>
    <div class="column add_region_block">
        <div class="ui labeled icon buttons toggle">
            <div class="ui teal button add">
                <i class="add sign box icon"></i>
                ADD REGION
            </div> 
        </div>
        <div class="ui labeled icon buttons">
            <div class="ui red button delete-selected" data-href="/region/some_remove">
                <i class="trash icon"></i>
                DELETE SELECTED
            </div>
        </div>
        <h2 class="toggle-header invisible">Add New Region:</h2>

        <div class="ui form invisible">
            <form action="/region" method="post">
                <input type="hidden" name="id" value=""/>
                <div class="field">
                    <div class="ui input">
                        <input placeholder="Region name" type="text" name="name">

                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
                <div class="ui green submit button">Save</div>
                <div class="ui red cancel button">Cancel</div>
                <div class="ui error message"></div>
            </form>
        </div>
    </div>
</div>