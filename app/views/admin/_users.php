<div class="ui two column relaxed grid">
    <div class="column">
        <h2>Users List:</h2>

        <div id="users-list">
            <?= View::make('admin.lists._users_list', ['users' => $users])->render() ?>
        </div>

    </div>
    <div class="column user_block">
        <div class="add_user_block">
            <div class="ui labeled icon buttons toggle">
                <div class="ui teal button add">
                    <i class="add sign box icon"></i>
                    ADD USER
                </div>
            </div>
            <h2 class="toggle-header">Add New User:</h2>
            <?php echo View::make('blocks.user-form', ['regions' => $regions, 'countries' => $countries]); ?>
        </div>

        <div class="edit_user_block">
            <h2 class="toggle-header">Edit User:</h2>
            <?php echo View::make('blocks.user-form', ['regions' => $regions, 'countries' => $countries]); ?>
        </div>
    </div>
    
</div>