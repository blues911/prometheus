<div class="ui two column relaxed grid">
    <div class="column">
        <h2>Subregions List:</h2>
        <div id="subregions-list" data-deleted="false">
            <?= View::make('admin.lists._subregions_list', ['subregions' => $subregions])->render() ?>
        </div>
    </div>
    <div class="column add_subregion_block">
        <div class="ui labeled icon buttons toggle">
            <div class="ui teal button add">
                <i class="add sign box icon"></i>
                ADD SUBREGION
            </div>
        </div>
        <div class="ui labeled icon buttons">
            <div class="ui red button delete-selected" data-href="/subregion/some_remove">
                <i class="trash icon"></i>
                DELETE SELECTED
            </div>
        </div>
        <h2 class="toggle-header invisible">Add New Subregion:</h2>

        <div class="ui form invisible">
            <form action="/subregion" method="post">
                <input type="hidden" name="id" value=""/>
                <div class="field">
                    <div class="ui input">
                        <input placeholder="Subregion name" type="text" name="name">

                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui fluid selection dropdown region">
                        <div class="text" data-default="Select Region">Select Region</div>
                        <i class="dropdown icon"></i>
                        <input type="hidden" name="region">

                        <div class="menu">
                            <?php foreach ($regions as $region): ?>
                                <div class="item" data-value="<?= $region->id ?>"><?= $region->name ?></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="ui green submit button">Save</div>
                <div class="ui red cancel button">Cancel</div>
                <div class="ui error message"></div>
            </form>
        </div>
    </div>
</div>