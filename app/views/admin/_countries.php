<div class="ui two column relaxed grid">
    <div class="column">
        <h2>Countries List:</h2>
        <div id="countries-list" data-deleted="false">
            <?= View::make('admin.lists._countries_list', ['countries' => $countries])->render() ?>
        </div>
    </div>
    <div class="column add_country_block">
        <div class="ui labeled icon buttons toggle">
            <div class="ui teal button add">
                <i class="add sign box icon"></i>
                ADD COUNTRY
            </div>
        </div>
        <div class="ui labeled icon buttons">
            <div class="ui red button delete-selected" data-href="/country/some_remove">
                <i class="trash icon"></i>
                DELETE SELECTED
            </div>
        </div>
        <h2 class="toggle-header invisible">Add New Country:</h2>

        <div class="ui form invisible">
            <form action="/country" method="post">
                <input type="hidden" name="id" value=""/>
                <div class="field">
                    <div class="ui input">
                        <input placeholder="Country name" type="text" name="name">

                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input placeholder="ISO code" type="text" name="iso">

                        <div class="ui corner label">
                            <i class="icon asterisk"></i>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui fluid selection dropdown region">
                        <div class="text" data-default="Select Region">Select Region</div>
                        <i class="dropdown icon"></i>
                        <input type="hidden" name="region">

                        <div class="menu">
                            <?php foreach ($regions as $region): ?>
                                <div class="item" data-value="<?= $region->id ?>"><?= $region->name ?></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <?php if ($subregions) :?>
                <div class="field">
                    <div class="ui fluid selection dropdown subregion">
                        <div class="text" data-default="Select Subregion">Select Subregion</div>
                        <i class="dropdown icon"></i>
                        <input type="hidden" name="subregion">

                        <div class="menu">
                            <?php foreach ($subregions as $subregion): ?>
                                <div class="item" data-value="<?= $subregion->id ?>"><?= $subregion->name ?></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <div class="ui green submit button">Save</div>
                <div class="ui red cancel button">Cancel</div>
                <div class="ui error message"></div>
            </form>
        </div>
    </div>
</div>