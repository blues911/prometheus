<div id="kpi-page" class="ui segment">

    <div class="ui form" id="kpi-regions">
        <div class="fields inline">

            <?php foreach ($regions as $region): ?>
                <div class="field ui checkbox">
                    <input name="<?= $region->id ?>" type="checkbox">
                    <label><?= $region->name ?></label>
                </div>
            <?php endforeach; ?>
            <?php if (isset($currentUser) && $currentUser->is_superadmin): ?>
                <a href="kpi-dashboard-old?year=2016" class="kpi-year-switch">2016</a>
                <a href="kpi-dashboard-old?year=2017" class="kpi-year-switch">2017</a>
                <a href="/kpi-dashboard/?year=2018" class="kpi-year-switch">2018</a>
            <?php endif; ?>
        </div>
    </div>

    <?php foreach ($data as $regionName => $region): ?>
        <div class="kpi-table-<?= $region['headers']['region_id'] ?: $regionName ?>">
            <div class="ui pointing below label header"><?= ucfirst($regionName) ?></div>
            <table class="ui fixed selectable celled table">
                <thead>
                <tr>
                    <th class="relative">
                        <?= ucfirst($regionName) ?> KPIs
                        <?php if (isset($currentUser) && $currentUser->is_superadmin): ?>
                            <div class="edit-column-block">
                                <a class="ui basic yellow button skip-click icon edit-column-btn"
                                   title="Edit general info"
                                   data-region-id="<?= $region['headers']['region_id'] ?>"
                                   data-region-name="<?= $regionName ?>">
                                    <i class="icon-edit-yellow icon"></i>
                                </a>
                            </div>
                        <?php endif; ?>
                    </th>
                    <th class="pre"><?= $region['headers']['net_effect'] ?></th>
                    <th class="pre"><?= $region['headers']['impact_index'] ?></th>
                    <th class="pre"><?= $region['headers']['negative'] ?></th>
                    <th class="pre"><?= $region['headers']['company_mindshare'] ?></th>
                    <th class="pre"><?= $region['headers']['b2b_mindshare'] ?></th>
                    <th class="pre"><?= $region['headers']['b2c_mindshare'] ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($region['records'] as $records): ?>
                    <?php if ((isset($records['status']) && $records['status'] == App\Models\KpiRecord::STATUS_ACTIVE) || (isset($currentUser) && $currentUser->is_superadmin)): ?>
                        <tr>
                            <td class="relative">
                                <?php if (isset($currentUser) && $currentUser->is_superadmin): ?>
                                    <div class="ui read-only checkbox status">
                                        <input type="checkbox" disabled
                                            <?= (isset($records['status']) && $records['status'] == App\Models\KpiRecord::STATUS_ACTIVE) ? 'checked' : '' ?>>
                                    </div>
                                <?php endif; ?>
                                <?= App\Models\KpiRecord::getRecordNameByDate($records['date']) ?>
                                <?php if (isset($currentUser) && $currentUser->is_superadmin): ?>
                                    <div class="edit-column-block">
                                        <a class="ui basic yellow button skip-click icon edit-column-btn"
                                           title="Edit general info"
                                           data-region-id="<?= $records['region_id'] ?>"
                                           data-region-name="<?= $regionName ?>" data-date="<?= $records['date'] ?>">
                                            <i class="icon-edit-yellow icon"></i>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            </td>
                            <td class="<?= isset($records['is_net_effect']) && !$records['is_net_effect'] ? 'negative' : '' ?>">
                                <?= App\Models\KpiHeader::netEffectFormat($records['net_effect']) ?></td>
                            <td class="<?= isset($records['is_impact_index']) && !$records['is_impact_index'] ? 'negative' : '' ?>">
                                <?= $records['impact_index'] == 0 ? 0 : number_format($records['impact_index'], 2, '.', '') ?></td>
                            <td class="<?= isset($records['is_negative']) && !$records['is_negative'] ? 'negative' : '' ?>">
                                <?= $records['negative'] == 0 ? 0 : number_format($records['negative'], 2, '.', '') ?>%
                            </td>
                            <td class="kasp-lab <?= isset($records['is_company_mindshare']) && !$records['is_company_mindshare'] ? 'negative' : '' ?>">
                                <span class="pre"><?= isset($records['company_mindshare']) ? $records['company_mindshare'] : '' ?></span>
                            </td>
                            <td class="kasp-lab <?= isset($records['is_b2b_mindshare']) && !$records['is_b2b_mindshare'] ? 'negative' : '' ?>">
                                <span class="pre"><?= isset($records['b2b_mindshare']) ? $records['b2b_mindshare'] : '' ?></span>
                            </td>
                            <td class="kasp-lab <?= isset($records['is_b2c_mindshare']) && !$records['is_b2c_mindshare'] ? 'negative' : '' ?>">
                                <span class="pre"><?= isset($records['b2c_mindshare']) ? $records['b2c_mindshare'] : '' ?></span>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    <?php endforeach; ?>

    <div id="kpi-headers-modal" class="ui modal">
        <div class="header"></div>
        <div class="content">
            <form class="ui form">
                <input name="region_id" type="hidden">
                <div class="field">
                    <label>Net Effect</label>
                    <textarea rows="2" name="net_effect" placeholder="Net Effect"></textarea>
                </div>
                <div class="field">
                    <label>Impact Index</label>
                    <textarea rows="2" name="impact_index" placeholder="Impact Index"></textarea>
                </div>
                <div class="field">
                    <label>Negative</label>
                    <textarea rows="2" name="negative" placeholder="Negative"></textarea>
                </div>
                <div class="field">
                    <label>Company Mindshare</label>
                    <textarea rows="2" name="company_mindshare" placeholder="Company Mindshare"></textarea>
                </div>
                <div class="field">
                    <label>B2B Mindshare</label>
                    <textarea rows="2" name="b2b_mindshare" placeholder="B2B Mindshare"></textarea>
                </div>
                <div class="field">
                    <label>B2C Mindshare</label>
                    <textarea rows="2" name="b2c_mindshare" placeholder="B2C Mindshare"></textarea>
                </div>
            </form>
        </div>
        <div class="actions">
            <div class="teal ui button ok save-edit">Save</div>
            <div class="ui button basic cancel no-border-button">Cancel</div>
        </div>
    </div>

    <div id="kpi-records-modal" class="ui modal">
        <div class="header"></div>
        <div class="content">
            <form class="ui form">
                <input name="region_id" type="hidden">
                <input name="date" type="hidden">
                <div class="field ui checkbox">
                    <input name="status" type="checkbox">
                    <label>Active</label>
                </div>
                <div class="field ui checkbox">
                    <input name="is_net_effect" type="checkbox">
                    <label>Net Effect Achieved</label>
                </div>
                <div class="field ui checkbox">
                    <input name="is_impact_index" type="checkbox">
                    <label>Impact Index Achieved</label>
                </div>
                <div class="field ui checkbox">
                    <input name="is_negative" type="checkbox">
                    <label>Negative Achieved</label>
                </div>
                <div class="field">
                    <label>Company Mindshare</label>
                    <textarea rows="2" name="company_mindshare" placeholder="Company Mindshare"></textarea>
                </div>
                <div class="field ui checkbox">
                    <input name="is_company_mindshare" type="checkbox">
                    <label>Company Mindshare Achieved</label>
                </div>
                <div class="field">
                    <label>B2B Mindshare</label>
                    <textarea rows="2" name="b2b_mindshare" placeholder="B2B Mindshare"></textarea>
                </div>
                <div class="field ui checkbox">
                    <input name="is_b2b_mindshare" type="checkbox">
                    <label>B2B Mindshare Achieved</label>
                </div>
                <div class="field">
                    <label>B2C Mindshare</label>
                    <textarea rows="2" name="b2c_mindshare" placeholder="B2C Mindshare"></textarea>
                </div>
                <div class="field ui checkbox">
                    <input name="is_b2c_mindshare" type="checkbox">
                    <label>B2C Mindshare Achieved</label>
                </div>
            </form>
        </div>
        <div class="actions">
            <div class="teal ui button ok save-edit">Save</div>
            <div class="ui button basic cancel no-border-button">Cancel</div>
        </div>
    </div>

</div>