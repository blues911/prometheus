<script type="text/javascript">
    var regionId        = <?= $regionId?>;
    var baseFilters     = <?= json_encode($filter, JSON_FORCE_OBJECT) ?>;
    var dateFrom        = '<?= $dateFrom?>';
    var dateTo          = '<?= $dateTo ?>';
    var defaultDateFrom = '<?= $defaultDateFrom ?>';
    var defaultDateTo   = '<?= $defaultDateTo ?>';
    var tableMode       = '<?= $mode ?>';
    var exportCount     = '<?= Config::get('app.export.coverage_count') ?>';
    var coding_closed_months  = <?=json_encode($codingClosedMonths)?>;
</script>
<div class="ui page table coverage" id="coverage-page">

    <div class="ui segment">

        <div id="delete-all-modal" class="ui modal">
            <div class="header">Delete all coverages</div>
            <div class="content">
                <span><b></b> coverage(s) will be permanently deleted and cannot be recovered. Are you sure?</span>
            </div>
            <div class="actions">
                <div class="red ui button approve">Yes</div>
                <div class="ui button basic no-border-button cancel">Cancel</div>
            </div>
        </div>

        <div id="delete-modal" class="ui modal">
            <div class="header">Delete</div>
            <div class="content">
                <span>Are you sure you want delete selected coverage?</span>
            </div>
            <div class="actions">
                <div class="red ui button delete">Yes</div>
                <div class="ui button basic no-border-button cancel">Cancel</div>
            </div>
        </div>

        <div id="edit-coverage-modal" class="ui modal">
            <div class="header"></div>
            <div class="content">
            </div>
            <div class="actions">
                <div class="teal ui button ok save-edit">Save</div>
                <div class="ui button basic no-border-button cancel">Cancel</div>
            </div>
        </div>

        <div id="export-modal" class="ui modal">
            <div class="header">Export coverage</div>
            <div class="content">
                <span>Export coverages takes few minutes. You can download the file on the <a href="/export">export</a> page</span>
            </div>
            <div class="actions">
                <div class="ui button basic no-border-button">CLOSE</div>
            </div>
        </div>

        <div id="coding-process-notify-modal" class="ui modal">
            <i class="close icon"></i>
            <div class="header">COVERAGE CODING PROCESS</div>
            <div class="content">
                <p>Coverage coding process in <span id="closed-month-name"></span> was completed.To add a new article please send a request to Natalia Banke: <a href="mailto:<?=Config::get('app.email.support')?>"><?=Config::get('app.email.support')?></a></p>
            </div>
        </div>

        <div id="coding-process-modal" class="ui modal">
            <div class="header">COVERAGE CODING PROCESS</div>
            <div class="content"></div>
            <div class="actions">
                <div class="teal ui button ok save">Save</div>
                <div class="ui button no-border-button cancel">Cancel</div>
            </div>
        </div>

        <div class="row table-actions ui form">
            <div class="ui form">
                <div class="fields">
                    <div class="field">
                        <div class="ui checkbox" id="select-all-coverage">
                            <input type="checkbox" name="select-all">
                        </div>
                        <a class="ui teal button icon add" title="Add new coverage"><i class="plus icon"></i></a>
                        <a href="/coverage/export" class="ui blue-btn basic button icon export_btn" title="Export coverage list to Excel">
                            <i class="icon-download icon"></i></a>
                        <a class="ui red basic button icon delete disabled" title="Delete selected coverage"><i class="remove icon"></i></a>
                        <a class="ui red basic button icon delete-all" title="Delete all coverage"><i class="remove icon"></i> All</a>
                        <? if (Auth::user()->role == App\Models\User::ROLE_ADMINISTRATOR): ?>
                            <a class="ui icon green button approve disabled" title="Approve selected coverage"><i class="checkmark icon"></i></a>
                            <a class="ui icon green button approve_all" title="Approve all coverage"><i class="checkmark icon"></i> All</a>
                        <? endif; ?>
                        <? if (Auth::user()->role == App\Models\User::ROLE_ADMINISTRATOR): ?>
                        <a id="coding-process" class="ui teal button icon" title="Coding process">
                            <i class="calendar icon"></i>
                        </a>
                        <? endif; ?>
                    </div>
                    <input type="hidden" disabled="disabled" id="other_filters" value="<?= htmlspecialchars(json_encode($filter, JSON_FORCE_OBJECT | ENT_NOQUOTES)) ?>">
                    <div class="wide field" style="min-width: 120px">
                        <div class="ui selection dropdown" id="region_dropdown_filter" title="Filter by region">
                            <input type="hidden" name="region_id" id="region-filter" value="0">

                            <div class="default text">Choose region...</div>
                            <i class="dropdown icon"></i>

                            <div class="menu">
                                <div class="item" data-value="0">All</div>
                                <?php foreach ($regions as $region): ?>
                                    <div class="item" data-value="<?= $region->id ?>"><?= $region->name ?></div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <? if (in_array(Auth::user()->role, [App\Models\User::ROLE_ADMINISTRATOR, App\Models\User::ROLE_MANAGER])): ?>
                    <div class="wide field" style="width: 160px">
                        <div class="ui selection dropdown" id="user_dropdown_filter" title="Filter by user">
                            <input type="hidden" name="user_id" id="user-filter" value="0">

                            <div class="default text">Choose user...</div>
                            <i class="dropdown icon"></i>

                            <div class="menu">
                                <div class="item" data-value="0">All</div>
                                <?php foreach ($users as $user): ?>
                                      <div class="item" data-value="<?= $user->id ?>"><?= $user->name ?></div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <? endif; ?>

                    <div id="datepicker-start" class="two wide field input-daterange" style="width: 135px !important;">
                        <div class="ui right labeled icon input first-block" title="Start date">
                            <input type="text" name="start" value="<?=$dateFrom?>" id="start_range">
                            <i class="calendar icon add-on start"></i>
                        </div>
                    </div>
                    <div id="datepicker-end" class="two wide field input-daterange second-block" style="width: 135px !important;">
                        <div class="ui right labeled icon input" title="End date">
                            <input type="text" name="end" value="<?=$dateTo?>" id="end_range">
                            <i class="calendar icon add-on end"></i>
                        </div>
                    </div>

                    <div class="field">
                        <a class="ui teal button icon clear-filters" title="Clear filter">
                            <i class="remove icon"></i>
                        </a>
                    </div>
                    <div class="toggle" id="table-switcher">
                        <div class="ui button icon circle full <?=($mode == App\Models\Coverage::MODE_FULL ? 'active teal' : 'grey')?>" 
                            data-table-mode="<?=App\Models\Coverage::MODE_FULL?>" title="Full">
                            <i class="sidebar icon"></i>
                        </div>
                        <div class="ui button icon circle compact <?=($mode == App\Models\Coverage::MODE_COMPACT ? 'active teal' : 'grey')?>" 
                            data-table-mode="<?=App\Models\Coverage::MODE_COMPACT?>" title="Compact">
                            <i class="pause icon"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- eof row -->

        <div class="row">

            <table class="stripe cell-border row-border order-column <?= $mode ?>" id="fixed-table">
                <thead>
                    <tr>
                        <? if ($mode == App\Models\Coverage::MODE_FULL): ?>
                            <th class="coverage-col-id"></th>
                            <th class="coverage-col-publication">Publication name</th>
                            <th class="coverage-col-status">Stat.</th>
                            <th>Owner</th>
                            <th>Reach</th>
                            <th>Date</th>
                            <th class="coverage-col-place">Place</th>
                            <th>Author</th>
                            <th>Headline</th>
                            <th class="coverage-col-media sub-columns">
                                <div>Media</div>
                                <div class="ui three column centered grid sub-columns">
                                    <div class="field column text-center no-padding">List</div>
                                    <div class="field column text-center no-padding">Type</div>
                                    <div class="field column text-center no-padding">Cat.</div>
                                    <div class="field column text-center no-padding">Subcat.</div>
                                </div>
                            </th>
                            <th class="coverage-col-message-penetration">Mess. pen.</th>
                            <th>Vis.</th>
                            <th class="coverage-col-desired-article">Des. art.</th>
                            <th class="coverage-col-img">Img</th>
                            <th class="coverage-col-url" style="min-width: 250px;">URL</th>
                            <th>Brand/product mention</th>
                            <th class="coverage-col-mentioned-products">Mentioned products</th>
                            <th class="coverage-col-speakers-quoted">Speakers quoted</th>
                            <th class="coverage-col-3rd-speakers-quoted">3rd party sp. quoted</th>
                            <th class="coverage-col-tonality sub-columns">
                                <div>Tonality</div>
                                <div class="ui two column centered grid sub-columns">
                                    <div class="field column text-center no-padding">Header</div>
                                    <div class="field column text-center no-padding">Tonality</div>
                                </div>
                            </th>
                            <th class="coverage-col-negative">Why negative?</th>
                            <th>Event name</th>
                            <th class="coverage-col-newsbreak-global">Newsbreak Global</th>
                            <th class="coverage-col-newsbreak-local">Newsbreak Local</th>
                            <th class="coverage-col-campaign">Themes/Campaigns</th>
                            <th class="coverage-col-business" nowrap="">Business segment</th>
                            <th>Impact</th>
                            <th>Net effect</th>
                            <th class="coverage-col-file">File</th>
                            <th class="coverage-col-comments" style="min-width: 300px;">Comments</th>
                            <th class="coverage-col-actions">Action</th>
                        <? else: ?>
                            <th class="coverage-col-general-compact">General</th>
                            <th class="coverage-col-publication-compact">Publication</th>
                            <th class="coverage-col-media-compact sub-columns">Media</th>
                            <th class="coverage-col-analytics-compact">Analytics</th>
                            <th class="coverage-col-mentioned-compact">Mentioned & Speakers quoted</th>
                            <th class="coverage-col-tonality-compact">Tonality & Penetration</th>
                            <th class="coverage-col-events-compact">Analytics</th>
                            <th class="coverage-col-impact-compact">Impact</th>
                        <? endif; ?>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot></tfoot>
            </table>

        </div>

        <div class="row table-actions">
            <div class="field footer-menu">
                <a class="ui teal button icon add" title="Add new coverage"><i class="plus icon"></i></a>
                <a href="/coverage/export" class="ui blue-btn basic button icon export_btn" title="Export coverage list to Excel"><i class="icon-download icon"></i></a>
                <a class="ui red basic button icon delete disabled" title="Delete selected coverage"><i class="remove icon"></i></a>
                <a class="ui red basic button icon delete-all" title="Delete all coverage"><i class="remove icon"></i> All</a>
                <? if (Auth::user()->role == App\Models\User::ROLE_ADMINISTRATOR): ?>
                    <a class="ui icon green button approve disabled" title="Approve selected coverage"><i class="checkmark icon"></i></a>
                    <a class="ui icon green button approve_all" title="Approve all coverage"><i class="checkmark icon"></i> All</a>
                    <a class="ui icon blue button return_to_draft disabled" title="Return to draft selected coverage"><i class="reply icon"></i></a>
                <? endif; ?>
            </div>
        </div>
        <!-- eof row -->
    </div>
    <!-- eof segment -->

</div><!-- eof page -->

<script id="coding-process-template" type="text/template">
    <div class="ui form coding-year-form" data-year="">
        <div class="ui three column grid no-margin-bottom">
            <div class="field column no-margin-bottom">
                <div class="ui right icon input coding-date">
                    <input type="text" name="coding_data[<%= year %>][year]" value="<%= year %>" readonly>
                </div>
            </div>
            <div class="field column no-margin-bottom">
                <a href="#" class="ui button icon blue handle-coding-year" title="Settings"><i class="setting icon"></i></a>
                <a href="#" class="ui button icon red delete-coding-year" title="Delete"><i class="remove icon"></i></a>
            </div>
        </div>
        <div class="ui equal width form coding-months" style="display: none;">
            <div class="grouped fields">
                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" id="<%= year %>_january" name="coding_data[<%= year %>][months][january]" checked>
                        <label for="<%= year %>_january">January</label>
                    </div>
                </div>
                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" id="<%= year %>_february" name="coding_data[<%= year %>][months][february]" checked>
                        <label for="<%= year %>_february">February</label>
                    </div>
                </div>
                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" id="<%= year %>_march" name="coding_data[<%= year %>][months][march]" checked>
                        <label for="<%= year %>_march">March</label>
                    </div>
                </div>
                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" id="<%= year %>_april" name="coding_data[<%= year %>][months][april]" checked>
                        <label for="<%= year %>_april">April</label>
                    </div>
                </div>
                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" id="<%= year %>_may" name="coding_data[<%= year %>][months][may]" checked>
                        <label for="<%= year %>_may">May</label>
                    </div>
                </div>
                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" id="<%= year %>_june" name="coding_data[<%= year %>][months][june]" checked>
                        <label for="<%= year %>_june">June</label>
                    </div>
                </div>
                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" id="<%= year %>_july" name="coding_data[<%= year %>][months][july]" checked>
                        <label for="<%= year %>_july">July</label>
                    </div>
                </div>
                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" id="<%= year %>_august" name="coding_data[<%= year %>][months][august]" checked>
                        <label for="<%= year %>_august">August</label>
                    </div>
                </div>
                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" id="<%= year %>_september" name="coding_data[<%= year %>][months][september]" checked>
                        <label for="<%= year %>_september">September</label>
                    </div>
                </div>
                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" id="<%= year %>_october" name="coding_data[<%= year %>][months][october]" checked>
                        <label for="<%= year %>_october">October</label>
                    </div>
                </div>
                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" id="<%= year %>_november" name="coding_data[<%= year %>][months][november]" checked>
                        <label for="<%= year %>_november">November</label>
                    </div>
                </div>
                <div class="inline field">
                    <div class="ui checkbox">
                        <input type="checkbox" id="<%= year %>_december" name="coding_data[<%= year %>][months][december]" checked>
                        <label for="<%= year %>_december">December</label>
                    </div>
                </div>
                <br/>
            </div>
        </div>
    </div>
</script>