<link rel="stylesheet" type="text/css" href="/css/admin-form.css">
<link rel="stylesheet" type="text/css" href="/vendor/DataTables/extensions/FixedColumns/css/dataTables.fixedColumns.css">
<link rel="stylesheet" type="text/css" href="/vendor/DataTables/css/jquery.dataTables.css">

<script src="/vendor/DataTables/js/jquery.dataTables.js"></script>
<script src="/vendor/DataTables/extensions/FixedColumns/js/dataTables.fixedColumns.js"></script>

<script src="/js/export.js"></script>