<link rel="stylesheet" type="text/css" href="/vendor/DataTables/extensions/FixedColumns/css/dataTables.fixedColumns.css">
<link rel="stylesheet" type="text/css" href="/vendor/DataTables/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="/css/admin-form.css">
<script src="/vendor/highcharts/highcharts.js"></script>
<!--Fix canvas-tool.js error "Cannot read property 'prototype' of undefined"-->
<script>
	if (Highcharts != null && Highcharts.CanVGRenderer == null) {
		Highcharts.CanVGRenderer = {};
	}
</script>
<script src="/vendor/highcharts/highcharts-more.js"></script>
<script src="/vendor/highcharts/no-data-to-display.js"></script>
<script src="/vendor/highcharts/modules/export/canvas-tools.js"></script>
<script src="/vendor/highcharts/modules/export/exporting.js"></script>
<script src="/vendor/highcharts/modules/export/export-csv.js"></script>
<script src="/vendor/highcharts/modules/export/highcharts-export-clientside.js"></script>
<script src="/vendor/highcharts/modules/data.js"></script>
<script src="/vendor/d3.min.js"></script>
<script src="/vendor/d3.tip.js"></script>
<script src="/vendor/angular.js"></script>
<script src="/js/clipboard.js"></script>
<? if (empty($publicReport) || !$publicReport): ?>
    <script src="/js/ZeroClipboard/ZeroClipboard.js"></script>
    <script src="/js/highcharts-init.js"></script>
<? else: ?>
    <script src="/js/highcharts-init-public.js"></script>
<? endif; ?>
<script src="/vendor/DataTables/js/jquery.dataTables.js"></script>
<script src="/vendor/DataTables/extensions/FixedColumns/js/dataTables.fixedColumns.js"></script>
<script src="/js/charts-app.js"></script>
<script src="/js/api-factory.js"></script>
<script src="/js/directive.js"></script>
<script src="/js/numeral.min.js"></script>
<script src="/js/scrollmenu.js"></script>



