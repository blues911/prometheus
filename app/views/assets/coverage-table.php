<link rel="stylesheet" type="text/css" href="/vendor/jScrollPane/jquery.jscrollpane.css">
<link rel="stylesheet" type="text/css" href="/vendor/DataTables/extensions/FixedColumns/css/dataTables.fixedColumns.css">
<link rel="stylesheet" type="text/css" href="/vendor/DataTables/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="/vendor/tagit/tagit.css">

<script src="/vendor/DataTables/js/jquery.dataTables.js"></script>
<script src="/vendor/DataTables/extensions/FixedColumns/js/dataTables.fixedColumns.js"></script>
<script src="/js/coverage-table.js"></script>
<script src="/vendor/tagit/tagit.js"></script>

<script src="/vendor/jScrollPane/jquery.mousewheel.js"></script>
<script src="/vendor/jScrollPane/jquery.jscrollpane.min.js"></script>