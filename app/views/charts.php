<? $isPrivateReport = (boolean) (empty($publicReport) || !$publicReport); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script type="text/javascript">
    var region          = "<?=$filters['region_id']?>";
    var events          = <?=json_encode($eventData)?>;
    var news            = <?=json_encode($newsData)?>;
    var products        = <?=json_encode($productData)?>;
    var regions         = <?=json_encode($regions)?>;
    var subregions      = <?=json_encode($subregions)?>;
    var hash            = <?=json_encode(!empty($hash) ? $hash : "")?>;
    var filters         = <?=json_encode($filters)?>;
    var countries       = <?=json_encode($countries)?>;
    var campaigns       = <?=json_encode($campaignData)?>;
    var business        = <?=json_encode($businessData)?>;
    var speakers        = <?=json_encode($speakerData)?>;
    var reports         = <?=json_encode($reports)?>;
    var isPrivateReport = <?=(int)$isPrivateReport?>;
    var is_pdf          = false;
</script>

<div ng-app="Reports" ng-controller="ReportController" ng-init="init()" id="reports-page">
    <div class="ui top attached tabular menu charts ui one column centered grid">
        <a class="active item" data-tab="executive_summary" ng-click="active_tab('executive_summary')">Executive Summary</a>
        <a class="item" data-tab="general_information" ng-click="active_tab('general_information')">General Information</a>
        <a class="item" data-tab="comparison" ng-click="active_tab('comparison')">Comparison</a>
        <a class="item" data-tab="dynamics" ng-click="active_tab('dynamics')">Dynamics</a>
        <? if (Auth::check() && Auth::user()->role == App\Models\User::ROLE_ADMINISTRATOR && $isPrivateReport) : ?>
            <a class="item" data-tab="reports" ng-click="active_tab('reports')">Report Lists</a>
        <? endif; ?>
        <button class="ui teal button" id="fixMenu">Freeze panel</button>
    </div>

    <div id="reports">
        <? if ($isPrivateReport): ?>
            <div class="ui two column centered grid" id="filters-menu">
                <div class="centered row ui bottom attached segment">
                    <div class="fourteen wide column filters">
                        <div class="ui form">

                            <div class="fields">
                                <div class="width-13-percent wide field input-daterange">
                                    <div class="ui labeled icon input first-block" title="Start date">
                                        <input type="text" name="start" value="<?=$filters['start']?>" placeholder="Start date" class="">
                                        <i class="calendar icon add-on start"></i>
                                    </div>
                                </div>
                                <div class="width-13-percent wide field input-daterange second-block">
                                    <div class="ui labeled icon input" title="End date">
                                        <input type="text" name="end" value="<?=$filters['end']?>" placeholder="End date">
                                        <i class="calendar icon add-on end"></i>
                                    </div>
                                </div>
                                <div class="buttons-container wide field">
                                    <a class="ui teal button icon report-filter wide" title="Apply report filter" ng-click="filter()">
                                        <i class="filter icon"></i>
                                    </a>
                                    <a class="ui transparent button icon wide refresh-report-filter" ng-click="clear()" title="Clear filter">
                                        <i class="remove icon"></i>
                                    </a>
                                    <? if (Auth::check() && Auth::user()->role === App\Models\User::ROLE_ADMINISTRATOR): ?>
                                        <a class="ui transparent button icon save-report-filter wide" title="Save report filter" ng-click="open_report_modal()">
                                            <i class="save icon"></i>
                                        </a>
                                        <a class="ui transparent button icon export-report-filter-pdf wide" title="Export report filter to PDF" ng-click="save_pdf()">
                                            <i class="file pdf outline icon"></i>
                                        </a>
                                    <? endif; ?>
                                </div>
                            </div>

                            <div class="fields">
                                <div class="width-13-percent wide field" id="region_filter_div">
                                    <div class="ui selection dropdown region-filter" title="Filter by region">
                                        <input type="hidden" name="region_id" value="">
                                        <div class="default text">All regions</div>
                                        <i class="dropdown icon"></i>
                                        <div class="menu">
                                            <div class="item" data-value="">All regions</div>
                                            <?php foreach ($regions as $region): ?>
                                                <div class="item" data-value="<?= $region->id ?>"><?= $region->name ?></div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="width-13-percent wide field" id="subregion_filter_div">
                                    <div class="ui selection dropdown subregion-filter" title="Filter by subregion">
                                        <input type="hidden" name="subregion_id" value="">
                                        <div class="default text">All subregions</div>
                                        <i class="dropdown icon"></i>
                                        <div class="menu">
                                            <div class="item" data-value="">All subregions</div>
                                            <?php foreach ($subregions as $subregion): ?>
                                                <div class="item" data-value="<?= $subregion->id ?>"><?= $subregion->name ?></div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="width-13-percent wide field">
                                    <div class="ui selection dropdown country-filter">
                                        <input type="hidden" name="country_id" id="country_id" value="">
                                        <div class="default text">All countries</div>
                                        <i class="dropdown icon"></i>
                                        <div class="menu">
                                            <div class="item" data-value="">All countries</div>
                                            <?php foreach ($countries as $country): ?>
                                                <div class="item" data-value="<?= $country->id ?>"><?= $country->name ?></div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="width-15-percent wide field">
                                    <div class="ui selection dropdown media-category" title="Filter by media category">
                                        <input type="hidden" name="media_category" id="media_category_filter" value="<?=$filters['media_category']?>">

                                        <div class="default text">All media categories</div>
                                        <i class="dropdown icon"></i>

                                        <div class="menu">
                                            <div class="item" data-value="">All media categories</div>
                                            <?php foreach (\App\Models\Coverage::getMediaCategories() as $value => $name): ?>
                                                <div class="item" data-value="<?= $value ?>"><?= ucfirst($name) ?></div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="width-15-percent wide field">
                                    <div class="ui selection dropdown media-subcategory" title="Filter by media subcategory">
                                        <input type="hidden" name="media_subcategory" id="media_subcategory_filter" value="<?=$filters['media_subcategory']?>">

                                        <div class="default text">All media subcategories</div>
                                        <i class="dropdown icon"></i>

                                        <div class="menu">
                                            <div class="item" data-value="">All media subcategories</div>
                                            <?php foreach ($subcategoriesData as $key => $value): ?>
                                                <div class="item" data-value="<?= $value['id'] ?>"><?= ucfirst($value['label']) ?></div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="width-13-percent wide field">
                                    <div class="ui selection dropdown media-list" title="Filter by media list">
                                        <input type="hidden" name="media_list" id="media_list_filter" value="<?=$filters['media_list']?>">

                                        <div class="default text">All media</div>
                                        <i class="dropdown icon"></i>

                                        <div class="menu">
                                            <div class="item" data-value="">All media</div>
                                            <?php foreach (\App\Models\Coverage::getMediaList() as $value => $name): ?>
                                                <div class="item" data-value="<?= $value ?>"><?= ucfirst($name) ?></div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="width-13-percent wide field">
                                    <div class="ui selection dropdown business" title="Business segment">
                                        <input type="hidden" name="business" id="business_filter" value="<?=$filters['business']?>">

                                        <div class="default text">All business segment</div>
                                        <i class="dropdown icon"></i>

                                        <div class="menu">
                                            <div class="item" data-value="">All business segment</div>
                                            <?php foreach ($businessData as $item): ?>
                                                <div class="item" data-value="<?= $item['id'] ?>"><?= ucfirst($item['value']) ?></div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="fields" ng-hide="activated_tab == 'comparison'">
                                <div class="width-19-percent wide field">
                                    <div class="ui labeled icon input" title="Filter by campaign">
                                        <input type="hidden" id="campaign_id" value="<?=$filters['campaign_id']?>">
                                        <input type="text" name="campaign" id="campaign_filter" value="<?=$filters['campaign']?>" placeholder="Campaign">
                                    </div>
                                </div>

                                <div class="width-19-percent wide field">
                                    <div class="ui labeled icon input" title="Filter by newsbreak">
                                        <input type="hidden" id="news_id" value="<?=$filters['newsbreak_id']?>">
                                        <input type="text" name="news" id="news_filter" value="<?=$filters['newsbreak']?>" placeholder="Newsbreak">
                                    </div>
                                </div>

                                <div class="width-19-percent wide field" id="product_filter_div">
                                    <div class="ui labeled icon input" title="Filter by product">
                                        <input type="hidden" id="product_id" value="<?=$filters['product_id']?>">
                                        <input type="text" name="product" id="product_filter" value="<?=$filters['product']?>" placeholder="Product">
                                    </div>
                                </div>

                                <div class="width-19-percent wide field">
                                    <div class="ui labeled icon input" title="Filter by speaker">
                                        <input type="hidden" id="speaker_id" value="<?=$filters['speaker_id']?>">
                                        <input type="text" name="speaker" id="speaker_filter" value="<?=$filters['speaker']?>" placeholder="Speaker">
                                    </div>
                                </div>

                                <div class="width-19-percent wide field" id="event_filter_div">
                                    <div class="ui labeled icon input" title="Filter by event">
                                        <input type="text" name="event" id="event_filter" value="<?=$filters['event']?>" placeholder="Event">
                                    </div>
                                </div>
                            </div>

                            <div class="fields ng-hide" ng-show="activated_tab == 'comparison'">
                                <div class="width-19-percent wide field">
                                    <div class="ui label filter">Campaigns</div>
                                    <a ng-click="clear_filter('campaigns')" title="Clear list" class="ui transparent button top icon wide"><i class="remove icon"></i></a>
                                    <a ng-click="set_top('campaigns')" title="Top 10" class="ui teal button top icon wide">Top 10</a>
                                    <div class="ui selection search multiple dropdown" dropdown="" data-ng-model="filters.campaigns" data-filter="campaigns">
                                        <input type="hidden" name="campaign_list" id="campaign_list_filter">

                                        <div class="default text">Choose campaigns</div>
                                        <i class="dropdown icon"></i>

                                        <div class="menu">
                                            <?php foreach ($campaignData as $item): ?>
                                                <div class="item" data-value="<?= $item['id'] ?>"><?= ucfirst($item['value']) ?></div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="width-19-percent wide field">
                                    <div class="ui label filter">Newsbreaks</div>
                                    <a ng-click="clear_filter('newsbreaks')" title="Clear list" class="ui transparent button top icon wide"><i class="remove icon"></i></a>
                                    <a ng-click="set_top('newsbreaks')" title="Top 10" class="ui teal button top icon wide">Top 10</a>
                                    <div class="ui selection search multiple dropdown" dropdown="" data-ng-model="filters.newsbreaks" data-filter="newsbreaks">
                                        <input type="hidden" name="newsbreak_list" id="newsbreak_list_filter">

                                        <div class="default text">Choose newsbreaks</div>
                                        <i class="dropdown icon"></i>

                                        <div class="menu">
                                            <?php foreach ($newsData as $item): ?>
                                                <div class="item" data-value="<?= $item['id'] ?>"><?= ucfirst($item['value']) ?></div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="width-19-percent wide field">
                                    <div class="ui label filter">Products</div>
                                    <a ng-click="clear_filter('products')" title="Clear list" class="ui transparent button top icon wide"><i class="remove icon"></i></a>
                                    <a ng-click="set_top('products')" title="Top 10" class="ui teal button top icon wide">Top 10</a>
                                    <div class="ui selection search multiple dropdown" dropdown="" data-ng-model="filters.products" data-filter="products">
                                        <input type="hidden" name="product_list" id="product_list_filter">

                                        <div class="default text">Choose products</div>
                                        <i class="dropdown icon"></i>

                                        <div class="menu">
                                            <?php foreach ($productData as $item): ?>
                                                <div class="item" data-value="<?= $item['id'] ?>"><?= ucfirst($item['value']) ?></div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="width-19-percent wide field">
                                    <div class="ui label filter">Speakers</div>
                                    <a ng-click="clear_filter('speakers')" title="Clear list" class="ui transparent button top icon wide"><i class="remove icon"></i></a>
                                    <a ng-click="set_top('speakers')" title="Top 10" class="ui teal button top icon wide">Top 10</a>
                                    <div class="ui selection search multiple dropdown" dropdown="" data-ng-model="filters.speakers" data-filter="speakers">
                                        <input type="hidden" name="speaker_list" id="speaker_list_filter">

                                        <div class="default text">Choose speakers</div>
                                        <i class="dropdown icon"></i>

                                        <div class="menu">
                                            <?php foreach ($speakerData as $item): ?>
                                                <div class="item" data-value="<?= $item['id'] ?>"><?= ucfirst($item['value']) ?></div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="width-19-percent wide field">
                                    <div class="ui label filter">Events</div>
                                    <a ng-click="clear_filter('events')" title="Clear list" class="ui transparent button top icon wide"><i class="remove icon"></i></a>
                                    <a ng-click="set_top('events')" title="Top 10" class="ui teal button top icon wide">Top 10</a>
                                    <div class="ui selection multiple dropdown" dropdown="" data-ng-model="filters.events" data-filter="events">
                                        <input type="hidden" name="event_list" id="event_list_filter">

                                        <div class="default text">Choose events</div>
                                        <i class="dropdown icon"></i>

                                        <div class="menu">
                                            <?php foreach ($eventData as $item): ?>
                                                <div class="item" data-value="<?= $item ?>"><?= ucfirst($item) ?></div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="twelve wide centered column">
                        <div class="ui center aligned grid main-totals-menu">
                            <span class="main-totals">Net Effect:&nbsp;<strong><span id="net_effect">-</span></strong></span>
                            <span class="main-totals">Impact Index:&nbsp;<strong><span id="impact_index">-</span></strong></span>
                            <span class="main-totals">Media Outreach:&nbsp;<strong><span id="media_outreach">-</span></strong></span>
                            <span class="main-totals">Number of Articles:&nbsp;<strong><span id="coverage_count">-</span></strong></span>
                        </div>
                    </div>

                    <div class="ui one column centered grid charts-nav-buttons" ng-show="activated_tab == 'executive_summary' || activated_tab == 'general_information'">
                        <div class="ui large buttons">
                          <div class="ui information_filter net_effect basic button" title="Show information by Net Effect" data-value="net_effect">Net Effect</div>
                          <div class="ui information_filter numbers basic button" title="Show information by Numbers of Articles" data-value="numbers">Number of articles</div>
                        </div>
                    </div>

                    <div class="ui one column centered grid charts-nav-buttons" ng-show="activated_tab == 'dynamics'">
                        <div class="ui large buttons">
                          <div class="ui chart_date_filter week basic button" title="Show dynamics by weeks" data-value="week">Weeks</div>
                          <div class="ui chart_date_filter month basic button" title="Show dynamics by months" data-value="month">Months</div>
                        </div>
                    </div>

                </div>
            </div>
        <? elseif (!$isPrivateReport): ?>
            <div class="ui two column centered grid" id="filters-menu">
                <div class="centered row ui bottom attached segment">
                    <div class="fourteen wide column filters">
                        <div class="ui form">
                            <div class="two fields">
                                <div class="width-13-percent wide field input-daterange">
                                    <div class="ui labeled icon input first-block public" title="Start date">
                                        <input disabled name="start" type="text" value="<?=$filters['start']?>" placeholder="Start date">
                                        <i class="calendar icon add-on start"></i>
                                    </div>
                                </div>
                                <div class="width-13-percent wide field input-daterange second-block">
                                    <div class="ui labeled icon input" title="End date">
                                        <input disabled name="end" type="text"value="<?=$filters['end']?>" placeholder="End date">
                                        <i class="calendar icon add-on end"></i>
                                    </div>
                                </div>

                                <div class="width-13-percent wide field" id="region_filter_div">
                                    <div class="ui labeled icon input" title="Filter by region">
                                        <input disabled type="text" value="<?=$filters['region']?>" placeholder="All regions">
                                    </div>
                                </div>

                                <div class="width-15-percent wide field">
                                    <div class="ui labeled icon input" title="Filter by country">
                                        <input disabled type="text" value="<?=$filters['country']?>" placeholder="All countries">
                                    </div>
                                </div>

                                <div class="width-15-percent wide field">
                                    <div class="ui labeled icon input" title="Filter by media category">
                                         <input disabled type="text" value="<?=$filters['media_category_show']?>" placeholder="All media categories">
                                    </div>
                                </div>
                                
                                <div class="width-15-percent wide field">
                                    <div class="ui labeled icon input" title="Filter by media subcategory">
                                         <input disabled type="text" value="<?=$filters['media_subcategory_show']?>" placeholder="All media subcategories">
                                    </div>
                                </div>

                                <div class="width-13-percent wide field">
                                    <div class="ui labeled icon input" title="Filter by media list">
                                        <input disabled type="text" value="<?=$filters['media_list_show']?>" placeholder="All media lists">
                                    </div>
                                </div>
                                <div class="width-15-percent wide field">
                                    <div class="ui labeled icon input" title="Filter by business segment">
                                         <input disabled type="text" value="<?=$filters['business']?>" placeholder="All business segment">
                                    </div>
                                </div>
                            </div>
                            <div class="fields" ng-hide="activated_tab == 'comparison'">
                                <div class="width-20-percent wide field">
                                    <div class="ui labeled icon input" title="Filter by campaign">
                                        <input disabled type="text" value="<?=$filters['campaign']?>"  placeholder="Campaign">
                                    </div>
                                </div>

                                <div class="width-20-percent wide field">
                                    <div class="ui labeled icon input" title="Filter by newsbreak">
                                        <input disabled type="text" value="<?=$filters['newsbreak']?>"  placeholder="Newsbreak">
                                    </div>
                                </div>

                                <div class="width-17-percent wide field" id="product_filter_div">
                                    <div class="ui labeled icon input" title="Filter by product">
                                        <input disabled type="text" value="<?=$filters['product']?>"  placeholder="Product">
                                    </div>
                                </div>

                                <div class="width-20-percent wide field">
                                    <div class="ui labeled icon input" title="Filter by speaker">
                                        <input disabled type="text" value="<?=$filters['speaker']?>"  placeholder="Speaker">
                                    </div>
                                </div>

                                <div class="width-20-percent wide field" id="event_filter_div">
                                    <div class="ui labeled icon input" title="Filter by event">
                                        <input disabled type="text" value="<?=$filters['event']?>"  placeholder="Event">
                                    </div>
                                </div>
                            </div>
                            <div class="fields ng-hide" ng-show="activated_tab == 'comparison'">
                                <div class="width-20-percent wide field">
                                    <div class="ui label filter">Campaigns</div>
                                    <ul>
                                        <?php foreach ($topName['campaigns'] as $item): ?>
                                            <li><?= ucfirst($item) ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>

                                <div class="width-20-percent wide field">
                                    <div class="ui label filter">Newsbreaks</div>
                                    <ul>
                                        <?php foreach ($topName['newsbreaks'] as $item): ?>
                                            <li><?= ucfirst($item) ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>

                                <div class="width-20-percent wide field">
                                    <div class="ui label filter">Products</div>
                                    <ul>
                                        <?php foreach ($topName['products'] as $item): ?>
                                            <li><?= ucfirst($item) ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>

                                <div class="width-20-percent wide field">
                                    <div class="ui label filter">Speakers</div>
                                    <ul>
                                        <?php foreach ($topName['speakers'] as $item): ?>
                                            <li><?= ucfirst($item) ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>

                                <div class="width-20-percent wide field">
                                    <div class="ui label filter">Events</div>
                                    <ul>
                                        <?php foreach ($topName['events'] as $item): ?>
                                            <li><?= ucfirst($item) ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="twelve wide centered column">
                        <div class="ui center aligned grid main-totals-menu">
                            <span class="main-totals">Net Effect:&nbsp;<strong><span id="net_effect">-</span></strong></span>
                            <span class="main-totals">Impact Index:&nbsp;<strong><span id="impact_index">-</span></strong></span>
                            <span class="main-totals">Media Outreach:&nbsp;<strong><span id="media_outreach">-</span></strong></span>
                            <span class="main-totals">Number of Articles:&nbsp;<strong><span id="coverage_count">-</span></strong></span>
                        </div>
                    </div>

                    <div class="ui one column centered grid charts-nav-buttons" ng-show="activated_tab == 'executive_summary' || activated_tab == 'general_information'">
                        <div class="ui large buttons">
                          <div class="ui information_filter net_effect basic button" title="Show information by Net Effect" data-value="net_effect">Net Effect</div>
                          <div class="ui information_filter numbers basic button" title="Show information by Numbers of Articles" data-value="numbers">Number of articles</div>
                        </div>
                    </div>

                    <div class="ui one column centered grid charts-nav-buttons" ng-show="activated_tab == 'dynamics'">
                        <div class="ui large buttons">
                          <div class="ui chart_date_filter week basic button" title="Show dynamics by weeks" data-value="week">Weeks</div>
                          <div class="ui chart_date_filter month basic button" title="Show dynamics by months" data-value="month">Months</div>
                        </div>
                    </div>
                </div>
            </div>
        <? endif; ?>

        <div class="ui bottom attached tab segment charts regional" data-tab="executive_summary">

            <div class="ui three column doubling stackable centered grid">               
                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-net-effect" type="checkbox">
                    </div>
                    <? endif; ?>
                    Net Effect</div>
                    <div id="chart-net-effect" class="chart-nest pie"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="impact-index" type="checkbox">
                    </div>
                    <? endif; ?>
                    Impact index</div>
                    <div id="impact-index" class="chart-nest pie"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-reach" type="checkbox">
                    </div>
                    <? endif; ?>
                    Media Outreach</div>
                    <div id="chart-reach" class="chart-nest pie"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-articles" type="checkbox">
                    </div>
                    <? endif; ?>
                    Number of articles</div>
                    <div id="chart-articles" class="chart-nest pie"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-main-tonality" type="checkbox">
                    </div>
                    <? endif; ?>
                    Main tonality</div>
                    <div id="chart-main-tonality" class="chart-nest pie"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="key_meesages" type="checkbox">
                    </div>
                    <? endif; ?>
                    KEY MESSAGES</div>
                    <div id="key_meesages"></div>
                    <br/>
                    <div class="seven wide column" ng-if="message_penetrations.length > 0">
                        <h3 class="no-bottom-margin font-weight-normal">
                            Key message penetration
                            <a id="save_message_penetration_xls" class="download-xls highcharts-button" title="Download XLS File"></a>
                        </h3>
                        <table class="ui compact celled table <?= $isPrivateReport ? 'private' : '' ?>" id="message_penetration_table">
                            <thead>
                                <th ng-repeat="message_penetration in message_penetrations" class="font-weight-normal">{{message_penetration['title']}}</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td ng-repeat="message_penetration in message_penetrations">
                                        <div class="clickable" ng-click="go_to_coverage({'key_message_penetration': message_penetration['message_penetration']})" ng-if="is_private_report">{{message_penetration['count']}}</div>
                                        <div ng-if="!is_private_report">{{message_penetration['count']}}</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ui one column centered grid">
                <div class="column centered">
                    <div class="fifteen wide column chart-holder">
                        <div class="ui pointing below label header">
                        <? if ($isPrivateReport): ?>
                        <div class="ui checkbox pdf" title="Select for export into pdf">
                            <input name="chart-indexes-comparison" type="checkbox">
                        </div>
                        <? endif; ?>
                        3 Indexes comparison</div>
                        <div id="chart-indexes-comparison" class="chart-nest bubble" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
            
            <div class="ui one column centered grid">
                <div class="column chart-holder" ng-if="is_show_kpi()">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-regional-statistics" type="checkbox">
                    </div>
                    <? endif; ?>
                    Kaspersky Lab Regional Statistics</div>
                    <a id="save_regional_statistics_xls" class="download-xls highcharts-button" title="Download XLS File"></a>
                    <div class="fixed-first-column-table-block">
                        <table class="fixed-column-table kpi ui compact selectable celled definition table">
                            <thead>
                            <tr><th></th></tr>
                            </thead>
                            <tbody>
                            <tr><td>Articles<br/><small>The number of news items sourced from online and print news</small></td></tr>
                            <tr>
                                <td>
                                    Media Outreach<br/>
                                    <small>
                                        The total potential audience produced by media coverage within a specified time period, measured by impressions
                                        (unique users, followers, etc)
                                    </small>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Impact Index<br/>
                                    <small>
                                        A composite index for measuring the quality of media coverage.
                                        It shows the level of influence on the audience
                                    </small>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Net Effect<br/>
                                    <small>
                                        Combines Impact Index with Reach to define the number of impressions of the article after which people are likely to remember news
                                    </small>
                                </td>
                            </tr>
                            <tr class="vertical_stat">
                                <td>
                                    <div><div class="vertical">Media Category</div></div>
                                    <div class="menu" style="min-height: 120px;">
                                        <div>General Interest</div>
                                        <div>IT</div>
                                        <div>Business</div>
                                        <div>Vertical Media</div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="vertical_stat">
                                <td>
                                    <div><div class="vertical" style="margin-top: 11px;">Media List</div></div>
                                    <div class="menu">
                                        <div>Diamond</div>
                                        <div>Platinum</div>
                                        <div>Gold</div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="vertical_stat">
                                <td>
                                    <div><div class="vertical" style="margin-top: 21px;">Main Tonality</div></div>
                                    <div class="menu">
                                        <div>Positive</div>
                                        <div>Neutral</div>
                                        <div>Negative</div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="vertical_stat">
                                <td>
                                    <div><div class="vertical" style="margin-top: 75%;">Media Subcategory</div></div>
                                    <div class="menu" style="min-height: 120px;">
                                        <div>Automotive</div>
                                        <div>Banking and insurance</div>
                                        <div>Education</div>
                                        <div>Enterprise</div>
                                        <div>Financial</div>
                                        <div>Gaming</div>
                                        <div>Government</div>
                                        <div>Healthcare</div>
                                        <div>Industrial</div>
                                        <div>Lifestyle</div>
                                        <div>Motorsport</div>
                                        <div>MSP media</div>
                                        <div>Parental media</div>
                                        <div>Pet media</div>
                                        <div>SMB Media</div>
                                        <div>Travel</div>
                                        <div>Virtualization</div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="original-table-block">
                            <table class="kpi ui compact selectable celled definition table <?= $isPrivateReport ? 'private' : '' ?>" ng-cloak="" id="kpi_table">
                            <thead>
                                <tr>
                                    <th style="width: 300px;"></th>
                                    <th ng-if="kpi_total_exists()">Total</th>
                                    <th nowrap="nowrap" ng-repeat="location in location_stat['labels']" ng-if="!is_total_column(location.id)">{{location.name}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="min-width: 300px;">Articles<br/><small>The number of news items sourced from online and print news</small></td>
                                    <td ng-if="kpi_total_exists()">
                                        <div class="clickable" ng-click="go_to_coverage({'status': 'approved'})" ng-if="is_private_report">{{location_stat['articles']['total']}}</div>
                                        <div ng-if="!is_private_report">{{location_stat['articles']['total']}}</div>
                                    </td>
                                    <td ng-repeat="location in location_stat['labels']" ng-if="!is_total_column(location.id)">
                                        <div class="clickable" ng-click="go_to_coverage({'location_id': location.id})" ng-if="is_private_report">{{get_kpi_value('articles', location.id)}}</div>
                                        <div ng-if="!is_private_report">{{get_kpi_value('articles', location.id)}}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="min-width: 300px;">
                                        Media Outreach<br/>
                                        <small>
                                            The total potential audience produced by media coverage within a specified time period, measured by impressions
                                            (unique users, followers, etc)
                                        </small>
                                    </td>
                                    <td ng-if="kpi_total_exists()">
                                        <div class="clickable" ng-click="go_to_coverage({'status': 'approved'})" ng-if="is_private_report">{{location_stat['media_outreach']['total']}}</div>
                                        <div ng-if="!is_private_report">{{location_stat['media_outreach']['total']}}</div>
                                    </td>
                                    <td ng-repeat="location in location_stat['labels']" ng-if="!is_total_column(location.id)">
                                        <div class="clickable" ng-click="go_to_coverage({'location_id': location.id})" ng-if="is_private_report">{{get_kpi_value('media_outreach', location.id)}}</div>
                                        <div ng-if="!is_private_report">{{get_kpi_value('media_outreach', location.id)}}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="min-width: 300px;">
                                        Impact Index<br/>
                                        <small>
                                            A composite index for measuring the quality of media coverage.
                                            It shows the level of influence on the audience
                                        </small>
                                    </td>
                                    <td ng-if="kpi_total_exists()">
                                        <div class="clickable" ng-click="go_to_coverage({'status': 'approved'})" ng-if="is_private_report">{{location_stat['impact_index']['total']}}</div>
                                        <div ng-if="!is_private_report">{{location_stat['impact_index']['total']}}</div>
                                    </td>
                                    <td ng-repeat="location in location_stat['labels']" ng-if="!is_total_column(location.id)">
                                        <div class="clickable" ng-click="go_to_coverage({'location_id': location.id})" ng-if="is_private_report">{{get_kpi_value('impact_index', location.id)}}</div>
                                        <div ng-if="!is_private_report">{{get_kpi_value('impact_index', location.id)}}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="min-width: 300px;">
                                        Net Effect<br/>
                                        <small>
                                            Combines Impact Index with Reach to define the number of impressions of the article after which people are likely to remember news
                                        </small>
                                    </td>
                                    <td ng-if="kpi_total_exists()">
                                        <div class="clickable" ng-click="go_to_coverage({'status': 'approved'})" ng-if="is_private_report">{{location_stat['net_effect']['total']}}</div>
                                        <div ng-if="!is_private_report">{{location_stat['net_effect']['total']}}</div>
                                    </td>
                                    <td ng-repeat="location in location_stat['labels']" ng-if="!is_total_column(location.id)">
                                        <div class="clickable" ng-click="go_to_coverage({'location_id': location.id})" ng-if="is_private_report">{{get_kpi_value('net_effect', location.id)}}</div>
                                        <div ng-if="!is_private_report">{{get_kpi_value('net_effect', location.id)}}</div>
                                    </td>
                                </tr>
                                <tr class="vertical_stat">
                                    <td style="min-width: 300px;">
                                        <div><div class="vertical">Media Category</div></div>
                                        <div class="menu" style="min-height: 120px;">
                                            <div>General Interest</div>
                                            <div>IT</div>
                                            <div>Business</div>
                                            <div>Vertical Media</div>
                                        </div>
                                    </td>
                                    <td class="sub_menu" ng-if="kpi_total_exists()">
                                        <div ng-repeat="media_category in media_categories" class="clickable" ng-click="go_to_coverage({'media_category': media_category})" ng-if="is_private_report">{{get_kpi_value('media_category', 'total', media_category)}}</div>
                                        <div ng-repeat="media_category in media_categories" ng-if="!is_private_report">{{get_kpi_value('media_category', 'total', media_category)}}</div>
                                    </td>
                                    <td class="sub_menu" ng-repeat="location in location_stat['labels']" ng-if="!is_total_column(location.id)">
                                        <div ng-repeat="media_category in media_categories" class="clickable" ng-click="go_to_coverage({'location_id': location.id, 'media_category': media_category})" ng-if="is_private_report">{{get_kpi_value('media_category', location.id, media_category)}}</div>
                                        <div ng-repeat="media_category in media_categories" ng-if="!is_private_report">{{get_kpi_value('media_category', location.id, media_category)}}</div>
                                    </td>
                                </tr>
                                <tr class="vertical_stat">
                                    <td style="min-width: 300px;">
                                        <div><div class="vertical" style="margin-top: 11px;">Media List</div></div>
                                        <div class="menu">
                                            <div>Diamond</div>
                                            <div>Platinum</div>
                                            <div>Gold</div>
                                        </div>
                                    </td>
                                    <td class="sub_menu" ng-if="kpi_total_exists()">
                                        <div ng-repeat="media_list in media_lists" class="clickable" ng-click="go_to_coverage({'media_list': media_list})" ng-if="is_private_report">{{get_kpi_value('media_list', 'total', media_list)}}</div>
                                        <div ng-repeat="media_list in media_lists" ng-if="!is_private_report">{{get_kpi_value('media_list', 'total', media_list)}}</div>
                                    </td>
                                    <td class="sub_menu" ng-repeat="location in location_stat['labels']" ng-if="!is_total_column(location.id)">
                                        <div ng-repeat="media_list in media_lists" class="clickable" ng-click="go_to_coverage({'location_id': location.id, 'media_list': media_list})" ng-if="is_private_report">{{get_kpi_value('media_list', location.id, media_list)}}</div>
                                        <div ng-repeat="media_list in media_lists" ng-if="!is_private_report">{{get_kpi_value('media_list', location.id, media_list)}}</div>
                                    </td>
                                </tr>
                                <tr class="vertical_stat">
                                    <td style="min-width: 300px;">
                                        <div><div class="vertical" style="margin-top: 21px;">Main Tonality</div></div>
                                        <div class="menu">
                                            <div>Positive</div>
                                            <div>Neutral</div>
                                            <div>Negative</div>
                                        </div>
                                    </td>
                                    <td class="sub_menu" ng-if="kpi_total_exists()">
                                        <div ng-repeat="main_tonality in main_tonalities" class="clickable" ng-click="go_to_coverage({'main_tonality': main_tonality})" ng-if="is_private_report">{{get_kpi_value('main_tonality', 'total', main_tonality)}}</div>
                                        <div ng-repeat="main_tonality in main_tonalities" ng-if="!is_private_report">{{get_kpi_value('main_tonality', 'total', main_tonality)}}</div>
                                    </td>
                                    <td class="sub_menu" ng-repeat="location in location_stat['labels']" ng-if="!is_total_column(location.id)">
                                        <div ng-repeat="main_tonality in main_tonalities" class="clickable" ng-click="go_to_coverage({'location_id': location.id, 'main_tonality': main_tonality})" ng-if="is_private_report">{{get_kpi_value('main_tonality', location.id, main_tonality)}}</div>
                                        <div ng-repeat="main_tonality in main_tonalities" ng-if="!is_private_report">{{get_kpi_value('main_tonality', location.id, main_tonality)}}</div>
                                    </td>
                                </tr>
                                <tr class="vertical_stat">
                                    <td style="min-width: 300px;">
                                        <div><div class="vertical">Media Subategory</div></div>
                                        <div class="menu" style="min-height: 120px;">
                                            <div>Automotive</div>
                                            <div>Banking and insurance</div>
                                            <div>Education</div>
                                            <div>Enterprise</div>
                                            <div>Financial</div>
                                            <div>Gaming</div>
                                            <div>Government</div>
                                            <div>Healthcare</div>
                                            <div>Industrial</div>
                                            <div>Lifestyle</div>
                                            <div>Motorsport</div>
                                            <div>MSP media</div>
                                            <div>Parental media</div>
                                            <div>Pet media</div>
                                            <div>SMB Media</div>
                                            <div>Travel</div>
                                            <div>Virtualization</div>
                                        </div>
                                    </td>
                                    <td class="sub_menu" ng-if="kpi_total_exists()">
                                        <div ng-repeat="media_subcategory in media_subcategories" class="clickable" ng-click="go_to_coverage({'media_subcategory': media_subcategory})" ng-if="is_private_report">{{get_kpi_value('media_subcategory', 'total', media_subcategory)}}</div>
                                        <div ng-repeat="media_subcategory in media_subcategories" ng-if="!is_private_report">{{get_kpi_value('media_subcategory', 'total', media_subcategory)}}</div>
                                    </td>
                                    <td class="sub_menu" ng-repeat="location in location_stat['labels']" ng-if="!is_total_column(location.id)">
                                        <div ng-repeat="media_subcategory in media_subcategories" class="clickable" ng-click="go_to_coverage({'location_id': location.id, 'media_subcategory': media_subcategory})" ng-if="is_private_report">{{get_kpi_value('media_subcategory', location.id, media_subcategory)}}</div>
                                        <div ng-repeat="media_subcategory in media_subcategories" ng-if="!is_private_report">{{get_kpi_value('media_subcategory', location.id, media_subcategory)}}</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="ui one column centered grid">
                <div class="column chart-holder">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-product-mentions" type="checkbox">
                    </div>
                    <? endif; ?>
                    Product Mentions</div>
                    <a id="save_product_mentions_xls" class="download-xls highcharts-button" title="Download XLS File"></a>
                    <div class="fixed-first-column-table-block">
                        <table class="fixed-column-table ui compact selectable celled definition table">
                            <thead>
                            <tr><th></th></tr>
                            </thead>
                            <tbody>
                            <tr><td>Articles with product mentions</td></tr>
                            <tr><td>Percent of articles with product mentions</td></tr>
                            <tr><td>Impact Index of articles with product mentions</td></tr>
                            <tr><td>Net Effect of articles with product mentions</td></tr>
                            <tr class="vertical_stat">
                                <td>
                                    <div><div class="vertical">Media Category</div></div>
                                    <div class="menu" style="min-height: 120px;">
                                        <div>General Interest</div>
                                        <div>IT</div>
                                        <div>Business</div>
                                        <div>Vertical Media</div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="vertical_stat">
                                <td>
                                    <div><div class="vertical" style="margin-top: 11px;">Media List</div></div>
                                    <div class="menu">
                                        <div>Diamond</div>
                                        <div>Platinum</div>
                                        <div>Gold</div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="vertical_stat">
                                <td>
                                    <div><div class="vertical" style="margin-top: 21px;">Main Tonality</div></div>
                                    <div class="menu">
                                        <div>Positive</div>
                                        <div>Neutral</div>
                                        <div>Negative</div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="vertical_stat">
                                <td>
                                    <div><div class="vertical" style="margin-top: 75%;">Media Subcategory</div></div>
                                    <div class="menu" style="min-height: 120px;">
                                        <div>Automotive</div>
                                        <div>Banking and insurance</div>
                                        <div>Education</div>
                                        <div>Enterprise</div>
                                        <div>Financial</div>
                                        <div>Gaming</div>
                                        <div>Government</div>
                                        <div>Healthcare</div>
                                        <div>Industrial</div>
                                        <div>Lifestyle</div>
                                        <div>Motorsport</div>
                                        <div>MSP media</div>
                                        <div>Parental media</div>
                                        <div>Pet media</div>
                                        <div>SMB Media</div>
                                        <div>Travel</div>
                                        <div>Virtualization</div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="original-table-block">
                            <table id="product_mentions_table" class="ui compact selectable celled definition table <?= $isPrivateReport ? 'private' : '' ?>" ng-cloak="">
                            <thead>
                            <th style="width: 300px;"></th>
                            <th  ng-if="product_mention_total_exists()">Total</th>
                            <th nowrap="nowrap" ng-repeat="location in product_mention['labels']" ng-if="!is_total_column(location.id)">{{location.name}}</th>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="min-width: 300px;">Articles with product mentions</td>
                                <td ng-if="product_mention_total_exists()">
                                    <div class="clickable" ng-click="go_to_coverage({'mention_article': 1})" ng-if="is_private_report">{{product_mention['articles']['total']}}</div>
                                    <div ng-if="!is_private_report">{{product_mention['articles']['total']}}</div>
                                </td>
                                <td ng-repeat="location in product_mention['labels']" ng-if="!is_total_column(location.id)">
                                    <div class="clickable" ng-click="go_to_coverage({'mention_article': 1, 'location_id': location.id})" ng-if="is_private_report">{{product_mention_value('articles', location.id)}}</div>
                                    <div ng-if="!is_private_report">{{product_mention_value('articles', location.id)}}</div>
                                </td>
                            </tr>
                            <tr>
                                <td style="min-width: 300px;">Percent of articles with product mentions</td>
                                <td  ng-if="product_mention_total_exists()">
                                    <div class="clickable" ng-click="go_to_coverage({'mention_article': 1})" ng-if="is_private_report">{{product_mention['percent']['total']}}</div>
                                    <div ng-if="!is_private_report">{{product_mention['percent']['total']}}</div>
                                </td>
                                <td ng-repeat="location in product_mention['labels']" ng-if="!is_total_column(location.id)">
                                    <div class="clickable" ng-click="go_to_coverage({'mention_article': 1, 'location_id': location.id})" ng-if="is_private_report">{{product_mention_value('percent', location.id)}}</div>
                                    <div ng-if="!is_private_report">{{product_mention_value('percent', location.id)}}</div>
                                </td>
                            </tr>
                            <tr>
                                <td style="min-width: 300px;">Impact Index of articles with product mentions</td>
                                <td ng-if="product_mention_total_exists()">
                                    <div class="clickable" ng-click="go_to_coverage({'mention_article': 1})" ng-if="is_private_report">{{product_mention['impact_index']['total']}}</div>
                                    <div ng-if="!is_private_report">{{product_mention['impact_index']['total']}}</div>
                                </td>
                                <td ng-repeat="location in product_mention['labels']" ng-if="!is_total_column(location.id)">
                                    <div class="clickable" ng-click="go_to_coverage({'mention_article': 1, 'location_id': location.id})" ng-if="is_private_report">{{product_mention_value('impact_index', location.id)}}</div>
                                    <div ng-if="!is_private_report">{{product_mention_value('impact_index', location.id)}}</div>
                                </td>
                            </tr>
                            <tr>
                                <td style="min-width: 300px;">Net Effect of articles with product mentions</td>
                                <td ng-if="product_mention_total_exists()">
                                    <div class="clickable" ng-click="go_to_coverage({'mention_article': 1})" ng-if="is_private_report">{{product_mention['net_effect']['total']}}</div>
                                    <div ng-if="!is_private_report">{{product_mention['articles']['total']}}</div>
                                </td>
                                <td ng-repeat="location in product_mention['labels']" ng-if="!is_total_column(location.id)">
                                    <div class="clickable" ng-click="go_to_coverage({'mention_article': 1, 'location_id': location.id})" ng-if="is_private_report">{{product_mention_value('net_effect', location.id)}}</div>
                                    <div ng-if="!is_private_report">{{product_mention_value('net_effect', location.id)}}</div>
                                </td>
                            </tr>
                            <tr class="vertical_stat">
                                <td>
                                    <div><div class="vertical">Media Category</div></div>
                                    <div class="menu" style="min-height: 120px;">
                                        <div>General Interest</div>
                                        <div>IT</div>
                                        <div>Business</div>
                                        <div>Vertical Media</div>
                                    </div>
                                </td>
                                <td ng-if="product_mention_total_exists()" class="sub_menu">
                                    <div ng-repeat="media_category in media_categories" class="clickable" ng-click="go_to_coverage({'mention_article': 1, 'media_category': media_category})" ng-if="is_private_report">{{product_mention_value('media_category', 'total', media_category)}}</div>
                                    <div ng-repeat="media_category in media_categories" ng-if="!is_private_report">{{product_mention_value('media_category', 'total', media_category)}}</div>
                                </td>
                                <td class="sub_menu" ng-repeat="location in product_mention['labels']" ng-if="!is_total_column(location.id)">
                                    <div ng-repeat="media_category in media_categories" class="clickable" ng-click="go_to_coverage({'mention_article': 1, 'location_id': location.id, 'media_category': media_category})" ng-if="is_private_report">{{product_mention_value('media_category', location.id, media_category)}}</div>
                                    <div ng-repeat="media_category in media_categories" ng-if="!is_private_report">{{product_mention_value('media_category', location.id, media_category)}}</div>
                                </td>
                            </tr>
                            <tr class="vertical_stat">
                                <td>
                                    <div><div class="vertical" style="margin-top: 11px;">Media List</div></div>
                                    <div class="menu">
                                        <div>Diamond</div>
                                        <div>Platinum</div>
                                        <div>Gold</div>
                                    </div>
                                </td>
                                <td ng-if="product_mention_total_exists()" class="sub_menu">
                                    <div ng-repeat="media_list in media_lists" class="clickable" ng-click="go_to_coverage({'mention_article': 1, 'media_list': media_list})" ng-if="is_private_report">{{product_mention_value('media_list', 'total', media_list)}}</div>
                                    <div ng-repeat="media_list in media_lists" ng-if="!is_private_report">{{product_mention_value('media_list', 'total', media_list)}}</div>
                                </td>
                                <td class="sub_menu" ng-repeat="location in product_mention['labels']" ng-if="!is_total_column(location.id)">
                                    <div ng-repeat="media_list in media_lists" class="clickable" ng-click="go_to_coverage({'mention_article': 1, 'location_id': location.id, 'media_list': media_list})" ng-if="is_private_report">{{product_mention_value('media_list', location.id, media_list)}}</div>
                                    <div ng-repeat="media_list in media_lists" ng-if="!is_private_report">{{product_mention_value('media_list', location.id, media_list)}}</div>
                                </td>
                            </tr>
                            <tr class="vertical_stat">
                                <td>
                                    <div><div class="vertical" style="margin-top: 21px;">Main Tonality</div></div>
                                    <div class="menu">
                                        <div>Positive</div>
                                        <div>Neutral</div>
                                        <div>Negative</div>
                                    </div>
                                </td>
                                <td ng-if="product_mention_total_exists()" class="sub_menu">
                                    <div ng-repeat="main_tonality in main_tonalities" class="clickable" ng-click="go_to_coverage({'mention_article': 1, 'main_tonality': main_tonality})" ng-if="is_private_report">{{product_mention_value('main_tonality', 'total', main_tonality)}}</div>
                                    <div ng-repeat="main_tonality in main_tonalities" ng-if="!is_private_report">{{product_mention_value('main_tonality', 'total', main_tonality)}}</div>
                                </td>
                                <td class="sub_menu" ng-repeat="location in product_mention['labels']" ng-if="!is_total_column(location.id)">
                                    <div ng-repeat="main_tonality in main_tonalities" class="clickable" ng-click="go_to_coverage({'mention_article': 1, 'location_id': location.id, 'main_tonality': main_tonality})" ng-if="is_private_report">{{product_mention_value('main_tonality', location.id, main_tonality)}}</div>
                                    <div ng-repeat="main_tonality in main_tonalities" ng-if="!is_private_report">{{product_mention_value('main_tonality', location.id, main_tonality)}}</div>
                                </td>
                            </tr>
                            <tr class="vertical_stat">
                            <td style="min-width: 300px;">
                                    <div><div class="vertical">Media Subategory</div></div>
                                    <div class="menu" style="min-height: 120px;">
                                        <div>Automotive</div>
                                        <div>Banking and insurance</div>
                                        <div>Education</div>
                                        <div>Enterprise</div>
                                        <div>Financial</div>
                                        <div>Gaming</div>
                                        <div>Government</div>
                                        <div>Healthcare</div>
                                        <div>Industrial</div>
                                        <div>Lifestyle</div>
                                        <div>Motorsport</div>
                                        <div>MSP media</div>
                                        <div>Parental media</div>
                                        <div>Pet media</div>
                                        <div>SMB Media</div>
                                        <div>Travel</div>
                                        <div>Virtualization</div>
                                    </div>
                                </td>
                                <td class="sub_menu" ng-if="kpi_total_exists()">
                                    <div ng-repeat="media_subcategory in media_subcategories" class="clickable" ng-click="go_to_coverage({'media_subcategory': media_subcategory})" ng-if="is_private_report">{{product_mention_value('media_subcategory', 'total', media_subcategory)}}</div>
                                    <div ng-repeat="media_subcategory in media_subcategories" ng-if="!is_private_report">{{product_mention_value('media_subcategory', 'total', media_subcategory)}}</div>
                                </td>
                                <td class="sub_menu" ng-repeat="location in location_stat['labels']" ng-if="!is_total_column(location.id)">
                                    <div ng-repeat="media_subcategory in media_subcategories" class="clickable" ng-click="go_to_coverage({'location_id': location.id, 'media_subcategory': media_subcategory})" ng-if="is_private_report">{{product_mention_value('media_subcategory', location.id, media_subcategory)}}</div>
                                    <div ng-repeat="media_subcategory in media_subcategories" ng-if="!is_private_report">{{product_mention_value('media_subcategory', location.id, media_subcategory)}}</div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ui bottom attached tab segment charts general" data-tab="general_information">
            <input type="hidden" id="general_information_filter" value="net_effect" />

            <div class="ui three column centered doubling stackable grid">
                
                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-media-type" type="checkbox">
                    </div>
                    <? endif; ?>
                    Media types</div>
                    <div id="chart-media-type" class="chart-nest pie"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-media-list" type="checkbox">
                    </div>
                    <? endif; ?>
                    Media lists</div>
                    <div id="chart-media-list" class="chart-nest pie"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-media-category" type="checkbox">
                    </div>
                    <? endif; ?>
                    Media categories</div>
                    <div id="chart-media-category" class="chart-nest pie"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-subcategories" type="checkbox">
                    </div>
                    <? endif; ?>
                    Media Subcategories</div>
                    <div id="chart-subcategories" class="chart-nest pie"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-mention-relation" type="checkbox">
                    </div>
                    <? endif; ?>
                    Products mentions</div>
                    <div id="chart-mention-relation" class="chart-nest pie"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-speaker-quote" type="checkbox">
                    </div>
                    <? endif; ?>
                    Speakers quoted</div>
                    <div id="chart-speaker-quote" class="chart-nest pie"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-third-speaker-quote" type="checkbox">
                    </div>
                    <? endif; ?>
                    3rd party Speakers</div>
                    <div id="chart-third-speaker-quote" class="chart-nest pie"></div>
                </div>
            </div>

            <div class="ui two column centered doubling stackable grid">

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-products" type="checkbox">
                    </div>
                    <? endif; ?>
                    Products rating</div>
                    <div id="chart-products" class="chart-nest bar"></div>
                </div>
                <!--
                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="security_intelligence" type="checkbox">
                    </div>
                    <? endif; ?>
                    Security Intelligence</div>
                    <div id="security_intelligence" class="chart-nest bar"></div>
                </div>-->

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-speakers-count" type="checkbox">
                    </div>
                    <? endif; ?>
                    Speakers rating</div>
                    <div id="chart-speakers-count" class="chart-nest bar"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-newsbreaks" type="checkbox">
                    </div>
                    <? endif; ?>
                    Newsbreaks Rating</div>
                    <div id="chart-newsbreaks" class="chart-nest bar"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-campaign" type="checkbox">
                    </div>
                    <? endif; ?>
                    Themes/Campaigns rating</div>
                    <div id="chart-campaign" class="chart-nest bar"></div>
                </div>

                <div class="column chart-holder">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-desired-article" type="checkbox">
                    </div>
                    <? endif; ?>
                    Desired/Undesired articles</div>
                    <div id="chart-desired-article" class="chart-nest pie"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-events" type="checkbox">
                    </div>
                    <? endif; ?>
                    Events rating</div>
                    <div id="chart-events" class="chart-nest bar"></div>
                </div>

            </div>

            <div class="ui one column doubling stackable grid container">
                <div class="ui pointing below label column header chart-holder">
                <? if ($isPrivateReport): ?>
                <div class="ui checkbox pdf" title="Select for export into pdf">
                    <input name="chart-top-story" type="checkbox">
                </div>
                <? endif; ?>
                A Top Story &mdash; publication with Impact Index &ge; 0,75{{test}}</div>
                <div class="column" ng-cloak>
                    <ol class="nine wide column centered">
                        <li ng-repeat="top_story in top_stories" style="padding: 10px;">
                            <div><strong>{{top_story.headline}}</strong></div>
                            <div><a ng-href="{{top_story.url}}" target="_blank">{{top_story.url | truncate : 100}}</a></div>
                            <div>{{top_story.country}}, {{top_story.date}}, Impact Index &mdash; {{top_story.impact_index}}</div>
                        </li>
                    </ol>
                </div>
            </div>
            
        </div>
        <div class="ui bottom attached tab segment charts timeline" data-tab="dynamics">
            <input type="hidden" id="timeline_filter" value="month" />
            
            <div class="ui one column centered grid">
                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-net-dynamic" type="checkbox">
                    </div>
                    <? endif; ?>
                    Net Effect</div>
                    <div id="chart-net-dynamic" class="chart-nest line"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-impact-dynamic" type="checkbox">
                    </div>
                    <? endif; ?>
                    Impact Index</div>
                    <div id="chart-impact-dynamic" class="chart-nest line"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-reach-dynamic" type="checkbox">
                    </div>
                    <? endif; ?>
                    Media Outreach</div>
                    <div id="chart-reach-dynamic" class="chart-nest line"></div>
                </div>

                <div class="column">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-articles-dynamic" type="checkbox">
                    </div>
                    <? endif; ?>
                    Number of Articles</div>
                    <div id="chart-articles-dynamic" class="chart-nest line"></div>
                </div>

            </div>
        </div>

        <div class="ui bottom attached tab segment charts comparison" id="comparison-tab" data-tab="comparison" style="padding-top: 0px;">
            <div class="ui one column centered grid">
                <div class="column chart-holder">
                    <div class="ui pointing below label header">
                    <? if ($isPrivateReport): ?>
                    <div class="ui checkbox pdf" title="Select for export into pdf">
                        <input name="chart-comparison" type="checkbox">
                    </div>
                    <? endif; ?>
                    Comparison ratings</div>
                    <a id="save_comparison_xls" class="download-xls highcharts-button" title="Download XLS File" ng-click="export($event)"></a>
                    <div class="fixed-first-column-table-block">
                        <table class="fixed-column-table kpi ui compact selectable celled definition table">
                            <thead>
                            <tr><th></th></tr>
                            </thead>
                            <tbody>
                            <tr><td>Articles<br/><small>The number of news items sourced from online and print news</small></td></tr>
                            <tr>
                                <td>
                                    Media Outreach<br/>
                                    <small>
                                        The total potential audience produced by media coverage within a specified time period, measured by impressions
                                        (unique users, followers, etc)
                                    </small>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Impact Index<br/>
                                    <small>
                                        A composite index for measuring the quality of media coverage.
                                        It shows the level of influence on the audience
                                    </small>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Net Effect<br/>
                                    <small>
                                        Combines Impact Index with Reach to define the number of impressions of the article after which people are likely to remember news
                                    </small>
                                </td>
                            </tr>
                            <tr class="vertical_stat">
                                <td>
                                    <div><div class="vertical">Media Category</div></div>
                                    <div class="menu" style="min-height: 120px;">
                                        <div>General Interest</div>
                                        <div>IT</div>
                                        <div>Business</div>
                                        <div>Vertical Media</div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="vertical_stat">
                                <td>
                                    <div><div class="vertical" style="margin-top: 11px;">Media List</div></div>
                                    <div class="menu">
                                        <div>Diamond</div>
                                        <div>Platinum</div>
                                        <div>Gold</div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="vertical_stat">
                                <td>
                                    <div><div class="vertical" style="margin-top: 21px;">Main Tonality</div></div>
                                    <div class="menu">
                                        <div>Positive</div>
                                        <div>Neutral</div>
                                        <div>Negative</div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="original-table-block">
                        <table class="kpi ui compact selectable celled definition table <?= $isPrivateReport ? 'private' : '' ?>" ng-cloak="" id="comparison_table">
                            <thead>
                                <tr>
                                    <th style="width: 300px;"></th>
                                    <th>Total</th>
                                    <th nowrap="nowrap" ng-repeat="location in comparison['labels']" ng-if="!is_total_column(location.id)" title="{{location.filter_by}}">{{location.name}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="min-width: 300px;">Articles<br/><small>The number of news items sourced from online and print news</small></td>
                                    <td>
                                        <div class="clickable" ng-click="go_to_coverage_with_total_sections({'status': 'approved'})" ng-if="is_private_report">{{comparison['articles']['total']}}</div>
                                        <div ng-if="!is_private_report">{{comparison['articles']['total']}}</div>
                                    </td>
                                    <td ng-repeat="location in comparison['labels']" ng-if="!is_total_column(location.id)">
                                        <div class="clickable" ng-click="go_to_coverage_with_section(location.filter_by, location.filter_id, {})" ng-if="is_private_report">{{get_comparison_value('articles', location.id)}}</div>
                                        <div ng-if="!is_private_report">{{get_comparison_value('articles', location.id)}}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="min-width: 300px;">
                                        Media Outreach<br/>
                                        <small>
                                            The total potential audience produced by media coverage within a specified time period, measured by impressions
                                            (unique users, followers, etc)
                                        </small>
                                    </td>
                                    <td>
                                        <div class="clickable" ng-click="go_to_coverage_with_total_sections({'status': 'approved'})" ng-if="is_private_report">{{comparison['media_outreach']['total']}}</div>
                                        <div ng-if="!is_private_report">{{comparison['media_outreach']['total']}}</div>
                                    </td>
                                    <td ng-repeat="location in comparison['labels']" ng-if="!is_total_column(location.id)">
                                        <div class="clickable" ng-click="go_to_coverage_with_section(location.filter_by, location.filter_id, {})" ng-if="is_private_report">{{get_comparison_value('media_outreach', location.id)}}</div>
                                        <div ng-if="!is_private_report">{{get_comparison_value('media_outreach', location.id)}}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="min-width: 300px;">
                                        Impact Index<br/>
                                        <small>
                                            A composite index for measuring the quality of media coverage.
                                            It shows the level of influence on the audience
                                        </small>
                                    </td>
                                    <td>
                                        <div class="clickable" ng-click="go_to_coverage_with_total_sections({'status': 'approved'})" ng-if="is_private_report">{{comparison['impact_index']['total']}}</div>
                                        <div ng-if="!is_private_report">{{comparison['impact_index']['total']}}</div>
                                    </td>
                                    <td ng-repeat="location in comparison['labels']" ng-if="!is_total_column(location.id)">
                                        <div class="clickable" ng-click="go_to_coverage_with_section(location.filter_by, location.filter_id, {})" ng-if="is_private_report">{{get_comparison_value('impact_index', location.id)}}</div>
                                        <div ng-if="!is_private_report">{{get_comparison_value('impact_index', location.id)}}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="min-width: 300px;">
                                        Net Effect<br/>
                                        <small>
                                            Combines Impact Index with Reach to define the number of impressions of the article after which people are likely to remember news
                                        </small>
                                    </td>
                                    <td>
                                        <div class="clickable" ng-click="go_to_coverage_with_total_sections({'status': 'approved'})" ng-if="is_private_report">{{comparison['net_effect']['total']}}</div>
                                        <div ng-if="!is_private_report">{{comparison['net_effect']['total']}}</div>
                                    </td>
                                    <td ng-repeat="location in comparison['labels']" ng-if="!is_total_column(location.id)">
                                        <div class="clickable" ng-click="go_to_coverage_with_section(location.filter_by, location.filter_id, {})" ng-if="is_private_report">{{get_comparison_value('net_effect', location.id)}}</div>
                                        <div ng-if="!is_private_report">{{get_comparison_value('net_effect', location.id)}}</div>
                                    </td>
                                </tr>
                                <tr class="vertical_stat">
                                    <td style="min-width: 300px;">
                                        <div><div class="vertical">Media Category</div></div>
                                        <div class="menu"  style="min-height: 120px;">
                                            <div>General Interest</div>
                                            <div>IT</div>
                                            <div>Business</div>
                                            <div>Vertical Media</div>
                                        </div>
                                    </td>
                                    <td class="sub_menu">
                                        <div ng-repeat="media_category in media_categories" class="clickable" ng-click="go_to_coverage_with_total_sections({'media_category': media_category})" ng-if="is_private_report">{{get_comparison_value('media_category', 'total', media_category)}}</div>
                                        <div ng-repeat="media_category in media_categories" ng-if="!is_private_report">{{get_comparison_value('media_category', 'total', media_category)}}</div>
                                    </td>
                                    <td class="sub_menu" ng-repeat="location in comparison['labels']" ng-if="!is_total_column(location.id)">
                                        <div ng-repeat="media_category in media_categories" class="clickable" ng-click="go_to_coverage_with_section(location.filter_by, location.filter_id, {'media_category': media_category})" ng-if="is_private_report">{{get_comparison_value('media_category', location.id, media_category)}}</div>
                                        <div ng-repeat="media_category in media_categories" ng-if="!is_private_report">{{get_comparison_value('media_category', location.id, media_category)}}</div>
                                    </td>
                                </tr>
                                <tr class="vertical_stat">
                                    <td style="min-width: 300px;">
                                        <div><div class="vertical" style="margin-top: 11px;">Media List</div></div>
                                        <div class="menu">
                                            <div>Diamond</div>
                                            <div>Platinum</div>
                                            <div>Gold</div>
                                        </div>
                                    </td>
                                    <td class="sub_menu">
                                        <div ng-repeat="media_list in media_lists" class="clickable" ng-click="go_to_coverage_with_total_sections({'media_list': media_list})" ng-if="is_private_report">{{get_comparison_value('media_list', 'total', media_list)}}</div>
                                        <div ng-repeat="media_list in media_lists" ng-if="!is_private_report">{{get_comparison_value('media_list', 'total', media_list)}}</div>
                                    </td>
                                    <td class="sub_menu" ng-repeat="location in comparison['labels']" ng-if="!is_total_column(location.id)">
                                        <div ng-repeat="media_list in media_lists" class="clickable" ng-click="go_to_coverage_with_section(location.filter_by, location.filter_id, {'media_list': media_list})" ng-if="is_private_report">{{get_comparison_value('media_list', location.id, media_list)}}</div>
                                        <div ng-repeat="media_list in media_lists" ng-if="!is_private_report">{{get_comparison_value('media_list', location.id, media_list)}}</div>
                                    </td>
                                </tr>
                                <tr class="vertical_stat">
                                    <td style="min-width: 300px;">
                                        <div><div class="vertical"  style="margin-top: 21px;">Main Tonality</div></div>
                                        <div class="menu">
                                            <div>Positive</div>
                                            <div>Neutral</div>
                                            <div>Negative</div>
                                        </div>
                                    </td>
                                    <td class="sub_menu">
                                        <div ng-repeat="main_tonality in main_tonalities" class="clickable" ng-click="go_to_coverage_with_total_sections({'main_tonality': main_tonality})" ng-if="is_private_report">{{get_comparison_value('main_tonality', 'total', main_tonality)}}</div>
                                        <div ng-repeat="main_tonality in main_tonalities" ng-if="!is_private_report">{{get_comparison_value('main_tonality', 'total', main_tonality)}}</div>
                                    </td>
                                    <td class="sub_menu" ng-repeat="location in comparison['labels']" ng-if="!is_total_column(location.id)">
                                        <div ng-repeat="main_tonality in main_tonalities" class="clickable" ng-click="go_to_coverage_with_section(location.filter_by, location.filter_id, {'main_tonality': main_tonality})" ng-if="is_private_report">{{get_comparison_value('main_tonality', location.id, main_tonality)}}</div>
                                        <div ng-repeat="main_tonality in main_tonalities" ng-if="!is_private_report">{{get_comparison_value('main_tonality', location.id, main_tonality)}}</div>
                                    </td>
                                </tr>
                                <tr class="vertical_stat">
                                    <td style="min-width: 300px;">
                                        <div><div class="vertical" style="margin-top: 75%;">Media Subategory</div></div>
                                        <div class="menu" style="min-height: 120px;">
                                            <div>Automotive</div>
                                            <div>Banking and insurance</div>
                                            <div>Education</div>
                                            <div>Enterprise</div>
                                            <div>Financial</div>
                                            <div>Gaming</div>
                                            <div>Government</div>
                                            <div>Healthcare</div>
                                            <div>Industrial</div>
                                            <div>Lifestyle</div>
                                            <div>Motorsport</div>
                                            <div>MSP media</div>
                                            <div>Parental media</div>
                                            <div>Pet media</div>
                                            <div>SMB Media</div>
                                            <div>Travel</div>
                                            <div>Virtualization</div>
                                        </div>
                                    </td>
                                    <td class="sub_menu">
                                        <div ng-repeat="media_subcategory in media_subcategories" class="clickable" ng-click="go_to_coverage_with_total_sections({'media_subcategory': media_subcategory})" ng-if="is_private_report">{{get_comparison_value('media_subcategory', 'total', media_subcategory)}}</div>
                                        <div ng-repeat="media_subcategory in media_subcategories" ng-if="!is_private_report">{{get_comparison_value('media_subcategory', 'total', media_subcategory)}}</div>
                                    </td>
                                    <td class="sub_menu" ng-repeat="location in comparison['labels']" ng-if="!is_total_column(location.id)">
                                        <div ng-repeat="media_subcategory in media_subcategories" class="clickable" ng-click="go_to_coverage_with_section(location.filter_by, location.filter_id, {'media_subcategory': media_subcategory})" ng-if="is_private_report">{{get_comparison_value('media_subcategory', location.id, media_subcategory)}}</div>
                                        <div ng-repeat="media_subcategory in media_subcategories" ng-if="!is_private_report">{{get_comparison_value('media_subcategory', location.id, media_subcategory)}}</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <? if (Auth::check() && Auth::user()->role == App\Models\User::ROLE_ADMINISTRATOR && (empty($publicReport) || !$publicReport)): ?>
            <div class="page admin ui tab bottom attached charts segment manage reports" data-tab="reports">
                <div class="ui one column relaxed grid">
                    <div class="column">
                        <div class="column" style="padding-top: 0px;">
                            <table class="ui fixed single line celled table" id="charts-report-list-table" width="100%">
                                <thead>
                                    <th class="col-reportname">Filename</th>
                                    <th class="col-date">Date</th>
                                    <th class="col-actions"></th>
                                </thead>
                            </table>
                        </div>
                    </div>

                    <div id="edit-report-modal" class="ui modal">
                        <div class="header"></div>
                        <div class="content">
                        </div>
                        <div class="actions">
                            <div class="teal ui button ok save-edit">Save</div>
                            <div class="ui button cancel basic no-border-button">Cancel</div>
                        </div>
                    </div>

                </div>
            </div>
        <? endif; ?>
    </div>

    <div class="ui modal report-modal" modal="" visible="new_report.is_show_modal">
        <i ng-click="hide_modal()" class="close icon"></i>
        <div class="header">Save report filter</div>
        <div class="content">
            <div class="ui form">
                <div class="field first-step" ng-if="new_report.first_step">
                    <input type="text" id="report_name" ng-model="new_report.data.name" placeholder="Report name">
                </div>
                <div class="field second-step" ng-if="new_report.second_step">
                    <input type="text" selected-input="" id="report_url" ng-model="new_report.data.url" placeholder="Report url" readonly="readonly">
                </div>
            </div>
        </div>
        <div class="actions">
            <div class="ui button" ng-if="new_report.first_step" ng-click="hide_modal()">Cancel</div>
            <div class="positive ui button approve" ng-if="new_report.first_step" ng-click="save_report()">Save</div>
            <div class="ui positive right labeled icon button second-step" ng-if="new_report.second_step" ng-click="hide_modal()">
                Ok<i class="checkmark icon" ></i>
            </div>
        </div>
    </div>

    <div id="export-pdf" class="ui modal">
        <div class="header">Export pdf</div>
        <div class="content">
            <span>Export pdf takes few minutes. You can download the file on the <a href="/export">export</a> page</span>
        </div>
        <div class="actions">
            <div class="ui cancel button basic no-border-button">CLOSE</div>
        </div>
    </div>

</div>