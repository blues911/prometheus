<script type="text/javascript">
    var is_new          = <?=json_encode(isset($new))?>;
    var user_id         = <?=Auth::user()->id?>;
    var user_region_id  = <?=Auth::user()->region_id?>;
    var user_country_id = <?=Auth::user()->country_id?>;
    var events          = <?=json_encode($eventData)?>;
    var news            = <?=json_encode($newsData)?>;
    var speakers        = <?=json_encode($speakersData)?>;
    var news_breaks     = <?=json_encode($newsBreaks)?>;
    var speakers_quoted = <?=json_encode($speakersQuoted)?>;
    var access_edit     = <?= (isset($access_edit) ? json_encode($access_edit) : json_encode(true))?>;
    var coding_closed_months  = <?=json_encode($codingClosedMonths)?>;
</script>

<div class="ui data-saved success message">
    <i class="close icon"></i>
    <div class="header">
        Data updated successfully!
    </div>
    <p>Please, keep filling</p>
</div>

<form action="/coverage" method="post" id="coverage-form" class="instant-update">

<div class="ui tabular menu sections ordered steps">
    <div class="step item active" data-tab="general">
        <div class="content">
            <div class="title">General</div>
            <div class="description">Put in the general information</div>
        </div>
    </div>
    <div class="disabled step item" data-tab="analytical-pt1">
        <div class="content">
            <div class="title">Analytical Part 1</div>
            <div class="description">Put in the analytical information</div>
        </div>
    </div>
    <div class="disabled step item" data-tab="analytical-pt2">
        <div class="content">
            <div class="title">Analytical Part 2</div>
            <div class="description">Put in the analytical information</div>
        </div>
    </div>
    <div class="disabled step item" data-tab="speakers">
        <div class="content">
            <div class="title">Speakers</div>
            <div class="description">Put in the data about speakers</div>
        </div>
    </div>
    <div class="disabled step item" data-tab="impact">
        <div class="content">
            <div class="title">Impact</div>
            <div class="description">Impact Index & Net Effect information</div>
        </div>
    </div>
</div>

<div class="ui active tab segment" data-tab="general">
    <div class="ui form general">
        <input type="hidden" disabled="disabled" name="id" id="id" value="<?= $coverage->id ?>">

        <div class="fields">
            <div class="field">
                <label>Region</label>

                <div class="ui selection dropdown region-list <?= (Auth::user()->role === App\Models\User::ROLE_USER ? 'disabled' : '' )?>">
                    <input type="hidden" name="region_id" value="<?= $coverage->region->id ?>">
                    <div class="default text"><?= $coverage->region->name ?></div>
                    <i class="dropdown icon"></i>
                    <div class="menu">
                        <?php foreach ($regions as $region): ?>
                            <div class="item" data-value="<?= $region->id ?>"><?= $region->name ?></div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

            <div class="field">
                <label>Country</label>

                <div class="ui selection dropdown countries-list">
                    <input type="hidden" name="country_id" value="<?= $coverage->country->id ?>">

                    <div class="default text"><?= $coverage->country->name ?></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <?php foreach ($countries as $country): ?>
                            <div class="item" data-value="<?= $country->id ?>"><?= $country->name ?></div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

            <div class="field">
                <label for="date">Date of Publication</label>

                <div class="ui right labeled icon input date">
                    <input type="text" id="date" name="date" value="<?= (isset($coverage->date)) ? date('Y-m-d', strtotime($coverage->date)) : '' ?>">
                    <i class="calendar icon add-on"></i>
                </div>
            </div>

            <div class="five wide field">
                <label>Publication Name</label>
                <div class="ui selection dropdown media">
                    <input type="hidden" name="name" value="<?= $coverage->name ?>">

                    <div class="default text"><?= $coverage->name ?></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <?php foreach ($media as $item): ?>
                            <div class="item"
                                 data-value="<?= $item->name ?>"
                                 data-media="<?= htmlspecialchars(App\Models\Media::getMediaForItem($item)); ?>"
                                 data-subcategories="<?= $item->subcategoriesIds() ?>"><?= $item->name ?></div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="fields">
            <div class="two wide field">
                <label for="name">Reach</label>

                <div class="ui right labeled icon input blue">
                    <input id="reach_field" type="text" name="reach" id="name" value="<?= $coverage->reach ?>" disabled="disabled">
                    <i id="reach_icon" class="lock icon"></i>
                </div>
            </div>
            <div class="four wide field">
                <label for="author">Author</label>

                <div class="ui right labeled icon input">
                    <input type="text" name="author" id="author" value="<?= $coverage->author ?>">
                    <i class="user icon"></i>
                </div>
            </div>
            <div class="six wide field">
                <label for="headline">Headline</label>
                <input type="text" name="headline" id="headline" value="<?= $coverage->headline ?>">
            </div>
        </div>

        <h4 class="ui horizontal header divider">Media options</h4>

        <div class="fields">
            <div class="field">
                <label>Media List</label>

                <div class="ui selection disabled dropdown media-list">
                    <input type="hidden" name="media_list" value="<?= $coverage->media_list ?>">

                    <div class="default text"><?= $coverage->media_list ?></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <div class="item" data-value="0.6">Gold</div>
                        <div class="item" data-value="0.8">Platinum</div>
                        <div class="item" data-value="1">Diamond</div>
                    </div>
                </div>
            </div>

            <div class="field">
                <label>Media Type</label>

                <div class="ui selection disabled dropdown media-type">
                    <input type="hidden" name="media_type" id="media_type_field" value="<?= $coverage->media_type ?>">

                    <div class="default text"><?= $coverage->media_type ?></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <div class="item" data-value="agency">Agency</div>
                        <div class="item" data-value="online">Online</div>
                        <div class="item" data-value="print">Print</div>
                        <div class="item" data-value="radio">Radio</div>
                        <div class="item" data-value="tv">TV</div>
                    </div>
                </div>
            </div>

            <div class="field">
                <label>Media Category</label>

                <div class="ui selection disabled dropdown media-category">
                    <input type="hidden" name="media_category" value="<?= $coverage->media_category ?>">

                    <div class="default text"><?= $coverage->media_category ?></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <div class="item" data-value="vertical">Vertical</div>
                        <div class="item" data-value="business">Business Media</div>
                        <div class="item" data-value="it">IT Media</div>
                        <div class="item" data-value="general">General Media</div>
                    </div>
                </div>
            </div>

            <div class="field">
                <label>Media Subcategories</label>

                <div class="ui selection disabled dropdown multiple media-subcategories">
                    <?php if (isset($active_subcategories['list'])) : ?>
                        <input type="hidden" name="media_subcategories" value="<?= $active_subcategories['ids'] ?>">
                        <?php foreach ($active_subcategories['list'] as $subcategory) : ?>
                            <a class="ui label" data-value="<?= $subcategory['id'] ?>"><?= $subcategory['title'] ?><i class="delete icon"></i></a>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <input type="hidden" name="media_subcategories" value="">
                    <?php endif; ?>
                    <div class="default text"></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <?php foreach ($subcategories as $subcategory): ?>
                            <div class="item" data-value="<?= $subcategory->id ?>"><?= $subcategory->title ?></div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

        </div>

        <div class="ui right labeled icon purple button go-stage" id="goto-stage2" title="Go to the next stage">
            <i class="right chevron icon"></i>
            Next Stage
        </div>
    </div>
</div>
<div class="ui tab segment" data-tab="analytical-pt1">

    <div class="ui form analytical-pt1">
        <div class="fields">
            <div class="field">
                <label>Visibility <i class="small red dashboard icon"></i></label>

                <div class="ui selection dropdown">
                    <input type="hidden" name="visibility" value="<?= $coverage->visibility ?>">

                    <div class="default text"><?= $coverage->visibility ?></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <div class="item" data-value="0.4">0.4</div>
                        <div class="item" data-value="0.75">0.75</div>
                        <div class="item" data-value="1">1</div>
                    </div>
                </div>
            </div>

            <div class="field">
                <label>Image <i class="small red dashboard icon"></i></label>

                <div class="ui selection dropdown">
                    <input type="hidden" name="image" value="<?= $coverage->image ?>">

                    <div class="default text"><?= $coverage->image ?></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <div class="item" data-value="0.8">Absent</div>
                        <div class="item" data-value="1">Present</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="fields analytics">
            <div class="five wide field">
                <label>Key Message Penetration <i class="small red dashboard icon"></i></label>

                <div class="ui selection dropdown">
                    <input type="hidden" name="key_message_penetration"
                           value="<?= $coverage->key_message_penetration ?>">

                    <div class="default text"><?= $coverage->key_message_penetration ?></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <div class="item" data-value="present">Clearly present</div>
                        <div class="item" data-value="extent">Present to some extent</div>
                        <div class="item" data-value="absent">Absent</div>
                        <div class="item" data-value="counter">Counter message</div>
                    </div>
                </div>
            </div>
            <!-- eof field -->
            <div class="three wide field">
                <label>Brand or Product Mention <i class="small red dashboard icon"></i></label>

                <div class="ui selection dropdown">
                    <input type="hidden" name="mention" value="<?= $coverage->mention ?>">

                    <div class="default text"><?= $coverage->mention ?></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <div class="item" data-value="1">Headline</div>
                        <div class="item" data-value="0.9">Lead</div>
                        <div class="item" data-value="0.8">Body</div>
                        <div class="item" data-value="0.5">Context</div>
                    </div>
                </div>
            </div>
        </div>

        <h4 class="ui horizontal header divider">Products mentioned</h4>

        <div class="fields products">
            <div class="five wide field">
                <?php foreach ($products as $product): ?>
                    <div class="inline field">
                        <div class="ui checkbox">
                            <input type="checkbox" id="ch-<?= $product->id ?>" name="product_id"
                                   value="<?= $product->id ?>" <?=
                            /** @var $attachedProducts array */
                            in_array(
                                $product->id,
                                $attachedProducts
                            ) ? 'checked' : '' ?> >
                            <label for="ch-<?= $product->id ?>"><?= $product->name ?></label>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>


        <div class="ui left labeled icon purple button back-stage" id="backto-stage1" title="Go to the previous stage">
            Previous Stage
            <i class="left chevron icon"></i>
        </div>
        <div class="ui right labeled icon purple button go-stage" id="goto-stage3" title="Go to the next stage">
            <i class="right chevron icon"></i>
            Next Stage
        </div>

    </div>

</div>
<div class="ui tab segment" data-tab="analytical-pt2">

    <div class="ui form analytical-pt2">
        <div class="fields">
            <div class="field">
                <label>Header Tonality <i class="small red dashboard icon"></i></label>

                <div class="ui selection dropdown">
                    <input type="hidden" name="header_tonality" value="<?= $coverage->header_tonality ?>">

                    <div class="default text"><?= $coverage->header_tonality ?></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <div class="item" data-value="0.5">Negative</div>
                        <div class="item" data-value="0.85">Neutral</div>
                        <div class="item" data-value="1">Positive</div>
                    </div>
                </div>
            </div>

            <div class="field">
                <label>Main Tonality <i class="small red dashboard icon"></i></label>

                <div class="ui selection dropdown">
                    <input type="hidden" name="main_tonality" value="<?= $coverage->main_tonality ?>">

                    <div class="default text"><?= $coverage->main_tonality ?></div>
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        <div class="item" data-value="negative">Negative</div>
                        <div class="item" data-value="neutral">Neutral</div>
                        <div class="item" data-value="positive">Positive</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="fields analytics">
            <div class="seven wide field">
                <label for="negative_explanation">Show Explanation about the Reason of Negative</label>
                <textarea rows="3" name="negative_explanation" id="negative_explanation"><?= trim($coverage->negative_explanation) ?></textarea>
            </div>
        </div>
        <br/>
        <div class="fields">
            <div class="seven wide field">
                <label for="event_name">Event Name</label>
                <input type="text" name="event_name" id="event_name" value="<?= $coverage->event_name ?>">
            </div>
        </div>
        <br/>

        <div class="fields">
            <div class="seven wide field">
                <label for="news_break">Newsbreak</label>
                <ul name="news_break_control" class="seven wide" id="news_break_control"></ul>
                <input type="hidden" name="news_break" id="news_break" value="">
            </div>
        </div>

        <div class="fields article-url">
            <div class="two wide field">
                <label>Desired Article</label>

                <div class="grouped inline fields inline-radios">
                    <div class="inline field">
                        <div class="ui checkbox">
                            <input id="desired_article" name="desired_article" type="checkbox" <?= $coverage->desired_article || isset($new) ? 'checked="checked"' : '' ?>>
                            <label for="desired_article">Yes</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="five wide field">
                <label>URL (if available)</label>
                <input type="text" name="url" placeholder="http://" value="<?= $coverage->url ?>">
            </div>
        </div>

        <div class="ui two column doubling stackable grid">
            <div class="column" style="padding-left: 0px;">
                <h4 class="ui horizontal header divider">Themes / Campaigns</h4>
                <div class="fields products">
                    <div class="field">
                        <? foreach ($campaigns as $campaign): ?>
                            <div class="inline field">
                                <div class="ui checkbox">
                                    <input type="checkbox" id="cam-<?= $campaign->id ?>" name="campaign_id" value="<?= $campaign->id ?>"
                                    <?= in_array($campaign->id, $attachedCampaigns) ? 'checked' : '' ?> >
                                    <label for="cam-<?= $campaign->id ?>"><?= $campaign->name ?></label>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="column business">
                <h4 class="ui horizontal header divider">Business segment <i class="small red dashboard icon" style="font-size: 1em;"></i></h4>
                <div class="fields">
                    <div class="field">
                        <? foreach ($business as $business_row): ?>
                            <div class="inline field">
                                <div class="ui checkbox">
                                    <input type="checkbox" id="bus-<?= $business_row->id ?>" name="business_id" value="<?= $business_row->id ?>"
                                    <?= in_array($business_row->id, $attachedBusiness) ? 'checked' : '' ?> >
                                    <label for="bus-<?= $business_row->id ?>"><?= $business_row->name ?></label>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
                <div class="ui red pointing prompt label transition hidden">Business segment is requared</div>
            </div>
        </div>

        <div class="ui left labeled icon purple button back-stage" id="backto-stage2" title="Go to the previous stage">
            Previous Stage
            <i class="left chevron icon"></i>
        </div>
        <div class="ui right labeled icon purple button go-stage" id="goto-stage4" title="Go to the next stage">
            <i class="right chevron icon"></i>
            Next Stage
        </div>

    </div>

</div>
<div class="ui tab segment" data-tab="speakers">
    <div class="ui form speakers">
        <div class="fields analytics">

            <div class="two wide field">
                <label>Speaker's Quote <i class="small red dashboard icon"></i></label>
                <div class="inline field">
                    <div class="ui checkbox">
                        <input id="speaker_quote" type="checkbox" value="" <?= ($coverage->speaker_quote == '1') ? 'checked' : '' ?>>
                        <label for="speaker_quote">Present</label>
                    </div>
                </div>
            </div>

            <div class="eight wide field speakers_quoted_block">
                <label for="speakers_quoted_control">Speakers Quoted (Choose the speaker from the drop-down list)</label>
                <ul name="speakers_quoted_control" class="seven wide <?= ($coverage->speaker_quote == '1') ? '' : 'disabled' ?>" id="speakers_quoted_control"></ul>
                <input type="hidden" name="speakers_quoted" id="speakers_quoted" value="">
            </div>

            <div class="two wide field new-speaker">
                <label>&nbsp;</label>
                <a href="#" id="add-new-speaker" class="ui basic button icon <?= ($coverage->speaker_quote == '1') ? '' : 'disabled' ?>" title="Add new speaker">Add New Speaker</a>
            </div>
        </div>

        <div class="fields tooltip">
            <div class="two wide field">
                <div class="ui pointing red label">Do not forget to fill in Speakers Quoted</div>
            </div>
        </div>

        <div class="fields analytics">

            <div class="two wide field">
                <label>Third Party Speakers <i class="small red dashboard icon"></i></label>

                <div class="inline field">
                    <div class="ui checkbox">
                        <input id="third_speakers_quotes" type="checkbox" value="" <?= $coverage->third_speakers_quotes ? 'checked' : '' ?>>
                        <label for="third_speakers_quotes">Present</label>
                    </div>
                </div>
            </div>

            <div class="eight wide field">
                <label for="third_speakers_quoted">Third Party Speakers Quoted</label>
                <input type="text" name="third_speakers_quoted" id="third_speakers_quoted" value="<?= $coverage->third_speakers_quoted ?>" <?= ($coverage->third_speakers_quoted) ? '' : 'disabled'?> >
            </div>
        </div>

        <div class="ui left labeled icon purple button back-stage" id="backto-stage3" title="Go to the previous stage">
            Previous Stage
            <i class="left chevron icon"></i>
        </div>
        <div class="ui right labeled icon purple button go-stage" id="goto-stage5" title="Go to the next stage">
            <i class="right chevron icon"></i>
            Next Stage
        </div>

    </div>

</div>
<div class="ui tab segment" data-tab="impact">
    <div class="ui form result">
        <div class="field">
            <input type="hidden" name="impact_index" value="<?= $coverage->impact_index ?>">
            <input type="hidden" name="net_effect" value="<?= $coverage->net_effect ?>">
            <div class="ui purple statistic">
                <div class="value">
                    <span id="impact_index"><?= round($coverage->impact_index, 1) ?></span>
                </div>
                <div class="label">
                    Impact Index
                </div>
            </div>

            <div class="ui pink statistic">
                <div class="value">
                    <span id="net_effect"><?= $coverage->net_effect ?></span>
                </div>
                <div class="label">
                    Net Effect
                </div>
            </div>
        </div>

        <div class="field">
            <div class="ui green finish button button-comment add-comment"> Add comment</div>
        </div>

        <div class="fields comment-text">
            <div class="three wide field"></div>
            <div class="ten wide field">
                <label for="comments">Please leave a comment if any</label>
                <textarea name="comments" id="comments"><?= $coverage->comments ?></textarea>
            </div>
        </div>

        <br/>

        <div class="field">
            <div class="ui green finish button button-save-comment"> Save comment</div>
        </div>

        <div class="field">
            <span>Adding data is completed. You can come back to the </span>
            <a href="/coverage" class="ui green finish button" title="Return to coverages list">
                coverage
            </a> page.
            <br/>
            <? if (!empty($access_complete) && $access_complete): ?>
                <a href="/coverage/complete?id=<?=$coverage->id?>" class="ui green finish button"> Complete coverage</a>
            <? endif; ?>
        </div>
    </div>
</div>

</form>

<div id="add-new-speaker-modal" class="ui modal">
    <i class="close icon"></i>
    <div class="header">ADD NEW SPEAKER</div>
    <div class="content">
        <p>To add a new speaker’s name please send a request to Natalia Banke: <a href="mailto:<?=Config::get('app.email.support')?>"><?=Config::get('app.email.support')?></a></p>
    </div>
</div>

<div id="coding-process-notify-modal" class="ui modal">
    <i class="close icon"></i>
    <div class="header">COVERAGE CODING PROCESS</div>
    <div class="content">
        <p>Coverage coding process in <span id="closed-month-name"></span> was completed.To add a new article please send a request to Natalia Banke: <a href="mailto:<?=Config::get('app.email.support')?>"><?=Config::get('app.email.support')?></a></p>
    </div>
</div>
