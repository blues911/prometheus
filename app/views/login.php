<section class="loginform">
    <div class="loginformbg">
        <img src="/img/pr-extend.png" width="297" height="212" class="logo">

        <? if (Session::has('message')): ?>
            <div class="ui error message">
                <i class="close icon"></i>

                <div class="header">
                    <?= Session::get('message') ?>
                </div>
            </div>
        <? endif; ?>
        <form action="/signin" method="post">
            <input type="hidden" name="time-offset" value="0">
            <span class="input input--jiro">
                <input class="input__field input__field--jiro" type="text" name="email" id="email" />
                <label class="input__label input__label--jiro" for="email">
                    <span class="input__label-content input__label-content--jiro">Username</span>
                </label>
            </span>

            <span class="input input--jiro">
                <input class="input__field input__field--jiro" type="password" name="password" id="password" />
                <label class="input__label input__label--jiro" for="password">
                    <span class="input__label-content input__label-content--jiro">Password</span>
                </label>
            </span>

            <center>
                <div class="ui checkbox">
                    <input type="checkbox" id="show-password" name="show-password"/>
                    <label>Show Password</label>
                </div>
            </center>

            <center>
                <input type="submit" class="ui green huge submit button" value="Login"/>
                <input type="submit" class="ui green huge change-password-login button" value="Send a password"/>
            </center>
        </form>
    </div>

    <div class="ui modal email-modal-login">
        <i class="close icon"></i>
        <div class="header">Send a password</div>
        <div class="content">
            <div class="ui form">
                <div class="field first-new-password">
                    <input name="email" type="text" id="input-email-login" placeholder="Welcome back to Prometheus! Please confirm your email.">
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>
                </div>
                <div class="field second-new-password" style="display:none">
                    <p id="status">We've sent a new password reset link to your email. Please, check it</p>
                </div>
            </div>
        </div>
        <div class="actions">
            <div class="ui button first-new-password">Cancel</div>
            <div class="positive ui button approve first-new-password">Send</div>
            <div class="ui positive right labeled icon button second-new-password" style="display:none">
                Ok<i class="checkmark icon"></i>
            </div>
        </div>
    </div>
</section>