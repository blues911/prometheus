<div class="ui page admin" id="import-page">
    <div class="ui top attached tabular menu charts">
        <a class="active item" data-tab="main_import">Import</a>
        <a class="item" data-tab="import_history">History</a>
    </div>

    <div class="ui bottom attached active tab segment no-padding-top" data-tab="main_import">
        <div class="import">

            <div class="ui two column doubling stackable grid">
                <div class="column upload" id="import-coverage">
                    <div class="header"><i class="icon-coverage icon" style="margin-bottom: -2px;"></i>Coverage Table</div>
                    <form action="/import/coverage" method="POST" enctype="multipart/form-data">
                        <div class="drop fade">
                            <div class="upload-progress" style="display: none;">upload <span class="value">0</span>%</div>
                            <div class="import-progress start" style="display: none;">import <span class="value">0</span>%</div>
                            <div class="status" style="display: none;">status: <span class="value ui header"></span></div>
                            <div class="tip">Drop file here</div>
                            <a class="ui button teal upload-add" href="#"><i class="plus icon"></i></a>
                            <input type="file" name="file">
                            <a style="display: none" href="/" class="error-link">Download file with errors</a>
                            <? if (Auth::user()->role == \App\Models\User::ROLE_ADMINISTRATOR): ?>
                                <div class="ui checkbox">
                                  <input type="checkbox" name="approve">
                                  <label>Approve after import</label>
                                </div>
                            <? endif; ?>
                        </div>
                    </form>
                </div>

                <div class="column upload" id="import-media">
                    <div class="header"><i class="icon-media icon" style="margin-bottom: -2px;"></i>Media List</div>
                    <form action="/import/media" method="POST" enctype="multipart/form-data">
                        <div class="drop fade">
                            <div class="upload-progress" style="display: none;">upload <span class="value">0</span>%</div>
                            <div class="import-progress start" style="display: none;">import <span class="value">0</span>%</div>
                            <div class="status" style="display: none;">status: <span class="value ui header"></span></div>
                            <div class="tip">Drop file here</div>
                            <a class="ui button teal upload-add" href="#"><i class="plus icon"></i></a>
                            <input type="file" name="file">
                            <a style="display: none" href="/" class="error-link">Download file with errors</a>
                        </div>
                    </form>
                </div>
            </div>

            <?php $notifications = \App\Models\SystemUser::getUnreadNotifications($currentUser->id); ?>
            
            <div class="ui one column grid">
                <div class="column">
                <? if (!empty($notifications)): ?>
                    <table class="ui fixed single line celled table">
                        <tbody>
                            <?php foreach ($notifications as $n) : ?>
                                <? $data = json_decode($n->data, true); ?>
                                <? if ($data['error']): ?>
                                    <tr class="item import-errors" data-msg-id="<?= $n->id ?>">
                                        <td class="ui center aligned no-padding icon-column">
                                            <? if ($n->type == \App\Models\Notification::IMPORT_COVERAGE): ?>
                                                <i class="icon-coverage-color icon"></i>
                                            <? else: ?>
                                                <i class="icon-media-color icon"></i>
                                            <? endif; ?>
                                        </td>
                                        <td><i class="dropdown icon"></i><?= isset($data['filename']) ? $data['filename'] : '' ?></td>
                                        <td class="menu">
                                            <span class="approve">
                                                <a href="#" class="ui no button red mini">No</a>
                                                <a href="/import/delete/<?= $n->id ?>" class="ui yes button green mini">Yes</a>
                                            </span>
                                            <a href="#" class="right ui delete button red mini basic" title="Delete import file"><i class="delete icon"></i></a>
                                        </td>
                                    </tr>
                                    <tr class="item-error" style="display: none;">
                                        <td colspan="3">
                                            <? foreach ($data['errors'] as $f => $e): ?>
                                                <div class="ui accordion">
                                                    <div class="title">
                                                        <i class="dropdown icon"></i>
                                                        <?= implode('<br>', $e['messages']) ?> <span class="rows_count">(<?=  count($e['rows']) ?>)</span>
                                                    </div>
                                                    <div class="content">
                                                        <p style="display: block ! important;" class="transition visible"><?= implode(', ', $e['rows']) ?></p>
                                                    </div>
                                                </div>
                                            <? endforeach; ?>
                                        </td>
                                    </tr>
                                <? else: ?>
                                    <tr class="item import" data-msg-id="<?= $n->id ?>">
                                        <td class="ui center aligned no-padding icon-column">
                                            <? if ($n->type == \App\Models\Notification::IMPORT_COVERAGE): ?>
                                                <i class="icon-coverage-color icon"></i>
                                            <? else: ?>
                                                <i class="icon-media-color icon"></i>
                                            <? endif; ?>
                                        </td>
                                        <td><?= isset($data['filename']) ? $data['filename'] : '' ?></td>
                                        <td class="menu">
                                            <span class="approve">
                                                <a href="#" class="ui no button red mini">No</a>
                                                <a href="/import/delete/<?= $n->id ?>" class="ui yes button green mini">Yes</a>
                                            </span>
                                            <a href="#" class="ui delete button red mini basic" title="Delete import file"><i class="delete icon"></i></a>
                                        </td>
                                    </tr>
                                <? endif; ?>
                                
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <? endif; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="ui tab segment bottom no-padding-top" data-tab="import_history">

        <div class="ui one column relaxed grid import-history">
            <div class="column table-actions ui form">
                <div class="ui form">
                    <div class="fields">
                        <div class="three wide field">
                            <div class="ui selection dropdown" id="type_dropdown_filter" title="Filter by import file type">
                                <input type="hidden" name="type" id="type-filter" value="">

                                <div class="default text">Choose import type...</div>
                                <i class="dropdown icon"></i>

                                <div class="menu">
                                    <?php foreach (\App\Models\ImportFile::getFilterByType() as $key => $filter): ?>
                                        <div class="item" data-value="<?= $key ?>"><?= $filter ?></div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div id="datepicker-start" class="two wide field input-daterange" style="width: 135px !important;">
                            <div class="ui right labeled icon input first-block" title="Start date">
                                <input type="text" name="start" value="<?=$dateFrom?>" id="start_range">
                                <i class="calendar icon add-on start"></i>
                            </div>
                        </div>
                        <div id="datepicker-end" class="two wide field input-daterange second-block" style="width: 135px !important;">
                            <div class="ui right labeled icon input" title="End date">
                                <input type="text" name="end" value="<?=$dateTo?>" id="end_range">
                                <i class="calendar icon add-on end"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column" style="padding-top: 0px; margin-top: 25px;">
                <table class="ui fixed single line celled table" id="import-history-table" width="100%">
                    <thead>
                        <th class="col-file-type">Icon</th>
                        <th class="col-filename">Filename</th>
                        <th class="col-date">Date</th>
                        <th class="col-actions"></th>
                    </thead>
                </table>
            </div>
        </div>
    </div>

</div>
