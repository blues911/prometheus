<section class="loginform">
    <div class="loginformbg">
        <img src="/img/pr-extend.png" width="297" height="212" class="logo">

        <? if (Session::has('message')) : ?>
            <div class="ui error message">
                <i class="close icon"></i>

                <div class="header">
                    <?= Session::get('message') ?>
                </div>
            </div>
        <? endif; ?>
        <form id="passwords" sign="<?= $sign ?>">
            <span class="input input--jiro">
                <div id="error" class="text-error"></div>
            </span>
            <span class="input input--jiro">
                <input class="input__field input__field--jiro" type="password" name="password" id="password" />
                <label class="input__label input__label--jiro" for="password">
                    <span class="input__label-content input__label-content--jiro">New Password</span>
                </label>
            </span>

            <span class="input input--jiro">
                <input class="input__field input__field--jiro" type="password" name="password_confirmation" id="password_confirmation" />
                <label class="input__label input__label--jiro" for="password_confirmation">
                    <span class="input__label-content input__label-content--jiro">Repeat Password</span>
                </label>
            </span>

            <center>
                <input type="submit" class="ui green huge submit button" value="Send"/>
            </center>
        </form>
    </div>
</section>