# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'prometheus'

# Repository settings
set :repo_url, 'git@github.com:Fahrenheit-451/prometheus.git'
set :scm, :git

# Default branch is :master
ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Deploy directory on remote server /mnt/data/webapps/prometheus-staging
set :deploy_to, '/mnt/data/webapps/prometheus'

# Store only 5 releases
set :keep_releases, 5

# Shared directories between releases
set :linked_dirs, %w{ vendor app/storage }