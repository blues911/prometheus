namespace :deploy do

  task :app_setup do
    on roles(:all) do

      # Install all vendors with composer
      execute "cd #{release_path} && composer install"

      # Patches file
      execute "cp #{current_path}/vendor-patches/LaravelExcelReader.php #{current_path}/vendor/maatwebsite/excel/src/Maatwebsite/Excel/Readers/"

      # add database administration tool
      execute "mkdir -p #{current_path}/public/adminer"
      execute "ln -s    #{shared_path}/adminer.php #{current_path}/public/adminer/adminer.php"

      # Execute migrations
      execute "/usr/bin/php #{current_path}/artisan migrate --force"

      # Upload deploy config
      stage = fetch(:stage).to_s
      upload! "app/config/capistrano/deploy/#{stage}.rb", "#{shared_path}/capistrano/#{stage}.rb"
    end
  end

  after :finishing, "deploy:app_setup"
  after :finishing, "deploy:cleanup"

end