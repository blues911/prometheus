server 'ec2-52-17-118-150.eu-west-1.compute.amazonaws.com', user: 'prometheus', roles: %w{app db web}
set :deploy_to, '/mnt/data/webapps/prometheus'

Capistrano::Env.use do |env|

  # Application config
  env.add 'APP_DEBUG', 'false'
  env.add 'APP_URL', 'http://pr.kaspersky.com'
  env.add 'APP_KEY', ''
  env.add 'APP_ASSETS_VERSION', ''
  env.add 'APP_EMAIL_SENDER', ''
  env.add 'APP_EMAIL_SENDER_NAME', 'Prometheus Service'
  env.add 'APP_EMAIL_QUEUE', 'emails'

  # Database config
  env.add 'DB_HOST', 'localhost'
  env.add 'DB_DATABASE', 'prmeasurement'
  env.add 'DB_USERNAME', 'root'
  env.add 'DB_PASSWORD', ''

  # Email config
  env.add 'MAIL_DRIVER', 'smtp'
  env.add 'MAIL_HOST', 'email-smtp.eu-west-1.amazonaws.com'
  env.add 'MAIL_PORT', '465'
  env.add 'MAIL_USERNAME', ''
  env.add 'MAIL_PASSWORD', ''
  env.add 'MAIL_ENCRYPTION', 'ssl'

  env.add 'SES_KEY', ''
  env.add 'SES_SECRET', ''
  env.add 'SES_REGION', ''

  env.filemode = 0644

end