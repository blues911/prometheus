<?php

namespace Exception;

/**
 * Class APIException
 * @package Exception
 */
class APIException extends \Exception
{
    /**
     * Response-ready JSON representation
     * @return array
     */
    public function getJSON()
    {
        return [
            'error' => true,
            'message' => $this->getMessage(),
            'code' => $this->getCode()
        ];
    }
}
