<?php

use Exception\APIException;

App::missing(
    function ($exception) {
        return Response::view('errors.404', [], 404);
    }
);

App::error(
    function (APIException $e) {
        return Response::json($e->getJSON());
    }
);
