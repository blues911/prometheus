<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'App\Controllers\IndexController@home');

Route::post('signin', 'App\Controllers\AuthController@signin');
Route::get('logout', 'App\Controllers\AuthController@logout');

Route::get('/dev/loginAsDevUser', 'App\Controllers\DevController@loginAsDevUser');
Route::get('/dev/loginByUserId/id/{id}', 'App\Controllers\DevController@loginByUserId');
Route::post('/user/send-mail-with-password', 'App\Controllers\UserController@sendMailWithPassword');
Route::get('/user/change-password', 'App\Controllers\UserController@changePassword');
Route::get('/user/set-random-password', 'App\Controllers\UserController@setRandomPassword');
Route::post('/user/send-new-password', 'App\Controllers\UserController@sendNewPassword');
Route::get('/reports', 'App\Controllers\IndexController@reports');
Route::get('/pdf', 'App\Controllers\IndexController@pdfReports');

/**
 * KPI
 */
if (Carbon\Carbon::now() < Carbon\Carbon::create(2018,01,01)) {
    Route::get('/kpi-dashboard', 'App\Controllers\KpiController@index');
}

else {
    Route::get('/kpi-dashboard', 'App\Controllers\KpiV2Controller@index');
    Route::get('/kpi-dashboard-old', 'App\Controllers\KpiController@index');
}



Route::get('/help', 'App\Controllers\HelpController@index');
Route::get('/help/download/id/{id}', 'App\Controllers\HelpController@download');
Route::post('/help/import/id/{id}', 'App\Controllers\HelpController@import');
Route::get('/stats/export/{name}', 'App\Controllers\StatsController@export');
Route::get('/test', 'App\Controllers\TestController@index');
Route::controller('stats', 'App\Controllers\StatsController');

Route::group(
    ['before' => 'auth'],
    function () {
        Route::post('/user/send-mail-for-confirm-password', 'App\Controllers\UserController@sendMailForConfirmPassword');
        Route::post('/coverage/set-table-mode', 'App\Controllers\CoverageController@setTableMode');
        Route::post('coverage/change-coding-process', 'App\Controllers\CoverageController@changeCodingProcess');
        Route::post('coverage/check-coding-process', 'App\Controllers\CoverageController@CheckCodingProcess');
        Route::get('coverage/get-coding-process-modal', 'App\Controllers\CoverageController@getCodingProcessModal');
        Route::get('coverage/export', 'App\Controllers\CoverageController@export');
        Route::get('coverage/complete', 'App\Controllers\CoverageController@complete');
        Route::get('coverage/approve', 'App\Controllers\CoverageController@approve');
        Route::get('coverage/return-to-draft', 'App\Controllers\CoverageController@returnToDraft');
        Route::get('coverage/approve-all', 'App\Controllers\CoverageController@approveAll');
        Route::get('coverage/get-edit-modal', 'App\Controllers\CoverageController@getEditModal');
        Route::get('report/get-edit-modal', 'App\Controllers\ReportController@getEditModal');
        Route::delete('coverage/delete-all', 'App\Controllers\CoverageController@deleteAll');

        Route::get('/users/get-users', 'App\Controllers\UserController@getUsers');
        Route::get('/regions/get-regions', 'App\Controllers\RegionController@getRegions');
        Route::get('/subregions/get-subregions', 'App\Controllers\SubregionController@getSubregions');
        Route::get('/countries/get-countries', 'App\Controllers\CountryController@getCountries');
        Route::get('/products/get-products', 'App\Controllers\ProductController@getProducts');
        Route::get('/media/get-media', 'App\Controllers\MediaController@getMedia');
        Route::get('/events/get-events', 'App\Controllers\EventController@getEvents');
        Route::get('/campaigns/get-campaigns', 'App\Controllers\CampaignController@getCampaigns');
        Route::get('/business/get-business', 'App\Controllers\BusinessController@getBusiness');
        Route::get('/newsbreaks/get-newsbreaks', 'App\Controllers\NewsbreakController@getNewsbreaks');
        Route::get('/speakers/get-speakers', 'App\Controllers\SpeakerController@getSpeakers');
        Route::get('/subcategories/get-subcategories', 'App\Controllers\SubcategoryController@getSubcategories');

        Route::post('/user/restore/{id}', 'App\Controllers\UserController@restore');
        Route::post('/region/restore/{id}', 'App\Controllers\RegionController@restore');
        Route::post('/subregion/restore/{id}', 'App\Controllers\SubregionController@restore');
        Route::post('/country/restore/{id}', 'App\Controllers\CountryController@restore');
        Route::post('/product/restore/{id}', 'App\Controllers\ProductController@restore');
        Route::post('/media/restore/{id}', 'App\Controllers\MediaController@restore');
        Route::post('/event/restore/{id}', 'App\Controllers\EventController@restore');
        Route::post('/campaign/restore/{id}', 'App\Controllers\CampaignController@restore');
        Route::post('/business/restore/{id}', 'App\Controllers\BusinessController@restore');
        Route::post('/newsbreak/restore/{id}', 'App\Controllers\NewsbreakController@restore');
        Route::post('/speaker/restore/{id}', 'App\Controllers\SpeakerController@restore');
        Route::post('/subcategory/restore/{id}', 'App\Controllers\SubcategoryController@restore');

        Route::resource('coverage', 'App\Controllers\CoverageController');
        Route::resource('product', 'App\Controllers\ProductController');
        Route::resource('country', 'App\Controllers\CountryController');
        Route::resource('region', 'App\Controllers\RegionController');
        Route::resource('subregion', 'App\Controllers\SubregionController');
        Route::resource('user', 'App\Controllers\UserController');
        Route::resource('report', 'App\Controllers\ReportController');
        Route::resource('media', 'App\Controllers\MediaController');
        Route::resource('event', 'App\Controllers\EventController');
        Route::resource('campaign', 'App\Controllers\CampaignController');
        Route::resource('business', 'App\Controllers\BusinessController');
        Route::resource('newsbreak', 'App\Controllers\NewsbreakController');
        Route::resource('speaker', 'App\Controllers\SpeakerController');
        Route::resource('subcategory', 'App\Controllers\SubcategoryController');

        Route::controller('import', 'App\Controllers\ImportController');
        Route::controller('export', 'App\Controllers\ExportController');
        Route::get('coverage/import/status', 'App\Controllers\ImportController@checkProgress');
        Route::get('coverage/import/file_error/{file}', 'App\Controllers\ImportController@getFileError');
        Route::get('media/import/status', 'App\Controllers\ImportController@checkProgress');
        Route::get("/import/history", 'App\Controllers\ImportController@getHistory');
        Route::get("/charts/report-list", 'App\Controllers\IndexController@getReportList');

        Route::get('signout', 'App\Controllers\AuthController@signout');

        Route::get('/add', 'App\Controllers\IndexController@dataForm');
        Route::get('/charts', 'App\Controllers\IndexController@charts');
        Route::get('/admin', 'App\Controllers\IndexController@admin');
        Route::get('/charts/save-pdf', 'App\Controllers\IndexController@savePdf');

        Route::post('/business/some_remove', 'App\Controllers\BusinessController@destroySomeRecords');
        Route::post('/speaker/some_remove', 'App\Controllers\SpeakerController@destroySomeRecords');
        Route::post('/subcategory/some_remove', 'App\Controllers\SubcategoryController@destroySomeRecords');
        Route::post('/newsbreak/some_remove', 'App\Controllers\NewsbreakController@destroySomeRecords');
        Route::post('/campaign/some_remove', 'App\Controllers\CampaignController@destroySomeRecords');
        Route::post('/event/some_remove', 'App\Controllers\EventController@destroySomeRecords');
        Route::post('/media/some_remove', 'App\Controllers\MediaController@destroySomeRecords');
        Route::post('/product/some_remove', 'App\Controllers\ProductController@destroySomeRecords');
        Route::post('/country/some_remove', 'App\Controllers\CountryController@destroySomeRecords');
        Route::post('/region/some_remove', 'App\Controllers\RegionController@destroySomeRecords');
        Route::post('/subregion/some_remove', 'App\Controllers\SubregionController@destroySomeRecords');

        Route::get('/kpi/headers-by-region', 'App\Controllers\KpiController@getHeadersByRegion');
        Route::get('/kpi/records-by-region', 'App\Controllers\KpiController@getRecordsByRegion');
        Route::post('/kpi/save-headers', 'App\Controllers\KpiController@saveHeaders');
        Route::post('/kpi/save-records', 'App\Controllers\KpiController@saveRecords');

        Route::get('/kpi/v2/headers-by-region', 'App\Controllers\KpiV2Controller@getHeadersByRegion');
        Route::get('/kpi/v2/records-by-region', 'App\Controllers\KpiV2Controller@getRecordsByRegion');
        Route::post('/kpi/v2/save-headers', 'App\Controllers\KpiV2Controller@saveHeaders');
        Route::post('/kpi/v2/save-records', 'App\Controllers\KpiV2Controller@saveRecords');
    }
);

Route::group(
    [
        'prefix'=>'api',
        'before' => 'api'
    ],function (){
    /**
     * Входные параметры в формате JSON:
     * ?filter={"date":{"start":"2015-01-01","end":"2015-12-31"},"speakers_ids":"1,2,3,4","speakers_name":"Evgeny Chereshnev,Eugene Kaspersky"]}&page=1&api_token=fsdfsdf33rdfsdfe34fdse
     */
    Route::post('/stats/regions', 'App\Controllers\ApiStatsController@getRegionIndexes');
    Route::post('/stats/speakers', 'App\Controllers\ApiStatsController@getSpeakers');
    Route::post('/stats/kpi', 'App\Controllers\ApiStatsController@getKpi');
    Route::post('/stats/mindshare', 'App\Controllers\ApiStatsController@getMindshare');


});