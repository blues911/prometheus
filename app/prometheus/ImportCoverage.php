<?php
/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at Fahrenheit 451
 *
 * Date: 18.03.15
 * Time: 1:37
 */

namespace App\Prometheus;

use App\Models\BaseValidator;
use App\Models\Country;
use App\Models\Coverage;
use App\Models\Product;
use App\Models\Campaign;
use App\Models\Business;
use App\Models\Region;
use App\Models\Subcategory;
use App\Models\SystemUser;
use App\Models\ImportFile;
use App\Models\NewsBreak;
use App\Models\Speaker;
use App\Models\User;
use Auth;
use http\Env\Response;
use Validator;
use DB;
use Config;
use Cache;
use App\Models\Notification;

/**
 * Class ImportCoverage
 * @package App\Prometheus
 */
class ImportCoverage
{
    private $mutators = [
        'listType' => [
            'gold' => Coverage::MEDIA_LIST_GOLD,
            'platinum' => Coverage::MEDIA_LIST_PLATINUM,
            'diamond' => Coverage::MEDIA_LIST_DIAMOND
        ],

        'image' => [
            'present' => Coverage::IMAGE_PRESENT,
            'absent' => Coverage::IMAGE_ABSENT
        ],

        'keymsg' => [
            'counter-message' => Coverage::KEY_MESSAGE_PENETRATION_COUNTER,
            'clearly present' => Coverage::KEY_MESSAGE_PENETRATION_PRESENT,
            'present to some extent' => Coverage::KEY_MESSAGE_PENETRATION_EXTENT,
            'absent' => Coverage::KEY_MESSAGE_PENETRATION_ABSENT
        ],

        'mention' => [
            'headline' => Coverage::MENTION_HEADLINE,
            'lead' => Coverage::MENTION_LEAD,
            'body' => Coverage::MENTION_BODY,
            'context' => Coverage::MENTION_CONTEXT
        ],

        'headerTonality' => [
            'negative' => Coverage::HEADER_TONALITY_NEGATIVE,
            'neutral' => Coverage::HEADER_TONALITY_NEUTRAL,
            'positive' => Coverage::HEADER_TONALITY_POSITIVE
        ],

        'presence' => [
            'present' => true,
            'absent' => false
        ],

        'speaker_quote' => [
            'present' => Coverage::SPEAKER_QUOTE_PRESENT,
            'absent' => Coverage::SPEAKER_QUOTE_ABSENT
        ],

        'third_speakers_quotes' => [
            'present' => Coverage::THIRD_SPEAKERS_QUOTES_PRESENT,
            'absent' => Coverage::THIRD_SPEAKERS_QUOTES_ABSENT
        ],

        'types' => [
            'information agency' => Coverage::MEDIA_TYPE_AGENCY,
            'agency' => Coverage::MEDIA_TYPE_AGENCY,
            'online' => Coverage::MEDIA_TYPE_ONLINE,
            'tv' => Coverage::MEDIA_TYPE_TV,
            'print' => Coverage::MEDIA_TYPE_PRINT,
            'radio' => Coverage::MEDIA_TYPE_RADIO
        ],

        'categories' => [
            'business media' => Coverage::MEDIA_CATEGORY_BUSINESS,
            'business' => Coverage::MEDIA_CATEGORY_BUSINESS,
            'general interest' => Coverage::MEDIA_CATEGORY_GENERAL,
            'general' => Coverage::MEDIA_CATEGORY_GENERAL,
            'it' => Coverage::MEDIA_CATEGORY_IT,
            'vertical' => Coverage::MEDIA_CATEGORY_VERTICAL
        ]
    ];

    private $fieldsMap = [
        'region' => 'region',
        'country' => 'country',
        'products_mentioned_standardized_names' => 'product',
        'date_of_publication_ddmmyyyy' => 'date',
        'publication_name' => 'name',
        'reach_circulation_for_print_media_website_visitors_per_week_broadcast_reach' => 'reach',
        'author' => 'author',
        'headline' => 'headline',
        'media_list_diamond_platinum_gold' => 'media_list',
        'media_type_information_agency_online_print_tv_radio' => 'media_type',
        'media_category_vertical_business_it_general_interest' => 'media_category',
        'visibility' => 'visibility',
        'image_present_absent' => 'image',
        'key_message_penetration_clearly_present_present_to_some_extent_absent_counter_message'
        => 'key_message_penetration',
        'brand_or_product_mention_headline_lead_body_context' => 'mention',
        'speakers_quotepresent_absent' => 'speaker_quote',
        'third_party_speakers_supporting_quotes_present_absent' => 'third_speakers_quotes',
        'speakers_quoted' => 'speakers_quoted',
        'third_party_speakers_quoted' => 'third_speakers_quoted',
        'header_tonality_positive_neutral_negative' => 'header_tonality',
        'main_tonality_positive_neutral_negative' => 'main_tonality',
        'short_explanation_about_the_reason_of_negative' => 'negative_explanation',
        'campaign' => 'campaign',
        'business_segment' => 'business',
        'event_name_please_mark_if_the_coverage_resulted_from_kl_press_events' => 'event_name',
        'newsbreak' => 'news_break_global',
        'impact_index' => 'impact_index',
        'net_effect' => 'net_effect',
        'desired_article_yesno' => 'desired_article',
        'url_if_available' => 'url',
        'comments' => 'comments',
        'local_newsbreak' => 'news_break_local',
        'media_subcategory_lifestyle_smb_media_enterprise_financial_virtualization_automotive_industrial_banking_and_insurance_travel_parental_media_gaming_pet_media_motorsport_healthcare_government_msp_media_education' => 'media_subcategories',
        'owner_code' => 'owner_id'
    ];

    private $canBeNull = [
        'reach',
        'url',
        'net_effect',
        'media_type',
        'visibility',
        'media_category',
        'key_message_penetration',
        'mention',
        'image',
        'header_tonality',
        'main_tonality',
        'comments',
        'media_subcategories',
        'owner_id'

    ];

    /**
     * @var Validator
     */
    private $validator;
    private $data;
    private $errors;
    private $xls;
    private $userId;
    private $systemUser;
    private $fileName;
    private $originalFileName;
    private $importFile;
    private $isNeedApprove;

    /**
     *
     */
    public function __construct($xls, $userId, $fileName, $originalFileName, $isNeedApprove = false, $fileId = null)
    {
        $this->xls = $xls;
        $this->userId = $userId;
        $this->systemUser = SystemUser::find($userId);
        $this->fileName = $fileName;
        $this->originalFileName = $originalFileName;
        $this->isNeedApprove = $isNeedApprove;
        $this->importFile = !empty($fileId) ? ImportFile::find($fileId) : null;
    }

    /**
     * @param array $data
     */
    public function save(array $data)
    {
        $products = [];
        $campaigns = [];
        $newsbreaks = [];
        $speakers = [];
        $business = [];
        $mediaSubcategory = [];

        $region = Region::where('name', '=', trim($data['region']))->first();
        $country = Country::where('name', '=', trim($data['country']))->first();


        if (empty($region) || empty($country) || $country->region_id != $region->id) {
            return;
        }

        if (is_array($data['product'])) {
            foreach ($data['product'] as $p) {
                $product = Product::where('name', '=', trim($p))->first();
                if ($product) {
                    $products[] = $product;
                }
            }
        }
        if (!empty($data['campaign']) && is_array($data['campaign'])) {
            foreach ($data['campaign'] as $p) {
                $campaign = Campaign::where('name', '=', trim($p))->first();
                if ($campaign) {
                    $campaigns[] = $campaign;
                }
            }
        }
        if (!empty($data['business']) && is_array($data['business'])) {
            foreach ($data['business'] as $p) {
                $business_row = Business::where('name', '=', trim($p))->first();
                if ($business_row) {
                    $business[] = $business_row;
                }
            }
        }

        if (!empty($data['news_break_local']) && is_array($data['news_break_local'])) {
            foreach ($data['news_break_local'] as $p) {
                //    $newsbreaks[] = NewsBreak::firstOrCreate(['name' => trim($p), 'type' => NewsBreak::TYPE_LOCAL]);
                if (!$newsbreak = NewsBreak::withTrashed()->where('name', '=', trim($p))->where('type', '=', NewsBreak::TYPE_LOCAL)->first()) {
                    $newsbreak = new NewsBreak();
                    $newsbreak->name = trim($p);
                    $newsbreak->type = NewsBreak::TYPE_LOCAL;
                    $newsbreak->save();
                }
                $newsbreaks[] = $newsbreak;
            }
        }

        if (!empty($data['news_break_global']) && is_array($data['news_break_global'])) {
            foreach ($data['news_break_global'] as $p) {
                if (!$newsbreak = NewsBreak::withTrashed()->where('name', '=', trim($p))->where('type', '=', NewsBreak::TYPE_GLOBAL)->first()) {
                    $newsbreak = new NewsBreak();
                    $newsbreak->name = trim($p);
                    $newsbreak->type = NewsBreak::TYPE_GLOBAL;
                    $newsbreak->save();
                }
                $newsbreaks[] = $newsbreak;
            }
        }

        if (!empty($data['speakers_quoted']) && is_array($data['speakers_quoted'])) {
            foreach ($data['speakers_quoted'] as $p) {
                if (! $speaker = Speaker::withTrashed()->where('name','=',trim($p))->first()) {
                    $speaker = new Speaker();
                    $speaker->name=trim($p);
                    $speaker->save();
                }
                $speakers[] =   $speaker ;
            }
            $data['speaker_quote'] = Coverage::SPEAKER_QUOTE_PRESENT;
        }



        if (!empty($data['media_subcategories']) && is_array($data['media_subcategories'])) {
            foreach ($data['media_subcategories'] as $p) {
                $mediaSubcategory[] = Subcategory::where(['title' => trim($p)])->first();
            }
        }


        $coverage = Coverage::where("name", "=", $data['name'])
            ->where("date", "=", date("Y-m-d H:i:s", strtotime($data['date'])))
            ->where("headline", "=", $data['headline'])
            ->where("country_id", "=", $country->id)
            ->where("author", "=", $data['author'])
            ->first();

        if (empty($data['owner_id']) || $data['owner_id'] == "") {
            if (!empty($coverage) && $this->userId != $coverage->owner_id && $this->systemUser->role != SystemUser::ROLE_ADMINISTRATOR) {
                return;
            } else {
                unset($data['owner_id']);
            }
        }

        if (empty($coverage)) {
            $coverage = new Coverage;
            $coverage->owner()->associate($this->systemUser->getModel());
        } else {
            $coverageProducts = $coverage->products()->get();
            foreach ($coverageProducts as $product) {
                $coverage->products()->detach($product->product_id);
            }

            $coverageCampaigns = $coverage->campaigns()->get();
            foreach ($coverageCampaigns as $campaign) {
                $coverage->campaigns()->detach($campaign->campaign_id);
            }

            $coverageBusiness = $coverage->business()->get();
            foreach ($coverageBusiness as $business) {
                $coverage->business()->detach($business->business_id);
            }

            $coverageNewsbreaks = $coverage->newsbreaks()->get();
            foreach ($coverageNewsbreaks as $newsbreak) {
                $coverage->newsbreaks()->detach($newsbreak->newsbreak_id);
            }

            $coverageSpeakers = $coverage->speakers()->get();
            foreach ($coverageSpeakers as $speaker) {
                $coverage->speakers()->detach($speaker->speaker_id);
            }

            $coverageSubcategories = $coverage->subcategories()->get();
            foreach ($coverageSubcategories as $coverageSubcategory) {
                $coverage->subcategories()->detach($coverageSubcategory->subcategory_id);
            }
        }

        $coverage->region()->associate($region);
        $coverage->country()->associate($country);
        $coverage->file()->associate($this->importFile);
        $coverage->fill($data);
        if ($coverage->status == Coverage::STATUS_APPROVED && !$this->isNeedApprove) {
            $coverage->status = Coverage::STATUS_COMPLETE;
        }
        if ($this->isNeedApprove) {
            $coverage->status = Coverage::STATUS_APPROVED;
        }
        $coverage->save();
        $coverage->products()->saveMany($products);
        $coverage->campaigns()->saveMany($campaigns);
        $coverage->business()->saveMany($business);
        $coverage->newsbreaks()->saveMany($newsbreaks);
        $coverage->speakers()->saveMany($speakers);
        $coverage->subcategories()->saveMany($mediaSubcategory);
    }

    /**
     * run
     *
     * @return bool
     */
    public function run()
    {

        $this->data = Import::convertCoverageData($this->xls, $this->fieldsMap, $this->fileName);

        $count = count($this->data);
        $rules = BaseValidator::$rules[BaseValidator::IMPORT_COVERAGE];
        if ($this->systemUser->role === User::ROLE_USER) {
            $rules['region'] .= "|in:{$this->systemUser->region_id}";
        }
        for ($i = 0; $i < $count; $i++) {
            foreach ($this->data[$i] as $key => $value) {
                $this->data[$i][$key] = $this->mutate($key, $value);

            }

            $this->validator = BaseValidator::make($this->data[$i], $rules);

            if (!$this->validate()) {
                $this->errors[] = ['messages' => $this->validator->errors()->toArray(), 'row' => $i + 1];
            } else {
                if (empty($this->data[$i]['net_effect'])) {
                    $this->data[$i]['net_effect'] = round($this->data[$i]['reach'] * $this->data[$i]['impact_index'], 2);
                }
            }
        }
        Import::updateProgress(95, "ok", $this->fileName);


        if (!$this->errors) {
            if (empty($this->importFile)) {
                $oldImportFile = ImportFile::where('name', '=', $this->originalFileName)->orderBy('created_at', 'asc')->first();;
                if (!empty($oldImportFile)) {
                    $userId = $oldImportFile->user_id;
                    $importFile = new ImportFile();
                    $importFile->name = $this->originalFileName;
                    $importFile->filename = md5($this->fileName);
                    $importFile->user_id = $userId;
                } else {
                    $importFile = new ImportFile();
                    $importFile->name = $this->originalFileName;
                    $importFile->filename = md5($this->fileName);
                    $importFile->user_id = $this->systemUser->id;

                }
                $importFile->type = ImportFile::IMPORT_COVERAGE;
                $importFile->save();

                $newName = Config::get('app.imports_path') . "/" . $importFile->filename;
                rename($this->xls, $newName);

                $this->importFile = $importFile;
            }

            foreach ($this->data as $item) {
                $this->save($item);
            }

            Import::updateProgress(100, "ok", $this->fileName);
            return true;
        }

        $excel = new ExcelValidator($this->xls, $this->errors, Notification::IMPORT_COVERAGE);
        $errorFile = $excel->validate();

        Cache::put($this->fileName . '_queue:error_file', $errorFile, 30);

        Import::updateProgress(100, "error", $this->fileName);
        return false;
    }

    /**
     * get Errors
     *
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * get Data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param $name
     * @param $value
     * @return bool|float|string
     */
    protected function mutate($name, $value)
    {

        $value = trim($value);
        if (empty($value) && $name != "desired_article") {

            return in_array($name, $this->canBeNull) ? null : '';
        }

        switch ($name) {
            case "date":

                return date('Y-m-d H:i:s', strtotime(trim($value, "`'")));
            case "region":
            case "country":
                return str_replace("_", " ", $value);
            case "reach":
                return (float)preg_replace('![^0-9\.]+!u', '', $value);
            case "media_list":
                return $this->mutators['listType'][strtolower($value)];
            case "media_type":
                return $this->mutators['types'][strtolower($value)];
            case "media_category":
                return $this->mutators['categories'][strtolower($value)];
            case "visibility":
                return (float)str_replace(',', '.', $value);
            case "image":
                return $this->mutators['image'][strtolower($value)];
            case "key_message_penetration":
                return $this->mutators['keymsg'][strtolower($value)];
            case "mention":
                return $this->mutators['mention'][strtolower($value)];
            case "speaker_quote":
                $value = strtolower($value);
                return !empty($this->mutators['speaker_quote'][$value]) ? $this->mutators['speaker_quote'][$value] : $this->mutators['speaker_quote']['absent'];
            case "third_speakers_quotes":
                $value = strtolower($value);
                return !empty($this->mutators['third_speakers_quotes'][$value]) ? $this->mutators['third_speakers_quotes'][$value] : $this->mutators['third_speakers_quotes']['absent'];
            case "header_tonality":
                return $this->mutators['headerTonality'][strtolower($value)];
            case "main_tonality":
                return strtolower($value);
            case "impact_index":
                return (float)str_replace(',', '.', $value);
            case "net_effect":
                return (float)preg_replace('![^0-9\.]+!u', '', $value);
            case "desired_article":
                return strtolower($value) == 'no' ? false : true;
            case "url":
                if (in_array(
                    $value,
                    [
                        'None',
                        'N/A',
                        'Print',
                        'PDF attached',
                        'See attach',
                        'Print coverage (please find in the attachment)',
                        'Text in PDF avail. Upon request'
                    ]
                )
                ) {
                    return '';
                }

                return $value;
            case "product":
            case "campaign":
            case "news_break_local":
            case "news_break_global":
            case "business":
                if (in_array($value, ['No', 'no']) || !$value) {
                    return [];
                }

                return explode(',', $value);

            case "third_speakers_quoted":
                $tmp = strtolower($value);
                return (in_array($tmp, BaseValidator::$rules[BaseValidator::NOT_IN_FOR_SPEAKERS]) || !$value) ? "" : $value;

            case "speakers_quoted":
                $tmp = strtolower($value);
                if (in_array($tmp, BaseValidator::$rules[BaseValidator::NOT_IN_FOR_SPEAKERS]) || !$value) {
                    return [];
                }

                $value = str_replace([",", "/", "&", " and "], ",", $value);
                return explode(",", $value);
            case "media_subcategories":
                if (!is_array($value) && !empty($value) && $value != "") {
                    $value = explode(",", $value);
                }
                return $value;
            default:
                return $value;
        }
    }

    /**
     * Validation
     *
     * @return bool
     */
    protected function validate()
    {
        if ($this->validator->fails()) {
            return false;
        }

        return true;
    }


}