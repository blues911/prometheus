<?php

use App\Models\ExportFile;
use mikehaertl\wkhtmlto\Pdf;
use App\Models\SaveReport;

/**
 * Class ExportBackground
 * @package App
 */
class ExportChartsBackground
{
    /**
     * @param $job
     * @param $data
     */
    public function fire($job, $data)
    {
        $job->delete();

        echo "Start export charts job "  .date('h:i:s') . "\n";

        $name     = $data['filename'];
        $filename = md5($name . time());
        $pdf = new Pdf($data['report']['url'] . "?" . http_build_query(['t' => $data['report']['hash']]));
        $pdf->setOptions(['javascript-delay' => 5000]);
        $pdf->saveAs(Config::get('app.tmp_path') . '/' . $filename . '.pdf');
        if (!$pdf->saveAs(Config::get('app.tmp_path') . '/' . $filename . '.pdf')) {
            echo $pdf->getError();
        }

        $exportFile           = new ExportFile();
        $exportFile->name     = $name . '.pdf';
        $exportFile->filename = $filename . '.pdf';
        $exportFile->user_id  = $data['user']['id'];
        $exportFile->type     = SaveReport::PDF_TYPE;
        $exportFile->save();
    }

}
