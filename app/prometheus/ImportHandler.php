<?php
/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at Fahrenheit 451
 *
 * Date: 18.03.15
 * Time: 1:15
 */

use App\Models\Notification;
use App\Models\SystemUser;
use App\Prometheus\ImportCoverage;
use App\Prometheus\ImportMedia;

/**
 * Class ImportHandler
 * @package App
 */
class ImportHandler
{

    protected $importer;
    protected $type;

    /**
     * @param $job
     * @param $data
     */
    public function fire($job, $data)
    {
        $job->delete();
        if (empty($data['type'])) {
            return;
        }
        $this->type = $data['type'];
        if ($this->type == Notification::IMPORT_COVERAGE) {
            $this->coverage($data);
        }
        else {
            $this->media($data);
        }
    }

    /**
     * Add import coverage file to queue
     *
     * @param $data
     */
    public function coverage($data)
    {
        $this->importer = new ImportCoverage(
            $data['file'],
            $data['user_id'],
            $data['file_name'],
            $data['original_filename'],
            $data['need_approve'],
            $data['file_id']
        );
        $this->importer->run();
        $this->addNotification($data);
    }

    /**
     * Add import media file to queue
     *
     * @param $data
     */
    public function media($data)
    {
        $this->importer = new ImportMedia(
            $data['file'],
            $data['user_id'],
            $data['file_name'],
            $data['original_filename'],
            $data['file_id']
        );
        
        $this->importer->run();
        $this->addNotification($data);
    }

    /**
     * Add notification by import result
     *
     * @param $data
     */
    public function addNotification($data)
    {
        $notification = new Notification;
        $notification->type = $this->type;
        $errors             = $this->importer->getErrors();
        if (empty($errors)) {
            $notification->data = json_encode(['error' => false, 'filename' => $data['original_filename']]);
        }
        else {
            $_err   = [];
            foreach ($errors as $error) {
                foreach ($error['messages'] as $k => $v) {
                    if (!isset($_err[$k])) {
                        $_err[$k] = [
                            'messages' => [],
                            'rows'     => []
                        ];
                    }

                    $_err[$k]['messages'] = array_unique(array_merge($_err[$k]['messages'], $v));
                    $_err[$k]['rows'][]   = $error['row'] + 1;
                }
            }

            $notification->data = json_encode([
                'error'    => true,
                'errors'   => $_err,
                'filename' => $data['original_filename']
            ]);
        }
        $notification->recipient()->associate(SystemUser::find($data['user_id'])->getModel());
        $notification->save();
    }

}