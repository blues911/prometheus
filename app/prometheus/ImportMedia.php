<?php

namespace App\Prometheus;

use Maatwebsite\Excel\Filters\ChunkReadFilter;
use Cache;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Reader_Excel2007;
use PHPExcel_Shared_Date;
use PHPExcel_Worksheet;
use PHPExcel_Worksheet_Row;
use PHPExcel_Worksheet_RowIterator;
use PHPExcel_Style_Fill;
use App\Models\BaseValidator;
use App\Models\Country;
use App\Models\Region;
use App\Models\SystemUser;
use App\Models\ImportFile;
use App\Models\User;
use App\Models\Coverage;
use App\Models\Media;
use Auth;
use Validator;
use DB;
use Config;
use App\Prometheus\ExcelValidator;
use App\Models\Notification;

/**
 * Class ImportMedia
 * @package App\Prometheus
 */
class ImportMedia
{

    private $listType = [
        'gold' => Coverage::MEDIA_LIST_GOLD,
        'platinum' => Coverage::MEDIA_LIST_PLATINUM,
        'diamond' => Coverage::MEDIA_LIST_DIAMOND
    ];

    private $types = [
        'information agency' => Coverage::MEDIA_TYPE_AGENCY,
        'agency' => Coverage::MEDIA_TYPE_AGENCY,
        'online' => Coverage::MEDIA_TYPE_ONLINE,
        'tv' => Coverage::MEDIA_TYPE_TV,
        'print' => Coverage::MEDIA_TYPE_PRINT,
        'radio' => Coverage::MEDIA_TYPE_RADIO
    ];

    private $categories = [
        'business media' => Coverage::MEDIA_CATEGORY_BUSINESS,
        'business' => Coverage::MEDIA_CATEGORY_BUSINESS,
        'general interest' => Coverage::MEDIA_CATEGORY_GENERAL,
        'general' => Coverage::MEDIA_CATEGORY_GENERAL,
        'it' => Coverage::MEDIA_CATEGORY_IT,
        'vertical' => Coverage::MEDIA_CATEGORY_VERTICAL
    ];

    private $xls;
    private $data;
    private $fileName;
    private $originalFileName;
    private $importFile;

    public $errors;

    /**
     * Init class
     */
    public function __construct($xls, $userId, $fileName, $originalFileName, $fileId = null)
    {
        $this->xls = $xls;
        $this->fileName = $fileName;
        $this->systemUser = SystemUser::find($userId);
        $this->originalFileName = $originalFileName;
        $this->importFile = !empty($fileId) ? ImportFile::find($fileId) : null;
    }

    /**
     * get Errors
     *
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Save import records
     * @param array $data
     */
    public function save(array $data)
    {
        $region = Region::where('name', '=', $data['region'])->first();
        $country = Country::where('name', '=', $data['country'])->first();

        if (empty($country) || empty($region) || $country->region_id != $region->id) {
            return;
        }

        $media = Media::where("name", "=", $data['name'])
            ->where("country_id", "=", $country->id)
            ->where("region_id", "=", $country->region_id)
            ->first();

        if (empty($media)) {
            $media = new Media();
            $media->name = $data['name'];
        }

        $media->region()->associate($region);
        $media->country()->associate($country);
        $media->list = $data['list'];
        $media->type = $data['type'];
        $media->category = $data['category'];
        $media->reach = $data['reach'];
        $media->save();

        if (!empty($data['subcategories'])) {
            $subcategories = \App\Models\Subcategory::all()->toArray();
            $mediaSubcat = [];
            foreach ($subcategories as $item) {
                $mediaSubcat[$item['id']] = strtolower($item['title']);
            }

            $result = [];
            $excelData = explode(',', $data['subcategories']);

            foreach ($excelData as $k => $v) {
                $v = ltrim(rtrim($v));
                $v = preg_replace('/\s+/', ' ', $v);
                $v = strtolower($v);
                if (in_array($v, $mediaSubcat)) {
                    $result[] = array_search($v, $mediaSubcat);
                }
            }

            $media->subcategories()->sync($result);
        } else {
            $media->subcategories()->detach();
        }
    }

    /**
     * Run import
     *
     * @return bool
     */
    public function run()
    {
        set_time_limit(0);

        $totalRows = \Excel::load($this->xls)->getTotalRowsOfFile();
        $sheet = \Excel::load($this->xls)->get()[0];
        $list = $sheet->toArray();

        $first_key = key($list);
        $subcategory=$this->findIndex('media_subcategory_',$list[$first_key]);

        foreach ($list as $key => $rowBunch) {
            $reach = $rowBunch['reach_circulation_for_print_media_website_visitors_per_week_broadcast_reach'];
            $country = trim($rowBunch['country']);
            $region = trim($rowBunch['region']);

            $media = [
                'country' => str_replace("_", " ", $country),
                'region' => str_replace("_", " ", $region),
                'name' => trim($rowBunch['publication_name']),
                'category' => trim(strtolower($rowBunch['media_category_vertical_business_it_general_interest'])),
                'list' => trim(strtolower($rowBunch['media_list_diamond_platinum_gold'])),
                'type' => trim(strtolower($rowBunch['media_type_information_agency_online_print_tv_radio'])),
                'reach' => intval(preg_replace('![^0-9]+!u', '', $reach)),
                'subcategories' => $rowBunch[$subcategory]
            ];

            $media['list'] = isset($this->listType[$media['list']]) ? $this->listType[$media['list']] : $media['list'];
            $media['type'] = isset($this->types[$media['type']]) ? $this->types[$media['type']] : $media['type'];
            $media['category'] = isset($this->categories[$media['category']]) ? $this->categories[$media['category']] : $media['category'];

            $validator = BaseValidator::make($media, BaseValidator::IMPORT_MEDIA);

            if ($validator->fails()) {
                $this->errors[] = ['messages' => $validator->errors()->toArray(), 'row' => $key + 2];
            }

            $this->data[] = $media;
            $progress = round(count($this->data) / ($totalRows / 80));
            self::updateProgress($progress, "ok", $this->fileName);
        }

        self::updateProgress(85, "ok", $this->fileName);

        if (!$this->errors) {
            if (empty($this->importFile)) {
                $importFile = new ImportFile();
                $importFile->name = $this->originalFileName;
                $importFile->filename = md5($this->fileName);
                $importFile->user_id = $this->systemUser->id;
                $importFile->type = ImportFile::IMPORT_MEDIA;
                $importFile->save();

                rename($this->xls, Config::get('app.imports_path') . "/" . $importFile->filename);

                $this->importFile = $importFile;
            }

            foreach ($this->data as $item) {
                $this->save($item);
            }

            Import::updateProgress(100, "ok", $this->fileName);
            return true;
        }

        $excel = new ExcelValidator($this->xls, $this->errors, Notification::IMPORT_MEDIA);
        $errorFile = $excel->validate();

        Cache::put($this->fileName . '_queue:error_file', $errorFile, 30);

        Import::updateProgress(100, "error", $this->fileName);
        return false;
    }

    /**
     * @param array $progress
     */
    public static function updateProgress($progress, $status = "ok", $fileName)
    {
        Cache::put($fileName . '_queue:progress', $progress, 30);
        Cache::put($fileName . '_queue:status', $status, 30);
    }

    /**
     * @param $pattern string Искомая строка
     * @param $list string Массив
     * @return bool|int|string
     */
    protected function findIndex($pattern,$list){

        foreach ($list as $key=>$v){
            $offset=strpos($key, $pattern);
            if ($offset===0) {
                return $key;
            }
        }
        return false;
    }

}