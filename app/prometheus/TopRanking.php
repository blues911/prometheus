<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 01.06.2017
 * Time: 17:58
 */

namespace App\Prometheus;

use App\Models\NewsBreak;
use App\Models\Speaker;
use App\Models\Campaign;
use Maatwebsite\Excel\Classes\PHPExcel;
use PHPExcel_IOFactory;


class TopRanking
{

    private $newsBreaks;
    private $speakers;
    private $campaigns;
    private $subcategories;


    /**
     * @param int $limit
     * @return mixed
     */
    public function getNewsbreakTop($limit = 50)
    {
        return NewsBreak::getTop($limit);
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function getSpeakerTop($limit = 50)
    {
        return Speaker::orderBy('created_at', 'desc')
            ->take($limit)->get()->toArray();
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function getCampaignTop($limit = 50)
    {
        return Campaign::getTop($limit);
    }


    /**
     * @return mixed
     */
    public function getXls()
    {
        $excel = new PHPExcel();
        $excelWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $excel->getActiveSheet()->setTitle("speakers");
        $speakerTop = $this->getSpeakerTop();
        $this->populateXls($excel, $speakerTop);


        $excel->createSheet(1, 'campaign');
        $excel->setActiveSheetIndex(1);
        $campaignTop = $this->getCampaignTop();
        $this->populateXls($excel, $campaignTop);

        $excel->createSheet(2, 'newsbreak');
        $excel->setActiveSheetIndex(2);
        $speakerTop = $this->getNewsbreakTop();
        $this->populateXls($excel, $speakerTop);


        $excelWriter->save('test.xls');
        return true;
    }

    /**
     * @param PHPExcel $excel
     * @param array $result
     * @return mixed
     */
    public function populateXls($excel, $result)
    {
        foreach ($result as $rowNum => $data) {
            $i = 0;
            foreach ($data as $name => $value) {
                $excel->getActiveSheet()->setCellValueByColumnAndRow($i, $rowNum + 1, '' . $value);
                ++$i;
            }

        }

    }
}