<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 01.06.2017
 * Time: 15:09
 */

namespace App\Prometheus;

use App\Models\Notification;
use PHPExcel_IOFactory;
use PHPExcel_Style_Fill;
use Config;

class ExcelValidator
{

    private $xls;
    private $errors;
    private $type;
    private $column;


    public function __construct($xls, $errors, $type)
    {
        $this->xls = $xls;
        $this->errors = $errors;
        if ($type == Notification::IMPORT_MEDIA) {
            $this->column = 'I';
        } else {
            $this->column = 'AI';
        }
        $this->type = $type;
    }

    public function validate()
    {
        $excel = false;
        $formats = ['Excel5', 'Excel2007', 'Excel2003XML', 'OOCalc', 'Gnumeric'];

        foreach ($formats as $format) {
            $excel = PHPExcel_IOFactory::createReader($format);
            if ($excel->canRead($this->xls)) {
                break;
            }
        }
        $excel = $excel->load($this->xls);
        $excel->setActiveSheetIndex(0);

        foreach ($this->errors as $error) {
            if ($this->type == Notification::IMPORT_MEDIA) {
                $row = $error['row'];
            } else {
                $row = $error['row'] + 1;
            }

            $excel->getActiveSheet()
                ->getStyle("A" . $row . ":" . $this->column . $row)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('FF0000');


                foreach ($error['messages'] as $message) {

                    $previousMessage = $excel->getActiveSheet()->getCell($this->column . $row)->getValue();

                    if (!empty($previousMessage)) {
                        $msg = $previousMessage . " ; " . $message[0];
                    } else {
                        $msg = $message[0];
                    }

                    $excel->getActiveSheet()->setCellValue($this->column . $row, $msg);
                }

                $excel->getActiveSheet()->unfreezePane();
                $excel->getActiveSheet()->setSelectedCell('A2');

        }

        $char = '0123456789zxcvbnmasdfghjklqwertyuiopZXCVBNMASDFGHJKLQWERTYUIOP';
        $over_name = '';
        for ($i = 0; $i < 12; $i++)
            $over_name .= substr($char, rand(0, 61), 1);
        $filename = time() . "_" . $over_name . ".xlsx";
        if (!file_exists(Config::get('app.imports_path') . "/errors")) {
            mkdir(Config::get('app.imports_path') . "/errors", 0777, true);
        }

        $newName = Config::get('app.imports_path') . "/errors/" . $filename;


        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save($this->xls);
        rename($this->xls, $newName);

        return "/coverage/import/file_error/{$filename}";

    }
}