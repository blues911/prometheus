<?php

use App\Models\ExportFile;
use App\Models\Coverage;

/**
 * Class ExportBackground
 * @package App
 */
class ExportBackground
{
    /**
     * @param $job
     * @param $data
     */
    public function fire($job, $data)
    {
        $job->delete();

        echo "Start export coverage job "  .date('h:i:s') . "\n";
        $exportData = Coverage::prepareDataForExport($data['filter'], $data['user'], $data['search']);


        $name = $data['filename'];
        $filename = md5($name . time());


        $writer = new XLSXWriter();

        //Добавляем колонку с дубликатами
        $exportData['item'][0][]="Duplicate";

        foreach ($exportData['item'] as $index=>$item) {
            $rowStyle=null;
            if (isset($exportData['duplicate'][$index])) {

                echo $index . " S| ";
                $item[]=$exportData['duplicate'][$index]['message'];

                $rowStyle = array('fill'=>"#FFFF00");
            }

            $writer->writeSheetRow('Sheet1', $item, $rowStyle );
        }

//        \Excel::create($filename, function ($excel) use ($exportData) {
//            $excel->sheet('Coverage', function ($sheet) use ($exportData) {
//                $sheet->setAutoSize(false);
//                $sheet->setColumnFormat(['D' => 'dd.mm.yyyy']);
//                $sheet->fromArray($exportData['item'], null, 'A1', true, false);
                /* Отмечаем дубли */
//                foreach ($exportData['duplicate'] as   $duplicate) {
//
//
//
//                    $sheet->row($duplicate['row'], function ($row) {
//                        $row->setBackground('#FFFF00');
//                    });
//                    $sheet->cell('AI'.$duplicate['row'], function($cell) use ($duplicate) {
//                        $cell->setValue($duplicate['message']);
//
//                    });
//                }
                 /* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */
//            });
//        })->store('xls', Config::get('app.tmp_path'));

        $writer->writeToFile( Config::get('app.tmp_path') . '/' . $filename . '.xls');

        $exportFile = new ExportFile();
        $exportFile->name = $name . '.xls';
        $exportFile->filename = $filename . '.xls';
        $exportFile->user_id = $data['user']['id'];
        $exportFile->type = 'xls';
        $exportFile->save();

        echo "Complete export coverage job " .date('h:i:s') . "\n";
    }

}
