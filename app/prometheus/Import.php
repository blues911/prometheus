<?php
namespace App\Prometheus;

/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at Fahrenheit 451
 * 
 * Date: 12.03.15
 * Time: 20:42
 */
use Maatwebsite\Excel\Filters\ChunkReadFilter;
use Cache;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Reader_Excel2007;
use PHPExcel_Shared_Date;
use PHPExcel_Worksheet;
use PHPExcel_Worksheet_Row;
use PHPExcel_Worksheet_RowIterator;


/**
 * Class Import
 * @package App\Prometheus
 */
class Import
{

    /**
     * @var array
     */
    protected static $data = [];
    protected static $used = 0;

    /**
     * @param $xlsFilePath
     * @param array $map
     * @return array
     */
    public static function convertCoverageData($xlsFilePath, array $map, $fileName)
    {
        set_time_limit(0);

        $totalRows = \Excel::filter('chunk')->load($xlsFilePath)->getTotalRowsOfFile();
        $chunkSize = 250;
        self::updateProgress(0, "ok", $fileName);

        \Excel::filter('chunk')->load($xlsFilePath)->chunk(
            $chunkSize,
            function ($chunk) use ($map, $totalRows, $chunkSize, $fileName) {
                $chunk = $chunk->toArray();

                if (isset($chunk[0])) {
                    $rows = $chunk[0];
                    $count = count($rows);
                } else {
                    return false;
                }

                for ($i = 0; $i < $count; $i++) {
                    $row = $rows[$i];
                    Import::replaceArrayKeys($row, $map, true);
                    $rows[$i] = $row;
                }

                self::pushChunk($rows);
                self::$used += $chunkSize;
                $progress = round(self::$used / ($totalRows / 90));
                self::updateProgress($progress, "ok", $fileName);
            }
        );

        return self::$data;
    }

    /**
     * @param array $progress
     */
    public static function updateProgress($progress, $status = "ok", $fileName)
    {
        Cache::put($fileName.'_queue:progress', $progress, 30);
        Cache::put($fileName.'_queue:status', $status, 30);
    }

    /**
     * @param array $chunk
     */
    public static function pushChunk(array $chunk)
    {
        self::$data = array_merge(self::$data, $chunk);
    }

    /**
     * @param $path
     */
    public static function save($path)
    {
        file_put_contents($path, json_encode(self::$data));
    }

    /**
     * Replaces keys in associative array by $map
     * @param array $array
     * @param array $map
     * @param bool $clean
     * @return array
     */
    public static function replaceArrayKeys(array &$array, array $map, $clean = false)
    {
        $tmp = [];

        foreach ($map as $key => $value) {
            if (array_key_exists($key, $array)) {
                $tmp[$value] = $array[$key];
            }
        }

        if (!$clean) {
            foreach ($array as $k => $v) {
                if (!array_key_exists($k, $map)) {
                    $tmp[$k] = $v;
                }
            }
        }

        $array = $tmp;

        return $array;
    }
}
