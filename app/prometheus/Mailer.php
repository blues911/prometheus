<?php
namespace App\Prometheus;

use App;
use Config;

/**
 * Class Mailer
 * @package App\Prometheus
 */
class Mailer
{

    /**
     * Send email by SES
     *
     * @param array $params
     *
     * @return void
     */
    public static function send($params = []) {
        $client = App::make('aws')->get('ses');
        $emails = is_array($params['to']) ? $params['to'] : [$params['to']];
        $sender = Config::get('app.email.sender');

        $emailSentId = $client->sendEmail([
            // Source is required
            'Source' => "{$_ENV['APP_EMAIL_SENDER_NAME']} <{$sender}>",
            // Destination is required
            'Destination' => [
                'ToAddresses' => $emails
            ],
            // Message is required
            'Message' => [
                // Subject is required
                'Subject' => [
                    // Data is required
                    'Data'    => $params['subject'],
                    'Charset' => 'UTF-8',
                ],
                // Body is required
                'Body' => [
                    'Html' => [
                        // Data is required
                        'Data'    => $params['message'],
                        'Charset' => 'UTF-8',
                    ],
                ],
            ],
        ]);

        return !empty($emailSentId);
    }
}
