<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Subregion extends \Eloquent
{
    use SoftDeletingTrait;

    protected $dates    = ['deleted_at'];
    protected $table    = 'subregions';
    protected $fillable = [
        'name',
        'region_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }
}