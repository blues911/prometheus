<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Business extends \Eloquent
{
    use SoftDeletingTrait;

    protected $dates      = ['deleted_at'];
    protected $table      = 'business';
    protected $fillable   = ['name', 'status'];

    const STATUS_ACTIVE   = 'active';
    const STATUS_ARCHIVED = 'archived';

    /**
     * get list campaigns
     *
     * @return mixed
     */
    public static function getList()
    {
        return \DB::table('business')
            ->where('status', '=', self::STATUS_ACTIVE)
            ->whereNull('deleted_at')
            ->orderBy('name')
            ->lists("name");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function coverage()
    {
        return $this->belongsToMany('App\Models\Coverage', 'business_coverage', 'business_id', 'coverage_id');
    }

    /**
     * get Active Campaigns
     *
     * @return mixed
     */
    public static function getActiveBusiness()
    {
        return \DB::table('business')
            ->where('status', '=', self::STATUS_ACTIVE)
            ->whereNull('deleted_at')
            ->orderBy('name')
            ->get();
    }

    /**
     * Get business by id
     *
     * @param int $id
     *
     * @return string
     */
    public static function getBusinessById($id)
    {
        $record = \DB::table('business')
            ->where('id', '=', $id)
            ->whereNull('deleted_at')
            ->first();
        if (!isset($record)) {
            return '';
        }
        return $record->name;
    }
}