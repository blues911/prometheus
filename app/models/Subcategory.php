<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Subcategory extends \Eloquent
{
    use SoftDeletingTrait;

    public $timestamps = false;
    protected $table = 'subcategory';
    protected $fillable = ['slug', 'title'];

    const SUBCATEGORIES_PAGE_SIZE_DEFAULT = 25;

}
