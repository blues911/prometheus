<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at
 *
 * Date: 26.10.14
 * Time: 18:25
 */
class Country extends \Eloquent
{
    use SoftDeletingTrait;

    protected $dates    = ['deleted_at'];
    protected $table    = 'countries';
    protected $fillable = [
        'iso_code',
        'name',
        'region_id',
        'subregion_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subregion()
    {
        return $this->belongsTo('App\Models\Subregion');
    }

    public static function getCountryById($id)
    {
        $record = \DB::table('countries')
            ->where('id', '=', $id)
            ->whereNull('deleted_at')
            ->first();
        if (!isset($record)) {
            return '';
        }
        return $record->name;
    }
}
