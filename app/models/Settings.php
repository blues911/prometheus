<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Settings extends \Eloquent
{
    // TODO use this model for global site settings
    public $timestamps = false;
    protected $table = 'settings';
    protected $fillable = ['title', 'slug', 'value'];
}