<?php
namespace App\Models;

class ImportFile extends \Eloquent
{
    protected $table    = 'import_files';
    protected $fillable = ['name', 'filename', 'user_id'];

    const IMPORT_COVERAGE = 'import_coverage';
    const IMPORT_MEDIA    = 'import_media';

    public function getCreatedAtAttribute($value)
    {
        $offset = \Session::get('time-offset', 0);
        return date('Y-m-d H:i:s', strtotime($value) + $offset);
    }

    /**
     * @return array
     */
    public static function getFilterByType()
    {
        return [
            0                 => "Tables and lists",
            "import_coverage" => "Tables",
            "import_media"    => "Lists",
        ];
    }

    /**
     * @param $skip
     * @param $limit
     * @param $filter
     * @param $search
     *
     * @return mixed
     */
    public static function getActiveUserImportFiles($skip = 0, $limit = 10, $filter = [], $search = '')
    {
        $statement = new ImportFile;
        if (!empty($filter['user_id'])) {
            $statement = $statement->where("user_id", "=", $filter['user_id']);
        }
        if (!empty($filter['from_date'])) {
            $statement = $statement->where("created_at", ">=", $filter['from_date']);
        }
        if (!empty($filter['to_date'])) {
            $statement = $statement->where("created_at", "<", $filter['to_date']);
        }
        if (!empty($filter['type'])) {
            $statement = $statement->where("type", "=", $filter['type']);
        }

        if (!empty($search)) {
            $statement = $statement->where('name', 'LIKE', $search."%");
        }
        if ($skip > 0) {
            $statement = $statement->skip($skip);
        }

        if ($limit > 0) {
            $statement = $statement->limit($limit);
        }

        return $statement->orderBy('id', 'DESC')->get();
    }

}