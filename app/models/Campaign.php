<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Campaign extends \Eloquent
{
    use SoftDeletingTrait;

    protected $dates      = ['deleted_at'];
    protected $table      = 'campaigns';
    protected $fillable   = ['name', 'status'];

    const STATUS_ACTIVE   = 'active';
    const STATUS_ARCHIVED = 'archived';

    /**
     * get list campaigns
     *
     * @return mixed
     */
    public static function getList()
    {
        return \DB::table('campaigns')
            ->where('status', '=', self::STATUS_ACTIVE)
            ->whereNull('deleted_at')
            ->orderBy('name')
            ->lists("name");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function coverage()
    {
        return $this->belongsToMany('App\Models\Coverage', 'campaigns_coverage', 'campaign_id', 'coverage_id');
    }

    /**
     * get Active Campaigns
     *
     * @return mixed
     */
    public static function getActiveCampaigns()
    {
        return \DB::table('campaigns')
            ->where('status', '=', self::STATUS_ACTIVE)
            ->whereNull('deleted_at')
            ->orderBy('name')
            ->get();
    }

    /**
     * Get TOP
     *
     * @param int $limit
     *
     * @return mixed
     */
    public static function getTop($params)
    {
        $builder = \DB::table('campaigns_coverage as сс')
            ->select('campaigns.name', \DB::raw('count(1) as total'), \DB::raw('campaigns.id as id'))
            ->join('campaigns', 'campaigns.id', '=', 'сс.campaign_id')
            ->join('coverage', 'coverage.id', '=', 'сс.coverage_id')
            ->where('coverage.status', '=', Coverage::STATUS_APPROVED)
            ->whereNull('campaigns.deleted_at')
            ->groupBy('campaigns.name')
            ->orderBy('total', 'desc')
            ->limit($params['limit']);

        if (isset($params['date'])) {
            $builder->where('coverage.date', '>=', $params['date']['start'])
                ->where('coverage.date', '<', $params['date']['end']);
        }

        return $builder->get();
    }

}