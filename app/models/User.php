<?php
namespace App\Models;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use App\Prometheus\Mailer;
use View;
use URL;

/**
 * Class User
 */
class User extends \Eloquent implements UserInterface, RemindableInterface
{

    const ROLE_ADMINISTRATOR         = 'administrator';
    const ROLE_MANAGER               = 'manager';
    const ROLE_USER                  = 'user';

    const EVENT_USER_RANDOM_PASSWORD = 'EVENT_USER_RANDOM_PASSWORD';
    const EVENT_USER_CHANGE_PASSWORD = 'EVENT_USER_CHANGE_PASSWORD';
    const EVENT_USER_NEW_PASSWORD    = 'EVENT_USER_NEW_PASSWORD';
    const EVENT_USER_CREATE          = 'EVENT_USER_CREATE';

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'system_users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Send user notification by email
     *
     * @param int|string $recipient
     * @param string     $type
     * @param string     $vars
     *
     * @return bool
     */
    public static function notify($recipient, $type, $vars = [])
    {
        $options = [
            self::EVENT_USER_CHANGE_PASSWORD => ['s' => 'KL Prometheus', 't' => 'emails.user.change-password'],
            self::EVENT_USER_NEW_PASSWORD    => ['s' => 'KL Prometheus', 't' => 'emails.user.new-password'],
            self::EVENT_USER_RANDOM_PASSWORD => ['s' => 'KL Prometheus', 't' => 'emails.user.random-password'],
            self::EVENT_USER_CREATE          => ['s' => 'KL Prometheus', 't' => 'emails.user.create'],
        ];

        if (empty($options[$type])) {
            return;
        }

        $message = View::make($options[$type]['t'], $vars)->render();
        return Mailer::send(['to' => $recipient, 'subject' => $options[$type]['s'], 'message' => $message]);
    }

}
