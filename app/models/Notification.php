<?php
/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at Fahrenheit 451
 * 
 * Date: 19.03.15
 * Time: 18:45
 */

namespace App\Models;

/**
 * Class Notification
 * @package App\Models
 */
class Notification extends \Eloquent
{
    protected $table    = 'notifications';
    protected $fillable = [
        'user_id',
        'type',
        'data',
        'status'
    ];

    const IMPORT_COVERAGE = 'import_coverage';
    const IMPORT_MEDIA    = 'import_media';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recipient()
    {
        return $this->belongsTo('App\Models\SystemUser', 'user_id');
    }
}
