<?php
namespace App\Models;

use Validator;

class BaseValidator
{

    const CHANGE_PASSWORD           = 'CHANGE_PASSWORD';
    const ENTER_EMAIL               = 'ENTER_EMAIL';
    const SIGNIN                    = 'SIGNIN';
    const IMPORT_MEDIA              = 'IMPORT_MEDIA';
    const FILTER_BY_DATE_AND_REGION = 'FILTER_BY_DATE_AND_REGION';
    const MEDIA                     = 'MEDIA';
    const SYSTEM_USER               = 'SYSTEM_USER';
    const IMPORT_COVERAGE           = 'IMPORT_COVERAGE';
    const COVERAGE                  = 'COVERAGE';
    const TYPE_DOC                  = 'TYPE_DOC';
    const TYPE_XLS                  = 'TYPE_XLS';
    const TYPE_PDF                  = 'TYPE_PDF';
    const NOT_IN_FOR_SPEAKERS       = 'NOT_IN_FOR_SPEAKERS';
    const SPEAKER                   = 'SPEAKER';
    const THIRD_SPEAKER             = 'THIRD_SPEAKER';

    // set of rules
    public static $rules = [
        self::CHANGE_PASSWORD => [
            'password'              => 'required|min:8',
            'password_confirmation' => 'required|same:password',
        ],
        self::ENTER_EMAIL => [
            'email'  => 'required|email',
        ],
        self::SIGNIN => [
            'email'    => 'required|email',
            'password' => 'required|min:8'
        ],
        self::IMPORT_MEDIA => [
            'region'        => 'exists:regions,name',
            'country'       => 'exists:countries,name',
            'list'          => 'in:1,0.8,0.6',
            'type'          => 'in:agency,online,print,tv,radio',
            'category'      => 'in:vertical,business,it,general',
            'subcategories' => 'media_subcategories'
        ],
        self::FILTER_BY_DATE_AND_REGION => [
            'start'     => 'date',
            'end'       => 'date',
            'region_id' => 'integer'
        ],
        self::MEDIA => [
            'list'       => 'in:1,0.8,0.6',
            'type'       => 'in:agency,online,print,tv,radio',
            'category'   => 'in:vertical,business,it,general',
            'region_id'  => 'regex:![0-9]{1,8}!',
            'country_id' => 'regex:![0-9]{1,8}!',
            'reach'      => 'regex:![0-9]{1,8}!',
            'name'       => 'alpha_spaces'
        ],
        self::SYSTEM_USER => [
            'email'      => 'required|email|unique:system_users,email',
            'password'   => 'min:8',
            'region_id'  => 'regex:![0-9]{1,8}!',
            'country_id' => 'regex:![0-9]{1,8}!',
            'name'       => 'required|min:2',
            'role'       => 'in:administrator,manager,user'
        ],
        self::IMPORT_COVERAGE => [
            'region'                  => 'exists:regions,name',
            'country'                 => 'exists:countries,name',
            'date'                    => 'date_format:Y-m-d H:i:s|after:01.01.2000',
            'reach'                   => 'numeric',
            'media_list'              => 'in:1,0.8,0.6',
            'media_type'              => 'in:agency,online,print,tv,radio',
            'media_category'          => 'in:vertical,business,it,general',
            'visibility'              => 'numeric',
            'image'                   => 'in:1,0.8',
            'key_message_penetration' => 'in:absent,present,extent,counter',
            'mention'                 => 'in:1,0.9,0.8,0.5',
            'third_speakers_quotes'   => 'in:1,0',
            'speaker_quote'           => 'in:1,0.8',
            'speakers_quoted'         => 'required_if:speaker_quote,1|speakers_quoted',
            'third_speakers_quoted'   => 'required_if:third_speakers_quotes,1',
            'header_tonality'         => 'in:0.5,0.85,1',
            'main_tonality'           => 'in:negative,positive,neutral',
            'impact_index'            => 'numeric',
            'net_effect'              => 'numeric',
            'desired_article'         => 'boolean',
            'media_subcategories'     => 'media_subcategories'
        ],
        self::COVERAGE => [
            'owner_id'                => 'integer',
            'region_id'               => 'integer',
            'country_id'              => 'integer',
            'reach'                   => 'numeric',
            'url'                     => 'url',
            'media_list'              => 'in:1,0.8,0.6',
            'media_type'              => 'alpha_dash',
            'media_category'          => 'alpha_dash',
            'visibility'              => 'numeric',
            'image'                   => 'in:1,0.8',
            'key_message_penetration' => 'in:absent,present,extent,counter',
            'mention'                 => 'in:1,0.9,0.8,0.5',
            'third_speakers_quotes'   => 'in:1,0',
            'header_tonality'         => 'in:0.5,0.85,1',
            'main_tonality'           => 'in:negative,positive,neutral',
            'speaker_quote'           => 'in:1,0.8',
            'desired_article'         => 'boolean',
            'impact_index'            => 'numeric',
            'net_effect'              => 'numeric',
            'status'                  => 'alpha_dash',
            'date'                    => 'date'
        ],
        self::TYPE_DOC => [
            'file' => 'mimes:doc,docx'
        ],
        self::TYPE_XLS => [
            'file' => 'mimes:xls,xlsx,xlsm'
        ],
        self::TYPE_PDF => [
            'file' => 'mimes:pdf'
        ],
        self::NOT_IN_FOR_SPEAKERS => ['no', 'none', 'n/a', 'na', 'not available', 'no name', 'no speaker'],
        self::SPEAKER => [
            'name' => ["regex:/^([a-zA-Z]{2,}(-[a-zA-Z]{2,})*)(\s[a-zA-Z]{2,}(-[a-zA-Z]{2,})*)+$/i"]
        ],
        self::THIRD_SPEAKER => [
            'third_speakers' => ["regex:/^[^_0-9!@#$%№;:?*.^<>~\\[\\]\\-_='\"\\\\|\\/{}()\\+]+$/i"]
        ],
    ];

    /**
     * Validation
     *
     * @param $params
     * @param $rule
     *
     * @return null
     */
    public static function make($params, $rule)
    {
        if (is_array($rule)) {
            return Validator::make($params, $rule);
        }
        if (!isset(self::$rules[$rule])) {
            return null;
        }
        if ($rule == static::IMPORT_MEDIA) {
            Validator::extend('sub_cat', function ($attribute, $value, $parameters) {

                $result = true;
                $subcategories = \App\Models\Subcategory::all()->toArray();
                $media = [];

                foreach ($subcategories as $item) {
                    $media[] = strtolower($item['title']);
                }

                if (!empty($value)) {
                    foreach ($value as $k => $v) {
                        $v = trim($v);
                        $v = preg_replace('/\s+/', ' ', $v);
                        $v = strtolower($v);
                        if (!in_array($v, $media)) {
                            $result = false;
                        }
                    }
                }

                return (bool) $result;
            });

            $messages = array(
                'sub_cat' => 'The :attribute field is wrong.',
            );

            return Validator::make($params, self::$rules[$rule], $messages);
        }

        return Validator::make($params, self::$rules[$rule]);
    }

}