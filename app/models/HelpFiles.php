<?php
namespace App\Models;

class HelpFiles extends \Eloquent
{

    const TYPE_DOC = 'doc';
    const TYPE_XLS = 'xls';
    const TYPE_PDF = 'pdf';

    protected $table = 'help_files';

    protected $types = [
        self::TYPE_DOC => ['doc', 'docx'],
        self::TYPE_XLS => ['xls', 'xlsx', 'xlsm'],
        self::TYPE_PDF => ['pdf']
    ];

    /**
     * Получает правило для валидации типа файла
     *
     * @return string
     */
    public function getRule()
    {
        if ($this->type === self::TYPE_DOC) {
            return BaseValidator::TYPE_DOC;
        }
        if ($this->type === self::TYPE_XLS) {
            return BaseValidator::TYPE_XLS;
        }
        if ($this->type === self::TYPE_PDF) {
            return BaseValidator::TYPE_PDF;
        }
        return '';
    }

    /**
     * get list files
     *
     * @return mixed
     */
    public static function getList()
    {
        return \DB::table('help_files')->orderBy('name')->get();
    }

    /**
     * Update filename and format
     * @param $filename
     * @return mixed
     */
    public function updateFileName($filename, $originName)
    {
        $result = explode('.', $originName);
        $name   = array_shift($result);
        $format = null;
        if (count($result) > 0) {
            $format = array_shift($result);
        }
        $this->filename = $filename;
        $this->format   = $format;
        return $this->save();
    }

    /**
     * get content-type for Header
     * @return string
     */
    public function getContentType()
    {
        if ($this->type === self::TYPE_DOC) {
            return 'Content-Type: application/vnd.ms-word';
        }
        if ($this->type === self::TYPE_XLS) {
            return 'Content-Type: application/vnd.ms-excel';
        }
        if ($this->type === self::TYPE_PDF) {
            return 'Content-Type: application/pdf';
        }
        return '';
    }

}