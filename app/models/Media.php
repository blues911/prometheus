<?php
namespace App\Models;

use Validator;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at Fahrenheit 451
 *
 * Date: 20.11.14
 * Time: 17:37
 */
class Media extends \Eloquent
{
    use SoftDeletingTrait;

    const DEFAULT_ORDER      = 'name';
    const ORDER_NAME_ASC     = 'name.asc';
    const ORDER_NAME_DESC    = 'name.desc';
    const ORDER_REGION_ASC   = 'region.asc';
    const ORDER_REGION_DESC  = 'region.desc';
    const ORDER_COUNTRY_ASC  = 'country.asc';
    const ORDER_COUNTRY_DESC = 'country.desc';
    const MEDIA_PAGE_SIZE    = 30;

    protected $dates = ['deleted_at'];
    protected $table = 'media';

    protected $fillable = [
        'name',
        'list',
        'type',
        'category',
        'reach',
        'region_id',
        'country_id'
    ];

    public static $orderList = [
        self::ORDER_NAME_ASC     => 'Name <i class="arrow down icon"></i>',
        self::ORDER_NAME_DESC    => 'Name <i class="arrow up icon"></i>',
        self::ORDER_REGION_ASC   => 'Region <i class="arrow down icon"></i>',
        self::ORDER_REGION_DESC  => 'Region <i class="arrow up icon"></i>',
        self::ORDER_COUNTRY_ASC  => 'Country <i class="arrow down icon"></i>',
        self::ORDER_COUNTRY_DESC => 'Country <i class="arrow up icon"></i>',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo('App\Models\Region')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function subcategories()
    {
        return $this->belongsToMany('App\Models\Subcategory', 'media_subcategory', 'media_id', 'subcategory_id');
    }

    /**
     * @param $data
     * @return bool
     */
    public static function validate($data)
    {
        $v = BaseValidator::make($data, BaseValidator::MEDIA);
        return $v->errors();
    }

    /**
     * @param $filter
     *
     * @return mixed
     */
    public static function getFiltered($filter)
    {
        $statement = Media::select('media.*');
        if (!empty($filter['order_by'])) {
            $statement = Media::applyOrderBy($statement, $filter['order_by']);
        } else {
            $statement->orderBy(Media::DEFAULT_ORDER);
        }
        if (!empty($filter['region_id'])) {
            $statement->where('media.region_id', '=', $filter['region_id']);
        }
        if (!empty($filter['country_id'])) {
            $statement->where('media.country_id', '=', $filter['country_id']);
        }
        if (!empty($filter['name'])) {
            $statement->where('media.name', 'LIKE', "%".$filter['name']."%");
        }
        return $statement;
    }

    /**
     * @param $statement
     * @param $orderBy
     *
     * @return mixed
     */
    protected static function applyOrderBy($statement, $orderBy)
    {
        switch ($orderBy) {
            case Media::ORDER_REGION_ASC:
                $statement
                    ->join('regions', 'regions.id', '=', 'media.region_id')
                    ->orderBy('regions.name');
                break;

            case Media::ORDER_REGION_DESC:
                $statement
                    ->join('regions', 'regions.id', '=', 'media.region_id')
                    ->orderBy('regions.name', 'desc');
                break;

            case Media::ORDER_COUNTRY_ASC:
                $statement
                    ->join('countries', 'countries.id', '=', 'media.country_id')
                    ->orderBy('countries.name');
                break;

            case Media::ORDER_COUNTRY_DESC:
                $statement
                    ->join('countries', 'countries.id', '=', 'media.country_id')
                    ->orderBy('countries.name', 'desc');
                break;

            case Media::ORDER_NAME_DESC:
                $statement->orderBy('name', 'desc');
                break;

            default:
                $statement->orderBy(Media::DEFAULT_ORDER);
                break;
        }
        return $statement;
    }

    /**
     * Get record data to json
     *
     * @param object $item
     *
     * @return string
     */
    public static function getMediaForItem($item)
    {
        return json_encode([
            'name'          => $item->name,
            'list'          => $item->list,
            'type'          => $item->type,
            'category'      => $item->category,
            'subcategories' => $item->subcategories,
            'reach'         => $item->reach,
        ], JSON_FORCE_OBJECT | ENT_NOQUOTES);
    }

    public function subcategoriesIds()
    {   
        $row = [];
        foreach ($this->subcategories as $subcategory) {
            $row[] = $subcategory->id;
        }
        $result = (!empty($row)) ? implode(',',$row) : '';
        return $result;
    }
}