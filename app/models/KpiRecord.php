<?php
namespace App\Models;

class KpiRecord extends \Eloquent
{
    const STATUS_ACTIVE = 'active';
    const STATUS_HIDDEN = 'hidden';

	protected $table    = 'kpi_records';
    protected $fillable = [
        'region_id',
    	'is_company_mindshare', 
    	'is_b2b_mindshare', 
    	'is_b2c_mindshare', 
    	'company_mindshare', 
    	'b2b_mindshare', 
    	'b2c_mindshare', 
    	'status',
        'date'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    /**
     * Generates name for record by date.
     * @param  string $date
     * @return string
     */
    public static function getRecordNameByDate($date)
    {
        if (date('n', strtotime($date)) == 1) {
            return date('M Y', strtotime($date));
        }

        return date('M', strtotime('1')) . ' - ' . date('M Y', strtotime($date));
    }

}