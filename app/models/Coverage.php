<?php

namespace App\Models;

use Auth;
use DB;
use Illuminate\Support\Facades\Schema;
use Validator;
use PHPExcel_Shared_Date;

/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at
 *
 * Date: 26.10.14
 * Time: 17:58
 * @property mixed id
 * @property mixed region
 * @property mixed date
 * @property mixed name
 * @property mixed reach
 * @property mixed author
 * @property mixed headline
 * @property mixed media_list
 * @property mixed media_type
 * @property mixed media_category
 * @property mixed visibility
 * @property mixed image
 * @property mixed key_message_penetration
 * @property mixed mention
 * @property mixed third_speakers_quoted
 * @property mixed negative_explanation
 * @property mixed news_break
 * @property mixed event_name
 * @property mixed impact_index
 * @property mixed desired_article
 * @property mixed net_effect
 * @property mixed campaign
 * @property mixed url
 * @property mixed speaker_quote
 * @property mixed third_speakers_quotes
 * @property mixed speakers_quoted
 * @property mixed header_tonality
 * @property mixed main_tonality
 * @property mixed owner
 * @property mixed country
 * @property mixed region_id
 * @property mixed country_id
 * @property mixed owner_id
 * @property mixed file_id
 * @property mixed comments
 */
class Coverage extends \Eloquent
{

    const STATUS_DRAFT = 'draft';
    const STATUS_COMPLETE = 'complete';
    const STATUS_APPROVED = 'approved';

    const MEDIA_LIST_GOLD = '0.6';
    const MEDIA_LIST_PLATINUM = '0.8';
    const MEDIA_LIST_DIAMOND = '1';

    const IMAGE_PRESENT = '1';
    const IMAGE_ABSENT = '0.8';

    const KEY_MESSAGE_PENETRATION_PRESENT = 'present';
    const KEY_MESSAGE_PENETRATION_EXTENT = 'extent';
    const KEY_MESSAGE_PENETRATION_ABSENT = 'absent';
    const KEY_MESSAGE_PENETRATION_COUNTER = 'counter';

    const MENTION_HEADLINE = '1';
    const MENTION_LEAD = '0.9';
    const MENTION_BODY = '0.8';
    const MENTION_CONTEXT = '0.5';

    const HEADER_TONALITY_POSITIVE = '1';
    const HEADER_TONALITY_NEUTRAL = '0.85';
    const HEADER_TONALITY_NEGATIVE = '0.5';

    const MAIN_TONALITY_POSITIVE = 'positive';
    const MAIN_TONALITY_NEUTRAL = 'neutral';
    const MAIN_TONALITY_NEGATIVE = 'negative';

    const SPEAKER_QUOTE_ABSENT = '0.8';
    const SPEAKER_QUOTE_PRESENT = '1';

    const THIRD_SPEAKERS_QUOTES_ABSENT = '0';
    const THIRD_SPEAKERS_QUOTES_PRESENT = '1';

    const DESIRED_ARTICLE_NO = '0';
    const DESIRED_ARTICLE_YES = '1';

    const MEDIA_TYPE_AGENCY = 'agency';
    const MEDIA_TYPE_ONLINE = 'online';
    const MEDIA_TYPE_TV = 'tv';
    const MEDIA_TYPE_PRINT = 'print';
    const MEDIA_TYPE_RADIO = 'radio';

    const MEDIA_CATEGORY_VERTICAL = 'vertical';
    const MEDIA_CATEGORY_BUSINESS = 'business';
    const MEDIA_CATEGORY_IT = 'it';
    const MEDIA_CATEGORY_GENERAL = 'general';

    const ACCESS_EDIT_COVERAGE = 'access_edit_coverage';
    const ACCESS_VIEW_COVERAGE = 'access_view_coverage';
    const ACCESS_DELETE_COVERAGE = 'access_delete_coverage';
    const ACCESS_COMPLETE_COVERAGE = 'access_complete_coverage';
    const ACCESS_PUBLISH_COVERAGE = 'access_publish_coverage';
    const ACCESS_RETURN_TO_DRAFT = 'access_return_to_draft';

    const MODE_FULL = 'full';
    const MODE_COMPACT = 'compact';

    protected $table = 'coverage';

    protected $fillable = [
        'owner_id',
        'region_id',
        'country_id',
        'name',
        'reach',
        'headline',
        'author',
        'url',
        'media_list',
        'media_type',
        'media_category',
        'visibility',
        'image',
        'key_message_penetration',
        'mention',
        'speaker_quote',
        'third_speakers_quotes',
        'obsolete_speakers_quoted',
        'third_speakers_quoted',
        'header_tonality',
        'main_tonality',
        'negative_explanation',
        'event_name',
        'obsolete_news_break',
        'desired_article',
        'impact_index',
        'net_effect',
        'status',
        'date',
        'file_id',
        'comments'
    ];

    protected $errors = [];

    /**
     * Validation
     *
     * @param $data
     *
     * @return bool
     */
    public function validate($data)
    {
        $rules = BaseValidator::$rules[BaseValidator::COVERAGE];
        if ($this->owner->role === User::ROLE_USER) {
            $rules['region_id'] .= "|in:{$this->owner->region_id}";
        }
        $v = BaseValidator::make($data, $rules);
        if ($v->fails() || $this->region_id != $this->country->region_id) {
            $this->errors = $v->errors();
            return false;
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function errors()
    {
        return $this->errors;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\SystemUser', 'user_coverage', 'coverage_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'products_coverage', 'coverage_id', 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function campaigns()
    {
        return $this->belongsToMany('App\Models\Campaign', 'campaigns_coverage', 'coverage_id', 'campaign_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function business()
    {
        return $this->belongsToMany('App\Models\Business', 'business_coverage', 'coverage_id', 'business_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function newsbreaks()
    {
        return $this->belongsToMany('App\Models\NewsBreak', 'newsbreaks_coverage', 'coverage_id', 'newsbreak_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function speakers()
    {
        return $this->belongsToMany('App\Models\Speaker', 'speakers_coverage', 'coverage_id', 'speaker_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file()
    {
        return $this->belongsTo('App\Models\ImportFile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('App\Models\SystemUser', 'owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function subcategories()
    {
        return $this->belongsToMany('App\Models\Subcategory', 'coverage_subcategory', 'coverage_id', 'subcategory_id');
    }

    public static function getMediaCategories()
    {
        return [
            self::MEDIA_CATEGORY_GENERAL => 'General Media',
            self::MEDIA_CATEGORY_IT => 'IT Media',
            self::MEDIA_CATEGORY_BUSINESS => 'Business Media',
            self::MEDIA_CATEGORY_VERTICAL => 'Vertical',
        ];
    }

    public static function getMediaList()
    {
        return [
            self::MEDIA_LIST_GOLD => 'gold',
            self::MEDIA_LIST_PLATINUM => 'platinum',
            self::MEDIA_LIST_DIAMOND => 'diamond'
        ];
    }

    public static function getMainTonalityes()
    {
        return [
            self::MAIN_TONALITY_NEGATIVE => 'negative',
            self::MAIN_TONALITY_NEUTRAL => 'neutral',
            self::MAIN_TONALITY_POSITIVE => 'positive',
        ];
    }

    public static function getFiltersFields()
    {
        return [
            "event",
            "newsbreak_id",
            "product_id",
            "campaign_id",
            "speaker_id",
            "media_type",
            "media_list",
            "media_category",
            "speakers_quoted",
            "mention_article",
        ];
    }

    public static function getKeyMessagePenetrationTitle($name)
    {
        $list = [
            'absent' => 'Absent',
            'counter' => 'Counter-message',
            'present' => 'Clearly present',
            'extent' => 'Present to some extent',
        ];
        return $list[$name];
    }


    protected static function applyDateFilter($statement, $filter)
    {


        if ($filter) {
            $columns = Schema::getColumnListing('coverage');
            foreach ($filter as $field => $value) {
                if ($field == "from_date") {
                    $statement->where('c.date', '>=', $value);
                } else if ($field == "to_date") {
                    $statement->where('c.date', '<', $value);

                }
            }
        }
        return $statement;
    }

    /**
     * @param        $statement
     * @param        $filter
     * @param        $filtersFields
     * @param string $search
     *
     * @return mixed
     */
    protected static function applyFilter($statement, $filter, $filtersFields, $search = '', $user = [])
    {
        if (empty($user)) {
            $user = Auth::user();
        }
        if (!in_array($user['role'], [User::ROLE_ADMINISTRATOR, User::ROLE_MANAGER])) {
            $statement->where('c.owner_id', '=', $user['id']);
        } else {
            $statement->whereRaw("(c.status IN (?, ?) OR (c.owner_id = ? AND c.status = ?))",
                [self::STATUS_COMPLETE, self::STATUS_APPROVED, $user['id'], self::STATUS_DRAFT]
            );
        }

        if ($filter) {
            $columns = Schema::getColumnListing('coverage');
            foreach ($filter as $field => $value) {
                if ($field == "from_date") {
                    $statement->where('c.date', '>=', $value);
                } else if ($field == "to_date") {
                    $statement->where('c.date', '<', $value);
                } else if ('message_penetration' === $field) {
                    $statement->whereIn('c.key_message_penetration', 'with' === $value ? ['present', 'extent'] : ['absent', 'counter']);
                } else if (!in_array($field, $filtersFields) && in_array($field, $columns)) {
                    $statement->where('c.' . $field, '=', $value);
                }
            }
        }

        if (!empty($filter['media_type'])) {
            $statement->where('c.media_type', '=', $filter['media_type']);
        } else if (isset($filter['media_type'])) {
            $statement->whereNull('c.media_type');
        }
        if (!empty($filter['media_category'])) {
            $statement->where('c.media_category', '=', $filter['media_category']);
        } else if (isset($filter['media_category'])) {
            $statement->whereNull('c.media_category');
        }
        if (!empty($filter['media_subcategory'])) {
            $statement->where('cs.subcategory_id', '=', $filter['media_subcategory']);
        }

        if (isset($filter['speakers_quoted']) && $filter['speakers_quoted'] == 0) {
            $statement->leftJoin('speakers_coverage', 'c.id', '=', 'speakers_coverage.coverage_id');
            $statement->whereNull('speakers_coverage.speaker_id');
        } else if (isset($filter['speakers_quoted']) && $filter['speakers_quoted'] == 1) {
            $statement->join('speakers_coverage', 'c.id', '=', 'speakers_coverage.coverage_id');
        }

        if (!empty($filter['event'])) {
            $statement->where('c.event_name', 'LIKE', $filter['event'] . "%");
        }

        if (!empty($filter['media_list'])) {
            $mediaList = is_numeric($filter['media_list']) ? $filter['media_list'] : array_search($filter['media_list'], self::getMediaList());
            $statement->where('media_list', '=', $mediaList);
        }

        if (!empty($filter['newsbreak_id'])) {
            $statement->join('newsbreaks_coverage', 'c.id', '=', 'newsbreaks_coverage.coverage_id');
            $statement->where('newsbreaks_coverage.newsbreak_id', '=', $filter['newsbreak_id']);
        }

        if ((!empty($filter['product_id']) && !isset($filter['mention_article'])) || !empty($filter['mention_article']) ||
            (!empty($filter['security_intelligence_penetration']) && $filter['security_intelligence_penetration'] === 'with')) {
            $statement->join('products_coverage', 'c.id', '=', 'products_coverage.coverage_id');
        }

        if (isset($filter['mention_article']) && $filter['mention_article'] == 0) {
            $statement->whereRaw('c.id not in (select coverage_id from products_coverage)');
        }

        if (!empty($filter['product_id']) && !isset($filter['mention_article'])) {
            $statement->where('products_coverage.product_id', '=', $filter['product_id']);
        }

        if (!empty($filter['security_intelligence_penetration'])) {
            if ($filter['security_intelligence_penetration'] === "with") {
                $statement->join('products', 'products.id', '=', 'products_coverage.product_id');
                $statement->where('products.is_security_intelligence', '=', 1);
            } else {
                $countProduct = self::countProduct();
                $countSecurityIntelligence = self::countSecurityIntelligence();

                $statement->whereRaw("({$countProduct->toSql()}) > 0")
                    ->whereRaw("0 = ({$countSecurityIntelligence->toSql()})")
                    ->mergeBindings($countSecurityIntelligence)
                    ->mergeBindings($countProduct);
            }
        }

        if (!empty($filter['campaign_id'])) {
            $statement->join('campaigns_coverage', 'c.id', '=', 'campaigns_coverage.coverage_id');
            $statement->where('campaigns_coverage.campaign_id', '=', $filter['campaign_id']);
        }

        if (!empty($filter['business'])) {
            $statement->join('business_coverage', 'c.id', '=', 'business_coverage.coverage_id');
            $statement->where('business_coverage.business_id', '=', $filter['business']);
        }

        if (!empty($filter['speaker_id'])) {
            $statement->join('speakers_coverage', 'c.id', '=', 'speakers_coverage.coverage_id');
            $statement->where('speakers_coverage.speaker_id', '=', $filter['speaker_id']);
        }

        if (!empty($filter['main_tonality'])) {
            $statement->where('main_tonality', '=', $filter['main_tonality']);
        }

        if (!empty($filter['campaigns']) || !empty($filter['events']) || !empty($filter['newsbreaks']) || !empty($filter['products']) || !empty($filter['speakers'])) {
            if (!empty($filter['campaigns'])) {
                $statement->leftJoin('campaigns_coverage', 'c.id', '=', 'campaigns_coverage.coverage_id');
            }
            if (!empty($filter['newsbreaks'])) {
                $statement->leftJoin('newsbreaks_coverage', 'c.id', '=', 'newsbreaks_coverage.coverage_id');
            }
            if (!empty($filter['products'])) {
                $statement->leftJoin('products_coverage', 'c.id', '=', 'products_coverage.coverage_id');
            }
            if (!empty($filter['speakers'])) {
                $statement->leftJoin('speakers_coverage', 'c.id', '=', 'speakers_coverage.coverage_id');
            }

            $statement->where(function ($query) use ($filter) {
                if (!empty($filter['campaigns'])) {
                    $query = $query->whereIn("campaigns_coverage.campaign_id", explode(',', $filter['campaigns']), 'or');
                }
                if (!empty($filter['newsbreaks'])) {
                    $query = $query->whereIn("newsbreaks_coverage.newsbreak_id", explode(',', $filter['newsbreaks']), 'or');
                }
                if (!empty($filter['products'])) {
                    $query = $query->whereIn("products_coverage.product_id", explode(',', $filter['products']), 'or');
                }
                if (!empty($filter['speakers'])) {
                    $query = $query->whereIn("speakers_coverage.speaker_id", explode(',', $filter['speakers']), 'or');
                }
                if (!empty($filter['events'])) {
                    $query = $query->whereIn("c.event_name", explode(',', $filter['events']), 'or');
                }
            });
        }

        if (!empty($search)) {
            $statement->where(function ($query) use ($search) {
                $query->orWhere('c.headline', 'LIKE', '%' . $search . '%')
                    ->orWhere('c.author', 'LIKE', '%' . $search . '%')
                    ->orWhere('c.name', 'LIKE', '%' . $search . '%')
                    ->orWhere('c.event_name', 'LIKE', '%' . $search . '%');
            });
        }

        $statement = $statement
            ->join('system_users as su', 'su.id', '=', 'c.owner_id')
            ->join('countries as cn', 'cn.id', '=', 'c.country_id')
            ->join('regions as rg', 'rg.id', '=', 'c.region_id')
            ->leftJoin('import_files as imf', 'imf.id', '=', 'c.file_id');
        //    ->leftJoin('coverage_subcategory as cs', 'cs.coverage_id', '=', 'c.id');

        return $statement;
    }

    /**
     * @param int $skip
     * @param int $limit
     * @param array $filter
     * @param array $sort
     * @param string $search
     * @return array
     */
    public static function getList($skip = 0, $limit = 15, $filter = [], $sort = [], $search = '', $isDataForView = false, $onlyBaseData = false, $user = [])
    {

        ini_set('memory_limit', '5500M');

        $columns = [
            'c.id',
            'c.owner_id',
            'su.name as owner',
            'rg.name as region',
            'cn.name as country',
            'c.date',
            'c.name',
            'c.reach',
            'c.headline',
            'c.author',
            'c.url',
            'c.media_list',
            'c.media_type',
            'c.media_category',
            'c.visibility',
            'c.image',
            'c.key_message_penetration',
            'c.mention',
            'c.speaker_quote',
            'c.third_speakers_quotes',
            'c.third_speakers_quoted',
            'c.header_tonality',
            'c.main_tonality',
            'c.negative_explanation',
            'c.event_name',
            'c.desired_article',
            'c.impact_index',
            'c.net_effect',
            'c.status',
            'c.comments',
            'imf.name AS file',
            'imf.id AS file_id',
            'imf.created_at AS file_created_at'
        ];

        $statement = \DB::table('coverage as c')->select($columns);
        $statement = self::applyFilter($statement, $filter, Coverage::getFiltersFields(), $search, $user);

        if ($skip > 0) {
            $statement = $statement->skip($skip);
        }

        if ($limit > 0) {
            $statement = $statement->limit($limit);
        }

        if ($sort) {
            $statement = $statement->orderBy($sort['column'], $sort['direction']);
        }
        $statement->groupBy("c.id");

        $all = $statement->get();

        //////////////// Все выбрали баста! //////////////////////////////
        ///
        if ($onlyBaseData) {
            return $all;
        }
        $coverages = json_decode(json_encode($all), true);
        unset($all);
        unset($statement);





        $coverage_speakers_st = \DB::table('speakers_coverage as sc')
            ->select(['sc.coverage_id', 'speakers.name as name  '])
            ->join('speakers', 'sc.speaker_id', '=', 'speakers.id')
            ->join('coverage as c','c.id','=','sc.coverage_id');
        $coverage_speakers_st=self::applyDateFilter($coverage_speakers_st,$filter)->get();


        $coverage_speakers = [];
        foreach ($coverage_speakers_st as $item) {
            $coverage_speakers[$item->coverage_id][] = $item->name;
        }
        unset($coverage_speakers_st);

        unset($item);
        $coverage_newsbreaks_st = \DB::table('newsbreaks_coverage as cn')
            ->select(['cn.coverage_id', 'newsbreaks.name as name  ', 'newsbreaks.type as type'])
            ->join('newsbreaks', 'cn.newsbreak_id', '=', 'newsbreaks.id')
            ->join('coverage as c','c.id','=','cn.coverage_id');
        $coverage_newsbreaks_st=self::applyDateFilter($coverage_newsbreaks_st,$filter)->get();
        $coverage_newsbreaks = [];

        foreach ($coverage_newsbreaks_st as $item) {
            $coverage_newsbreaks[$item->coverage_id][] = ['name' => $item->name, 'type' => $item->type];
        }
        unset($coverage_newsbreaks_st);

        unset($item);
        $coverage_campaigns_st = \DB::table('campaigns_coverage as cp')
            ->select(['cp.coverage_id', 'campaigns.name as name  '])
            ->join('campaigns', 'cp.campaign_id', '=', 'campaigns.id')
            ->join('coverage as c','c.id','=','cp.coverage_id');
        $coverage_campaigns_st=self::applyDateFilter($coverage_campaigns_st,$filter)->get();
        $coverage_campaigns = [];

        foreach ($coverage_campaigns_st as $item) {
            $coverage_campaigns[$item->coverage_id][] = $item->name;
        }
        unset($coverage_campaigns_st);


        unset($item);
        $coverage_business_st = \DB::table('business_coverage as bc')
            ->select(['bc.coverage_id', 'business.name as name  '])
            ->join('business', 'bc.business_id', '=', 'business.id')
            ->join('coverage as c','c.id','=','bc.coverage_id');
        $coverage_business_st=self::applyDateFilter($coverage_business_st,$filter)->get();
        $coverage_business = [];

        foreach ($coverage_business_st as $item) {
            $coverage_business[$item->coverage_id][] = $item->name;
        }
        unset($coverage_business_st);


        unset($item);
        $coverage_subcategory_st = \DB::table('coverage_subcategory as cs')
            ->select(['cs.coverage_id', 'subcategory.title as name  '])
            ->join('subcategory', 'cs.subcategory_id', '=', 'subcategory.id')
            ->join('coverage as c','c.id','=','cs.coverage_id');
        $coverage_subcategory_st=self::applyDateFilter($coverage_subcategory_st,$filter)->get();
        $coverage_subcategory = [];

        foreach ($coverage_subcategory_st as $item) {
            $coverage_subcategory[$item->coverage_id][] = $item->name;
        }
        unset($coverage_subcategory_st);

        unset($item);
        $products_coverage_st = \DB::table('products_coverage as pc')
            ->select(['pc.coverage_id', 'products.name as name  '])
            ->join('products', 'pc.product_id', '=', 'products.id')
            ->join('coverage as c','c.id','=','pc.coverage_id');
        $products_coverage_st=self::applyDateFilter($products_coverage_st,$filter)->get();
        $products_coverage = [];

        foreach ($products_coverage_st as $item) {
            $products_coverage[$item->coverage_id][] = $item->name;
        }
        unset($products_coverage_st);
        unset($item);

        $lists = [];
        foreach ($coverages as $coverage) {
            $list = $coverage;
            $k = $coverage['id'];
            $newsbreaksLocalList = [];
            $newsbreaksGlobalList = [];

            if (isset($coverage_newsbreaks[$k])) {
                foreach ($coverage_newsbreaks[$k] as $newsbreak) {
                    if ($newsbreak['type'] == NewsBreak::TYPE_GLOBAL) {
                        $newsbreaksGlobalList[] = $newsbreak['name'];
                    } else {
                        $newsbreaksLocalList[] = $newsbreak['name'];
                    }
                }
            }


            $list['campaign'] = isset($coverage_campaigns[$k]) ? implode(', ', $coverage_campaigns[$k]) : "";
            $list['business'] = isset($coverage_business[$k]) ? implode(', ', $coverage_business[$k]) : "";
            $list['news_break_global'] = implode(', ', $newsbreaksGlobalList);
            $list['news_break_local'] = implode(', ', $newsbreaksLocalList);
            $list['speakers_quoted'] = isset($coverage_speakers[$k]) ? implode(', ', $coverage_speakers[$k]) : "";
            $list['products'] = isset($products_coverage[$k]) ? implode(', ', $products_coverage[$k]) : "";
            $list['subcategories'] = (isset($coverage_subcategory[$k])) ? implode(', ', $coverage_subcategory[$k]) : "";

            $list['date'] = $coverage['date'] ? $coverage['date'] : null;
            if ($isDataForView && isset($coverage[$k]['date']) && !is_null($coverage[$k]['date'])) {
                $list['date'] = date('d-m-Y', strtotime($coverages[$k]['date']));
            }
            $lists[$k] = $list;
        }

        return array_values($lists);
    }

    /**
     * @param int $skip
     * @param int $limit
     * @param array $filter
     * @param array $sort
     * @param string $search
     * @return array
     */
    public static function getListForView($skip = 0, $limit = 15, $filter = [], $sort = [], $search = '')
    {
        $list = Coverage::getList($skip, $limit, $filter, $sort, $search, true);
        $presentMaps = self::getListForViewMap();

        foreach ($list as &$item) {
            $item['DT_RowId'] = 'item-' . $item['id'];
            $item['DT_RowClass'] = $item['status'];
            $item['media_type'] = ucfirst($item['media_type']);
            $item['media_category'] = $item['media_category'] == 'it' ? "IT" : ucfirst($item['media_category']);
            $item['media_subcategories'] = $item['subcategories'];
            $item['media_list'] = !empty($presentMaps['media_list'][$item['media_list']]) ? $presentMaps['media_list'][$item['media_list']] : null;
            if ($item['image'] && !empty($presentMaps['image'][$item['image']])) {
                $item['image'] = $presentMaps['image'][$item['image']];
            }

            if ($item['key_message_penetration'] && !empty($presentMaps['key_message_penetration'][$item['key_message_penetration']])) {
                $item['key_message_penetration'] = $presentMaps['key_message_penetration'][$item['key_message_penetration']];
            } else {
                $item['key_message_penetration'] = '';
            }

            if ($item['mention'] && !empty($presentMaps['mention'][$item['mention']])) {
                $item['mention'] = $presentMaps['mention'][$item['mention']];
            }

            if ($item['header_tonality'] && !empty($presentMaps['header_tonality'][$item['header_tonality']])) {
                $item['header_tonality'] = $presentMaps['header_tonality'][$item['header_tonality']];
            }

            if ($item['main_tonality'] && !empty($presentMaps['main_tonality'][$item['main_tonality']])) {
                $item['main_tonality'] = $presentMaps['main_tonality'][$item['main_tonality']];
            } else {
                $item['main_tonality'] = '';
            }

            if (!empty($item['negative_explanation'])) {
                $item['negative_explanation'] = htmlspecialchars($item['negative_explanation']);
            }

            $item['impact_index'] = round((float)$item['impact_index'], 3);
            if ($item['net_effect'] > 0) {
                $item['net_effect'] = $item['net_effect'] >= 1000000 ? round($item['net_effect'] / 1000000) . 'M' : '< 1M';
            }
            if ($item['reach'] > 0) {
                $item['reach'] = $item['reach'] >= 1000000 ? round($item['reach'] / 1000000) . 'M' : '< 1M';
            }
            $file = $item['file'];

            $item['file'] = [
                'file' => $file,
                'file_id' => $item['file_id'],
                'file_created_at' => $item['file_created_at']
            ];

            $item['preference'] = [
                'id' => $item['id'],
                'access_publish' => self::access(self::ACCESS_PUBLISH_COVERAGE, $item),
                'access_complete' => self::access(self::ACCESS_COMPLETE_COVERAGE, $item),
                'access_edit' => self::access(self::ACCESS_EDIT_COVERAGE, $item),
                'access_delete' => self::access(self::ACCESS_DELETE_COVERAGE, $item),
                'access_return_to_draft' => self::access(self::ACCESS_RETURN_TO_DRAFT, $item),
            ];
        }

        return $list;
    }

    /**
     * @param array $filter
     *
     * @return string
     */
    protected static function getExportColumnMap()
    {
        return [
            "id" => "№",
            "region" => "Region",
            "country" => "Country",
            "date" => "Date of Publication dd.mm.yyyy",
            "name" => "Publication Name",
            "reach" => "Reach (Circulation for print media / website visitors per week / broadcast reach)", //+
            "author" => "Author",
            "headline" => "Headline",
            "media_list" => "Media List (Diamond, Platinum, Gold)",
            "media_type" => "Media Type (Information Agency, Online, Print, TV, Radio)",
            "media_category" => "Media Category (Vertical, Business, IT, General interest)",
            "visibility" => "Visibility",
            "image" => "Image (Present, Absent)",
            "key_message_penetration" => "Key Message Penetration (Clearly Present, Present to some extent, Absent, Counter-message)",
            "mention" => "Brand or Product Mention (Headline, Lead, Body, Context)",
            "products" => "Products Mentioned *standardized names",
            "speaker_quote" => "Speaker’s Quote(Present/ Absent)",
            "third_speakers_quotes" => "Third Party Speakers’ Supporting Quotes (Present/ Absent)",
            "speakers_quoted" => "Speakers Quoted",
            "third_speakers_quoted" => "Third Party Speakers Quoted",
            "header_tonality" => "Header Tonality (Positive, Neutral, Negative)",
            "main_tonality" => "Main Tonality (Positive, Neutral, Negative)",
            "negative_explanation" => "Short Explanation about the Reason of Negative",
            "campaign" => "Campaign",
            "event_name" => "Event Name (Please mark if the coverage resulted from KL press events)",
            "news_break_global" => "Newsbreak",
            "impact_index" => "Impact Index",
            "net_effect" => "Net Effect",
            "desired_article" => "Desired article (yes/no)",
            "url" => "URL (if available)",
            "comments" => "Comments",
            "business" => "Business Segment",
            "subcategories" => "Media Subcategory (Lifestyle, SMB media, Enterprise, Financial, Virtualization, Automotive, Industrial, Banking and Insurance, Travel, Parental media, Gaming, Pet media , Motorsport, Healthcare, Government, MSP media, Education)",
            "owner_id" => "Owner code"
        ];
    }

    /**
     * @return array
     */
    public static function getListForViewMap()
    {
        return [
            'visibility' => [
                '0.4' => '0.4',
                '0.75' => '0.75',
                '1' => '1',
            ],
            'media_list' => [
                self::MEDIA_LIST_GOLD => 'Gold',
                self::MEDIA_LIST_PLATINUM => 'Platinum',
                self::MEDIA_LIST_DIAMOND => 'Diamond'
            ],
            'image' => [
                self::IMAGE_PRESENT => 'Present',
                self::IMAGE_ABSENT => 'Absent'
            ],
            'key_message_penetration' => [
                self::KEY_MESSAGE_PENETRATION_PRESENT => 'Clearly present',
                self::KEY_MESSAGE_PENETRATION_EXTENT => 'Present to some extent',
                self::KEY_MESSAGE_PENETRATION_ABSENT => 'Absent',
                self::KEY_MESSAGE_PENETRATION_COUNTER => 'Counter-message'
            ],
            'mention' => [
                self::MENTION_HEADLINE => 'Headline',
                self::MENTION_LEAD => 'Lead',
                self::MENTION_BODY => 'Body',
                self::MENTION_CONTEXT => 'Context'
            ],
            'header_tonality' => [
                self::HEADER_TONALITY_POSITIVE => 'Positive',
                self::HEADER_TONALITY_NEUTRAL => 'Neutral',
                self::HEADER_TONALITY_NEGATIVE => 'Negative'
            ],
            'main_tonality' => [
                self::MAIN_TONALITY_POSITIVE => 'Positive',
                self::MAIN_TONALITY_NEUTRAL => 'Neutral',
                self::MAIN_TONALITY_NEGATIVE => 'Negative'
            ],
            'speaker_quote' => [
                self::SPEAKER_QUOTE_ABSENT => 'Absent',
                self::SPEAKER_QUOTE_PRESENT => 'Present'
            ],
            'third_speakers_quotes' => [
                self::THIRD_SPEAKERS_QUOTES_ABSENT => 'Absent',
                self::THIRD_SPEAKERS_QUOTES_PRESENT => 'Present'
            ],
            'desired_article' => [
                self::DESIRED_ARTICLE_NO => 'No',
                self::DESIRED_ARTICLE_YES => 'Yes'
            ],
            'media_type' => [
                self::MEDIA_TYPE_AGENCY => 'Information Agency',
                self::MEDIA_TYPE_ONLINE => 'Online',
                self::MEDIA_TYPE_TV => 'TV',
                self::MEDIA_TYPE_PRINT => 'Print',
                self::MEDIA_TYPE_RADIO => 'Radio'
            ],
            'media_category' => [
                self::MEDIA_CATEGORY_VERTICAL => 'Vertical',
                self::MEDIA_CATEGORY_BUSINESS => 'Business',
                self::MEDIA_CATEGORY_IT => 'IT',
                self::MEDIA_CATEGORY_GENERAL => 'General interest',
            ],
        ];
    }

    /**
     * @param array $field
     * @return array
     */
    public static function getFieldValues($field)
    {
        $map = self::getListForViewMap();
        return !empty($map[$field]) ? $map[$field] : [];
    }

    /**
     * @param array $filter
     *
     * @return string
     */
    public static function getCSV($filter = [])
    {
        $list = Coverage::getList(0, 0, $filter);
        if (empty($list)) {
            return '';
        }
        $columnMap = self::getExportColumnMap();
        $columns = array_keys($list[0]);

        foreach ($columns as &$c) {
            $c = $columnMap[$c];
        }

        $csv = [implode(';', $columns)];
        foreach ($list as $item) {
            $csv[] = implode(";", $item);
        }

        return implode("\n", $csv);
    }

    /**
     * @param array $filter
     * @param array $user
     * @param string $search
     *
     * @return string
     */
    public static function prepareDataForExport($filter = [], $user = [], $search = '')
    {
        $result = [];
        $arH = $dataH = $arDuplicate = [];
        $list = Coverage::getList(0, 0, $filter, [], $search, false, false, $user);

        if (empty($list)) {
            return $result;
        }
        $columnMap = self::getExportColumnMap();

        $presentMaps = self::getListForViewMap();
        $columns = array_values($columnMap);

        $result['item'][] = $columns;

        foreach ($list as $index => $item) {


            /**/
            $item['id'] = $index + 1;
            $item['owner_id'] = (int)$item['owner_id'];
            $item['date'] =  date('d.m.Y', strtotime($item['date']) );
            $item['media_list'] = !empty($presentMaps['media_list'][$item['media_list']]) ? $presentMaps['media_list'][$item['media_list']] : null;
            $item['visibility'] = (float)$item['visibility'];

            if (!empty($presentMaps['speaker_quote'][$item['speaker_quote']])) {
                $item['speaker_quote'] = $presentMaps['speaker_quote'][$item['speaker_quote']];
            } else {
                $item['speaker_quote'] = 'Absent';
            }
            if (!empty($presentMaps['third_speakers_quotes'][$item['third_speakers_quotes']])) {
                $item['third_speakers_quotes'] = $presentMaps['third_speakers_quotes'][$item['third_speakers_quotes']];
            } else {
                $item['third_speakers_quotes'] = 'Absent';
            }
            if (!empty($presentMaps['desired_article'][$item['desired_article']])) {
                $item['desired_article'] = $presentMaps['desired_article'][$item['desired_article']];
            } else {
                $item['desired_article'] = null;
            }
            if ($item['media_category'] && !empty($presentMaps['media_category'][$item['media_category']])) {
                $item['media_category'] = $presentMaps['media_category'][$item['media_category']];
            }
            if ($item['media_type'] && !empty($presentMaps['media_type'][$item['media_type']])) {
                $item['media_type'] = $presentMaps['media_type'][$item['media_type']];
            }
            if ($item['image'] && !empty($presentMaps['image'][$item['image']])) {
                $item['image'] = $presentMaps['image'][$item['image']];
            }
            if ($item['key_message_penetration'] && !empty($presentMaps['key_message_penetration'][$item['key_message_penetration']])) {
                $item['key_message_penetration'] = $presentMaps['key_message_penetration'][$item['key_message_penetration']];
            } else {
                $item['key_message_penetration'] = '';
            }
            if ($item['mention'] && !empty($presentMaps['mention'][$item['mention']])) {
                $item['mention'] = $presentMaps['mention'][$item['mention']];
            }
            if ($item['header_tonality'] && !empty($presentMaps['header_tonality'][$item['header_tonality']])) {
                $item['header_tonality'] = $presentMaps['header_tonality'][$item['header_tonality']];
            }
            if ($item['main_tonality'] && !empty($presentMaps['main_tonality'][$item['main_tonality']])) {
                $item['main_tonality'] = $presentMaps['main_tonality'][$item['main_tonality']];
            } else {
                $item['main_tonality'] = '';
            }

            $item['impact_index'] = (float)$item['impact_index'];
            $item['net_effect'] = (float)$item['net_effect'];
            $item['reach'] = (int)$item['reach'];

            $resultItem = [];

            foreach ($columnMap as $key => $value) {
                $resultItem[] = $item[$key];
            }

            $result['item'][] = $resultItem;


            /* Поиск дубликатов в данных*/
            $strH = $item['region']
                . $item['country']
                . $item['date']
                . $item['name']
                . $item['reach']
                . $item['headline'];
            $hash = base64_encode($strH);

            if (!isset($arH[$hash])) {
                $arH[$hash][] = $item['id'];
            } else {

                $arH[$hash][] = $item['id'];
                $arD[$arH[$hash][0]] = $hash;
                $arD[$item['id']] = $hash;
            }
        }
        if (isset ($arD)) {
            foreach ($arD as $k => $duplicate) {
                $dRow = implode(",", $arH[$duplicate]);
                $arDuplicate[$k  ] = ['message' => "Duplicate rows  {$dRow}", 'row' => $k + 1];
            }
        }
        $result['duplicate'] = $arDuplicate;

        return $result;
    }

    public static function approveByFilter($filter)
    {
        $coverages = Coverage::getList(0, 0, $filter, [], '', false, true);
        $ids = [];

        foreach ($coverages as $coverage) {
            $ids[] = $coverage->id;
        }

        return \DB::table('coverage')->whereIn('id', $ids)->update(['status' => self::STATUS_APPROVED]);
    }

    /**
     * @param $productId
     * @return bool
     */
    public function hasProduct($productId)
    {
        return !is_null(
            DB::table('products_coverage')
                ->where('coverage_id', $this->id)
                ->where('product_id', $productId)
                ->first()
        );
    }

    /**
     * @param $campaignId
     * @return bool
     */
    public function hasCampaign($campaignId)
    {
        return !is_null(
            DB::table('campaigns_coverage')
                ->where('coverage_id', $this->id)
                ->where('campaign_id', $campaignId)
                ->first()
        );
    }

    /**
     * @param $businessId
     * @return bool
     */
    public function hasBusiness($businessId)
    {
        return !is_null(
            DB::table('business_coverage')
                ->where('coverage_id', $this->id)
                ->where('business_id', $businessId)
                ->first()
        );
    }

    /**
     * @param $newsbreakId
     * @return bool
     */
    public function hasNewsbreak($newsbreakId)
    {
        return !is_null(
            DB::table('newsbreaks_coverage')
                ->where('coverage_id', $this->id)
                ->where('newsbreak_id', $newsbreakId)
                ->first()
        );
    }

    /**
     * @param $speakerId
     * @return bool
     */
    public function hasSpeaker($speakerId)
    {
        return !is_null(
            DB::table('speakers_coverage')
                ->where('coverage_id', $this->id)
                ->where('speaker_id', $speakerId)
                ->first()
        );
    }

    /**
     * @return bool
     */
    public function approve()
    {
        $this->status = self::STATUS_APPROVED;
        return $this->save();
    }

    /**
     * @return bool
     */
    public function toDraft()
    {
        $this->status = self::STATUS_DRAFT;
        return $this->save();
    }

    /**
     * @return bool
     */
    public function complete()
    {
        $this->status = self::STATUS_COMPLETE;
        return $this->save();
    }

    /**
     * @param $operation
     * @param $object
     *
     * @return bool
     */
    public static function access($operation, $object)
    {
        $user = Auth::user();
        switch ($operation) {
            case self::ACCESS_EDIT_COVERAGE:
                $result = !empty($object) && !empty($user) && (
                        $user->role == User::ROLE_ADMINISTRATOR ||
                        (
                            in_array($object['status'], [self::STATUS_DRAFT, self::STATUS_COMPLETE])
                            && ($user->id == $object['owner_id'] || $user->role = User::ROLE_MANAGER)
                        )
                    );
                break;
            case self::ACCESS_VIEW_COVERAGE:
                $result = !empty($object) && !empty($user) && (
                        in_array($user->role, [User::ROLE_ADMINISTRATOR, User::ROLE_MANAGER])
                        || $user->id == $object['owner_id'] && in_array($object['status'], [self::STATUS_DRAFT, self::STATUS_COMPLETE])
                    );
                break;
            case self::ACCESS_COMPLETE_COVERAGE:
                $result = !empty($object) && !empty($user) && $user->id == $object['owner_id'] && $object['status'] == self::STATUS_DRAFT;
                break;
            case self::ACCESS_RETURN_TO_DRAFT:
            case self::ACCESS_PUBLISH_COVERAGE:
                $result = !empty($object) && !empty($user) && $user->role == User::ROLE_ADMINISTRATOR && $object['status'] == self::STATUS_COMPLETE;
                break;
            case self::ACCESS_DELETE_COVERAGE:
                $result = !empty($object) && !empty($user) && ($user->role == User::ROLE_ADMINISTRATOR || $user->id == $object['owner_id']);
                break;

            default:
                $result = false;
        }
        return (boolean)$result;
    }

    /**
     * @param $filter
     * @param $search
     *
     * @return array
     */
    protected static function getCoverageForDelete($filter, $search)
    {
        $statement = \DB::table('coverage as c');
        $statement = self::applyFilter($statement, $filter, Coverage::getFiltersFields(), $search);
        $statement->groupBy("c.id");

        if (Auth::user()->role !== User::ROLE_ADMINISTRATOR) {
            $statement->where('c.owner_id', '=', Auth::user()->id);
        }

        // SQL don't support aliases at expressions of delete
        $items = $statement->select(['c.id as id', 'owner_id', 'c.status'])->get();
        $ids = [];
        foreach ($items as $item) {
            if (!Coverage::access(Coverage::ACCESS_DELETE_COVERAGE, (array)$item)) {
                continue;
            }
            $ids[] = $item->id;
        }
        return $ids;
    }

    /**
     * @param $filter
     * @param $search
     *
     * @return mixed
     */
    public static function deleteByFilter($filter, $search)
    {
        $ids = self::getCoverageForDelete($filter, $search);
        return \DB::table('coverage')->whereIn('id', $ids)->delete();
    }

    /**
     * @param $filter
     * @param $search
     *
     * @return mixed
     */
    public static function getCountCoverageForDelete($filter, $search)
    {
        $ids = self::getCoverageForDelete($filter, $search);
        return count($ids);
    }

    /**
     * @param        $data
     * @param        $key
     * @param string $emptySymbol
     *
     * @return string
     */
    protected static function getTableValue($data, $key, $emptySymbol = '-')
    {
        $value = !empty($data[$key]) ? $data[$key] : $emptySymbol;
        if ($emptySymbol !== $value) {
            $value = (false === strpos($value, 'M')) ? $value : ((float)str_replace('M', '', $value)) * 1000000;
        }
        return $value;
    }

    /**
     * @param $data
     *
     * @return array
     */
    protected static function prepareMedia($data)
    {
        $mediaCategories = self::getMediaCategories();
        foreach ($mediaCategories as &$mediaCategory) {
            $mediaCategory = [$mediaCategory];
        }
        $mediaList = ['diamond' => ['Diamond'], 'platinum' => ['Platinum'], 'gold' => ['Gold']];
        $mainTonality = ['positive' => ['Positive'], 'neutral' => ['Neutral'], 'negative' => ['Negative']];
        foreach ($data['labels'] as $label) {
            $label = (array)$label;
            $category = !empty($data['media_category'][$label['id']]) ? $data['media_category'][$label['id']] : [];
            foreach ($mediaCategories as $key => $value) {
                $mediaCategories[$key][] = self::getTableValue($category, $key);
            }
            $list = !empty($data['media_list'][$label['id']]) ? $data['media_list'][$label['id']] : [];
            foreach ($mediaList as $key => $value) {
                $mediaList[$key][] = self::getTableValue($list, $key);
            }
            $tonality = !empty($data['main_tonality'][$label['id']]) ? $data['main_tonality'][$label['id']] : [];
            foreach ($mainTonality as $key => $value) {
                $mainTonality[$key][] = self::getTableValue($tonality, $key);
            }
        }
        return ['categories' => $mediaCategories, 'list' => $mediaList, 'main_tonality' => $mainTonality];
    }

    /**
     * @param array $data
     *
     * @return array
     */
    protected static function prepareDefaultStatisticExport($data)
    {
        if (empty($data)) {
            return $data;
        }

        $titles = [''];
        $articles = ['Articles'];       // 'The number of news items sourced from online and print news'
        $mediaOutreach = ['Media Outreach']; // 'The total potential audience produced by media coverage within a specified time period, measured by impressions (unique users, followers, etc)'
        $impactIndex = ['Impact Index'];   // 'A composite index for measuring the quality of media coverage. It shows the level of influence on the audience'
        $netEffect = ['Net Effect'];     // 'Combines Impact Index with Reach to define the number of impressions of the article after which people are likely to remember news'

        foreach ($data['labels'] as $label) {
            $label = (array)$label;
            $titles[] = $label['name'];
            $articles[] = self::getTableValue($data['articles'], $label['id']);
            $mediaOutreach[] = self::getTableValue($data['media_outreach'], $label['id']);
            $impactIndex[] = self::getTableValue($data['impact_index'], $label['id']);
            $netEffect[] = self::getTableValue($data['net_effect'], $label['id']);
        }
        $result = [$titles, $articles, $mediaOutreach, $impactIndex, $netEffect];
        $media = self::prepareMedia($data);
        $result = array_merge($result, [['Media Category']], $media['categories'], [['Media List']], $media['list'], [['Main Tonality']], $media['main_tonality']);

        return $result;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public static function prepareRegionalKpiToExport($data)
    {
        return self::prepareDefaultStatisticExport($data);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public static function prepareRegionalProductMentionsToExport($data)
    {
        $titles = [''];
        $articles = ['Articles with product mentions'];
        $percent = ['Percent of articles with product mentions'];
        $impactIndex = ['Impact Index of articles with product mentions'];
        $netEffect = ['Net Effect of articles with product mentions'];
        foreach ($data['labels'] as $label) {
            $label = (array)$label;
            $titles[] = $label['name'];
            $articles[] = self::getTableValue($data['articles'], $label['id']);
            $percent[] = self::getTableValue($data['percent'], $label['id']);
            $impactIndex[] = self::getTableValue($data['impact_index'], $label['id']);
            $netEffect[] = self::getTableValue($data['net_effect'], $label['id']);
        }
        $result = [$titles, $articles, $percent, $impactIndex, $netEffect];
        $media = self::prepareMedia($data);
        $result = array_merge($result, [['Media Category']], $media['categories'], [['Media List']], $media['list'], [['Main Tonality']], $media['main_tonality']);
        return $result;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public static function prepareComparisonToExport($data)
    {
        return self::prepareDefaultStatisticExport($data);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public static function prepareTotalKeyMessagesToExport($data)
    {
        $titles = [''];
        $messagePenetration = ['Key Message Penetration'];
        foreach ($data as $item) {
            $titles[] = $item['title'];
            $messagePenetration[] = $item['count'];
        }
        return [$titles, $messagePenetration];
    }

    /**
     * Return all data for edit coverage
     *
     * @return array
     */
    public function getDataForEdit()
    {
        $products = $this->products()->get();
        $campaigns = $this->campaigns()->get();
        $business = $this->business()->get();
        $newsBreaks = $this->newsbreaks()->get();
        $speakers = $this->speakers()->get();
        $media = [];
        $countries = [];
        $ids = [];
        $campainIds = [];
        $businessIds = [];
        $newsBreaksNames = [];
        $speakersQuoted = [];

        foreach ($products as $product) {
            $ids[] = $product->id;
        }
        foreach ($campaigns as $campaign) {
            $campainIds[] = $campaign->id;
        }
        foreach ($business as $business_row) {
            $businessIds[] = $business_row->id;
        }
        foreach ($newsBreaks as $newsBreak) {
            $newsBreaksNames[] = $newsBreak->name;
        }
        foreach ($speakers as $speaker) {
            $speakersQuoted[] = $speaker->name;
        }

        if ($this->has('region')) {
            $builder = Media::where('region_id', '=', $this->region->id);

            if ($this->has('country')) {
                $builder->where('country_id', '=', $this->country->id);
            }

            $media = $builder->get()->sortBy('name');
            $countries = Country::where('region_id', '=', $this->region->id)->get()->sortBy('name');
        }

        return [
            'coverage' => $this,
            'attachedProducts' => $ids,
            'attachedCampaigns' => $campainIds,
            'attachedBusiness' => $businessIds,
            'newsBreaks' => $newsBreaksNames,
            'speakersQuoted' => $speakersQuoted,
            'media' => $media,
            'countries' => $countries,
        ];
    }

    /**
     * Calc net_effect and impact_index
     *
     * @return bool
     */
    public function calc()
    {
        $map = [
            "kmp" => [
                self::KEY_MESSAGE_PENETRATION_PRESENT => 1,
                self::KEY_MESSAGE_PENETRATION_EXTENT => 0.6,
                self::KEY_MESSAGE_PENETRATION_ABSENT => 0,
                self::KEY_MESSAGE_PENETRATION_COUNTER => 0,
            ],
            "main_tonality" => [
                self::MAIN_TONALITY_POSITIVE => 1,
                self::MAIN_TONALITY_NEUTRAL => 1,
                self::MAIN_TONALITY_NEGATIVE => 0,
            ],
            "speaker_quote" => [
                0 => 0.8,
                1 => 1,
            ]
        ];
        $map_tonality = empty($map["main_tonality"][$this->main_tonality]) ? 0 : $map["main_tonality"][$this->main_tonality];
        $map_key_message_penetration = empty($map["kmp"][$this->key_message_penetration]) ? 0 : $map["kmp"][$this->key_message_penetration];
        $qualityFactor = (float)$this->media_list * (float)$this->mention * (float)$this->visibility * $map["speaker_quote"][(int)$this->speaker_quote] * (float)$this->image;
        $tonality = (float)$this->header_tonality * $map_tonality;
        $impact_index = $map_key_message_penetration * 0.2 + $qualityFactor * $tonality * 0.71 + (float)$this->third_speakers_quotes * 0.09;
        $this->net_effect = round($impact_index * $this->reach);
        $this->impact_index = round($impact_index, 2);

        return $this->save();
    }

    /**
     * Get TOP
     *
     * @param int $limit
     *
     * @return mixed
     */
    public static function getTopEvents($params)
    {
        $builder = \DB::table('coverage')
            ->select('coverage.event_name as name', \DB::raw('count(1) as total'))
            ->whereRaw('LENGTH(event_name) > 0')
            ->where('coverage.status', '=', Coverage::STATUS_APPROVED)
            ->groupBy('coverage.event_name')
            ->orderBy('total', 'desc')
            ->limit($params['limit']);

        if (isset($params['date'])) {
            $builder->where('coverage.date', '>=', $params['date']['start'])
                ->where('coverage.date', '<', $params['date']['end']);
        }

        return $builder->get();
    }

    /**
     * Get TOP
     *
     * @param int $limit
     *
     * @return mixed
     */
    public static function getAllEvents()
    {
        $builder = \DB::table('coverage')
            ->select('coverage.event_name as name')
            ->whereRaw('LENGTH(event_name) > 0')
            ->where('coverage.status', '=', Coverage::STATUS_APPROVED)
            ->groupBy('coverage.event_name')
            ->orderBy('coverage.event_name');

        return $builder->lists("name");
    }

    /**
     * Get TOP
     *
     * @param int $limit
     *
     * @return mixed
     */
    public static function getTopSpeakers($params)
    {
        $builder = \DB::table('coverage')
            ->select('speakers.name', \DB::raw('count(1) as total'), \DB::raw('speakers.id as id'))
            ->join('speakers_coverage', 'coverage.id', '=', 'speakers_coverage.coverage_id')
            ->join('speakers', 'speakers.id', '=', 'speakers_coverage.speaker_id')
            ->where('coverage.status', '=', Coverage::STATUS_APPROVED)
            ->groupBy('speakers.name')
            ->orderBy('total', 'desc')
            ->limit($params['limit']);

        if (isset($params['date'])) {
            $builder->where('coverage.date', '>=', $params['date']['start'])
                ->where('coverage.date', '<', $params['date']['end']);
        }

        return $builder->get();
    }

    /**
     * Validates third_speakers_quoted.
     *
     * @param array $data
     *
     * @return bool|array
     */
    public static function validateThirdSpeakers(array $data)
    {
        $rules = BaseValidator::$rules[BaseValidator::THIRD_SPEAKER];
        $validator = BaseValidator::make($data, $rules);

        return $validator->fails() ? $validator->errors() : true;
    }

    /**
     * count of products with SecurityIntelligence
     *
     * @param  string $alias
     * @return mixed
     */
    public static function countSecurityIntelligence($alias = 'c')
    {
        return \DB::table('products_coverage')
            ->select(\DB::raw('count(*)'))
            ->join('products', 'products.id', '=', 'products_coverage.product_id')
            ->whereRaw("products_coverage.coverage_id = {$alias}.id")
            ->whereRaw('products.is_security_intelligence = 1');
    }

    /**
     * count of products
     * @param  string $alias
     * @return mixed
     */
    public static function countProduct($alias = 'c')
    {
        return \DB::table('products_coverage')
            ->select(\DB::raw('count(*)'))
            ->whereRaw("products_coverage.coverage_id = {$alias}.id");
    }

    /**
     * gets data by data & region for display kpi tables
     *
     * @param  string $date
     * @param  integer $region
     * @return array
     */
    public static function kpiDataByRegion($date, $region = null)
    {
        $start = date('Y', strtotime($date)) . '-01-01';
        $end = date('Y-m-d', strtotime('first month ' . $date));
        $month = date('n', strtotime($date));

        $builder = \DB::table('coverage')
            ->where('coverage.status', '=', Coverage::STATUS_APPROVED)
            ->where('date', '>=', $start)
            ->where('date', '<', $end);
        if ($region) {
            $builder->where('region_id', '=', $region);
        }
        $data = $builder->select(\DB::raw('avg(impact_index) as impact_index, sum(net_effect) as net_effect, count(*) as count'))->first();
        $negative = $builder->select(\DB::raw('count(*) as negative_count'))
            ->where('main_tonality', '=', Coverage::MAIN_TONALITY_NEGATIVE)
            ->first();

        return [
            'impact_index' => $data->impact_index,
            'net_effect' => $data->net_effect / $month,
            'negative' => $data->count ? $negative->negative_count * 100 / $data->count : null
        ];
    }


    /**
     * gets data by data & region for display kpi tables
     *
     * @param  string $date
     * @param  integer $region
     * @return array
     */
    public static function kpiDataV2ByRegion($date, $region = null)
    {
        $start = date('Y', strtotime($date)) . '-01-01';
        $end = date('Y-m-d', strtotime('first month ' . $date));
        $month = date('n', strtotime($date));

        $builder = \DB::table('coverage')
            ->where('coverage.status', '=', Coverage::STATUS_APPROVED)
            ->where('date', '>=', $start)
            ->where('date', '<', $end);
        if ($region) {
            $builder->where('region_id', '=', $region);
        }

        $b = clone $builder;
        $data = $b->select(\DB::raw('avg(impact_index) as impact_index, count(*) as count'))->first();


        $b = clone $builder;
        $avg_ne = $b->select(\DB::raw('sum(net_effect) as net_effect'))
            ->first();


        $b = clone $builder;
        $ne_ua = $b->select(\DB::raw('sum(net_effect) as net_effect'))
            ->where('desired_article', '=', "")
            ->first();


        $b = clone $builder;
        $ne_da = $b->select(\DB::raw('sum(net_effect) as net_effect'))
            ->where('desired_article', '=', 1)
            ->first();



        $negative = $builder->select(\DB::raw('count(*) as negative_count'))
            ->where('main_tonality', '=', Coverage::MAIN_TONALITY_NEGATIVE)
            ->first();

        $result=[
            'impact_index' => $data->impact_index,
            'net_effect_da' => $ne_da->net_effect / $month,
            'net_effect_ua' => $ne_ua->net_effect / $month,
            'negative' => $data->count ? $negative->negative_count * 100 / $data->count : null,
            'net_effect_ua_perc'=>0,
             'net_effect_da_perc'=>0
        ];

        if ($avg_ne->net_effect>0) {
            $result['net_effect_ua_perc']=($ne_ua->net_effect  )*100/$avg_ne->net_effect;
            $result['net_effect_da_perc']=($ne_da->net_effect  )*100/$avg_ne->net_effect;
        }

        return $result;
    }


    public function subcategoriesIds()
    {
        $row = [];
        foreach ($this->subcategories as $subcategory) {
            $row[] = $subcategory->id;
        }
        return $row;
    }
}
