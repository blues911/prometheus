<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class NewsBreak extends \Eloquent
{
    use SoftDeletingTrait;

    protected $dates    = ['deleted_at'];
    protected $table    = 'newsbreaks';
    protected $fillable = ['name', 'status', 'type', 'complete'];

    const STATUS_ACTIVE    = 'active';
    const STATUS_ARCHIVED  = 'archived';
    const TYPE_GLOBAL      = 'global';
    const TYPE_LOCAL       = 'local';
    const NEWSBREAKS_PAGE_SIZE_DEFAULT = 25;

    public static function getList()
    {
        return \DB::table('newsbreaks')
            ->where('status', '=', self::STATUS_ACTIVE)
            ->where('complete', '=', 1)
            ->whereNull('deleted_at')
            ->orderBy('name')
            ->lists("name");
    }

    public static function getActiveNewsBreak()
    {
        return \DB::table('newsbreaks')
            ->where('status', '=', self::STATUS_ACTIVE)
            ->where('complete', '=', 1)
            ->whereNull('deleted_at')
            ->orderBy('name')
            ->get();
    }

    public static function getNewsbreakById($id)
    {
        $record = \DB::table('newsbreaks')->where('id', '=', $id)->first();
        if (!isset($record)) {
            return '';
        }
        return $record->name;
    }

    /**
     * Get TOP
     *
     * @param int $limit
     *
     * @return mixed
     */
    public static function getTop($params)
    {
        $builder = \DB::table('newsbreaks_coverage as nс')
            ->select('newsbreaks.name', \DB::raw('count(1) as total'), \DB::raw('newsbreaks.id as id'))
            ->join('newsbreaks', 'newsbreaks.id', '=', 'nс.newsbreak_id')
            ->join('coverage', 'coverage.id', '=', 'nс.coverage_id')
            ->where('coverage.status', '=', Coverage::STATUS_APPROVED)
            ->where('newsbreaks.complete', '=', 1)
            ->whereNull('newsbreaks.deleted_at')
            ->groupBy('newsbreaks.name')
            ->orderBy('total', 'desc')
            ->limit($params['limit']);

        if (isset($params['date'])) {
            $builder->where('coverage.date', '>=', $params['date']['start'])
                ->where('coverage.date', '<', $params['date']['end']);
        }
        
          return $builder->get();
    }

}