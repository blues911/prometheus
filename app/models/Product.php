<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at
 *
 * Date: 26.10.14
 * Time: 18:20
 * @property mixed name
 */
class Product extends \Eloquent
{
    use SoftDeletingTrait;

    protected $dates    = ['deleted_at'];
    protected $table    = 'products';
    protected $fillable = [
        'name',
        'short_name'
    ];

    const STATUS_ACTIVE   = 'active';
    const STATUS_ARCHIVED = 'archived';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function coverages()
    {
        return $this->belongsToMany('App\Models\Coverage', 'products_coverage', 'product_id', 'coverage_id');
    }

    /**
     * Get TOP
     *
     * @param int $limit
     *
     * @return mixed
     */
    public static function getTop($params)
    {
        $builder = \DB::table('products_coverage as pc')
            ->select('pc.coverage_id', \DB::raw('count(1) as total'), \DB::raw('pc.product_id as id'))
            ->join('products', 'products.id', '=', 'pc.product_id')
            ->join('coverage', 'coverage.id', '=', 'pc.coverage_id')
            ->where('coverage.status', '=', Coverage::STATUS_APPROVED)
            ->whereNull('products.deleted_at')
            ->groupBy('pc.product_id')
            ->orderBy('total', 'desc')
            ->limit($params['limit']);

        if (isset($params['date'])) {
            $builder->where('coverage.date', '>=', $params['date']['start'])
                ->where('coverage.date', '<', $params['date']['end']);
        }

        return $builder->get();
    }

}