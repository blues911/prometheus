<?php
namespace App\Models;
use URL;
use DB;
use Auth;
use Hash;

class SaveReport extends \Eloquent
{
    const PDF_TYPE = 'pdf';
    const PAGE_TYPE = 'page';

    protected $table    = 'save_reports';
    protected $fillable = ['name', 'url', 'params', 'hash', 'tab', 'user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\SystemUser', 'user_id');
    }

    /**
     * Save report
     *
     * @param string $name
     * @param string $params
     *
     * @return bool
     */
    public static function add($name, $params, $tab, $type)
    {
        $object          = new SaveReport();
        $object->name    = $name;
        $object->url     = $type == self::PDF_TYPE ? URL::to('pdf') : URL::to('reports');
        $object->params  = $params;
        $object->hash    = Hash::make(time().$tab.$object->params);
        $object->tab     = $tab;
        $object->user_id = Auth::user()->id;
        $object->type    = $type;

        return $object->save() ? $object : null;
    }

    /**
     * Get report url
     *
     * @param string $url
     * @param string $hash
     * @param string $tab
     *
     * @return string
     */
    public static function getUrl($url, $hash, $tab)
    {
        $url = $url . "?" . http_build_query(['t' => $hash]);
        if (!empty($tab)) {
            $url .= "#".$tab;
        }
        return $url;
    }

    /**
     * Get save report
     *
     * @param string $hash
     *
     * @return SaveRepors
     */
    public static function getByHash($hash)
    {
        return SaveReport::where("hash", "=", $hash)->first();
    }

    /**
     * Get reports list
     *
     * @param int $skip
     * @param int $limit
     *
     * @return array
     */
    public static function getReportsList($skip = 0, $limit = 10)
    {
        $reports = \DB::table('save_reports')->where('type', self::PAGE_TYPE);
        if ($limit > 0) {
            $reports = $reports->take($limit);
        }
        if ($skip > 0) {
            $reports = $reports->offset($skip);
        }
        $reports = $reports->orderBy('created_at', 'desc')->get();

        $result  = [];
        foreach ($reports as $report) {
            $result[] = [
                'id'   => $report->id,
                'name' => $report->name,
                'url'  => SaveReport::getUrl($report->url, $report->hash, $report->tab),
                'created_at' => $report->created_at,
            ];
        }

        return $result;
    }

}