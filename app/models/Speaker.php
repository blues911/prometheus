<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Speaker extends \Eloquent
{
 	  use SoftDeletingTrait;

    protected $dates    = ['deleted_at'];
    protected $table    = 'speakers';
    protected $fillable = ['name'];

    const SPEAKERS_PAGE_SIZE_DEFAULT = 25;

    /**
     * @return array
     */
    public static function getList()
    {
        $results = \DB::table('speakers')
            ->whereNull('deleted_at')
            ->orderBy('name')
//            ->leftJoin('speakers_coverage', 'speakers.id', '=', 'speakers_coverage.speakers_id')
//            ->whereIn('speakers.id','speakers_coverage.speakers_id')
            ->lists('name');

        $results = array_filter($results, function ($name) {
            return preg_match('/^[^_0-9!@#$%№;:?*.^<>~\\[\\]\\-_=\'\"\\\\|\\/{}()\\+]+$/i', $name);
        });
        
        return array_values(array_unique($results));
    }

    /**
     * Validation
     *
     * @param array $data
     *
     * @return bool|array
     */
    public static function validate(array $data)
    {
        $rules = BaseValidator::$rules[BaseValidator::SPEAKER];
        $validator = BaseValidator::make($data, $rules);
        
        return $validator->fails() ? $validator->errors() : true;
    }
}
