<?php
namespace App\Models;

class KpiHeader extends \Eloquent
{
	protected $table    = 'kpi_headers';
    protected $fillable = [
        'region_id',
    	'net_effect', 
    	'impact_index', 
    	'negative', 
    	'company_mindshare', 
    	'b2b_mindshare', 
    	'b2c_mindshare', 
    	'year'
    ];

    public static function boot()
    {
        parent::boot();

        KpiHeader::creating(function($headers)
        {
            $prev = self::where('region_id', $headers->region_id)->where('year', (int)$headers->year - 1)->first();

            $headers->net_effect        = $prev ? $prev->net_effect         : 'Monthly average Net Effect 0';
            $headers->impact_index      = $prev ? $prev->impact_index       : 'Monthly average Impact Index 0';
            $headers->negative          = $prev ? $prev->negative           : 'Less than 0% of negative';
            $headers->company_mindshare = $prev ? $prev->company_mindshare  : 'Company mindshare. Remain/Achieve';
            $headers->b2b_mindshare     = $prev ? $prev->b2b_mindshare      : 'B2B Mindshare. Remain within';
            $headers->b2c_mindshare     = $prev ? $prev->b2c_mindshare      : 'B2C Mindshare. Remain';

            return true;
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    public static function netEffectFormat($value)
    {
        $value = round($value/1000000, 1);
        return $value == 0 ? 0 : number_format($value, 1, '.', '') . 'M';
    }
}