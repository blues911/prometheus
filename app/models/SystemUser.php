<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\Support\Facades\Hash;
use Validator;

/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at
 *
 * Date: 26.10.14
 * Time: 14:40
 * @property mixed region
 * @property mixed country
 * @property mixed password
 */
class SystemUser extends User
{
    use SoftDeletingTrait;

    const LENGTH_PASSWORD = 8;
    const USERS_PAGE_SIZE_DEFAULT = 25;

    protected $dates    = ['deleted_at'];
    protected $table    = 'system_users';
    protected $fillable = [
        'password',
        'email',
        'region_id',
        'country_id',
        'name',
        'role'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function coverages()
    {
        return $this->belongsToMany('App\Models\Coverage', 'user_coverage', 'user_id', 'coverage_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coverage()
    {
        return $this->belongsTo('App\Models\Coverage', 'id', 'owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notifications()
    {
        return $this->belongsTo('App\Models\Notification', 'id', 'user_id');
    }

    /**
     * @param $data
     * @return bool
     */
    public function validate($data)
    {   
        $rules = BaseValidator::$rules[BaseValidator::SYSTEM_USER];
        $rules['email'] .= ',' . (empty($data['id']) ? 'NULL' : $data['id']) . ',id';

        $v = BaseValidator::make($data, $rules);
        return $v->passes();
    }

    /**
     * @param $id
     * @return array|static[]
     */
    public static function getUnreadNotifications($id)
    {
        return \DB::table('notifications')
            ->select()
            ->where('status', '=', 'unread')
            ->where('user_id', '=', $id)
            ->orderBy("created_at", "desc")
            ->get();
    }

    /**
     * @param $email
     * @return SystemUser
     */
    public static function findByEmail($email)
    {
        return self::where('email', '=', $email)->first();
    }

    /**
     * @param $email
     * @return bool
     */
    public function generateSign()
    {
        $this->sign = Hash::make(time() . $this->email);
        return $this->save();
    }


    /**
     * @param $password
     * @return bool
     */
    public function changePassword($password)
    {
        $this->password = Hash::make($password);
        $this->sign     = null;
        return $this->save();
    }

    /**
     * @param $email
     * @return SystemUser
     */
    public static function findBySign($sign)
    {
        return self::where('sign', '=', $sign)->first();
    }
}
