<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Event extends \Eloquent
{
	use SoftDeletingTrait;

	protected $dates    = ['deleted_at'];
    protected $table    = 'events';
    protected $fillable = ['name', 'status'];

    const STATUS_ACTIVE    = 'active';
    const STATUS_ARCHIVED  = 'archived';

    public static function getList()
    {
        return \DB::table('events')
            ->where('status', '=', self::STATUS_ACTIVE)
            ->whereNull('deleted_at')
            ->orderBy('name')
            ->lists("name");
    }

}