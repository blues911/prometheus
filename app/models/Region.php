<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at
 *
 * Date: 26.10.14
 * Time: 18:21
 * @property mixed name
 */
class Region extends \Eloquent
{
    use SoftDeletingTrait;

    protected $dates    = ['deleted_at'];
    protected $table    = 'regions';
    protected $fillable = [
        'name'
    ];


    public static function getRegionById($id)
    {
        $record = \DB::table('regions')
            ->where('id', '=', $id)
            ->whereNull('deleted_at')
            ->first();
        if (!isset($record)) {
            return '';
        }
        return $record->name;
    }

}
