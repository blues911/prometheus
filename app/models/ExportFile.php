<?php
namespace App\Models;

class ExportFile extends \Eloquent
{
    protected $table    = 'export_files';
    protected $fillable = ['name', 'filename', 'user_id'];

    public function getCreatedAtAttribute($value)
    {
        $offset = \Session::get('time-offset', 0);
        return date('Y-m-d H:i:s', strtotime($value) + $offset);
    }

    /**
     * @param $skip
     * @param $limit
     * @param $filter
     * @param $search
     *
     * @return mixed
     */
    public static function getActiveUserExportFiles($skip = 0, $limit = 10, $filter = [], $search = '')
    {
        $statement = new ExportFile;
        if (!empty($filter['user_id'])) {
            $statement = $statement->where("user_id", "=", $filter['user_id']);
        }
        if (!empty($filter['from_date'])) {
            $statement = $statement->where("created_at", ">=", $filter['from_date']);
        }
        if (!empty($filter['to_date'])) {
            $statement = $statement->where("created_at", "<", $filter['to_date']);
        }

        if (!empty($search)) {
            $statement = $statement->where('name', 'LIKE', $search."%");
        }
        if ($skip > 0) {
            $statement = $statement->skip($skip);
        }

        if ($limit > 0) {
            $statement = $statement->limit($limit);
        }

        return $statement->orderBy('id', 'DESC')->get();
    }

}