<?php

namespace App\Helpers;

class ArrayHelper
{

    /**
     * @param mixed  $data
     * @param string $name
     * @param string $alternativeName
     * @param string $id
     *
     * @return array
     */
    public static function prepareDropDownList($data, $name = 'name', $alternativeName = '', $id = 'id')
    {
        $result = [];
        foreach ($data as $item) {
            $value = !empty($item->$name) || '' === $alternativeName ? $item->$name : $item->$alternativeName;
            if (empty($value)) {
                continue;
            }
            $result[] = [
                'id'    => $item->$id,
                'value' => $value,
                'label' => $value,
            ];
        }
        return $result;
    }

    /**
     * @param mixed  $data
     * @param string $field
     *
     * @return array
     */
    public static function getFieldListFromArray($data, $field = 'id')
    {
        $result = [];
        foreach ($data as $item) {
            $value = !empty($item->$field) ? $item->$field : null;
            if (empty($value)) {
                continue;
            }
            $result[] = $item->$field;
        }
        return $result;
    }

    public static function isPdfTabEmpty($tab, $filters)
    {
        if (empty($filters)) {
            return true;
        }

        $charts = [
            'executive_summary' => [
                'chart-net-effect',
                'impact-index',
                'chart-reach',
                'chart-articles',
                'chart-main-tonality',
                'key_meesages',
                'chart-indexes-comparison',
                'chart-regional-statistics',
                'chart-product-mentions',
            ],
            'general_information' => [
                'chart-media-type',
                'chart-media-list',
                'chart-media-category',
                'chart-mention-relation',
                'chart-speaker-quote',
                'chart-third-speaker-quote',
                'chart-products',
                'security_intelligence',
                'chart-speakers-count',
                'chart-newsbreaks',
                'chart-campaign',
                'chart-desired-article',
                'chart-events',
                'chart-top-story',
            ],
            'comparison' => [
                'chart-comparison',
            ],
            'dynamics' => [
                'chart-net-dynamic',
                'chart-impact-dynamic',
                'chart-reach-dynamic',
                'chart-articles-dynamic',
            ],
        ];

        return !empty(array_intersect($charts[$tab], $filters));
    }

}
