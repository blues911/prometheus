<?php

namespace App\Helpers;

class CurlHelper
{

    /**
     * @param string $url
     *
     * @return array
     */
    public static function curl($url)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        
        return json_decode(curl_exec($curl), true);
    }

}