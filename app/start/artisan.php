<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/
Artisan::add(new \App\Command\ImportCountries);
Artisan::add(new \App\Command\ImportCoverage);
Artisan::add(new \App\Command\DevCommand);
Artisan::add(new \App\Command\RemoveDoubleSpeakersCommand);
Artisan::add(new \App\Command\RemoveDoubleNewsbreaksCommand);
Artisan::add(new \App\Command\SetActiveSpeakersFromFileCommand);
