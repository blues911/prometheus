$.widget("admin.mediaFilter", {
	options: {
		sendButtonSelector:       '.send-button',
		countryFilterSelector:    '#country_filter',
		countryIdSelector:        '#country_id',
		regionDropdownSelector:   '#region_filter_dropdown',
		regionIdSelector:         '#region_id',
		nameFilterSelector:       '#name_filter',
		orderBySelector:          '#order_by_filter',
		url:                      '/media/get-media',
		contentContainerSelector: '#media-list',
		countries:                {}
	},
	_create: function() {
		this.sendButton     = this.element.find(this.options.sendButtonSelector);
		this.regionIdInput  = this.element.find(this.options.regionIdSelector);
		this.regionDropdown = this.element.find(this.options.regionDropdownSelector);
		this.countryIdInput = this.element.find(this.options.countryIdSelector);
		this.nameInput      = this.element.find(this.options.nameFilterSelector);
		this.orderByInput   = this.element.find(this.options.orderBySelector);
	},
	_init: function() {
		this._on(this.sendButton, {
			click: "_sendButtonClick"
		});
		this.regionDropdown.dropdown({
			onChange: $.proxy(this._regionDropdownChange, this)
		});
		this.oldParams = {};
	},
	processCountries: function(countries) {
		var countriesData = Array();
		$.each(countries, function (index, country) {
			countriesData.push({id: country.id, label: country.name, value:country.name});
		});
		return countriesData;
	},
	_regionDropdownChange: function(value) {
		if (!value) {
			return false;
		}
		this.countryIdInput.val('');
		$.get('/country', {
			region_id: value
		}, $.proxy(function (response) {
			$(this.options.countryFilterSelector).val('');
			$(this.options.countryFilterSelector).change();
			if (!$.isEmptyObject(response.countries)) {
				$(this.options.countryFilterSelector).autocomplete('option', 'source', this.processCountries(response.countries));
			}
		}, this));
	},
	_sendButtonClick: function(e) {
		this.send();
		this._saveFilter();
	},
	send: function(page, onlyDeleted) {
    var params = this._getParams(page);
    var onlyDeleted = $('#media-list').attr('data-deleted');
    params['only_deleted'] = onlyDeleted;
		// reduce the number of requests to the server
		if (JSON.stringify(params) === JSON.stringify(this.oldParams)) {
			return false;
    }
		var jqxhr = $.ajax({
			url: this.options.url,
			data: params,
			type: 'GET'
		});
		this.oldParams = params;
		jqxhr.done($.proxy(this._successHandler, this));
		jqxhr.fail($.proxy(this._errorHandler, this));
	},
	_saveFilter: function() {
		var filter   = this._getParams(),
			country  = $(this.options.countryFilterSelector).val();
		if (country.length > 0) {
			filter['country'] = country;
		}
		localStorage.setItem('admin_media_filter', JSON.stringify(filter));
	},
	_getParams: function(page) {
		var regionId  = this.regionIdInput.val(),
			country   = this.countryIdInput.val(),
			name      = this.nameInput.val(),
			orderBy   = this.orderByInput.val(),
			params    = {};
		if (regionId.length > 0) {
			params['region_id'] = regionId;
		}
		if (country.length > 0) {
			params['country_id'] = country;
		}
		if (name.length > 0) {
			params['name'] = name;
		}
		if (orderBy.length > 0) {
			params['order_by'] = orderBy;
		}
		if ('undefined' !== typeof page) {
			params['page'] = page;
		}
		return params;
	},
	_successHandler: function(data) {
		$(this.options.contentContainerSelector).html(data);
		$("body, html").animate({"scrollTop": 0}, 500);
		$('.ui.checkbox').checkbox({
	        'onChecked': function() {$(this).val('1');},
	        'onUnchecked': function() {$(this).val('0');}
	    });
	},
	_errorHandler: function(data) {
		this.oldParams = {};
	},
	initFilter: function() {
		if (null !== localStorage.getItem('admin_media_filter')) {
			var filter = JSON.parse(localStorage.getItem('admin_media_filter'));
			if (undefined !== filter['region_id']) {
				this.regionDropdown.dropdown('set value', filter['region_id']).dropdown('set selected', filter['region_id']);
			}
			if (undefined !== filter['name']) {
				this.nameInput.val(filter['name']);
			}
			if (undefined !== filter['order_by']) {
				$('#order_by_dropdown').dropdown('set value', filter['order_by']).dropdown('set selected', filter['order_by']);
			}
			if (undefined !== filter['country_id'] && undefined !== filter['country']) {
				this.countryIdInput.val(filter['country_id']);
				setTimeout(function(country) {
					$('#country_filter').val(country);
					$('#country_filter').change();
				}, 500, filter['country']);
			}
    }
		this.send();
	}
});

function getURLParameter(url, param) {
	var results = new RegExp('[\?&]' + param + '=([^&#]*)').exec(url);
	return results[1]
}

function initMediaForm() {
	$("#media_filter_form").mediaFilter();
	$('#country_filter').autocomplete({
		'minLength' :'2',
		'change'    : $.proxy(function(event, ui) {
			// записываем полученный id в скрытое поле
			this.value = ui.item ? ui.item.value : null;
			$('#country_id').val(ui.item ? ui.item.id : null);
			$('#country_filter').change();
			return false;
		}, this),
		'source': $("#media_filter_form").mediaFilter('processCountries', countries)
	});
	$("#media_filter_form").mediaFilter('initFilter');
	$(document.body).on('click', '#media-list .pagination a', function(e) {
    e.preventDefault();
		var href = $(e.currentTarget).attr('href');
		$("#media_filter_form").mediaFilter('send', getURLParameter(href, 'page'));
	});
}
