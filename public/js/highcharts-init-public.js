function highlightFilters() {
    $('.ui.form .fields').find('[name="start"]')
        .add($('.filters .form').find('[name="end"]'))
        .each(function () {
            processHighlight(this, 'datepicker');
        });

    $('.ui.form .fields').find('input[type=text]').each(function () {
        if (-1 === $.inArray($(this).attr('name'), ['start', 'end'])) {
            processHighlight(this);
        }
    });
}

function getFilter() {
    filters['token'] = hash;
    var tab          = $('.menu.charts .item.active').attr("data-tab");
    if (tab == "dynamics") {
        filters['date_filter'] = $("#timeline_filter").val();
    }

    return filters;
}

getCovarageTotals = function(filter) {
    $.ajax({
        url     : '/stats/coverage-totals',
        method  : 'GET',
        dataType: 'json',
        data    : filter || null,
        success: function (data) {
            if (typeof data !== 'undefined') {
                $("#coverage_count").html(data['coverage_count']);
                $("#impact_index").html(data['impact_index']);
                $("#net_effect").html(data['net_effect']);
                $("#media_outreach").html(data['media_outreach']);
            }
        }
    });
}

function getFileName() {
    var filename = '';
    if (filters.country_id != undefined) {
        $.each(countries, function () {
            if(this.id == filters.country_id) {
                filename = filename + '_' + this.value;
                return false;
            }
        });
    } else if (filters.region_id != undefined) {
        $.each(regions, function () {
            if(this.id == filters.region_id) {
                filename = filename + '_' + this.name;
                return false;
            }
        });
    }
    if (filters.start != undefined) {
        filename = filename + '_from' + filters.start;
    }
    if (filters.end != undefined) {
        filename = filename + '_to' + filters.end;
    }

    return filename;
}

getCovarageTotalsPdf = function(filter) {
    $.ajax({
        url     : '/stats/coverage-totals',
        method  : 'GET',
        dataType: 'json',
        data    : filter || null,
        success: function (data) {
            if (typeof data !== 'undefined') {
                $("#reports .main-totals-menu .coverage_count").html(data['coverage_count']);
                $("#reports .main-totals-menu .impact_index").html(data['impact_index']);
                $("#reports .main-totals-menu .net_effect").html(data['net_effect']);
                $("#reports .main-totals-menu .media_outreach").html(data['media_outreach']);
            }
        }
    });
}

getTotalKeyMessages = function(filter) {
    $.ajax({
        url     : '/stats/total-key-messages',
        method  : 'GET',
        dataType: 'json',
        data    : filter || null,
        success: function (data) {
            if (typeof data !== 'undefined') {
                if (data.length > 0) {
                    var thead = '',
                        tbody = '';
                    $('#key_message_penetration').removeClass('hide');
                    data.forEach(function(item) {
                        thead += '<th>' + item.title + '</th>';
                        tbody += '<td><div>' + item.count + '</div></td>'
                    });
                }
                $('#message_penetration_table thead').append(thead);
                $('#message_penetration_table tbody tr').append(tbody);
            }
        }
    });
}

getTopStoriesPdf = function(filter) {
    $.ajax({
        url     : '/stats/top-stories',
        method  : 'GET',
        dataType: 'json',
        data    : filter || null,
        success: function (data) {
            if (typeof data !== 'undefined') {
                var str = '';
                data.forEach(function(item) {
                    str += '<li style="padding: 10px;">' +
                        '<div><strong>' + item.headline + '</strong></div>' +
                        '<div><a ng-href="' + item.url + '" target="_blank">' + item.url + '</a></div>' +
                        '<div>' + item.country + ', ' + item.date + ', Impact Index &mdash; ' + item.impact_index + '</div></li>';
                });

                $('.charts.general .top_story ol').append(str);
            }
        }
    });
}

$(function () {
    "use strict";

    var maxLegendCount = 20;

    // Overwrite method to do padding for title
    Highcharts.Chart.prototype.layOutTitles = function (redraw) {
        var titleOffset = 0,
            title = this.title,
            subtitle = this.subtitle,
            options = this.options,
            titleOptions = options.title,
            subtitleOptions = options.subtitle,
            requiresDirtyBox,
            renderer = this.renderer,
            autoWidth = this.spacingBox.width - 100,
            PX = 'px';

        if (title) {
            title
                .css({ width: (titleOptions.width || autoWidth) + PX })
                .align(Highcharts.extend({
                    y: renderer.fontMetrics(titleOptions.style.fontSize, title).b - 3
                }, titleOptions), false, 'spacingBox');

            if (!titleOptions.floating && !titleOptions.verticalAlign) {
                titleOffset = title.getBBox().height;
            }
        }
        if (subtitle) {
            subtitle
                .css({ width: (subtitleOptions.width || autoWidth) + PX })
                .align(Highcharts.extend({
                    y: titleOffset + (titleOptions.margin - 13) + renderer.fontMetrics(titleOptions.style.fontSize, subtitle).b
                }, subtitleOptions), false, 'spacingBox');

            if (!subtitleOptions.floating && !subtitleOptions.verticalAlign) {
                titleOffset = Math.ceil(titleOffset + subtitle.getBBox().height);
            }
        }

        requiresDirtyBox = this.titleOffset !== titleOffset;
        this.titleOffset = titleOffset; // used in getMargins

        if (!this.isDirtyBox && requiresDirtyBox) {
            this.isDirtyBox = requiresDirtyBox;
            // Redraw if necessary (#2719, #2744)
            if (this.hasRendered && Highcharts.pick(redraw, true) && this.isDirtyBox) {
                this.redraw();
            }
        }
    };

    $(".chart_date_filter." + $("#timeline_filter").val()).addClass("teal");
    $(".information_filter." + $("#general_information_filter").val()).addClass("teal");

    $(document.body).on("click", ".chart_date_filter", function() {
        var filter = $(this).attr("data-value");
        $("#timeline_filter").val(filter);
        $(".chart_date_filter").removeClass("teal");
        $(".chart_date_filter." + filter).addClass("teal");
        var dataFilter            = getFilter();
        dataFilter['date_filter'] = filter;
        articlesDynamicChart.render('/stats/coverage-dynamic', dataFilter);
        impactDynamicChart.render('/stats/impact-dynamic', dataFilter);
        reachDynamicChart.render('/stats/reach-dynamic', dataFilter);
        netDynamicChart.render('/stats/net-dynamic', dataFilter);
    });

    $(document.body).on("click", ".information_filter", function() {
        var filter = $(this).attr("data-value");
        $("#general_information_filter").val(filter);
        $(".information_filter").removeClass("teal");
        $(".information_filter." + filter).addClass("teal");
        var dataFilter          = getFilter();
        dataFilter['filter_by'] = filter;
        coverageByTypeChart.render('/stats/coverage-by-type', dataFilter);
        coverageByListChart.render('/stats/coverage-by-list', dataFilter);
        coverageByCategories.render('/stats/coverage-by-categories', dataFilter);
        coverageByMainTonalityChart.render('/stats/coverage-by-main-tonality', dataFilter);
        speakerQuoteChart.render('/stats/speaker-quote', dataFilter);
        thirdSpeakerQuoteChart.render('/stats/third-speaker-quote', dataFilter);
        topProductsChart.render('/stats/top-products', dataFilter);
        topNewsbreakChart.render('/stats/top-newsbreaks', dataFilter);
        topEventsChart.render('/stats/top-events', dataFilter);
        topCampaignChart.render('/stats/top-campaigns', dataFilter);
        mentionsChart.render('/stats/mention-product-relation', dataFilter);
        desiredArticleChart.render('/stats/desired-articles', dataFilter);
        speakersCountChart.render('/stats/top-speakers-rating', dataFilter);
        subcategoriesChart.render('/stats/subcategories', dataFilter);
    });

    var articlesPieChart,
        keyMessagesBarChart,
        //securityIntelligenceBarChart,
        reachByRegionChart,
        coverageByTypeChart,
        coverageByListChart,
        coverageByCategories,
        coverageByMainTonalityChart,
        netByRegionChart,
        speakerQuoteChart,
        thirdSpeakerQuoteChart,
        topProductsChart,
        mentionsChart,
        desiredArticleChart,
        netDynamicChart,
        reachDynamicChart,
        articlesDynamicChart,
        impactDynamicChart,
        speakersCountChart,
        topNewsbreakChart,
        topCampaignChart,
        topEventsChart,
        indexesComparison,
        impactIndex,
        subcategoriesChart,

        options = {
            articles: {
                chart: {
                    renderTo: 'chart-articles',
                    type    : 'pie'
                },
                title: {
                    text: 'Number of articles'
                },
                legend: {
                    layout: "horizontal",
                    width: 500,
                    itemWidth: 250,
                    x: 40,
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: false
                    }
                },
                subtitle: {
                    useHTML: true
                },
                series: [
                    {name: 'Number of articles'}
                ]
            },
            impact_index: {
                chart: {
                    renderTo: 'impact-index',
                    type    : 'bar'
                },
                title: {
                    text: 'Impact index'
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    bar: {
                        colorByPoint: true
                    }
                },
                tooltip: {
                    enabled: true,
                },
                yAxis: {
                    title: {
                        text: ""
                    }
                },
                series: [
                    {}
                ]
            },
            key_meesages: {
                chart: {
                    renderTo: 'key_meesages',
                    type    : 'bar'
                },
                colors: ['#cccccc', '#1abc9c'],
                title: {
                    text: 'KEY MESSAGES'
                },
                subtitle: {
                    text: 'Messages pick up analysis'
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ""
                    }
                },
                legend: {
                    reversed: true
                },
                tooltip: {
                    enabled: true,
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}{point.format}</b><br/>'
                },
                plotOptions: {
                    series: {
                        showInLegend: false,
                        stacking: 'percent',
                        dataLabels: {
                            enabled: true,
                            format: null,
                            formatter: function() {
                                if (100000 < this.point.y) {
                                    return (this.point.y / 1000000).toFixed(2) + 'M';
                                } else {
                                    return this.point.y;
                                }
                            }
                        },
                    },
                },
                series: []
            },/*
            security_intelligence: {
                chart: {
                    renderTo: 'security_intelligence',
                    type    : 'bar'
                },
                colors: ['#cccccc', '#1abc9c'],
                title: {
                    text: 'Security Intelligence'
                },
                subtitle: {
                    text: ''
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ""
                    }
                },
                legend: {
                    reversed: true
                },
                tooltip: {
                    enabled: true,
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}{point.format}</b><br/>'
                },
                plotOptions: {
                    series: {
                        showInLegend: false,
                        stacking: 'percent',
                        dataLabels: {
                            enabled: true,
                            format: null,
                            formatter: function() {
                                if (100000 < this.point.y) {
                                    return (this.point.y / 1000000).toFixed(2) + 'M';
                                } else {
                                    return this.point.y;
                                }
                            }
                        }
                    }
                },
                series: []
            },*/
            reach: {
                chart: {
                    renderTo: 'chart-reach',
                    type    : 'pie'
                },
                title: {
                    text: 'Media Outreach'
                },
                subtitle: {
                    useHTML: true
                },
                legend: {
                    layout: "horizontal",
                    width: 500,
                    itemWidth: 250,
                    x: 40,
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null,
                            useHTML: true,
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                                style: {textShadow: 'none'}
                            }
                        },
                        allowPointSelect: false
                    }
                },
                series: [
                    {name: 'Media outreach'}
                ]
            },
            types: {
                chart: {
                    renderTo: 'chart-media-type',
                    type    : 'pie'
                },
                title: {
                    text: 'Media types'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null
                        },
                        allowPointSelect: false
                    }
                },
                series: [
                    {name: 'Media types'}
                ]
            },
            lists: {
                chart: {
                    renderTo: 'chart-media-list',
                    type    : 'pie'
                },
                title: {
                    text: 'Media lists'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null
                        },
                        allowPointSelect: false
                    }
                },
                series: [
                    {name: 'Media lists'}
                ]
            },
            categories: {
                chart: {
                    renderTo: 'chart-media-category',
                    type    : 'pie'
                },
                title: {
                    text: 'Media categories'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null
                        },
                        allowPointSelect: false
                    }
                },
                series: [
                    {name: 'Media categories'}
                ]
            },
            subcategories: {
                chart: {
                    renderTo: 'chart-subcategories',
                    type    : 'pie'
                },
                title: {
                    text: 'Subcategories'
                },
                subtitle: {
                    useHTML: true
                },
                legend: {
                    layout: "horizontal",
                    width: 500,
                    itemWidth: 250,
                    x: 40,
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null,
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        },
                        allowPointSelect: false
                    }
                },
                series: [
                    {name: "Subcategories"}
                ]
            },
            net: {
                chart: {
                    renderTo: 'chart-net-effect',
                    type    : 'pie'
                },
                title: {
                    text: 'Net Effect'
                },
                subtitle: {
                    useHTML: true
                },
                legend: {
                    layout: "horizontal",
                    width: 500,
                    itemWidth: 250,
                    x: 40,
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null,
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        },
                        allowPointSelect: false
                    }
                },
                series: [
                    {name: "Net effect"}
                ]
            },
            main_tonality: {
                chart: {
                    renderTo: 'chart-main-tonality',
                    type    : 'pie'
                },
                title: {
                    text: 'Main tonality'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null
                        },
                        allowPointSelect: false
                    }
                },
                series: [
                    {name: 'Main tonality'}
                ]
            },
            speaker_quote: {
                chart: {
                    renderTo: 'chart-speaker-quote',
                    type    : 'pie'
                },
                title: {
                    text: 'Speakers quoted'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null
                        },
                        allowPointSelect: false
                    }
                },
                series: [
                    {name: 'Speakers quoted'}
                ]
            },
            third_speaker_quote: {
                chart: {
                    renderTo: 'chart-third-speaker-quote',
                    type    : 'pie'
                },
                title: {
                    text: '3rd party Speakers'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null
                        },
                        allowPointSelect: false
                    }
                },
                series: [
                    {name: 'Third-party speakers quoted'}
                ]
            },
            products: {
                chart: {
                    renderTo: 'chart-products',
                    type    : 'bar'
                },
                title: {
                    text: 'Products rating'
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: null,
                            formatter: function() {
                                if (100000 < this.point.y) {
                                    return (this.point.y / 1000000).toFixed(2) + 'M';
                                } else {
                                    return this.point.y;
                                }
                            }
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: "Mentions"
                    }
                },
                series: [
                    {}
                ]
            },
            news_break: {
                chart: {
                    renderTo: 'chart-newsbreaks',
                    type    : 'bar'
                },
                title: {
                    text: 'Newsbreaks Rating'
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: null,
                            formatter: function() {
                                if (100000 < this.point.y) {
                                    return (this.point.y / 1000000).toFixed(2) + 'M';
                                } else {
                                    return this.point.y;
                                }
                            }
                        }
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    enabled: true,
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}{point.format}</b><br/>'
                },
                yAxis: {
                    title: {
                        text: "Mentions"
                    }
                },
                series: [
                    {}
                ]
            },
            event: {
                chart: {
                    renderTo: 'chart-events',
                    type    : 'bar'
                },
                title: {
                    text: 'Events rating'
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: null,
                            formatter: function() {
                                if (100000 < this.point.y) {
                                    return (this.point.y / 1000000).toFixed(2) + 'M';
                                } else {
                                    return this.point.y;
                                }
                            }
                        }
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    enabled: true,
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}{point.format}</b><br/>'
                },
                yAxis: {
                    title: {
                        text: "Mentions"
                    }
                },
                series: [
                    {}
                ]
            },
            campaign: {
                chart: {
                    renderTo: 'chart-campaign',
                    type    : 'bar'
                },
                title: {
                    text: 'Campaigns rating'
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: null,
                            formatter: function() {
                                if (100000 < this.point.y) {
                                    return (this.point.y / 1000000).toFixed(2) + 'M';
                                } else {
                                    return this.point.y;
                                }
                            }
                        }
                    }
                },
                tooltip: {
                    enabled: true,
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}{point.format}</b><br/>'
                },
                yAxis: {
                    title: {
                        text: "Mentions"
                    }
                },
                series: [
                    {}
                ]
            },
            speakers_count: {
                chart: {
                    renderTo: 'chart-speakers-count',
                    type    : 'bar'
                },
                title: {
                    text: 'Speakers rating'
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: null,
                            formatter: function() {
                                if (100000 < this.point.y) {
                                    return (this.point.y / 1000000).toFixed(2) + 'M';
                                } else {
                                    return this.point.y;
                                }
                            }
                        }
                    }
                },
                tooltip: {
                    enabled: true,
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}{point.format}</b><br/>'
                },
                yAxis: {
                    title: {
                        text: ""
                    }
                },
                series: [
                    {}
                ]
            },
            mention: {
                chart: {
                    renderTo: 'chart-mention-relation',
                    type: 'pie'
                },
                title: {
                    text: 'Products mentions'
                },
                legend: {
                    labelFormatter: function() {
                        if (100000 < this.y) {
                            return '<b>' + this.name + '</b>: ' + this.percentage.toFixed(1) + ' % (' + (this.y / 1000000).toFixed(1) + 'M) Impact Index: ' + this.avg_index;
                        } else {
                            return '<b>' + this.name + '</b>: ' + this.percentage.toFixed(1) + ' % (' + this.y + ') Impact Index: ' + this.avg_index;
                        }
                    },
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null,
                            formatter: function() {
                                if (100000 < this.point.y) {
                                    return '<b>' + this.point.name + '</b>:<br>' + this.point.percentage.toFixed(1) + ' % (' + (this.point.y / 1000000).toFixed(2) + 'M)<br>Impact Index: ' + this.point.avg_index;
                                } else {
                                    return '<b>' + this.point.name + '</b>:<br>' + this.point.percentage.toFixed(1) + ' % (' + this.point.y + ')<br>Impact Index: ' + this.point.avg_index;
                                }
                            }
                        },
                        allowPointSelect: false
                    }
                },
                series: [
                    {name: 'Products mentions in article'}
                ]
            },

            desired: {
                chart: {
                    renderTo: 'chart-desired-article',
                    type    : 'pie'
                },
                title: {
                    text: 'Desired/Undesired articles'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null
                        },
                        allowPointSelect: false
                    }
                },
                series: [
                    {name: 'Desired articles'}
                ]
            },

            articles_dynamic: {
                global: {
                    useUTC: false
                },
                colors: ['#00c4bc', '#d35400', '#f1c40f', '#9b59b6', '#27ae60', '#34495e', '#e74c3c'],
                chart: {
                    renderTo: 'chart-articles-dynamic',
                    type    : 'area'
                },
                title: {
                    text: 'Number of Articles Dynamics'
                },
                xAxis: {
                    gridLineWidth: 0.5,
                    gridZIndex: 10,
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        month: '%B %Y'
                    }
                },
                yAxis: {
                    gridLineWidth: 0.5,
                    gridZIndex: 10,
                    title: {
                        text: null
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    enabled: true,
                    pointFormatter: function () {
                        if (typeof this.newsbreak !== 'undefined' && this.newsbreak !== null) {
                            return this.newsbreak+'<b>: '+this.total+'</b> / <b>'+this.ne+'M</b><br>'+this.series.name+': <b>'+this.y+'</b>';
                        }
                        return 'No significant newsbreaks<br>' + this.series.name+': <b>'+this.y+'</b>';
                    }
                },
                plotOptions: {
                    area: {
                        fillOpacity: 0.5
                    }
                },
                series: [
                    {}
                ]
            },
            impact_dynamic: {
                global: {
                    useUTC: false
                },
                colors: ['#00c4bc', '#d35400', '#f1c40f', '#9b59b6', '#27ae60', '#34495e', '#e74c3c'],
                chart: {
                    renderTo: 'chart-impact-dynamic',
                    type    : 'area'
                },
                title: {
                    text: 'Impact Index Dynamics'
                },
                xAxis: {
                    gridLineWidth: 0.5,
                    gridZIndex: 10,
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        month: '%B %Y'
                    }
                },
                yAxis: {
                    gridLineWidth: 0.5,
                    gridZIndex: 10,
                    title: {
                        text: null
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    enabled: true,
                    pointFormatter: function () {
                        if (typeof this.newsbreak !== 'undefined' && this.newsbreak !== null) {
                            return this.newsbreak+'<b>: '+this.total+'</b> / <b>'+this.ne+'M</b><br>'+this.series.name+': <b>'+this.y+'</b>';
                        }
                        return 'No significant newsbreaks<br>' + this.series.name+': <b>'+this.y+'</b>';
                    }
                },
                plotOptions: {
                    area: {
                        fillOpacity: 0.5
                    }
                },
                series: [
                    {}
                ]
            },
            reach_dynamic: {
                global: {
                    useUTC: false
                },
                colors: ['#00c4bc', '#d35400', '#f1c40f', '#9b59b6', '#27ae60', '#34495e', '#e74c3c'],
                chart: {
                    renderTo: 'chart-reach-dynamic',
                    type    : 'area'
                },
                title: {
                    text: 'Media Outreach Dynamics'
                },
                xAxis: {
                    gridLineWidth: 0.5,
                    gridZIndex: 10,
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        month: '%B %Y'
                    }
                },
                yAxis: {
                    gridLineWidth: 0.5,
                    gridZIndex: 10,
                    title: {
                        text: null
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        fillOpacity: 0.5
                    }
                },
                tooltip: {
                    enabled: true,
                    pointFormatter: function () {
                        var reach = this.y / 1000000;
                        reach = Math.round(reach * 10) / 10;
                        if (typeof this.newsbreak !== 'undefined' && this.newsbreak !== null) {
                            return this.newsbreak+'<b>: '+this.total+'</b> / <b>'+this.ne+'M</b><br>'+this.series.name+': <b>'+reach+'M</b>';
                        }
                        return 'No significant newsbreaks<br>' + this.series.name+': <b>'+reach+'M</b>';
                    }
                },
                series: [
                    {}
                ]
            },
            net_dynamic: {
                global: {
                    useUTC: false
                },
                colors: ['#00c4bc', '#d35400', '#f1c40f', '#9b59b6', '#27ae60', '#34495e', '#e74c3c'],
                chart: {
                    renderTo: 'chart-net-dynamic',
                    type    : 'area'
                },
                title: {
                    text: 'Net Effect Dynamics'
                },
                xAxis: {
                    gridLineWidth: 0.5,
                    gridZIndex: 10,
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        month: '%B %Y'
                    }
                },
                yAxis: {
                    gridLineWidth: 0.5,
                    gridZIndex: 10,
                    title: {
                        text: null
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        fillOpacity: 0.5
                    }
                },
                tooltip: {
                    enabled: true,
                    pointFormatter: function () {
                        var net = this.y / 1000000;
                        net = Math.round(net * 10) / 10;
                        if (typeof this.newsbreak !== 'undefined' && this.newsbreak !== null) {
                            return this.newsbreak+'<b>: '+this.total+'</b> / <b>'+this.ne+'M</b><br>'+this.series.name+': <b>'+net+'M</b>';
                        }
                        return 'No significant newsbreaks<br>' + this.series.name+': <b>'+net+'M</b>';
                    }
                },
                series: [{}]
            },
            indexes_comparison: {
                chart: {
                    type     : 'bubble',
                    renderTo : 'chart-indexes-comparison'
                },
                plotOptions: {
                    bubble: {
                        cursor: 'default',
                        animation: false,
                        threshold: 0,
                        dataLabels: {
                            enabled: true,
                            formatter:function() {
                                return this.point.title;
                            }
                        }
                    }
                },
                xAxis: {
                    title: {
                        text: 'Impact Index'
                    }
                },
                yAxis: {
                    title: {
                        text: 'Media Outreach'
                    }
                },
                legend: {
                    enabled: is_pdf ? true : false,
                    layout: "horizontal",
                    width: 500,
                    itemWidth: 250,
                    labelFormatter: function() {
                        return '<b>' + this.name + '</b>' ;
                    }
                },
                title: {
                    text : '3 Indexes comparison',
                    margin: 35
                },
                tooltip: {
                    enabled: true,
                    pointFormatter: function () {
                        var net_effect     = this.z / 1000000;
                        var media_outreach = this.y / 1000000;
                        net_effect         = Math.round(net_effect * 10) / 10;
                        media_outreach     = Math.round(media_outreach * 10) / 10;
                        return 'Net Effect: <b>' + net_effect + 'M</b>' +
                            '<br/>Media Outreach: <b>' + media_outreach + 'M</b><br/>' +
                            'Impact Index: <b>' + this.x + '</b>';
                    }
                },
                subtitle: {
                    text: 'The size of bubbles is defined by Net Effect values'
                },
                exporting: {
                    buttons: {
                        xlsButton : {
                            onclick : function() {
                                var data = [["Region", "Net Effect", "Media Outreach", "Impact Index"]];
                                $.each(this.series, function(key, series) {
                                    data.push([series.data[0].title, series.data[0].z, series.data[0].y, series.data[0].x]);
                                });
                                this.exportChart({
                                    type     : Highcharts.exporting.MIME_TYPES.XLS,
                                    filename : this.options.title.text + getFileName()
                                }, {
                                    dataRows: data
                                });
                            }
                        }
                    }
                },
                series: [{}]
            }
        };

    var Vis = function (options, dataHandle) {
        this.chartOptions = options || {};
        this.dataHandle   = dataHandle || null;
        this.chart        = new Highcharts.Chart(options);
    };

    Vis.prototype = {
        render: function (url, params, reflow) {
            var vis = this;

            $.ajax({
                url     : url,
                method  : 'GET',
                dataType: 'json',
                data    : params || null,
                success: function (data) {

                    if (vis.dataHandle) {
                        data = vis.dataHandle(data, vis.chart);
                    }

                    if (typeof data.series !== 'undefined') {
                        var seriesLength = vis.chart.series.length;
                        for(var i = seriesLength -1; i > -1; i--) {
                            vis.chart.series[i].remove(false);
                        }
                        if (typeof data.categories !== 'undefined' && data.categories.length > 0) {
                            vis.chart.xAxis[0].setCategories(data.categories);
                        }
                        $.each(data.series, function(key, seriesData) {
                            vis.chart.addSeries(seriesData);
                        });
                    }
                    else {
                        vis.chart.series[0].setData(data);
                    }

                    if (true === reflow) {
                        vis.chart.reflow();
                    }

                    if (is_pdf) {
                        if ((vis.chartOptions.chart.renderTo == 'impact-index' && data.length > maxLegendCount) ||
                            (vis.chartOptions.chart.renderTo == 'key_meesages' && data.categories.length > maxLegendCount) ||
                            (vis.chartOptions.chart.renderTo == 'chart-net-effect' && data.length > maxLegendCount) ||
                            (vis.chartOptions.chart.renderTo == 'chart-reach' && data.length > maxLegendCount) ||
                            (vis.chartOptions.chart.renderTo == 'chart-articles' && data.length > maxLegendCount)) {
                                $('#' + vis.chartOptions.chart.renderTo).css('height', 800);
                                vis.chart.setSize(600, 800);
                        }
                    }

                }
            });

            return this;
        }
    };

    // Highcharts default options
    Highcharts.setOptions({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth    : 0,
            plotShadow         : false,
            style: { 'overflow': 'visible' }
        },
        credits: {
            enabled: false
        },
        lang: {
            numericSymbols: [ "k" , "M" , "B" , "T" , "P" , "E"],
            downloadPng: "Download PNG Image",
            downloadXls: "Download XLS File"
        },
        tooltip: {
            enabled: false
        },
        colors: ['#1abc9c', '#d35400', '#f1c40f', '#9b59b6', '#27ae60', '#34495e', '#e74c3c'],
        legend: {
            labelFormatter: function() {
                var percentage = "";
                if (typeof this.percentage !== 'undefined') {
                    percentage = this.percentage.toFixed(1) + '% ';
                }
                if (typeof this.y === 'undefined') {
                    return '<b>' + this.name + '</b> ' + percentage;
                }
                if (100000 < this.y) {
                    return '<b>' + this.name + '</b> ' + percentage + '(' + (this.y / 1000000).toFixed(1) + 'M)';
                } else {
                    return '<b>' + this.name + '</b> ' + percentage + '(' + this.y + ')';
                }
            },
            layout: "vertical"
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>:<br>{point.percentage:.1f} % ({point.y})',
                    formatter: function() {
                        if (100000 < this.point.y) {
                            return '<b>' + this.point.name + '</b>:<br>' + this.point.percentage.toFixed(1) + ' % (' + (this.point.y / 1000000).toFixed(2) + 'M)';
                        } else {
                            return '<b>' + this.point.name + '</b>:<br>' + this.point.percentage.toFixed(1) + ' % (' + this.point.y + ')';
                        }
                    },
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true,
            },
            bar: {
                dataLabels: {
                    enabled: true
                }
            },
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: '{point.x} cm, {point.y} kg'
                }
            },
            area: {
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, 'rgba(0, 196, 188, 0.4)'],
                        [1, 'rgba(0, 196, 188, 0.1)'],
                    ]
                },
            }
        },
        exporting: {
            scale: 1,
            buttons: {
                contextButton: {
                    enabled: false
                },
                xlsButton: {
                    symbol: 'url(../img/xls-min.png)',
                    onclick: function () {
                        this.exportChart({
                            type: Highcharts.exporting.MIME_TYPES.XLS,
                            filename: this.options.title.text + getFileName()
                        });
                    },
                    _titleKey: "downloadXls",
                    theme: {
                        'stroke-width': 0,
                        states: {
                            hover: {
                                fill: 'transparent'
                            },
                            select: {
                                fill: '#000'
                            }
                        }
                    },
                    x: 0,
                    y: -70
                },
                pngButton: {
                    symbol: 'url(../img/png-min.png)',
                    onclick: function () {
                        this.exportChart({
                            type: Highcharts.exporting.MIME_TYPES.PNG,
                            filename: this.options.title.text + getFileName()
                        });
                    },
                    _titleKey: "downloadPng",
                    theme: {
                        'stroke-width': 0,
                        states: {
                            hover: {
                                fill: 'transparent'
                            },
                            select: {
                                fill: '#000'
                            }
                        }
                    },
                    x: -24,
                    y: -70
                }
            }
        },
        title: {
            style: {"color":  "#646388"}
        }
    });

    if (!is_pdf) {
        indexesComparison = new Vis(options.indexes_comparison, function (data, chart) {
            var result     = [];
            data.forEach(function (v, i, a) {
                result.push({'name': v.name, 'data': [{ title: v.name, x: parseFloat(v.impact_index),
                    y: parseFloat(v.media_outreach), z: parseFloat(v.net_effect) }]});
            });

            return {'series': result};
        });
    } else {
        var indexesComparisonPdf = function() {
            var filter = getFilter();
            filter['filter_by'] = $("#general_information_filter").val();

            $.ajax({
                url     : '/stats/indexes-comparison',
                method  : 'GET',
                dataType: 'json',
                data : filter || null,
                success: function (data) {
                    var offset = 10;
                    var n = Math.ceil(data.length / offset);

                    for (var i = 0; i < n; i++) {
                        var id              = 'chart-indexes-comparison-' + i,
                            local_options   = options.indexes_comparison,
                            result          = [];

                        data.slice(i * offset, (i+1) * offset).forEach(function (v, i, a) {
                            result.push({
                                'name': (i+1) + ' - ' + v.name + ' (' + Math.round(parseFloat(v.net_effect) / 1000000 * 10) / 10 + 'M)',
                                'data': [{ 'title': i+1, 'x': parseFloat(v.impact_index), 'y': parseFloat(v.media_outreach), 'z': parseFloat(v.net_effect) }]
                            });
                        });

                        local_options.chart.renderTo = id;
                        local_options.series = result;

                        $('#chart-indexes-comparison').append('<center><div id="' + id + '" class="chart-nest bubble" style="width:100%;"></div></center>');

                        var chart = new Highcharts.Chart(local_options);
                    }
                }
            });
        }

    }

    articlesPieChart = new Vis(options.articles, function (data, chart) {
        var count = 0;

        data.forEach(function (v, i, a) {
            a[i] = { name: v.name, y: parseFloat(v.total) };
            count += parseFloat(v.total);
        });

        if (is_pdf && data.length > maxLegendCount) {
            var block = $('#' + options.articles.chart.renderTo);
            block.closest('.column').removeClass('auto-break');
            block.closest('.column').css({'page-break-before': 'always'});
        }

        return data;
    });

    keyMessagesBarChart = new Vis(options.key_meesages, function (data, chart) {
        var result     = {'without': {'name': 'Without messages', 'data': []}, 'with': {'name': 'With messages', 'data': []}};
        var categories = [];
        data.forEach(function (v, i, a) {
            result['without'].data.push({ name: v.name, y: parseFloat(v.without_messages) });
            result['with'].data.push({ name: v.name, y: parseFloat(v.with_messages) });
            categories.push(v.name);
        });
        if (is_pdf && data.length > maxLegendCount) {
            var block = $('#' + options.key_meesages.chart.renderTo);
            block.closest('.column').removeClass('auto-break');
            block.closest('.column').css({'page-break-before': 'always'});
        }
        return {'series': result, 'categories': categories};
    });
    /*
    securityIntelligenceBarChart = new Vis(options.security_intelligence, function (data, chart) {
        var result     = {'without': {'name': 'Without Security Intelligence', 'data': []}, 'with': {'name': 'With Security Intelligence', 'data': []}};
        var categories = [];
        data.forEach(function (v, i, a) {
            result['without'].data.push({ name: v.name, y: parseFloat(v.without_security_intelligence) });
            result['with'].data.push({ name: v.name, y: parseFloat(v.with_security_intelligence) });
            categories.push(v.name);
        });
        return {'series': result, 'categories': categories};
    });*/

    reachByRegionChart = new Vis(options.reach, function (data, chart) {
        var count = 0;

        data.forEach(function (v, i, a) {
            a[i] = { name: v.name, y: parseFloat(v.reach) };

            if (v.reach > 0) {
                count = parseFloat(v.reach + count);
            }
        });

        if (is_pdf && data.length > maxLegendCount) {
            var block = $('#' + options.reach.chart.renderTo);
            block.closest('.column').removeClass('auto-break');
            block.closest('.column').css({'page-break-before': 'always'});
        }

        return data;
    });

    coverageByTypeChart = new Vis(options.types, function (data, chart) {
        data.forEach(function (v, i, a) {
            var map = {
                'online': 'Online',
                'print' : 'Print',
                'agency': 'Agency',
                'tv'    : 'TV',
                'other' : 'Not Present',
                'radio' : 'Radio'
            };

            if (v.media_type === null) {
                v.media_type = 'other';
            }

            a[i] = {
                name: map[v.media_type],
                y: parseFloat(v.total),
                format: v.format
            };
        });

        return data;
    });

    coverageByListChart = new Vis(options.lists, function (data, chart) {
        data.forEach(function (v, i, a) {
            var map = {
                1      : 'Diamond',
                0.8    : 'Platinum',
                0.6    : 'Gold',
                'other': 'Other'
            };
            if (v.media_list === null) {
                v.media_list = 'other';
            } else {
                v.media_list = parseFloat(v.media_list);
            }
            a[i] = { name: map[v.media_list], y: parseFloat(v.total), format: v.format };
        });

        return data;
    });

    coverageByCategories = new Vis(options.categories, function (data) {
        data.forEach(function (v, i, a) {
            var map = {
                'it': 'IT',
                'vertical': 'Vertical',
                'general' : 'General',
                'business': 'Business',
                'other'   : 'Other'
            };

            if (v.media_category === null) {
                v.media_category = 'other';
            }

            a[i] = {
                name: map[v.media_category],
                y: parseFloat(v.total),
                format: v.format
            };
        });

        return data;
    });

    netByRegionChart = new Vis(options.net, function (data, chart) {
        var count = 0;

        data.forEach(function (v, i, a) {
            a[i] = {  name: v.name, y: parseFloat(v.net) };

            if (v.net > 0) {
                count = parseFloat(v.net + count);
            }
        });

        if (is_pdf && data.length > maxLegendCount) {
            var block = $('#' + options.net.chart.renderTo);
            block.closest('.column').removeClass('auto-break');
        }

        return data;
    });

    coverageByMainTonalityChart = new Vis(options.main_tonality, function (data) {
        data.forEach(function (v, i, a) {
            var map = {
                'positive': 'Positive',
                'negative': 'Negative',
                'neutral' : 'Neutral'
            };

            a[i] = { name: map[v.main_tonality], y: parseFloat(v.total), format: v.format };
        });

        return data;
    });

    speakerQuoteChart = new Vis(options.speaker_quote, function (data) {
        data.forEach(function (v, i, a) {
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format};
        });

        return data;
    });

    thirdSpeakerQuoteChart = new Vis(options.third_speaker_quote, function (data) {
        data.forEach(function (v, i, a) {
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format };
        });

        return data;
    });

    topProductsChart = new Vis(options.products, function (data, chart) {
        var p = [];
        data.forEach(function (v, i, a) {
            p.push(v.name);
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format };
        });
        chart.xAxis[0].setCategories(p);
        chart.series[0].name = $("#general_information_filter").val() == 'net_effect' ? 'Net Effect' : 'Articles';

        return data;
    });

    topNewsbreakChart = new Vis(options.news_break, function (data, chart) {
        var p = [];
        data.forEach(function (v, i, a) {
            p.push(v.name);
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format };
        });
        chart.xAxis[0].setCategories(p);
        chart.series[0].name = $("#general_information_filter").val() == 'net_effect' ? 'Net Effect' : 'Articles';
        return data;
    });

    topEventsChart = new Vis(options.event, function (data, chart) {
        var p = [];
        data.forEach(function (v, i, a) {
            p.push(v.name);
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format };
        });
        chart.xAxis[0].setCategories(p);
        chart.series[0].name = $("#general_information_filter").val() == 'net_effect' ? 'Net Effect' : 'Articles';
        return data;
    });

    topCampaignChart = new Vis(options.campaign, function (data, chart) {
        var p = [];
        data.forEach(function (v, i, a) {
            p.push(v.name);
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format };
        });
        chart.xAxis[0].setCategories(p);
        chart.series[0].name = $("#general_information_filter").val() == 'net_effect' ? 'Net Effect' : 'Articles';
        return data;
    });

    mentionsChart = new Vis(options.mention, function (data, chart) {
        var avg = [];
        data.forEach(function (v, i, a) {
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format, avg_index: v.avg_index };
            avg.push(v.avg_index);
        });
        return data;
    });

    desiredArticleChart = new Vis(options.desired, function (data, chart) {
        data.forEach(function (v, i, a) {
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format };
        });
        return data;
    });

    speakersCountChart = new Vis(options.speakers_count, function (data, chart) {
        var p = [];
        data.forEach(function (v, i, a) {
            p.push(v.name);
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format };
        });
        chart.xAxis[0].setCategories(p);
        chart.series[0].name = $("#general_information_filter").val() == 'net_effect' ? 'Net Effect' : 'Articles';
        return data;
    });

    impactIndex = new Vis(options.impact_index, function (data, chart) {
        var p = [];
        data.by_region.forEach(function (v, i, a) {
            p.push(v.name);
            a[i] = { name: v.name, y: parseFloat(v.avg_index) };
        });
        chart.xAxis[0].setCategories(p);
        chart.series[0].name = "Impact Index";
        chart.setTitle(null, { text: 'Average: ' + data.common});
        if (is_pdf &&  data.by_region.length > maxLegendCount) {
            var block = $('#' + options.impact_index.chart.renderTo);
            block.closest('.column').removeClass('auto-break');
            block.closest('.column').css({'page-break-before': 'always'});
        }

        return data.by_region;
    });

    /** Dynamic charts **/
    netDynamicChart = new Vis(options.net_dynamic, function (data, chart) {
        data.forEach(function (v, i, a) {
            a[i] = { x: parseInt(v.date), y: parseFloat(v.monthly_net), total: parseFloat(v.total), newsbreak: v.name, ne: v.net_effect };
        });

        chart.series[0].name = 'Net Effect';
        return data;
    });

    reachDynamicChart = new Vis(options.reach_dynamic, function (data, chart) {
        data.forEach(function (v, i, a) {
            a[i] = { x: parseInt(v.date), y: parseFloat(v.monthly_reach), total: parseFloat(v.total), newsbreak: v.name, ne: v.net_effect };
        });

        chart.series[0].name = 'Reach';
        return data;
    });

    articlesDynamicChart = new Vis(options.articles_dynamic, function (data, chart) {
        data.forEach(function (v, i, a) {
            a[i] = { x: parseInt(v.date), y: parseFloat(v.count), total: parseFloat(v.total), newsbreak: v.name, ne: v.net_effect };
        });

        chart.series[0].name = 'Articles';
        return data;
    });

    impactDynamicChart = new Vis(options.impact_dynamic, function (data, chart) {
        data.forEach(function (v, i, a) {
            a[i] = { x: parseInt(v.date), y: parseFloat(v.monthly_impact_index), total: parseFloat(v.total), newsbreak: v.name, ne: v.net_effect };
        });

        chart.series[0].name = 'Impact Index';
        return data;
    });

    subcategoriesChart = $('#chart-subcategories').length > 0 ? new Vis(options.subcategories, function (data, chart) {
        data.forEach(function (v, i, a) {
            // a[i] = { x: parseInt(v.date), y: parseFloat(v.count), total: parseFloat(v.total), newsbreak: v.name, ne: v.net_effect };
            a[i] = {  name: v.title, y: parseFloat(v.coverage_cnt), id: v.id };
        });

        chart.series[0].name = 'Subcategories';
        return data;
    }) : false;
    

    $('.menu.charts .item').tab({
        history: 'hash',
        onLoad: function (path) {
            var filter = getFilter();
            if (!is_pdf) {
                if (path === 'dynamics') {
                    filter['date_filter'] = $("#timeline_filter").val();
                    articlesDynamicChart.render('/stats/coverage-dynamic', filter, true);
                    impactDynamicChart.render('/stats/impact-dynamic', filter, true);
                    reachDynamicChart.render('/stats/reach-dynamic', filter, true);
                    netDynamicChart.render('/stats/net-dynamic', filter, true);
                }
                if (path === 'general_information' || is_pdf) {
                    filter['filter_by'] = $("#general_information_filter").val();
                    coverageByTypeChart.render('/stats/coverage-by-type', filter, true);
                    coverageByListChart.render('/stats/coverage-by-list', filter, true);
                    coverageByCategories.render('/stats/coverage-by-categories', filter, true);
                    mentionsChart.render('/stats/mention-product-relation', filter, true);
                    speakerQuoteChart.render('/stats/speaker-quote', filter, true);
                    thirdSpeakerQuoteChart.render('/stats/third-speaker-quote', filter, true);
                    topProductsChart.render('/stats/top-products', filter, true);
                    topNewsbreakChart.render('/stats/top-newsbreaks', filter, true);
                    topEventsChart.render('/stats/top-events', filter, true);
                    topCampaignChart.render('/stats/top-campaigns', filter, true);
                    desiredArticleChart.render('/stats/desired-articles', filter, true);
                    speakersCountChart.render('/stats/top-speakers-rating', filter, true);
                    subcategoriesChart.render('/stats/subcategories', filter, true);
                    //securityIntelligenceBarChart.render('/stats/security-intelligence', filter, true);
                }
                if (path === 'executive_summary') {
                    keyMessagesTotal(filter);
                    articlesPieChart.render('/stats/coverage-by-region', filter, true);
                    impactIndex.render('/stats/avg-index-by-region', filter, true);
                    reachByRegionChart.render('/stats/reach-by-region', filter, true);
                    netByRegionChart.render('/stats/net-by-region', filter, true);
                    coverageByMainTonalityChart.render('/stats/coverage-by-main-tonality', filter, true);
                    filter['filter_by'] = $("#general_information_filter").val();
                    keyMessagesBarChart.render('/stats/key-messages', filter, true);
                    indexesComparison.render('/stats/indexes-comparison', filter, true);
                }
            } else {
                filter['date_filter'] = $("#timeline_filter").val();
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-articles-dynamic', filter['charts']) != -1) {
                    articlesDynamicChart.render('/stats/coverage-dynamic', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-impact-dynamic', filter['charts']) != -1) {
                    impactDynamicChart.render('/stats/impact-dynamic', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-reach-dynamic', filter['charts']) != -1) {
                    reachDynamicChart.render('/stats/reach-dynamic', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-net-dynamic', filter['charts']) != -1) {
                    netDynamicChart.render('/stats/net-dynamic', filter, true);
                }

                filter['filter_by'] = $("#general_information_filter").val();
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-media-type', filter['charts']) != -1) {
                    coverageByTypeChart.render('/stats/coverage-by-type', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-media-list', filter['charts']) != -1) {
                    coverageByListChart.render('/stats/coverage-by-list', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-media-category', filter['charts']) != -1) {
                    coverageByCategories.render('/stats/coverage-by-categories', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-mention-relation', filter['charts']) != -1) {
                    mentionsChart.render('/stats/mention-product-relation', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-speaker-quote', filter['charts']) != -1) {
                    speakerQuoteChart.render('/stats/speaker-quote', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-third-speaker-quote', filter['charts']) != -1) {
                    thirdSpeakerQuoteChart.render('/stats/third-speaker-quote', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-products', filter['charts']) != -1) {
                    topProductsChart.render('/stats/top-products', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-newsbreaks', filter['charts']) != -1) {
                    topNewsbreakChart.render('/stats/top-newsbreaks', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-events', filter['charts']) != -1) {
                    topEventsChart.render('/stats/top-events', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-campaign', filter['charts']) != -1) {
                    topCampaignChart.render('/stats/top-campaigns', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-desired-article', filter['charts']) != -1) {
                    desiredArticleChart.render('/stats/desired-articles', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-speakers-count', filter['charts']) != -1) {
                    speakersCountChart.render('/stats/top-speakers-rating', filter, true);
                }

                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-articles', filter['charts']) != -1) {
                    articlesPieChart.render('/stats/coverage-by-region', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('impact-index', filter['charts']) != -1) {
                    impactIndex.render('/stats/avg-index-by-region', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-reach', filter['charts']) != -1) {
                    reachByRegionChart.render('/stats/reach-by-region', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-net-effect', filter['charts']) != -1) {
                    netByRegionChart.render('/stats/net-by-region', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-main-tonality', filter['charts']) != -1) {
                    coverageByMainTonalityChart.render('/stats/coverage-by-main-tonality', filter, true);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-subcategories', filter['charts']) != -1) {
                    subcategoriesChart.render('/stats/subcategories', filter, true);
                }
                filter['filter_by'] = $("#general_information_filter").val();
                if ($.isEmptyObject(filter['charts']) || $.inArray('key_meesages', filter['charts']) != -1) {
                    keyMessagesBarChart.render('/stats/key-messages', filter, true);
                    getTotalKeyMessages(filter);
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-indexes-comparison', filter['charts']) != -1) {
                    indexesComparisonPdf();
                }
                if ($.isEmptyObject(filter['charts']) || $.inArray('chart-top-story', filter['charts']) != -1) {
                    getTopStoriesPdf(filter);
                }

                delete filter.comparison;
                getCovarageTotalsPdf(filter);

                $('#reports .tab').each(function(){
                    $(this).find('.auto-break.column:not(.hide)').each(function(index){
                        if (index != 0 && index%2 == 0) {
                            $(this).css('page-break-before', 'always');
                        }

                    });
                });
            }
        }
    });

    var keyMessagesTotal = function(filter) {
        $.ajax({
            url     : '/stats/total-key-messages',
            method  : 'GET',
            dataType: 'json',
            data    : filter || null,
            success: function (data) {
               if (typeof data !== 'undefined') {
                    $('#key_message_present').html(0);
                    $('#key_message_extent').html(0);
                    $('#key_message_counter').html(0);
                    $('#key_message_absent').html(0);

                    $.each(data, function(key, val) {
                        $('#key_message_' + key).html(val);
                    });
               }
            }
        });
    };
    highlightFilters();

    $(document).on('mouseenter', '.fixed-first-column-table-block', function() {
        $('.fixed-column-table tbody tr').hover(function(){
            var index = $(this).index() + 1;
            $(this).parents('.fixed-first-column-table-block')
                .find('.original-table-block table tbody tr:nth-child('+index+')')
                .css('background', 'rgba(0, 0, 0, 0.05)')
                .css('color', 'rgba(0, 0, 0, 0.95)');
        }, function(){
            var index = $(this).index() + 1;
            $(this).parents('.fixed-first-column-table-block')
                .find('.original-table-block table tbody tr:nth-child('+index+')')
                .css('background', 'rgba(0, 0, 0, 0)')
                .css('color', 'rgba(0, 0, 0, 0.8)');
        });

        $('.original-table-block tbody tr').hover(function(){
            var index = $(this).index() + 1;
            $(this).parents('.fixed-first-column-table-block')
                .find('.fixed-column-table tbody tr:nth-child('+index+')')
                .css('background', 'rgba(0, 0, 0, 0.05)')
                .css('color', 'rgba(0, 0, 0, 0.95)');
        }, function(){
            var index = $(this).index() + 1;
            $(this).parents('.fixed-first-column-table-block')
                .find('.fixed-column-table tbody tr:nth-child('+index+')')
                .css('background', 'rgba(0, 0, 0, 0)')
                .css('color', 'rgba(0, 0, 0, 0.8)');
        });
    })
});