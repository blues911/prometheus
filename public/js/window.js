$(function()
{
    var wh    = $(window).height();
    var ww    = $(window).width();
    var bh    = $('.loginformbg').outerHeight();
    var bw    = $('.loginformbg').outerWidth();
    var pos_y = wh/2-bh/2;
    var pos_x = ww/2-bw/2;
    $('.loginformbg').css('top', pos_y);
    $('.loginformbg').css('left',pos_x);


    [].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
        // in case the input is already filled..
        if( inputEl.value.trim() !== '' ) {
            classie.add( inputEl.parentNode, 'input--filled' );
        }

        // events:
        inputEl.addEventListener( 'focus', onInputFocus );
        inputEl.addEventListener( 'blur', onInputBlur );
    } );

    function onInputFocus( ev ) {
        classie.add( ev.target.parentNode, 'input--filled' );
    }

    function onInputBlur( ev ) {
        if( ev.target.value.trim() === '' ) {
            classie.remove( ev.target.parentNode, 'input--filled' );
        }
    }

});

$(window).on('resize', function()
{
    var wh    = $(window).height();
    var ww    = $(window).width();
    var bh    = $('.loginformbg').outerHeight();
    var bw    = $('.loginformbg').outerWidth();
    var pos_y = wh/2-bh/2;
    var pos_x = ww/2-bw/2;
    $('.loginformbg').css('top', pos_y);
    $('.loginformbg').css('left',pos_x);
});