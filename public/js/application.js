$(document).ready(function (){

    var coverageForm = $('#coverage-form'),
        notification = $('#notification'),
        menuItem = $('.menu.sections .item'),
        tabName = $('.menu.sections .active.item').attr('data-tab'),
        countriesLoaded = false,
        userId = user_id || "";

    if ($.inArray($("#media_type_field").val(), ["tv", "radio"]) !== -1) {
        $("#reach_field").removeAttr("disabled");
        $("#reach_icon").hide();
    } else {
        $("#reach_icon").show();
        $("#reach_field").attr("disabled", "disabled");
    }

    var message = {
        success: function () {
            $('.message.success').fadeIn();

            setTimeout(function () {
                $('.message.success').fadeOut();
            }, 1700);
        }
    };

    menuItem.tab({
        onLoad: function () {
            var item = $('.menu.sections .active.item');

            item.removeClass('disabled');

            if (!item.prev().hasClass('has-error')) {
            	item.prev().addClass('completed');
            }
        }
    });

    $('#backto-stage1').click(function () {
        menuItem.tab('change tab', 'general');
    });
    $('#goto-stage2').click(function () {
        var form  = $('.form.general');
        var rules = [
            {
                type  : 'empty',
                prompt: 'Empty field is not allowed'
            }
        ];
        var validators = {
            fields: {
                region: {
                    identifier: 'region',
                    rules     : rules
                },
                country: {
                    identifier: 'country',
                    rules     : rules
                },
                date: {
                    identifier: 'date',
                    rules     : [
                        {
                            type  : 'empty',
                            prompt: 'Empty field is not allowed'
                        },
                        {
                            type: 'is_coding_completed',
                            prompt: 'Coverage coding process in this month was completed'
                        }
                    ]
                },
                name: {
                    identifier: 'name',
                    rules     : rules
                },
                reach: {
                    identifier: 'reach',
                    rules     : rules
                },
                headline: {
                    identifier: 'headline',
                    rules     : rules
                },
                media_list: {
                    identifier: 'media_list',
                    rules     : rules
                },
                media_type: {
                    identifier: 'media_type',
                    rules     : rules
                },
                media_category: {
                    identifier: 'media_category',
                    rules     : rules
                }
            },
            inline: true,
            onSuccess: function () {
              $('.menu.sections .active.item').removeClass('has-error');
                menuItem.tab('change tab', 'analytical-pt1');
            }
        };

        $.fn.form.settings.rules.is_coding_completed = function (value) {
            var is_not_error = true;
            var format = value.split('-');
            var isAdmin = ($("meta[name=is_admin]").attr('content') == 'yes') ? true : false;

            format.pop();
            format = format.join('-');

            if (isAdmin == false && Object.keys(coding_closed_months).length > 0) {
                $.each(coding_closed_months, function(k, v){
                    if (format == v) {
                        is_not_error = false;
                        return true;
                    }
                });
            }

            return is_not_error;
        }

        form.form(validators);

        var result = form.form('validate form');

        if (result == false) {
        	$('.menu.sections .active.item').removeClass('completed').addClass('has-error');
        }
    });

    $('#backto-stage2').click(function () {
        menuItem.tab('change tab', 'analytical-pt1');
    });
    $('#goto-stage3').click(function () {
        var form  = $('.form.analytical-pt1');
        var rules = [
            {
                type  : 'empty',
                prompt: 'Empty field is not allowed'
            }
        ];
        var validators = {
            fields: {
                visibility: {
                    identifier: 'visibility',
                    rules     : rules
                },
                image: {
                    identifier: 'image',
                    rules     : rules
                },
                key_message_penetration: {
                    identifier: 'key_message_penetration',
                    rules     : rules
                },
                mention: {
                    identifier: 'mention',
                    rules     : rules
                }
            },
            inline: true,
            onSuccess: function () {
              $('.menu.sections .active.item').removeClass('has-error');
                menuItem.tab('change tab', 'analytical-pt2');
            }
        };

        form.form(validators);

        var result = form.form('validate form');
  
        if (result == false) {
        	$('.menu.sections .active.item').removeClass('completed').addClass('has-error');
        }
    });

    $('#backto-stage3').click(function () {
        menuItem.tab('change tab', 'analytical-pt2');
    });
    $('#goto-stage4').click(function () {
        var form  = $('.form.analytical-pt2');
        var rules = [
            {
                type  : 'empty',
                prompt: 'Empty field is not allowed'
            }
        ];
        var validators = {
        	fields: {
        		header_tonality: { identifier: 'header_tonality', rules: rules },
            	main_tonality: { identifier: 'main_tonality', rules: rules }
        	},
            inline: true,
            onSuccess: function () {
            	$('.menu.sections .active.item').removeClass('has-error');
                menuItem.tab('change tab', 'speakers');
            }
        };

        if (form.find('input[name="main_tonality"]').val() === 'negative' && !form.find('textarea').val()) {
            validators.fields.negative_explanation = {
                identifier: 'negative_explanation',
                rules     : rules
            };
        }

        if (form.find('.business input[name="business_id"]:checked').length < 1) {
            form.find('.business .ui.label').removeClass('hidden');
            return false;
        } else {
            form.find('.business .ui.label').addClass('hidden');
        }

        form.form(validators);

        var result = form.form('validate form');
        if (result == false) {
        	$('.menu.sections .active.item').removeClass('completed').addClass('has-error');
        }
    });

    $('#goto-stage5').click(function () {
        var form       = $('.form.speakers');
        var validators = {};
        var rules      = [
            {
                type  : 'empty',
                prompt: 'Empty field is not allowed'
            }
        ];

        validators = {
        	fields: {
        		speakers_quoted: {
	                identifier: 'speakers_quoted',
	                rules: [
	                    {
	                        type  : 'check_speakers',
	                        prompt: 'Field has an invalid value (no, none, n/a, na, not available, no name, no speaker)'
	                    },
	                    {
	                        type  : 'speakers_format',
	                        prompt: 'Speaker’s name has invalid format. Please, use "Firstname Lastname" one'
	                    },
	                    {
	                        type  : 'is_empty',
	                        prompt: 'Empty field is not allowed'
	                    }
	                ]
	            },
	            third_speakers_quoted: {
	                identifier: 'third_speakers_quoted',
	                rules: [
	                    {
	                        type  : 'empty',
	                        prompt: 'Empty field is not allowed'
	                    }
	                ]
	            }
        	},
        	inline: true,
            onSuccess: function () {

                function getField(form, name) {
                    return form.find('input[name="' + name + '"]').val() - 0;
                }

                var map = {
                        key_msg_pen: {
                            'absent' : 0,
                            'counter': 0,
                            'present': 1,
                            'extent' : 0.6
                        },
                        main_tonality : {
                            'negative': 0,
                            'positive': 1,
                            'neutral' : 1
                        }
                    },
                    keyMessagePenetration,
                    mainTonality,
                    form,
                    mediaType,
                    mention,
                    visibility,
                    speakersQuote,
                    image,
                    headTonality,
                    thirdPartySpeakers,
                    qualityFactor,
                    tonality,
                    impactIndex,
                    netEffect;

                form                  = $('#coverage-form');
                keyMessagePenetration = form.find('input[name="key_message_penetration"]').val();
                mainTonality          = form.find('input[name="main_tonality"]').val();
                keyMessagePenetration = map.key_msg_pen[keyMessagePenetration];
                mainTonality          = map.main_tonality[mainTonality];
                mediaType             = getField(form, 'media_list');
                mention               = getField(form, 'mention');
                visibility            = getField(form, 'visibility');
                speakersQuote         = $('#speakers_quoted').val() != '[]' ? 1 : 0.8;
                image                 = getField(form, 'image');
                headTonality          = getField(form, 'header_tonality');
                thirdPartySpeakers    = $('#third_speakers_quoted').val() ? 1 : 0;
                qualityFactor         = mediaType * mention * visibility * speakersQuote * image;
                tonality              = headTonality * mainTonality;
                impactIndex           = keyMessagePenetration * 0.2 + qualityFactor * tonality * 0.71 + thirdPartySpeakers * 0.09;
                netEffect             = impactIndex * getField(form, 'reach');

                form.find('input[name="impact_index"]').val(impactIndex).change();
                $('#impact_index').text(Number(impactIndex).toFixed(2));
                // Insert net effect
                form.find('input[name="net_effect"]').val(netEffect).change();
                netEffect = Math.round((netEffect / 1000000) * 100) / 100;
                $('#net_effect').text(netEffect + 'M');

                $('.menu.sections .active.item').removeClass('has-error');
                menuItem.tab('change tab', 'impact');
                $('.global-submit').css('opacity', 1);
            }
        };

        $.fn.form.settings.rules.check_speakers = function (value) {
            var list         = $.parseJSON(value);
            var is_not_error = true;
            list.forEach(function (speaker) {
                if ($.inArray(speaker.toLowerCase(), ['no', 'none', 'n/a', 'na', 'not available', 'no name', 'no speaker']) !== -1) {
                    is_not_error = false;
                    return true;
                }
            });
            return is_not_error;
        }
        $.fn.form.settings.rules.speakers_format = function (value) {
            var list         = $.parseJSON(value);
            var is_not_error = true;
            list.forEach(function (speaker) {
                if (!/^([a-zA-Z]{2,}(-[a-zA-Z]{2,})*)(\s[a-zA-Z]{2,}(-[a-zA-Z]{2,})*)+$/ig.test(speaker)) {
                    is_not_error = false;
                    return true;
                }
            });
            return is_not_error;
        }
        $.fn.form.settings.rules.is_empty = function(value) {
	        var list = $.parseJSON(value);
	        var disabled = $("#speakers_quoted_control").hasClass('disabled');
	        var is_not_error = true;
	        if (!disabled && list.length === 0) {
	            is_not_error = false;
	        }
	        return is_not_error;
	    }
	    $.fn.form.settings.rules.third_speakers_format = function (value) {
            return /^[^_0-9!@#$%№;:?*.^<>~\[\]\-_='"\\|\/{}()\+]+$/ig.test(value)
        }
        $.fn.form.settings.rules.not_in = function (value) {
            return !($.inArray(value.toLowerCase(), ['no', 'none', 'n/a', 'na', 'not available', 'no name', 'no speaker']) !== -1);
        }

        form.form(validators);
        var result = form.form('validate form');
        if (result == false) {
        	$('.menu.sections .active.item').removeClass('completed').addClass('has-error');
        }
    });

    $('#speaker_quote').click(function (e) {
        if ($(e.currentTarget).prop('checked')) {
            $('#speakers_quoted_control .ui-autocomplete-input').prop('disabled', false).val('');
            $('#speakers_quoted_control').removeClass('disabled');
            $("#speakers_quoted").removeAttr('disabled');
            $("#add-new-speaker").removeClass('disabled');
        } else {
            $('#speakers_quoted_control .ui-autocomplete-input').prop('disabled', true).val('');
            $('#speakers_quoted_control .ui-autocomplete-input').attr('disabled', 'disabled');
            $('#speakers_quoted_control').addClass('disabled');
            $("#speakers_quoted_control").tagit("fill", []);
            $("#speakers_quoted").attr('disabled', 'disabled');
            $("#speakers_quoted").change();
            $("#add-new-speaker").addClass('disabled');
        }
    });

    $('#third_speakers_quotes').click(function (e) {
        if ($(e.currentTarget).prop('checked')) {
            $('#third_speakers_quoted').removeAttr('disabled');
        } else {
            $('#third_speakers_quoted').val('').change();
            $('#third_speakers_quoted').attr('disabled', 'disabled');
        }
    });

    $("#add-new-speaker").click(function(){
      $("#add-new-speaker-modal").modal("setting", {
          autofocus: false,
          closable: false,
          onApprove: function () {
              return false;
          },
          close: '.button.close',
      }).modal("show");
    });

    $("#add-new-speaker-modal").on('click', '.close', function(){
      $("#add-new-speaker-modal").modal("hide");
    });

    $('.instant-update').find('input[type=text], input[type=checkbox], input[type="hidden"], textarea').change(function (e) {
        var id   = $('.instant-update').find('input[name="id"]').val();
        var el   = $(e.currentTarget);
        var name = el.attr('name');
        var data = {};

        // Если нет доступа к редактированию
        if (typeof access_edit !== 'undefined' && !access_edit) {
            return false;
        }

        if (name == 'comments' || name == 'region_id' || typeof name == "undefined") {
            return false;
        }

        if (name == 'country_id') {
            data['region_id'] = $(".region-list").dropdown('get value');
        }

        if (el.attr('type') == 'checkbox') {
            data['state'] = e.currentTarget.checked ? 1 : 0;
            if (el.attr('name') !== 'product_id' && el.attr('name') !== 'campaign_id' && el.attr('name') !== 'business_id') {
                el.val(data['state']);
            }
        }

        // this works for dropdown.media change event
        if  (el.attr('name') == 'media_subcategories') {
            return;
        }

        if  (el.attr('name') == 'speakers_quoted') {

            if ($('#speaker_quote').prop('checked')) {
                if ( $('#speakers_quoted').val() != '[]' ) {
                    data['speaker_quote'] = 1;
                    data['speaker_quote_state'] = 1;
                } else {
                    return;
                }
            } else {
                data['speaker_quote'] = 0.8;
                data['speaker_quote_state'] = 0.8;
            }

        } else if (el.attr('name') == 'third_speakers_quoted') {
            data['third_speakers_quotes'] = data['third_speakers_quotes_state'] = $('#third_speakers_quoted').val() ? 1 : 0;
        }

        data[name] = $.trim(el.val());
        // if (data[name] == "[]") {
        //     return false;
        // }

        if (data['date'] != undefined) {
            // Check Date of Publication (Coverage coding process)
            var result = true;
            var isAdmin = ($("meta[name=is_admin]").attr('content') == 'yes') ? true : false;
            var date = data['date'];
            var format = date.split('-');

            format.pop();
            format = format.join('-');

            if (isAdmin == false && Object.keys(coding_closed_months).length > 0) {
                $.each(coding_closed_months, function(k, v){
                    if (format == v) {
                        result = false;
                    }
                });
            }

            if (result == false) {
                return result;
            }
        }

        $.ajax({
            url     : "/coverage/" + id,
            method  : "PUT",
            dataType: "json",
            data    : data,
            success: function (data) {
                var field = el.closest('.field');
                field.removeClass('error');
                // field.find('.ui.label').remove();

                if (data.error == true) {
                    field.addClass('error');
                    field.append('<div class="ui red pointing prompt label transition visible">'+data.errors[el.attr('name')]+'</div>')
                } else {
                    message.success();
                }
            }
        });
    });

    coverageForm.submit(function () {
        var id = $('#coverage-form').find('input[name="id"]').val();

        $.ajax({
            url     : "/coverage/" + id,
            method  : "PUT",
            dataType: "json",
            data    : {
                status: "complete"
            },
            success: function () {
                notification.find('.sub.header').text('Coverage successfully saved');
                notification.dimmer('show');
                $('#coverage-form').find('input[type="submit"]').addClass('disabled');

                setTimeout(function () {
                    window.location.replace("/coverage");
                }, 1000);
            }
        });

        return false;
    });

    $('.ui.dropdown').dropdown();


    /*==== fix dropdown clear =====*/
    var currentValueDropdown = $('.ui.dropdown.media-subcategories').dropdown('get value').split(',');
    function clearLabelDropdown(currentValue) {
    	for (var i = 0; i < currentValue.length; i++) {
	    	$('.ui.dropdown.media-subcategories').dropdown('remove selected', currentValue[i])
	    }
    }

    $('.ui.dropdown.media-subcategories').dropdown({
    	onChange: function(value, text, $choice) {
            $(this).dropdown('initialize');
    		clearLabelDropdown(currentValueDropdown);
    		currentValueDropdown = $('.ui.dropdown.media-subcategories').dropdown('get value').split(',');
    	}
    });

    function putAjaxData(data) {
        var id   = $('.instant-update').find('input[name="id"]').val();

        $.ajax({
            url     : "/coverage/" + id,
            method  : "PUT",
            dataType: "json",
            data    : data,
            success: function (data) {

            }
        });
    }

    $('.ui.dropdown.media').dropdown({
        onChange: function (value, text, choice) {
            if (!value) {
                return false;
            }

            var mediaData = $.parseJSON($(choice).attr('data-media'));
            var mediaSubCategories = $(choice).attr('data-subcategories');

            if (mediaSubCategories != undefined) {
                mediaSubCategories = mediaSubCategories.replace('[', '').replace(']', '');
                mediaSubCategories = mediaSubCategories + ', ';
                mediaSubCategories = mediaSubCategories.split(',')
            } else {
                mediaSubCategories = '';
            }
            $('.ui.dropdown.media-subcategories').dropdown('set value', mediaSubCategories);
            mediaSubCategories = mediaSubCategories.join(',');
            mediaSubCategories = mediaSubCategories.substring(0, mediaSubCategories.length - 2);
            mediaSubCategories = mediaSubCategories ? mediaSubCategories : '0';
            var ajaxData = {media_subcategories: mediaSubCategories};
            putAjaxData(ajaxData);


            if ($.inArray(mediaData.type, ["tv", "radio"]) !== -1) {
                $("#reach_field").removeAttr("disabled");
                $("#reach_icon").hide();
            }
            else {
                $("#reach_icon").show();
                $("#reach_field").attr("disabled", "disabled");
            }

            $('.ui.dropdown.media-list')
                .dropdown('set selected', mediaData.list)
                .dropdown('set value', mediaData.list);

            $('.ui.dropdown.media-type')
                .dropdown('set selected', mediaData.type)
                .dropdown('set value', mediaData.type);

            $('.ui.dropdown.media-category')
                .dropdown('set selected', mediaData.category)
                .dropdown('set value', mediaData.category);

            $('input[name="reach"]').val(mediaData.reach).change();
        }
    });

    $('.ui.dropdown.region-list').dropdown({
        onChange: function (value) {
            if (!value) {
                return false;
            }

            $.get('/country', {
                region_id: value
            }, function (response) {
                countriesLoaded = true;
                var defaultText = '';

                $('.ui.dropdown.countries-list .menu').empty();
                $('.ui.dropdown.countries-list .text').text(defaultText);

                if (!$.isEmptyObject(response.countries)) {
                    response.countries.forEach(function (country) {
                        var item = $('<div>')
                            .addClass('item')
                            .attr('data-value', country.id)
                            .text(country.name);
                        $('.ui.dropdown.countries-list .menu').append(item);
                    });
                }
            });
        }
    });

    $('.ui.dropdown.countries-list').dropdown({
        onChange: function (value) {
            if (!value) {
                return false;
            }

            $.get('/media', {
                country_id: value
            }, function (response) {
                localStorage.setItem("country_id_" + userId, value);
                localStorage.setItem("region_id_" + userId, $(".region-list").dropdown('get value'));
                var defaultText = !$.isEmptyObject(response.media) ? response.media[0].name : '';

                $('.ui.dropdown.media .menu').empty();
                $('.ui.dropdown.media .text').text(defaultText);

                if (!$.isEmptyObject(response.media)) {
                    response.media.forEach(function (publication) {

                        var subcategories = [];
                        publication.subcategories.forEach(function (subcategory) {
                            subcategories.push(subcategory.id);
                        });

                        var item = $('<div>')
                            .addClass('item')
                            .attr('data-value', publication.name)
                            .attr('data-media', '{ "name": "' + publication.name + '", "list": "' + publication.list + '", "type": "' + publication.type + '", "category": "' + publication.category + '", "reach": "' + publication.reach + '" }')
                            .attr('data-subcategories', '[' + subcategories.join(',') + ']')

                            .text(publication.name);
                        $('.ui.dropdown.media .menu').append(item);
                    });
                }
            });
        }
    });

    if (typeof events !== "undefined") {
        jQuery('#event_name').autocomplete({
            'minLength':'2',
            'source'   : events
        });
    }

    $('#date').datepicker({
        format   : "yyyy-mm-dd",
        weekStart: 1,
        endDate  : "today",
        todayBtn : "linked"
    });

    $('.date i.add-on').click(function () {
        $('#date').datepicker('show');
    });

    $('#signin').form({
    	fields: {
    		email: {
	            identifier: 'email',
	            rules: [
	                {
	                    type  : 'empty',
	                    prompt: 'E-mail can\'t be empty'
	                },
	                {
	                    type  : 'email',
	                    prompt: "Invalid e-mail"
	                }
	            ]
	        },
	        password: {
	            identifier: 'password',
	            rules: [
	                {
	                    type  : 'empty',
	                    prompt: 'Password can\'t be empty'
	                },
	                {
	                    type  : 'length[8]',
	                    prompt: 'Your password must be at least 8 characters'
	                }
	            ]
	        }
    	},
       	inline: true
    });

    setTimeout(function() {
        // init Newsbreak
        $("#news_break_control").tagit({
            allowNewTags           : true,
            triggerKeys            : ['enter', 'tab'],
            select                 : true,
            editable               : false,
            sortable               : false,
            tagSource              : news,
            callbackOnInitialTagAdd: false,
            placeholder            : '',
            autocompleteMinLength  : 2,
            tagsChanged: function(tagValue, action, element) {
                var tags = [];
                $.each($("#news_break_control").tagit("tags"), function(k, v) {
                    tags.push(v.value);
                });

                $("#news_break").val(JSON.stringify(tags));
                if (action == 'added' || action == 'popped') {
                    $("#news_break").change();
                }
            }
        });
        var tagit_control_data = [];
        $.each(news_breaks, function(k, v) {
           tagit_control_data.push({label: v, value: v});
        });

        $("#news_break_control").tagit("fill", tagit_control_data);

        // init Speakers Quoted
        $("#speakers_quoted_control").tagit({
            triggerKeys            : ['enter', 'comma', 'tab'],
            allowNewTags           : false,
            select                 : true,
            editable               : false,
            sortable               : false,
            tagSource              : speakers,
            callbackOnInitialTagAdd: false,
            placeholder            : '',
            autocompleteMinLength  : 2,
            tagsChanged: function(tagValue, action, element) {
                if (action == 'added' && !/^([a-zA-Z]{2,}(-[a-zA-Z]{2,})*)(\s[a-zA-Z]{2,}(-[a-zA-Z]{2,})*)+$/.test(tagValue)){
                    $('#speakers_quoted_control').tagit("remove", 'tag', tagValue);
                    return;
                }
                var tags = [];
                $.each($("#speakers_quoted_control").tagit("tags"), function(k, v) {
                    tags.push(v.value);
                });

                $("#speakers_quoted").val(JSON.stringify(tags));
                if (action == 'added' || action == 'popped') {
                    $("#speakers_quoted").change();
                }
            }
        });

        tagit_control_data = [];
        $.each(speakers_quoted, function(k, v) {
           tagit_control_data.push({label: v, value: v});
        });

        $("#speakers_quoted_control").tagit("fill", tagit_control_data);
    });

    $(document.body).on('click', 'a.finish', function (e) {
        if (null !== localStorage.getItem('filter')) {
            var storage = JSON.parse(localStorage.getItem('filter'));
            if ('undefined' !== typeof storage['filter']) {
                var filter = JSON.parse(storage['filter']);
                if ('undefined' !== typeof storage['region']) {
                    filter['region_id'] = storage['region'];
                }
                if ("object" === typeof filter) {
                    e.preventDefault();
                    if ('undefined' !== typeof filter['status']) {
                        delete filter['status'];
                    }
                    var uri              = $(this).attr('href');
                    var separator        = (uri.indexOf('?') > -1) ? '&' : '?';
                    window.location.href = window.location.origin + uri + separator + $.param(filter);
                }
            }
        }
    });

    $('form .comment-text').hide();
    $('form .button-save-comment').hide();

    $('.button-comment').click(function () {
        if ($(this).hasClass('add-comment')) {
            $(this).removeClass('add-comment');
            $(this).addClass('hide-comment');
            $(this).text("Hide form");
            $(this).closest('form').find('.comment-text').show();
            $(this).closest('form').find('.button-save-comment').show();
        } else {
            $(this).removeClass('hide-comment');
            $(this).addClass('add-comment');
            $(this).text("Add comment");
            $(this).closest('form').find('.comment-text').hide();
            $(this).closest('form').find('.button-save-comment').hide();
        }
    });

    $('.button-save-comment').click(function () {
        var id   = $('.instant-update').find('input[name="id"]').val();
        var el   = $('.comment-text textarea');
        var name = el.attr('name');
        var data = {};

        data[name] = el.val();

        $.ajax({
            url     : "/coverage/" + id,
            method  : "PUT",
            dataType: "json",
            data    : data,
            success: function (data) {
                message.success();
            }
        });
    });

    setTimeout(function () {
        if (typeof is_new !== "undefined" && is_new) {
            save_region_id = localStorage.getItem("region_id_" + userId);
            if (typeof save_region_id === "undefined" || save_region_id === null) {
                // $(".countries-list").dropdown().find(".selected").trigger("click");
                $(".region-list").dropdown('set value', user_region_id).dropdown('set selected', user_region_id);
                // $(".region-list").dropdown().find(".selected").trigger("click");
                setTimeout(function () {
                    $(".countries-list").dropdown('set value', user_country_id).dropdown('set selected', user_country_id);
                }, 700)
            } else {
                $(".region-list").dropdown('set value', save_region_id).dropdown('set selected', save_region_id);
                // $(".region-list").dropdown().find(".selected").trigger("click");
                setTimeout(function () {
                    save_country_id = localStorage.getItem("country_id_" + userId);
                    if (typeof save_country_id === "undefined") {
                        $(".countries-list").dropdown('set value', user_country_id).dropdown('set selected', user_country_id);
                    } else {
                        $(".countries-list").dropdown('set value', save_country_id).dropdown('set selected', save_country_id);
                    }
                }, 700)
            }
        }
    });

    $("#coding-process-notify-modal").on('click', '.close', function(){
        $("#coding-process-notify-modal").modal("hide");
    });
});