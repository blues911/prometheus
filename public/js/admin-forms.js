/**
 * Created by Kirill Zorin <zarincheg@gmail.com>
 * Personal website: http://libdev.ru
 * at Fahrenheit 451
 *
 * Temporary file for JS event handlers
 *
 * Date: 29.10.14
 * Time: 1:38
 */

$(document).ready(function () {
    "use strict";

    $('.ui.dropdown').dropdown();
    $('.ui.checkbox').checkbox({
        'onChecked': function() {$(this).val('1');},
        'onUnchecked': function() {$(this).val('0');}
    });

    setTimeout(function() { $(".admin .tab.active").removeClass("loading"); }, 500);
    $('#admin-tabs .item').tab({
        onLoad: function(tabPath) {
            setTimeout(function() { $(".admin .tab.active").removeClass("loading"); }, 1000);
        }
    });

    $(".copy-btn").each(function(index, element) {
        var client = new ZeroClipboard(element);
    });

    var blocks = {
        'users'      : '.user_block',
        'regions'    : '.add_region_block',
        'subregions' : '.add_subregion_block',
        'countries'  : '.add_country_block',
        'products'   : '.add_product_block',
        'media'      : '.add_media_block',
        'events'     : '.add_event_block',
        'speakers'   : '.add_speaker_block',
        'campaigns'  : '.add_campaign_block',
        'business'   : '.add_business_block',
        'newsbreaks' : '.add_newsbreak_block',
        'subcategories' : '.add_subcategory_block'
    };

    var highlightFilters = function () {
        $('#region_id, #order_by_filter').each(function () {
            processHighlight(this, 'dropdown');
        });
        $('#country_filter, #name_filter').each(function () {
            processHighlight(this);
        });
    };

    function initStickyfloat(activeBlock) {
        $.each(blocks, function(key, block) {
            $(block).stickyfloat('destroy');
        });

        $(blocks[activeBlock]).stickyfloat();
    }

    $('#admin-tabs .item').on('click', function() {
        initStickyfloat($(this).attr("data-tab"));
    });

    var hash = window.location.hash.replace("#", '');
    if (typeof hash === 'undefined' || !hash.length) {
        hash = localStorage.getItem("active-tab");
    }
    localStorage.setItem("active-tab", '');
    if (hash !== null && typeof hash !== 'undefined' && hash.length > 0 && typeof hash !== 'null') {
        $('#admin-tabs .item').tab('change tab', hash);
        initStickyfloat(hash);
    }
    else {
        $('#admin-tabs .item').tab('change tab', 'users');
        initStickyfloat('users');
    }

    /* Fix v2 Semantic UI */
    $.fn.form.settings.rules.speakers_format = function (value) {
        return /^[^_0-9!@#$%№;:?*.,^<>~\[\]\-_='"\\|\/{}()\+]+$/ig.test(value);
    }

    $.fn.form.settings.rules.unique_speaker = function (value) {
        var result = true;
        var speakers = speakersNames;
        var id = $("#speaker-form").find("input[name=id]").val();

        if (id != '') {
            for (var i = 0; i < speakers.length; i++) {
                if (speakers[i]['id'] != id && speakers[i]['name'] == value) {
                    result = false;
                }
            }
        } else {
            for (var i = 0; i < speakersNames.length; i++) {
                if (speakers[i]['name'] == value) {
                    result = false;
                }
            }
        }

        return result;
    }

    $.fn.form.settings.rules.unique_newsbreak = function (value) {
        var result = true;
        var newsbreaks = newsbreaksNames;
        var id = $("#newsbreak-form").find("input[name=id]").val();

        if (id != '') {
            for (var i = 0; i < newsbreaks.length; i++) {
                if (newsbreaks[i]['id'] != id && newsbreaks[i]['name'] == value) {
                    result = false;
                }
            }
        } else {
            for (var i = 0; i < newsbreaksNames.length; i++) {
                if (newsbreaks[i]['name'] == value) {
                    result = false;
                }
            }
        }

        return result;
    }

    function tabInit(options, second) {
        var tab              = $(options.selector),
            form             = tab.find('.ui.form:not(.filter-form)'),
            toggle           = tab.find('.toggle'),
            toggleHeader     = tab.find('.toggle-header'),
            toggleHeaderText = toggleHeader.text();

        if (second === undefined) {
            tab.find('.toggle').click(function () {
                form.toggleClass('visible');
                toggle.toggleClass('invisible');
                toggleHeader.toggleClass('visible');

                form.find('.dropdown.media-subcategories').dropdown('clear');
            });
        }

        /* Fix v2 Semantic UI */
        var optRules = options.formRules;
        options.formRules = {};
        options.formRules.fields = optRules;
        options.formRules.onSuccess = function (event, fields) {

            options.formHandlers.onSuccess(form.find('form'));

            if (second === undefined) {
                form.toggleClass('visible');
                toggle.toggleClass('invisible');
                toggleHeader.toggleClass('visible').fadeOut();
                form.find('input').val('');
                form.find('[name=complete]').attr('checked', false);
                form.find('.dropdown')
                    .dropdown('set value', '');

                $.map(form.find('.dropdown .text'), function (v) {
                    $(v).text($(v).attr('data-default'));
                });
            }
        }

        form.form(options.formRules);

        $('.ui.accordion').accordion({
            onChange: function () {
            }
        });

        form.find('.cancel.button').click(function () {
            tabInitUsers(true, true);
            if (second === undefined) {
                toggleHeader.text(toggleHeaderText);
            }
            form.find('input').val('');
            form.find('[name=complete]').attr('checked', false);
            form.find('.dropdown')
                .dropdown('set value', '');

            $.map(form.find('.dropdown .text'), function (v) {
                $(v).text($(v).attr('data-default'));
            });
            if (second === undefined) {
                tab.find('.toggle').click();
            }
        });
    }

    function tabInitUsers()
    {
        var options = {
            selector : '.tab.manage.users',
            formRules: {
            	fields: {
            		email: {
	                    identifier: 'email',
	                    rules: [
	                        {
	                            type: 'empty',
	                            prompt: 'E-mail can\'t be empty'
	                        }
	                    ]
	                },
	                password: {
	                    identifier: 'password',
	                    optional: false,
	                    rules: [
	                        {
	                            type  : 'empty',
	                            prompt: 'Password can\'t be empty'
	                        },
	                        {
	                            type  : 'length[8]',
	                            prompt: 'Your password must be at least 8 characters'
	                        }
	                    ]
	                },
	                name: {
	                    identifier: 'name',
	                    rules: [
	                        {
	                            type  : 'empty',
	                            prompt: 'Full name can\'t be empty'
	                        }
	                    ]
	                },
	                region: {
	                    identifier: 'region',
	                    rules: [
	                        {
	                            type  : 'empty',
	                            prompt: 'User region can\'t be empty'
	                        }
	                    ]
	                },
	                country: {
	                    identifier: 'country',
	                    rules: [
	                        {
	                            type  : 'empty',
	                            prompt: 'User country can\'t be empty'
	                        }
	                    ]
	                },            		
            	},
                
                onSuccess: function () {
	                options.formHandlers.onSuccess(form.find('form'));

	                form.removeClass('visible');
	                toggle.removeClass('invisible');
	                toggleHeader.removeClass('visible');
	                form.find('input').val('');
	                form.find('.dropdown')
	                    .dropdown('set value', '');

	                $.map(form.find('.dropdown .text'), function (v) {
	                    $(v).text($(v).attr('data-default'));
	                });
	            }
            },
            formHandlers: {
                onSuccess: function (form) {
                    $.post(form.attr('action'), form.serialize(), function () {
                        var notification = $('#notification');
                        var page = $("div [data-tab='users'] .pagination li.active").text();
                        localStorage.setItem('admin_page', page);
                        notification.find('.sub.header').text('User data successfully saved');
                        notification.dimmer('show');
                        setTimeout(function () {
                            window.location.replace("/admin");
                        }, 1000);
                    });
                }
            }
        };
        var tab          = $(options.selector),
        form             = tab.find('.add_user_block .ui.form:not(.filter-form)'),
        toggle           = tab.find('.add_user_block .toggle'),
        toggleHeader     = tab.find('.add_user_block .toggle-header');

        tab.find('.toggle').click(function () {
            $('.toggle-header, .ui.form').removeClass('visible');
            form.addClass('visible');
            toggle.addClass('invisible');
            toggleHeader.addClass('visible');
        });

        form.form(options.formRules, {
            
        });

        form.find('.cancel.button').click(function () {
            form.removeClass('visible');
            toggle.removeClass('invisible');
            toggleHeader.removeClass('visible');
            form.find('input').val('');

            form.find('.dropdown')
                .dropdown('set value', '');
            $.map(form.find('.dropdown .text'), function (v) {
                $(v).text($(v).attr('data-default'));
            });
            form.removeClass('error');
            form.find('.field').removeClass('error');
            form.find('.error.message li').remove();
        });
    }

    function tabInitUsersEdit()
    {
        var options = {
            selector : '.tab.manage.users',
            formRules: {
            	fields: {
	                email: {
	                    identifier: 'email',
	                    rules: [
	                        {
	                            type: 'empty',
	                            prompt: 'E-mail can\'t be empty'
	                        }
	                    ]
	                },
	                password: {
	                    identifier: 'password',
	                    optional: true,
	                    rules: [
	                        {
	                            type  : 'length[8]',
	                            prompt: 'Your password must be at least 8 characters'
	                        }
	                    ]
	                },
	                name: {
	                    identifier: 'name',
	                    rules: [
	                        {
	                            type  : 'empty',
	                            prompt: 'Full name can\'t be empty'
	                        }
	                    ]
	                },
	                region: {
	                    identifier: 'region',
	                    rules: [
	                        {
	                            type  : 'empty',
	                            prompt: 'User region can\'t be empty'
	                        }
	                    ]
	                },
	                country: {
	                    identifier: 'country',
	                    rules: [
	                        {
	                            type  : 'empty',
	                            prompt: 'User country can\'t be empty'
	                        }
	                    ]
	                }
	            },
	            onSuccess: function () {
	                options.formHandlers.onSuccess(form.find('form'));

	                form.removeClass('visible');
	                toggleHeader.removeClass('visible');
	                form.find('input').val('');
	                form.find('.dropdown').dropdown('set value', '');
	                $.map(form.find('.dropdown .text'), function (v) {
	                    $(v).text($(v).attr('data-default'));
	                });
	            }
            },
            formHandlers: {
                onSuccess: function (form) {
                    $.post(form.attr('action'), form.serialize(), function () {
                        var notification = $('#notification');
                        var page = $("div [data-tab='users'] .pagination li.active").text();
                        var teals = $("div [data-tab='users'] .ui .circular").find("a");
                        var adminPagination = {
                          page: null,
                          teal: null
                        };
                        adminPagination.page = page;
                        teals.each(function(){
                          if ($(this).hasClass('teal')) {
                            adminPagination.teal = $(this).text();
                          }
                        });
                        localStorage.setItem('admin_pagination', JSON.stringify(adminPagination));
                        notification.find('.sub.header').text('User data successfully saved');
                        notification.dimmer('show');
                        setTimeout(function () {
                            window.location.replace("/admin");
                        }, 1000);
                    });
                }
            }
        };
        var tab          = $(options.selector),
        form             = tab.find('.edit_user_block .ui.form:not(.filter-form)'),
        toggleHeader     = tab.find('.edit_user_block .toggle-header');

        form.form(options.formRules);

        form.find('.cancel.button').click(function () {
            tab.find('.add_user_block .toggle').toggleClass('invisible');
            form.toggleClass('visible');
            toggleHeader.toggleClass('visible');
            tab.find('.users-list .item .content, .users-list .item .title').removeClass('active'); 
            form.find('input').val('');
            form.find('.dropdown').dropdown('set value', '');
            $.map(form.find('.dropdown .text'), function (v) {
                $(v).text($(v).attr('data-default'));
            });
            form.removeClass('error');
            form.find('.field').removeClass('error');
            form.find('.error.message li').remove();
        });
    }

    /**
     * Create new user
     */
    tabInitUsers();
    tabInitUsersEdit();

    /**
     * Create region
     */
    tabInit({
        selector : '.tab.manage.regions',
        formRules: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Name can\'t be empty'
                    }
                ]
            }
        },
        formHandlers: {
            onSuccess: function (form) {
                $.post(form.attr('action'), form.serialize(), function () {
                    $('#notification').dimmer('show');
                    setTimeout(function () {
                        $('#notification').dimmer('hide');
                        localStorage.setItem("active-tab", "regions");
                        window.location.replace("/admin");
                    }, 1000);
                });
            }
        }
    });

    /**
     * Create subregion
     */
    tabInit({
        selector : '.tab.manage.subregions',
        formRules: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Name can\'t be empty'
                    }
                ]
            },
            region: {
                identifier: 'region',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Region can\'t be empty'
                    }
                ]
            }
        },
        formHandlers: {
            onSuccess: function (form) {
                $.post(form.attr('action'), form.serialize(), function () {
                    $('#notification').dimmer('show');
                    setTimeout(function () {
                        $('#notification').dimmer('hide');
                        localStorage.setItem("active-tab", "subregions");
                        window.location.replace("/admin");
                    }, 1000);
                });
            }
        }
    });

    /**
     * Create country
     */
    tabInit({
        selector : '.tab.manage.countries',
        formRules: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Name can\'t be empty'
                    }
                ]
            },
            iso: {
                identifier: 'iso',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'ISO code can\'t be empty'
                    }
                ]
            }
        },
        formHandlers: {
            onSuccess: function (form) {
                $.post(form.attr('action'), form.serialize(), function () {
                    $('#notification').dimmer('show');
                    setTimeout(function () {
                        $('#notification').dimmer('hide');
                        localStorage.setItem("active-tab", "countries");
                        window.location.replace("/admin");
                    }, 1000);
                });
            }
        }
    });

    /**
     * Create product
     */
    tabInit({
        selector : '.tab.manage.products',
        formRules: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Product name can\'t be empty'
                    }
                ]
            },
            short_name: {
                identifier: 'short_name',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Product short name can\'t be empty'
                    }
                ]
            },
            status: {
                identifier: 'status',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Product status can\'t be empty'
                    }
                ]
            },
            is_security_intelligence: {
                identifier: 'is_security_intelligence',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Product security intelligence can\'t be empty'
                    }
                ]
            }
        },
        formHandlers: {
            onSuccess: function (form) {
                $.post(form.attr('action'), form.serialize(), function () {
                    $('#notification').dimmer('show');
                    setTimeout(function () {
                        $('#notification').dimmer('hide');
                        localStorage.setItem("active-tab", "products");
                        window.location.replace("/admin");
                    }, 1000);
                });
            }
        }
    });

    /**
     * Create media item
     */
    tabInit({
        selector : '.tab.manage.media',
        formRules: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Media name can\'t be empty'
                    }
                ]
            },
            region: {
                identifier: 'region',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Region can\'t be empty'
                    }
                ]
            },
            country: {
                identifier: 'country',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Country can\'t be empty'
                    }
                ]
            }
        },
        formHandlers: {
            onSuccess: function (form) {
                $.post(form.attr('action'), form.serialize(), function () {
                    var page = $("div [data-tab='media'] .pagination li.active").text();
                    var page = $("div [data-tab='media'] .pagination li.active").text();
                    var adminPagination = {
                      page: null,
                      teal: null
                    };
                    var isChecked = $("div [data-tab='media'] .deleted-items").hasClass('checked');
                    if (isChecked == false) {
                        // remember page
                        adminPagination.page = page;
                        localStorage.setItem('admin_pagination', JSON.stringify(adminPagination));
                    }
                    $('#notification').dimmer('show');
                    setTimeout(function () {
                        $('#notification').dimmer('hide');
                        localStorage.setItem("active-tab", "media");
                        window.location.replace("/admin");
                    }, 1000);
                    
                });
            }
        }
    });

    /**
     * Edit event
     */
    tabInit({
        selector: '.tab.manage.events',
        formRules: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Name can\'t be empty'
                    }
                ]
            },
            status: {
                identifier: 'status',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Event status can\'t be empty'
                    }
                ]
            }
        },
        formHandlers: {
            onSuccess: function (form) {
                $.each(form, function(index, currentForm) {
                    if (!$(currentForm).is(":visible")) {
                        form.splice(index, 1);
                    }
                });
                $.post(form.attr('action'), form.serialize(), function () {
                    $('#notification').dimmer('show');
                    setTimeout(function () {
                        $('#notification').dimmer('hide');
                        localStorage.setItem("active-tab", "events");
                        window.location.replace("/admin");
                    }, 1000);
                });
            }
        }
    });

    /**
     * Edit campaign
     */
    tabInit({
        selector: '.tab.manage.campaigns',
        formRules: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Name can\'t be empty'
                    }
                ]
            },
            status: {
                identifier: 'status',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Campaign status can\'t be empty'
                    }
                ]
            }
        },
        formHandlers: {
            onSuccess: function (form) {
                $.each(form, function(index, currentForm) {
                    if (!$(currentForm).is(":visible")) {
                        form.splice(index, 1);
                    }
                });
                $.post(form.attr('action'), form.serialize(), function () {
                    $('#notification').dimmer('show');
                    setTimeout(function () {
                        $('#notification').dimmer('hide');
                        localStorage.setItem("active-tab", "campaigns");
                        window.location.replace("/admin");
                    }, 1000);
                });
            }
        }
    });

    /**
     * Edit business
     */
    tabInit({
        selector: '.tab.manage.business',
        formRules: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Name can\'t be empty'
                    }
                ]
            },
            status: {
                identifier: 'status',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Business segment status can\'t be empty'
                    }
                ]
            }
        },
        formHandlers: {
            onSuccess: function (form) {
                $.each(form, function(index, currentForm) {
                    if (!$(currentForm).is(":visible")) {
                        form.splice(index, 1);
                    }
                });
                $.post(form.attr('action'), form.serialize(), function () {
                    $('#notification').dimmer('show');
                    setTimeout(function () {
                        $('#notification').dimmer('hide');
                        localStorage.setItem("active-tab", "business");
                        window.location.replace("/admin");
                    }, 1000);
                });
            }
        }
    });

    /**
     * Edit newsbreak
     */
    tabInit({
        selector: '.tab.manage.newsbreaks',
        formRules: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Name can\'t be empty'
                    },
                    {
                        type  : 'unique_newsbreak',
                        prompt: 'Newsbreak name must be unique'
                    }
                ]
            },
            status: {
                identifier: 'status',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Newsbreak status can\'t be empty'
                    }
                ]
            },
            status: {
                identifier: 'type',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Newsbreak type can\'t be empty'
                    }
                ]
            }
        },
        formHandlers: {
            onSuccess: function (form) {
                $.each(form, function(index, currentForm) {
                    if (!$(currentForm).is(":visible")) {
                        form.splice(index, 1);
                    }
                });
                $.post(form.attr('action'), form.serialize(), function () {
                    var page = $("div [data-tab='newsbreaks'] .pagination li.active").text();
                    var page = $("div [data-tab='newsbreaks'] .pagination li.active").text();
                    var teals = $("div [data-tab='newsbreaks'] .ui .circular").find("a");
                    var adminPagination = {
                      page: null,
                      teal: null
                    };
                    var isChecked = $("div [data-tab='newsbreaks'] .deleted-items").hasClass('checked');
                    if (isChecked == false) {
                        // remember page
                        adminPagination.page = page;
                        teals.each(function(){
                          if ($(this).hasClass('teal')) {
                            adminPagination.teal = $(this).text();
                          }
                        });
                        localStorage.setItem('admin_pagination', JSON.stringify(adminPagination));
                    }
                    $('#notification').dimmer('show');
                    setTimeout(function () {
                        $('#notification').dimmer('hide');
                        localStorage.setItem("active-tab", "newsbreaks");
                        window.location.replace("/admin");
                    }, 1000);
                });
            }
        }
    });

    /**
     * Edit speaker
     */
    tabInit({
        selector: '.tab.manage.speakers',
        formRules: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Name can\'t be empty'
                    },
                    {
                        type  : 'speakers_format',
                        prompt: 'Speaker name must must match "Firstname Lastname"'
                    },
                    {
                        type  : 'unique_speaker',
                        prompt: 'Speaker name must be unique'
                    }
                ]
            }
        },
        formHandlers: {
            onSuccess: function (form) {
                $.post(form.attr('action'), form.serialize(), function () {
                    var page = $("div [data-tab='speakers'] .pagination li.active").text();
                    var page = $("div [data-tab='speakers'] .pagination li.active").text();
                    var teals = $("div [data-tab='speakers'] .ui .circular").find("a");
                    var adminPagination = {
                      page: null,
                      teal: null
                    };
                    var isChecked = $("div [data-tab='speakers'] .deleted-items").hasClass('checked');
                    if (isChecked == false) {
                        // remember page
                        adminPagination.page = page;
                        teals.each(function(){
                          if ($(this).hasClass('teal')) {
                            adminPagination.teal = $(this).text();
                          }
                        });
                        localStorage.setItem('admin_pagination', JSON.stringify(adminPagination));
                    }
                    $('#notification').dimmer('show');
                    setTimeout(function () {
                        $('#notification').dimmer('hide');
                        localStorage.setItem("active-tab", "speakers");
                        window.location.replace("/admin");
                    }, 1000);
                });
            }
        }
    });

    /**
     * Edit subcategories
     */
    tabInit({
        selector: '.tab.manage.subcategories',
        formRules: {
            name: {
                identifier: 'title',
                rules: [
                    {
                        type  : 'empty',
                        prompt: 'Name can\'t be empty'
                    },
                ]
            }
        },
        formHandlers: {
            onSuccess: function (form) {
                $.post(form.attr('action'), form.serialize(), function () {
                    $('#notification').dimmer('show');
                    setTimeout(function () {
                        $('#notification').dimmer('hide');
                        localStorage.setItem("active-tab", "subcategories");
                        window.location.replace("/admin");
                    }, 1000);
                });
            }
        }
    });

    /* Handle delete */
    $(document.body).on("click", '.users a.delete, .regions a.delete, .subregions a.delete, .countries a.delete, .products a.delete, .speakers a.delete,'
        + '.media a.delete, .events a.delete, .campaigns a.delete, .business a.delete, .newsbreaks a.delete, .subcategories a.delete', function(e) {
        var b = $(e.currentTarget);

        if (b.hasClass('disabled')) {
            return false;
        }

        b.closest(".item").find('.active-block').fadeOut();
        b.closest(".item").find('.edit-block').fadeOut();

        b.animate({
            opacity: 0
        }, {
            complete: function () {
                b.css('display', 'none');
                b.parent().find('.approve').css({'display': 'inline'});
                b.parent().find('.approve').animate({
                    opacity: 1
                });
            }
        });

        return false;
    });

    /* Handle restore */
    $(document.body).on("click", '.users a.restore, .regions a.restore, .subregions a.restore, .countries a.restore, .products a.restore, .speakers a.restore,'
    + '.media a.restore, .events a.restore, .campaigns a.restore, .business a.restore, .newsbreaks a.restore, .subcategories a.restore', function(e) {
        var b = $(e.currentTarget);

        if (b.hasClass('disabled')) {
            return false;
        }

        b.closest(".item").find('.active-block').fadeOut();
        b.closest(".item").find('.edit-block').fadeOut();

        b.animate({
            opacity: 0
        }, {
            complete: function () {
                b.css('display', 'none');
                b.parent().find('.approve-restore').css({'display': 'inline'});
                b.parent().find('.approve-restore').animate({
                    opacity: 1
                });
            }
        });

        return false;
    });

    /**
     * Event Handlers
     */
    $(document.body).on("click", '.activated', function (e) {
        var b = $(e.currentTarget);

        if (b.hasClass('disabled')) {
            return false;
        }
        b.closest(".item").find('.delete-block').fadeOut();
        b.animate({
            opacity: 0
        }, {
            complete: function () {
                b.css('display', 'none');
                b.parent().find('.approve-active').css({'display': 'inline'});
                b.parent().find('.approve-active').animate({
                    opacity: 1
                });
            }
        });

        return false;
    });


    $(document.body).on("click", '.active-block .no', function (e) {
        var a = $(e.currentTarget).parent('.approve-active');

        a.animate({
            opacity: 0
        }, {
            complete: function () {
                a.css('display', 'none');
                a.parent().find('a.activated').css('display', 'inline-block');
                a.parent().find('a.activated').animate({
                    opacity: 1
                });
                a.closest(".item").find('.delete-block').fadeIn();
            }
        });

        return false;
    });

    $(document.body).on("click", '.active-block .yes', function (e) {
        var a = $(e.currentTarget);

        $.ajax({
            url : a.attr('href'),
            type: 'POST',
            data: {
                _method: 'PUT'
            },
            dataType: 'json',
            success: function () {
                localStorage.setItem("active-tab", a.closest('.tab').attr("data-tab"));
                window.location.reload();
            }
        });

        return false;
    });

    /* Handle yes/no for delete action */
    $(document.body).on("click", '.users .approve .no, .regions .approve .no, .subregions .approve .no, .countries .approve .no, .products .approve .no, .speakers .approve .no, .approve-active .no,'
        + '.media .approve .no, .events .approve .no, .campaigns .approve .no, .business .approve .no, .newsbreaks .approve .no, .subcategories .approve .no', function (e) {
        var a = $(e.currentTarget).parent('.approve');

        a.animate({
            opacity: 0
        }, {
            complete: function () {
                a.css('display', 'none');
                a.closest(".item").find('a.delete').css('display', 'inline');
                a.closest(".item").find('a.delete').animate({
                    opacity: 1
                });
                a.closest(".item").find('.edit-block').fadeIn();
                a.closest(".item").find('.active-block').fadeIn();
            }
        });

        return false;
    });
    $(document.body).on("click", '.users .approve .yes, .regions .approve .yes, .subregions .approve .yes, .countries .approve .yes, .products .approve .yes,.speakers .approve .yes,'
        + '.media .approve .yes, .events .approve .yes, .campaigns .approve .yes, .business .approve .yes, .newsbreaks .approve .yes, .subcategories .approve .yes', function (e) {
        var a = $(e.currentTarget);

        $.ajax({
            url : a.attr('href'),
            type: 'POST',
            data: {
                _method: 'DELETE'
            },
            dataType: 'json',
            success: function () {
                localStorage.setItem("active-tab", a.closest('.tab').attr("data-tab"));
                window.location.reload();
            }
        });

        return false;
    });

    /* Handle yes/no for restore action */
    $(document.body).on("click", '.users .approve-restore .no, .regions .approve-restore .no, .subregions .approve-restore .no, .countries .approve-restore .no, .products .approve-restore .no, .speakers .approve-restore .no, .approve-restore-active .no,'
        + '.media .approve-restore .no, .events .approve-restore .no, .campaigns .approve-restore .no, .business .approve-restore .no, .newsbreaks .approve-restore .no, .subcategories .approve-restore .no', function (e) {
        var a = $(e.currentTarget).parent('.approve-restore');

        a.animate({
            opacity: 0
        }, {
            complete: function () {
                a.css('display', 'none');
                a.closest(".item").find('a.restore').css('display', 'inline');
                a.closest(".item").find('a.restore').animate({
                    opacity: 1
                });
                a.closest(".item").find('.edit-block').fadeIn();
                a.closest(".item").find('.active-block').fadeIn();
            }
        });

        return false;
    });
    $(document.body).on("click", '.users .approve-restore .yes, .regions .approve-restore .yes, .subregions .approve-restore .yes, .countries .approve-restore .yes, .products .approve-restore .yes,.speakers .approve-restore .yes,'
        + '.media .approve-restore .yes, .events .approve-restore .yes, .campaigns .approve-restore .yes, .business .approve-restore .yes, .newsbreaks .approve-restore .yes, .subcategories .approve-restore .yes', function (e) {
        var a = $(e.currentTarget);
        $.ajax({
            url : a.attr('href'),
            type: 'POST',
            data: null,
            dataType: 'json',
            success: function () {
                localStorage.setItem("active-tab", a.closest('.tab').attr("data-tab"));
                window.location.reload();
            }
        });

        return false;
    });

    $(document.body).on("click", '.accordion .title a:not(.link)', function () {
        return false;
    });

    $(document.body).on("click", '.accordion.users-list .title a.name', function (e) {
        var tab = $('.tab.manage.users'),
            form = tab.find('.edit_user_block .ui.form'),
            toggleHeader = tab.find('.edit_user_block .toggle-header'),
            data = $(e.currentTarget).parent().next('.content.data');

        if (!$('.add_user_block .ui.form').hasClass('visible')) {
            tab.find('.add_user_block .toggle').toggleClass('invisible');
        }
        $('.add_user_block .toggle-header, .add_user_block .ui.form').removeClass('visible');

        form.toggleClass('visible');
        toggleHeader.toggleClass('visible');
        
        form.find('[name="id"]').val(data.attr('data-id'));
        form.find('[name="email"]').val(data.attr('data-email'));
        form.find('[name="name"]').val(data.attr('data-name'));
        form.find('[name="password"]').attr('placeholder', 'New password');

        form.find('.dropdown.role')
            .dropdown('set value', data.attr('data-role'))
            .dropdown('set selected', data.attr('data-role'));
        form.find('.dropdown.region')
            .dropdown('set value', data.attr('data-region'))
            .dropdown('set selected', data.attr('data-region'));
        setTimeout(function() {form.find('.dropdown.country')
            .dropdown('set value', data.attr('data-country'))
            .dropdown('set selected', data.attr('data-country'));
        }, 120);

        return false;
    });

    $(document.body).on("click", '.list.regions-list .item .header a.name', function (e) {
        var tab = $('.tab.manage.regions'),
            form = tab.find('.ui.form'),
            data = $(e.currentTarget).parent().parent('.content.data');

        tab.find('.toggle-header').text('Edit Region');
        form.find('[name="id"]').val(data.attr('data-id'));
        form.find('[name="name"]').val(data.attr('data-name'));

        if (!form.hasClass('visible')) {
            tab.find('.toggle').click();
        }

        return false;
    });

    $(document.body).on("click", '.list.subregions-list .item .header a.name', function (e) {
        var tab = $('.tab.manage.subregions'),
            form = tab.find('.ui.form'),
            data = $(e.currentTarget).parent().parent('.content.data');

        tab.find('.toggle-header').text('Edit Subregion');
        form.find('[name="id"]').val(data.attr('data-id'));
        form.find('[name="name"]').val(data.attr('data-name'));
        form.find('.dropdown.region')
            .dropdown('set value', data.attr('data-region'))
            .dropdown('set selected', data.attr('data-region'));

        if (!form.hasClass('visible')) {
            tab.find('.toggle').click();
        }

        return false;
    });


    $(document.body).on("click", '.list.countries-list .item .header a.name', function (e) {
        var tab = $('.tab.manage.countries'),
            form = tab.find('.ui.form'),
            data = $(e.currentTarget).parent().parent('.content.data');

        tab.find('.toggle-header').text('Edit Country');
        form.find('[name="id"]').val(data.attr('data-id'));
        form.find('[name="name"]').val(data.attr('data-name'));
        form.find('[name="iso"]').val(data.attr('data-iso'));
        form.find('.dropdown.region')
            .dropdown('set value', data.attr('data-region'))
            .dropdown('set selected', data.attr('data-region'));

        form.find('.dropdown.subregion')
            .dropdown('set value', data.attr('data-subregion'))
            .dropdown('set selected', data.attr('data-subregion'));

        if (!form.hasClass('visible')) {
            tab.find('.toggle').click();
        }

        return false;
    });

    $(document.body).on("change", '.add_country_block form .field .region', function (e) {
        var id = $(this).find('input[name=region]').val();
        var select = $('.add_country_block form').find('.subregion');
        var subregionId = select.find('input[name=subregion]').val();
        var list = {};
        $.each(subregions, function(key, item) {
            if (item['region_id'] == id) {
                list[item['id']] = item['name'];
            }
        });
        if (Object.keys(list).length > 0) {
            var options = $('.add_country_block form').find('.subregion .menu');
            if (subregionId !== undefined) {
                select.find('.text').text(list[subregionId]);
            } else {
                select.find('.text').text('Select Subregion');
            }
            options.empty();
            options.append('<div class="item" data-value="0">Select Subregion</div>');
            $.each(list, function(key, value) {
                options.append('<div class="item" data-value="'+key+'">'+value+'</div>');
            });
            select.show();
        } else {
            select.hide();
        }
    });

    $(document.body).on("click", '.list.products-list .item .header a.name', function (e) {
        var tab = $('.tab.manage.products'),
            form = tab.find('.ui.form'),
            data = $(e.currentTarget).parent().parent('.content.data');

        tab.find('.toggle-header').text('Edit Product');
        form.find('[name="id"]').val(data.attr('data-id'));
        form.find('[name="name"]').val(data.attr('data-name'));
        form.find('[name="short_name"]').val(data.attr('data-short-name'));
        form.find('.dropdown.product-status')
            .dropdown('set value', data.attr('data-status'))
            .dropdown('set selected', data.attr('data-status'));

        if(data.attr('data-security-intelligence') == '1') {
            form.find('.ui.checkbox').checkbox('check');
        } else {
            form.find('.ui.checkbox').checkbox('uncheck');
        }
        if (!form.hasClass('visible')) {
            tab.find('.toggle').click();
        }

        return false;
    });

    $(document.body).on("click", ".list.media-list .item .header a.name", function (e) {
        var tab = $('.tab.manage.media'),
            form = tab.find('.ui.form:not(.filter-form)'),
            data = $(e.currentTarget).parents('.content.data');

        tab.find('.toggle-header').text('Edit Media');
        form.find('[name="id"]').val(data.attr('data-id'));
        form.find('[name="name"]').val(data.attr('data-name'));
        form.find('[name="reach"]').val(data.attr('data-reach'));

        form.find('.dropdown.list-status')
            .dropdown('set value', data.attr('data-list'))
            .dropdown('set selected', data.attr('data-list'));
        form.find('.dropdown.region')
            .dropdown('set value', data.attr('data-region'))
            .dropdown('set selected', data.attr('data-region'));
        setTimeout(function() {
            form.find('.dropdown.country')
                .dropdown('set value', data.attr('data-country'))
                .dropdown('set selected', data.attr('data-country'));
        }, 250);
        form.find('.dropdown.type')
            .dropdown('set value', data.attr('data-type'))
            .dropdown('set selected', data.attr('data-type'));
        form.find('.dropdown.media-category')
            .dropdown('set value', data.attr('data-category'))
            .dropdown('set selected', data.attr('data-category'));

        var mediaSubCategories = data.attr('data-subcategories');

        if (mediaSubCategories != undefined) {
            mediaSubCategories = mediaSubCategories.split(',')
        } else {
            mediaSubCategories = '';
        }

        form.find('.dropdown.media-subcategories').dropdown('set exactly', mediaSubCategories);

        if (!form.hasClass('visible')) {
            tab.find('.toggle').click();
        }

        return false;
    });

    $(document.body).on("click", '.list.events-list .item .header a.name', function (e) {
        var tab = $('.tab.manage.events'),
            form = tab.find('.ui.form'),
            data = $(e.currentTarget).parent().parent('.content.data');

        tab.find('.toggle-header').text('Edit Event');
        form.find('[name="id"]').val(data.attr('data-id'));
        form.find('[name="name"]').val(data.attr('data-name'));
        form.find('.dropdown.event-status')
            .dropdown('set value', data.attr('data-status'))
            .dropdown('set selected', data.attr('data-status'));

        if (!form.hasClass('visible')) {
            tab.find('.toggle').click();
        }

        return false;
    });

    $(document.body).on("click", '.list.campaigns-list .item .header a.name', function (e) {
        var tab = $('.tab.manage.campaigns'),
            form = tab.find('.ui.form'),
            data = $(e.currentTarget).parent().parent('.content.data');

        tab.find('.toggle-header').text('Edit Campaign');
        form.find('[name="id"]').val(data.attr('data-id'));
        form.find('[name="name"]').val(data.attr('data-name'));
        form.find('.dropdown.campaign-status')
            .dropdown('set value', data.attr('data-status'))
            .dropdown('set selected', data.attr('data-status'));

        if (!form.hasClass('visible')) {
            tab.find('.toggle').click();
        }

        return false;
    });

    $(document.body).on("click", '.list.business-list .item .header a.name', function (e) {
        var tab = $('.tab.manage.business'),
            form = tab.find('.ui.form'),
            data = $(e.currentTarget).parent().parent('.content.data');
        tab.find('.toggle-header').text('Edit Business segment');
        form.find('[name="id"]').val(data.attr('data-id'));
        form.find('[name="name"]').val(data.attr('data-name'));
        form.find('.dropdown.business-status')
            .dropdown('set value', data.attr('data-status'))
            .dropdown('set selected', data.attr('data-status'));

        if (!form.hasClass('visible')) {
            tab.find('.toggle').click();
        }

        return false;
    });

    $(document.body).on("click", '.list.newsbreaks-list .item .header a.name', function (e) {
        var tab = $('.tab.manage.newsbreaks'),
            form = tab.find('.ui.form'),
            data = $(e.currentTarget).parent().parent('.content.data');

        tab.find('.toggle-header').text('Edit Newsbreak');
        form.find('[name="id"]').val(data.attr('data-id'));
        form.find('[name="name"]').val(data.attr('data-name'));
        form.find('.dropdown.newsbreak-status')
            .dropdown('set value', data.attr('data-status'))
            .dropdown('set selected', data.attr('data-status'));
        form.find('.dropdown.newsbreak-type')
            .dropdown('set value', data.attr('data-type'))
            .dropdown('set selected', data.attr('data-type'));

        if (data.attr('data-complete') == 1) {
            form.find('[name="complete"]').val(1);
            form.find('[name="complete"]').prop('checked', true);
            form.find('[name="complete"]').parent().addClass('checked');
        } else {
            form.find('[name="complete"]').val(0);
            form.find('[name="complete"]').prop('checked', false);
            form.find('[name="complete"]').parent().removeClass('checked');
        }

        if (!form.hasClass('visible')) {
            tab.find('.toggle').click();
        }

        return false;
    });

    $(document.body).on("click", '.list.speakers-list .item .header a.name', function (e) {
        var tab = $('.tab.manage.speakers'),
            form = tab.find('.ui.form'),
            data = $(e.currentTarget).parent().parent('.content.data');

        tab.find('.toggle-header').text('Edit Speaker');
        form.find('[name="id"]').val(data.attr('data-id'));
        form.find('[name="name"]').val(data.attr('data-name'));

        if (!form.hasClass('visible')) {
            tab.find('.toggle').click();
        }

        return false;
    });

    $(document.body).on("click", '.list.subcategories-list .item .header a.name', function (e) {
        var tab = $('.tab.manage.subcategories'),
            form = tab.find('.ui.form'),
            data = $(e.currentTarget).parent().parent('.content.data');

        tab.find('.toggle-header').text('Edit Subcategory');
        form.find('[name="id"]').val(data.attr('data-id'));
        form.find('[name="title"]').val(data.attr('data-name'));

        if (!form.hasClass('visible')) {
            tab.find('.toggle').click();
        }

        return false;
    });

    $('.ui.dropdown.region').dropdown({
        onChange: function (value) {
            if (!value) {
                return false;
            }

            $.get('/country', {
                region_id: value
            }, function (response) {
                var defaultText = !$.isEmptyObject(response.countries) ? response.countries[0].name : '';

                $('.ui.dropdown.country .menu').empty();
                $('.ui.dropdown.country .text').text(defaultText);

                if (!$.isEmptyObject(response.countries)) {
                    response.countries.forEach(function (country) {
                        var item = $('<div>')
                            .addClass('item')
                            .attr('data-value', country.id)
                            .text(country.name);
                        $('.ui.dropdown.country .menu').append(item);
                    });
                }
            });
        }
    });

    $(".visible-archived input").prop('checked', false);

    $(document.body).on("click", '.visible-archived', function () {
        if ($(this).hasClass('checked')) {
            $(this).closest('.column').find('.archived').removeClass('hidden-archived');
        } else {
            $(this).closest('.column').find('.archived').addClass('hidden-archived');
        }
    });

    $(".select-all input, .list .item .ui.checkbox input").prop('checked', false);

    $(document.body).on("click", '.select-all', function () {
        if ($(this).hasClass('checked')) {
            $(this).closest('.column').find('.list .item .ui.checkbox').checkbox('check');
        } else {
            $(this).closest('.column').find('.list .item .ui.checkbox').checkbox('uncheck');
        }
    });

    $(document.body).on("click", '.delete-selected', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var a = $(e.currentTarget);
        var itemIds = [];
        $(this).closest('.ui.tab').find('.list .item .ui.checkbox').each(function( index ) {
            if ($(this).checkbox('is checked')) {
                itemIds.push($(this).closest('.item').find('.content.data').data('id'));
            }
        });

        if (itemIds.length == 0) {
            return false;
        }
        $('.ui.modal.some-delete .selected-items-count').text(itemIds.length);
        $('.ui.modal.some-delete .approve').attr({
            'href': a.attr('data-href'),
            'data-selected-ids': itemIds.join(','),
            'data-tab': a.closest('.tab').attr("data-tab")
        });
        $('.ui.modal.some-delete').modal('show');
    });
    $(document.body).on("click", '.ui.modal.some-delete .approve', function (e) {
        var a = $(e.currentTarget);
        $.ajax({
            url : a.attr('href'),
            type: 'POST',
            data: {'ids': a.attr('data-selected-ids')},
            dataType: 'json',
            success: function () {
               localStorage.setItem("active-tab", a.attr("data-tab"));
               window.location.reload();
            }
        });

        return false;

    });



    setTimeout(function() {
        $('.password').val('')
    }, 1000);


    $('.add_user_block .show-password, .edit_user_block .show-password').prop('checked', false);
    $('.add_user_block .show-password, .edit_user_block .show-password').change(function (e) {
        var pass = $(this).closest('form').find('.password');
        if ($(this).prop('checked')) {
            pass.attr('type', 'text');
        } else {
            pass.attr('type', 'password');
        }
    });

    initMediaForm();
    highlightFilters();

    $('#region_id, #country_filter, #name_filter').on('change', function () {
        highlightFilters();
    });

    $(document.body).on('click', '.pagination a', function(e) {
        e.preventDefault();
        var tab = $("#admin-tabs a.active").data('tab');
        var href = $(e.currentTarget).attr('href');
        var limit = $('#'+tab+'-list .ui.labels a.teal').text();
        var onlyDeleted = $('#'+tab+'-list').attr('data-deleted');

        ajaxPaginate(href, {limit: limit, only_deleted: onlyDeleted}, '#'+tab+'-list');
    });

    $(document.body).on('click', '.ui.labels a:not(.teal)', function(e) {
        e.preventDefault();
        var tab = $("#admin-tabs a.active").data('tab');
        var href = '/'+tab+'/get-'+tab;
        var limit = $(e.currentTarget).text();
        var onlyDeleted = $('#'+tab+'-list').attr('data-deleted');

        ajaxPaginate(href, {limit: limit, only_deleted: onlyDeleted}, '#'+tab+'-list');
    });

    function ajaxPaginate(url, params, selector) {
        $.ajax({
            url: url,
            type: 'GET',
            data: params,
            success: function (data) {
                $(selector).html(data);
                $("body, html").animate({"scrollTop": 0}, 500);
                $('.ui.checkbox').checkbox({
                    'onChecked': function() {$(this).val('1');},
                    'onUnchecked': function() {$(this).val('0');}
                });
                localStorage.removeItem('admin_pagination');
            }
        });
    }

    paginate(); // only if admin_page exists

    function paginate()
    {
        var adminPagination = JSON.parse(localStorage.getItem('admin_pagination'));

        if (adminPagination !== null) {
            var hasPager = ['users','media', 'newsbreaks', 'speakers'];
            var tab = $("#admin-tabs a.active").data('tab');
            var limit = 25;

            if (adminPagination.teal !== null) {
                $("div [data-tab='"+tab+"'] .ui .circular").find("a#teal-"+adminPagination.teal)
                    .addClass('teal')
                    .siblings().removeClass('teal');
                limit = adminPagination.teal;
            }

            if ($.inArray(tab, hasPager) !== -1) {
                var href = '/'+tab+'/get-'+tab+'?page=' + adminPagination.page;

                ajaxPaginate(href, {limit: limit}, '#'+tab+'-list');
            }
        }
    }

    /* Handle deleted list */
    $(document.body).on('click', '.deleted-items', function(e) {
        e.preventDefault();
        var tab = $(this).data('tab');
        var isChecked = ($(this).hasClass('checked')) ? true : false;
        var href = '/'+tab+'/get-'+tab;
        var limit = 25;

        $('#'+tab+'-list').attr('data-deleted', isChecked);
        ajaxPaginate(href, {limit: limit, only_deleted: isChecked}, '#'+tab+'-list');
    });
});