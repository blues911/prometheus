var app = angular.module('Reports', []);
app.config(function ($httpProvider) {
    $httpProvider.defaults.transformRequest = function(data){
        if (data === undefined) {
            return data;
        }
        return $.param(data);
    };
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
});

app.controller('ReportController', ['$scope', '$timeout', 'api', function($scope, $timeout, api) {

    $scope.top_stories          = [];
    $scope.comparison           = [];
    $scope.location_stat        = [];
    $scope.message_penetrations = [];
    $scope.product_mention      = [];
    $scope.reports              = reports;
    $scope.new_report           = {
        'data'          : {'name': '', 'url': ''},
        'first_step'    : true,
        'second_step'   : false,
        'is_show_modal' : false
    };
    $scope.activated_tab        = null;
    $scope.media_categories     = ['general', 'it', 'business', 'vertical'];
    $scope.media_subcategories  = [
        'automotive',
        'banking and insurance',
        'education',
        'enterprise',
        'financial',
        'gaming',
        'government',
        'healthcare',
        'industrial',
        'lifestyle',
        'motorsport',
        'msp media',
        'parental media',
        'pet media',
        'smb media',
        'travel',
        'virtualization',
    ];
    $scope.media_lists          = ['diamond', 'platinum', 'gold'];
    $scope.main_tonalities      = ['positive', 'neutral', 'negative'];
    $scope.is_private_report    = isPrivateReport;
    $scope.edit_report_data     = {
        'id'       : '',
        'name'     : '',
        'index'    : '',
        'is_edit'  : false,
        'is_error' : false
    };

    $scope.comparison_titles = {
        'newsbreaks': 'Newsbreaks',
        'campaigns': 'Campaigns',
        'products': 'Products',
        'speakers': 'Speakers',
        'events': 'Events',
    };

    $scope.filters = {
        'newsbreaks': '',
        'campaigns':  '',
        'products':   '',
        'speakers':   '',
        'events':     ''
    };

    $scope.save_filters = $scope.filters;

    $scope.init = function() {
        filters = getFilter();
        if (null !== filters && typeof filters['comparison'] !== 'undefined') {
            $scope.filters = filters['comparison'];
        }
        $scope.activated_tab = $scope.get_tab();
    };

    $scope.set_top = function(filter) {
        reportFilter = localStorage.getItem('report_filter');
        options = {};
        if (reportFilter != null) {
            reportFilter = JSON.parse(reportFilter);
        }
        options['filter'] = filter;
        if (reportFilter['start'] !== 'undefined' && reportFilter['end'] !== 'undefined') {
            options['date_start'] = reportFilter['start'];
            options['date_end'] = reportFilter['end'];
        }
        $scope.filters[filter] = '';
        setTimeout(function() {
          api.top_filter(options, function(response) {
              $scope.filters[filter] = response.join();
              $scope.filter();
          });
        });
    };

    $scope.clear_filter = function(filter) {
        if (typeof $scope.filters[filter] == 'undefined') {
            return;
        }
        $scope.filters[filter] =  '';
        $scope.filter();
    };

    $scope.$watch("activated_tab", function() {
        $scope.dashboard($scope.activated_tab);
    }, true);

    $scope.get_tab = function() {
        if (null == window.location.hash.match(/#\//) && null == window.location.hash.match(/#/)) {
            return 'executive_summary';
        }
        return window.location.hash.replace(/#/, '').replace(/\//, '');
    };

    $scope.fill_filter = function() {
        savefilters = localStorage.getItem('report_filter');

        if (null !== savefilters) {
            savefilters = JSON.parse(savefilters);
        }
        if (null !== savefilters && typeof savefilters['comparison'] !== 'undefined') {
            $scope.savefilters = savefilters['comparison'];
        }
    };

    $scope.fill_filter();

    $scope.deleteInvalidFilter = function(page, filter) {
        invalidFilters = {
            'executive_summary'         : {'comparison' : 'comparison'},
            'general_information'       : {'comparison' : 'comparison'},
            'dynamics'                  : {'comparison' : 'comparison'}
        };

        if (typeof invalidFilters[page] === 'undefined') {
            return;
        }

        for(index in filter) { 
            if (index in invalidFilters[page]) {
               delete filter[index];
            }
        }
    }

    $scope.dashboard = function(page) {
        filter = angular.copy(getFilter());

        if (typeof page === 'undefined' || page === null) {
            return;
        }
        if ('executive_summary' === page) {
            $scope.get_location_stat();
            $scope.get_message_penetrations();
            $scope.get_product_mentions();
        }
        if ('general_information' === page) {
            $scope.get_top_stories();
        }
        if ('comparison' === page) {
            setTimeout(function() {
                $scope.get_comparison();
            })
        }

        $scope.deleteInvalidFilter(page, filter);
        getCovarageTotals(filter);
    };

    $scope.filter = function() {
        saveFilter({"comparison": $scope.filters});
        $scope.dashboard($scope.activated_tab);
    };

    $scope.clear = function() {
        $scope.filters = {
            'newsbreaks': '',
            'campaigns':  '',
            'products':   '',
            'speakers':   '',
            'events':     ''
        };
    };

    $scope.get_top_stories = function() {
        api.top_stories(getFilter(), function(response) {
            $scope.top_stories = response;
        });
    };

    $scope.get_comparison = function() {
        filters = angular.extend(getFilter(), $scope.filters);
        api.comparison(filters, function(response) {
            $scope.comparison = response;
        });
    };

    $scope.get_location_stat = function() {
        api.regional_kpi(getFilter(), function(response) {
            $scope.location_stat = response;
        });
    };

    $scope.get_product_mentions = function() {
        api.product_mentions(getFilter(), function(response) {
            $scope.product_mention = response;
        });
    };

    $scope.get_message_penetrations = function() {
        api.message_penetrations(getFilter(), function(response) {
            $scope.message_penetrations = response;
        });
    };

    $scope.active_tab = function(tab) {
        $scope.activated_tab = tab;
    };

    $scope.is_total_column = function(key) {
        return key === 'total';
    };

    $scope.row_exists = function(key) {
        return typeof $scope.comparison[key] !== 'undefined'
    };

    $scope.is_show_kpi = function() {
        $('.ui.checkbox').checkbox();
        return Object.keys($scope.location_stat).length > 0;
    };

    $scope.get_table_value = function(data, section, region, subsection) {
        if (
            typeof data[section] === 'undefined' ||
            typeof data[section][region] === 'undefined' ||
            (typeof subsection !== 'undefined' && typeof data[section][region][subsection] === 'undefined')
        ) {
            return '-';
        }
        if (typeof subsection !== 'undefined') {
            return data[section][region][subsection];
        }
        if (typeof subsection === 'undefined') {
            return data[section][region];
        }
        return '-';
    };

    $scope.get_kpi_value = function(section, location, subsection) {
        return $scope.get_table_value($scope.location_stat, section, location, subsection);
    };

    $scope.kpi_total_exists = function() {
        return undefined == $scope.location_stat['show_total'] || $scope.location_stat['show_total']
    }

    $scope.product_mention_total_exists = function() {
        return undefined == $scope.product_mention['show_total'] || $scope.product_mention['show_total']
    }

    $scope.product_mention_value = function(section, location, subsection) {
        return $scope.get_table_value($scope.product_mention, section, location, subsection);
    };

    $scope.get_comparison_value = function(section, location, subsection) {
        return $scope.get_table_value($scope.comparison, section, location, subsection);
    };

    $scope.go_to_coverage_with_section = function(section, location, filter) {
        map = {
            'newsbreaks': 'newsbreak_id',
            'campaigns': 'campaign_id',
            'products': 'product_id',
            'speakers': 'speaker_id',
            'events': 'event',
        };
        valid_filters = {
            'start' : 'start',
            'end' : 'end',
            'region_id' : 'region_id',
            'subregion_id' : 'subregion_id',
            'country_id' : 'country_id',
            'media_category' : 'media_category',
            'media_subcategory' : 'media_subcategory',
            'media_list' : 'media_list',
        };
        filter[map[section]] = location;
        return $scope.go_to_coverage(filter, valid_filters);
    };

    $scope.go_to_coverage_with_total_sections = function(filter) {
        filter = angular.extend(filter, $scope.filters)

        valid_filters = {
            'start' : 'start',
            'end' : 'end',
            'region_id' : 'region_id',
            'subregion_id' : 'subregion_id',
            'country_id' : 'country_id',
            'business' : 'business',
            'media_category' : 'media_category',
            'media_subcategory' : 'media_subcategory',
            'media_list' : 'media_list',
        };
        
        return $scope.go_to_coverage(filter, valid_filters);
    };

    

    $scope.go_to_coverage = function(filter, valid_filters) {
        var globalFilter = getFilter();
        if (undefined === filter) {
            redirectByFilters(globalFilter);
            return;
        }
        if (undefined !== filter.location_id) {
            globalFilter[getLocationFilterName()] = filter.location_id;
            delete filter.location_id;
        }
        if (undefined !== valid_filters) {
            for(var index in globalFilter) { 
                if (!(index in valid_filters)) {
                    delete globalFilter[index];
                }
            }
        }
        
        redirectByFilters($.extend(globalFilter, filter));
    };

    $scope.open_report_modal = function() {
        $scope.new_report.is_show_modal = true;
        $scope.new_report.data.url      = '';
        $scope.new_report.data.name     = '';
        $scope.new_report.first_step    = true;
        $scope.new_report.second_step   = false;
    };

    $scope.hide_modal = function()
    {
        $scope.new_report.is_show_modal = false;
    };

    $scope.export = function(e)
    {
        e.stopPropagation();
        var filter = getFilter();
        filter['filename'] = 'Comparison ratings' + getFileName();
        window.location.href = window.location.origin + "/stats/export/comparison?" + $.param(angular.extend(filter, $scope.filters));
    };

    $scope.save_report = function() {
        if ($scope.new_report.data.name.length <= 0) {
            $(".report-modal .field.first-step").addClass("error");
            return false;
        }
        $(".report-modal .field.first-step").removeClass("error");

        var options = {
            name: $scope.new_report.data.name,
            params: JSON.stringify(angular.extend(getFilter(), $scope.filters)),
            tab: $scope.get_tab()
        };
        api.create_report(options, function(response) {
            $scope.new_report.data.url    = response.url;
            $scope.new_report.first_step  = false;
            $scope.new_report.second_step = true;
            $scope.reports.unshift({
                'id'   : response.id,
                'name' : response.name,
                'url'  : response.url
            });
        });
    };

    $scope.save_pdf = function() {
        var filter = getFilter();
        var charts = [];
        $('#reports .ui.checkbox.pdf.checked').each(function() {
            charts.push($(this).find('input').attr('name'));
        });

        filter.filtered_by = $("#general_information_filter").val();
        filter.date_filter = $("#timeline_filter").val();
        filter.charts = charts.join();
        
        var options = {
            params: JSON.stringify(angular.extend(filter, $scope.filters)),
            tab: $scope.get_tab()
        };
        api.save_report_pdf(options, function(response) {
            $('#export-pdf').modal('show');
        });
    };

    $scope.delete_report = function(index) {
        if ($scope.reports.length > 0 && typeof $scope.reports[index] !== 'undefined') {
            var options = {
                'id'      : $scope.reports[index].id,
                '_method' : 'DELETE'
            };
            api.delete_report(options, function(response) {
                $scope.reports.splice(index, 1);
            });
        }
    };

    $scope.edit_report = function(index) {
        if (typeof $scope.reports[index] === 'undefined') {
            return false;
        }
        var report = $scope.reports[index];
        $scope.edit_report_data = {'id' : report.id, 'name': report.name, 'is_edit': true, 'index': index};
    };

    $scope.cancel_edit_report = function() {
        $scope.edit_report_data = {
            'id'       : '',
            'name'     : '',
            'index'    : '',
            'is_edit'  : false,
            'is_error' : false
        };
    };

    $scope.save_edit_report = function() {
        if ($scope.edit_report_data.name.length <= 0) {
            $scope.edit_report_data.is_error = true;
            return false;
        }
        api.edit_report($scope.edit_report_data, function(response) {
            if (typeof $scope.reports[$scope.edit_report_data.index] === 'undefined') {
                return false;
            }
            $scope.reports[$scope.edit_report_data.index].name = response.name;
            $scope.cancel_edit_report();
        });
    };

    if (isPrivateReport) {
        $scope.dashboard($scope.activated_tab);
    }
}]);
