app.filter('truncate', function () {
    return function (input, limit) {
        if (input !== null && input.length > limit) {
            return input.slice(0, limit)+' ...';
        } else {
            return input;
        }
    };
});

app.directive("zeroClipboard", function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            new ZeroClipboard(element);
        }
    };
});

app.directive("modal", function() {
    return {
        restrict: 'A',
        scope: {
            visible: '=',
        },
        link: function(scope, element, attrs) {
            scope.$watch("visible", function(value) {
                if (typeof scope.visible == 'undefined') {
                    return;
                }
                if (scope.visible) {
                    element.modal("setting", {
                        closable: false,
                        onApprove: function () {
                            return false;
                        }
                    }).modal('show');
                }
                else {
                    element.modal("hide");
                }
            });
        }
    };
});

app.directive("selectedInput", function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.click(function () {
                element.select();
            });
        }
    };
});

app.directive("dropdown", function() {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function(scope, element, attrs, ngModel) {
            element.dropdown({
                onChange: function (value) {
                    ngModel.$setViewValue(value);
                },
                fullTextSearch: 'exact-',
            });
            scope.$watch("filters", function(value) {
                if (value[attrs.filter].length > 0) {
                    element.dropdown('set selected', value[attrs.filter].split(","));
                    element.find(".ui.label").hover(function(e) {
                        $(this).attr('title', $(this).text());
                    });
                } else {
                    element.dropdown('clear');
                }

            }, true);

        }
    };
});
