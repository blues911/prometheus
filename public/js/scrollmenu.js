$(document).ready(function(){
    var menu = $("#filters-menu");
    var tabmenu = $("#reports-page .tabular.menu");
	var scroll_top = $('#page-body .header-row').outerHeight() + ($('#page-body .content-column').outerHeight() - $('#page-body .content-column').height())/2;
	var reports = $("#reports");
	var isActive = true;

	if (typeof $.cookie === 'function') {
		var cookied = $.cookie('fixedMenu');
		if (typeof cookied === 'string') {
			isActive = ($.cookie('fixedMenu') === 'true');
			if (!isActive) {
				$('#fixMenu').addClass('transparent');
				$('#fixMenu').text('Freeze Menu');
			} else {
				$('#fixMenu').removeClass('transparent');
				$('#fixMenu').text('Unfreeze Menu');
			}
		} 
	}
	
	$('#fixMenu').on('click', function() {
		isActive = !isActive;
		if (typeof $.cookie === 'function') {
			$.cookie('fixedMenu', isActive);
		}
		if (!isActive) {
			$(this).addClass('transparent');
			$(this).text('Freeze Menu');
		} else {
			$(this).removeClass('transparent');
			$(this).text('Unfreeze Menu');
		}
		$(this).blur();
	});

	$(window).scroll(function(){
		if (menu.css('display') != 'none') {
	        if ( $(this).scrollTop() > scroll_top && isActive){
				reports.css({'padding-top': menu.outerHeight() + tabmenu.outerHeight() + 'px'});
				menu.css({'top': tabmenu.outerHeight() + 'px'});
	            menu.addClass("fixed");
	            tabmenu.addClass("fixed-menu");
	        } else if($(this).scrollTop() <= scroll_top && menu.hasClass("fixed")) {
				reports.css({'padding-top': 0});
				menu.css({'top': 0});
	            menu.removeClass("fixed");
				tabmenu.removeClass("fixed-menu");
	        }
	    } else if (location.pathname + location.hash === '/charts#/reports') {
			if ( $(this).scrollTop() > scroll_top && isActive){
				reports.css({'padding-top': tabmenu.outerHeight() + 'px'});
				tabmenu.addClass("fixed-menu");
			} else if($(this).scrollTop() <= scroll_top && tabmenu.hasClass("fixed-menu")) {
				reports.css({'padding-top': 0});
				tabmenu.removeClass("fixed-menu");
			}
		}
	});
	

});