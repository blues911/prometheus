$(function() {
    $('#passwords').submit(function() {
        $.ajax({
            url     : '/user/send-new-password?sign='+$('#passwords').attr('sign'),
            method  : 'POST',
            dataType: 'json',
            data    : {
                password: $('#password').val(),
                password_confirmation: $('#password_confirmation').val()
            },
            success: function (response) {
                if (response.status == 'error') {
                    $('#password').addClass('error');
                    $('#password').closest('.input').addClass('error');
                    $('#password_confirmation').addClass('error');
                    $('#password_confirmation').closest('.input').addClass('error');
                    if (response.message !== 'undefined') {
                        $('#error').text(response.message);
                    } else {
                        $('#error').text('Check fields Password and Repeat Password');
                    }
                } else if (response.status == 'ok') {
                    $('#password').removeClass('error');
                    $('#password').closest('.input').removeClass('error');
                    $('#password_confirmation').removeClass('error');
                    $('#password_confirmation').closest('.input').removeClass('error');
                    $('#error').text('');
                    window.location.href = "/";
                }
            }
        });
        return false;
    });
});