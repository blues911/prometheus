$(document).ready(function () {
	var page = $('#kpi-page');
	var checkbox = JSON.parse(localStorage.getItem('kpi-regions')) ? JSON.parse(localStorage.getItem('kpi-regions')) : {};

	$('.ui.checkbox').checkbox();

	// -->VALIDATION
	$('#kpi-headers-modal .ui.form').form({
		on: 'blur',
		inline : true,
		fields: {
			net_effect   		: 'maxLength[255]',
			impact_index 		: 'maxLength[255]',
			negative 			: 'maxLength[255]',
			company_mindshare   : 'maxLength[255]',
			b2b_mindshare    	: 'maxLength[255]',
			b2c_mindshare    	: 'maxLength[255]'
		}
	});
	$('#kpi-records-modal .ui.form').form({
		on: 'blur',
		inline : true,
		fields: {
			company_mindshare   : 'maxLength[255]',
			b2b_mindshare    	: 'maxLength[255]',
			b2c_mindshare    	: 'maxLength[255]'
		}
	});
	// END VALIDATION--<

	page.find('table tr').hover(
		function() { $(this).find('.edit-column-block').show(); },
		function() { $(this).find('.edit-column-block').hide(); }
	);

	page.find('#kpi-regions .ui.checkbox').each(function(i, el) {
		var checkboxName = $(el).find('input').attr('name'),
		    table = page.find('.kpi-table-' + checkboxName);
		if (checkbox[checkboxName] == 1) {
			$(el).checkbox('check');
			table.show();
		} else {
			$(el).checkbox('uncheck');
			table.hide();
		}
	});
	localStorage.setItem("kpi-regions", JSON.stringify(checkbox));

	page.find('#kpi-regions .ui.checkbox').on('click', function() {
		var checkboxName = $(this).find('input').attr('name');
		var table = page.find('.kpi-table-' + checkboxName);
		if ($(this).checkbox('is checked')) {
			checkbox[checkboxName] = 1;
			table.show();
		} else {
			checkbox[checkboxName] = 0;
			table.hide();
		}
		localStorage.setItem("kpi-regions", JSON.stringify(checkbox));
	});

	page.find('thead .edit-column-btn').on('click', function() {
		var modal = $('#kpi-headers-modal'),
			form = modal.find('form'),
			el = $(this);
		modal.form('clear errors');
		$.ajax({
            method  : 'GET',
            url     : '/kpi/headers-by-region',
            dataType: 'json',
            data    : { id: $(this).attr("data-region-id")},
            success : function (response) {
            	if (response) {
            		modal.find('.header').text('Edit Headers for ' + el.attr('data-region-name'));
            		form.find('input[name="region_id"]').val(response.region_id);
					form.find('textarea[name="net_effect"]').val(response.net_effect);
					form.find('textarea[name="impact_index"]').val(response.impact_index);
					form.find('textarea[name="negative"]').val(response.negative);
					form.find('textarea[name="company_mindshare"]').val(response.company_mindshare);
					form.find('textarea[name="b2b_mindshare"]').val(response.b2b_mindshare);
					form.find('textarea[name="b2c_mindshare"]').val(response.b2c_mindshare);
					modal.modal('show');
            	}
            }
        });
	});

	page.find('tbody .edit-column-btn').on('click', function() {
		var modal = $('#kpi-records-modal'),
			form = modal.find('form'),
			el = $(this);
		modal.form('clear errors');
		$.ajax({
            method  : 'GET',
            url     : '/kpi/records-by-region',
            dataType: 'json',
            data    : { id: $(this).attr("data-region-id"), date: $(this).attr("data-date")},
            success : function (response) {
            	if (response) {
            		modal.find('.header').text('Edit Record for ' + el.attr('data-region-name') + ' - ' + el.closest('td').text());
             		form.find('input[name="region_id"]').val(response.region_id);
             		form.find('input[name="date"]').val(response.date);
					form.find('textarea[name="company_mindshare"]').val(response.company_mindshare);
					form.find('textarea[name="b2b_mindshare"]').val(response.b2b_mindshare);
					form.find('textarea[name="b2c_mindshare"]').val(response.b2c_mindshare);
					if (response.status == 'active') {
						form.find('input[name="status"]').closest('.ui.checkbox').checkbox('check');
					}
					if (response.is_impact_index == 1) {
						form.find('input[name="is_impact_index"]').closest('.ui.checkbox').checkbox('check');
					}
					if (response.is_net_effect == 1) {
						form.find('input[name="is_net_effect"]').closest('.ui.checkbox').checkbox('check');
					}
					if (response.is_negative == 1) {
						form.find('input[name="is_negative"]').closest('.ui.checkbox').checkbox('check');
					}
					if (response.is_company_mindshare == 1) {
						form.find('input[name="is_company_mindshare"]').closest('.ui.checkbox').checkbox('check');
					}
					if (response.is_b2b_mindshare == 1) {
						form.find('input[name="is_b2b_mindshare"]').closest('.ui.checkbox').checkbox('check');
					}
					if (response.is_b2c_mindshare == 1) {
						form.find('input[name="is_b2c_mindshare"]').closest('.ui.checkbox').checkbox('check');
					}
					modal.modal('show');
            	}
            }
        });
	});

	page.find('#kpi-headers-modal .save-edit').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var form = $('#kpi-headers-modal form');
        if (form.form('is valid')) {
			var editData = form.serialize();
	        $.ajax({
	            url     : "/kpi/save-headers",
	            method  : "POST",
	            data    : editData,
	            success: function (response) {
	                if (!response.error) {
	                    location.reload()
	                }
	            }
	        });
		}
    });

    page.find('#kpi-records-modal .save-edit').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var form = $('#kpi-records-modal form');
        if (form.form('is valid')) {
			var editData = form.serialize();
	        $.ajax({
	            url     : "/kpi/save-records",
	            method  : "POST",
	            data    : editData,
	            success: function (response) {
	                if (!response.error) {
	                    location.reload()
	                }
	            }
	        });
		}
    });

    page.find('.kasp-lab span').each(function() {
    	var str = $(this).text().replace(/\d+\s*Kaspersky\s*Lab\s*.\s*\d+([.,]\d+)?%/gi, '<span class="kasp-lab">' + '$&' + '</span>');
    	$(this).html(str);
    });

});