$(function () {
    var tableEl       = $('#export-history-table'),
    scrollBody        = $('.dataTables_scrollBody'),
    selected_delete   = [],
    selected_to_draft = [],
    selected_approve  = [],
    tableColumns,
    recordsForDelete  = 0,
    table             = tableEl.DataTable({
        scrollCollapse: true,
        searching     : false,
        processing    : true,
        language: {
            processing: '<img src="/img/loading.gif"/>',
            paginate: {
                "sNext": '<i class="caret right icon"></i>',
                "sPrevious": '<i class="caret left icon"></i>'
            }
        },
        aoColumnDefs: [
            {
                "bSortable": false,
                "aTargets" : "_all"
            },
            {
                "targets": "col-file-type",
                "render" : function (value, action, row, settings) {
                    return '<i class="icon-coverage square outline icon"></i>'
                }
            },
            {
                "targets": "col-filename",
                "render" : function (value, action, row, settings) {
                    return row.name;
                }
            },
            {
                "targets": "col-actions",
                "render" : function (value, action, row, settings) {
                    return '<a href="/export/file/' + row.id + '" class="right active-block ui download button blue mini basic" title="Download file"><i class="download icon"></i></a>'
                    + '<a href="#" class="right ui delete button red mini basic" title="Delete export file"><i class="delete icon"></i></a>'
                    + '<span class="approve">'
                        + '<a href="#" class="ui no button red mini">No</a>'
                        + '<a href="/export/delete/' + row.id + '" class="ui yes button green mini">Yes</a>'
                    + '</span>';
                }
            },
        ],
        columns: [
            { "data": "id", class: 'ui center aligned no-padding icon-column'},
            { "data": "name" },
            { "data": "created_at", class: 'date-column' },
            { "data": "type", class: 'menu' },
        ],
        dom: '<"top"f>rt<"bottom"ilp><"clear">',
        serverSide: true,
        ajax: {
            url : "/export/history",
            data: function (params) {
                params.start_range = $("#start_range").val();
                params.end_range   = $("#end_range").val();
            },
            dataSrc: function ( json ) {
                return json.data;
            }
        },
        drawCallback: function(settings) {
            tableEl.find("thead").remove();
            processHighlight($('#start_range'), 'datepicker');
        }
    });

    table.on('draw.dt', function () {
        table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i, data) {
            $(cell).closest("tr").attr("id", "export-history-" + table.row(i).data().id);
            $(cell).closest("tr").addClass("item");
        });
    });

    $('.input-daterange').datepicker({
        format        : "yyyy-mm-dd",
        weekStart     : 1,
        autoclose     : true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        table.draw();
    });

    $('#export-history-table').resize();

    $('.dataTables_filter label').addClass('ui transparent icon input');
    $('.dataTables_filter input').after('<i class="search icon"></i>');
    $('.dataTables_paginate').addClass('ui borderless pagination menu');
    $('.dataTables_paginate a').addClass('item');

    $('.dataTables_filter').click(function (e) {
        $('.dataTables_filter').css('border-color', 'rgba(39, 41, 43, 0.3)')
        $(this).find('input').animate({ width: '150px' }, 500);
    });

    $("body").click(function(e) {
        if ($(e.target).closest(".dataTables_filter").length === 0) {
            $('.dataTables_filter').css('border-color', 'rgba(39, 41, 43, 0.15)');
            $('.dataTables_filter input').animate({ width: '0px' }, 500);
        }
    });

    $(document.body).on("click", ".export-history a.delete", function(e) {
        var b = $(e.currentTarget);

        if (b.hasClass('disabled')) {
            return false;
        }

        b.closest(".item").find('.active-block').animate(
            {opacity: 0},
            {
                complete: function () {
                    b.closest(".item").find('.active-block').css('display', 'none');
                }
            }
        );

        b.animate({
            opacity: 0
        }, {
            complete: function () {
                b.parent().addClass("active-item-menu");
                b.css('display', 'none');
                b.parent().find('.approve').css({'display': 'inline'});
                b.parent().find('.approve').animate({
                    opacity: 1
                });
            }
        });

        return false;
    });

    $(document.body).on("click", ".export-history .no", function(e) {
        var a = $(e.currentTarget).parent('.approve');
        a.animate(
            {
                opacity: 0
            },
            {
                complete: function () {
                    a.parent().removeClass("active-item-menu");
                    a.css('display', 'none');
                    a.closest(".item").find('a.delete, .active-block').css('display', 'inline');
                    a.closest(".item").find('a.delete, .active-block').animate({
                        opacity: 1
                    });
                }
            }
        );
        return false;
    });

    $(document.body).on("click", ".export-history .yes", function(e) {
        var a = $(e.currentTarget);
        $.ajax({
            url     : a.attr('href'),
            method  : 'POST',
            dataType: 'json',
            success : function (response) {
                if (typeof response.id !== 'undefined') {
                    $("#export-history-" + response.id).remove();
                }
            }
        });

        return false;
    });
});