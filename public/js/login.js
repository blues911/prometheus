$(function(){
    var date = new Date();
    var currentTimeZoneOffsetInSeconds = - date.getTimezoneOffset() * 60;
    $('.loginform input[name="time-offset"]').val(currentTimeZoneOffsetInSeconds);

    $('.ui.checkbox').checkbox();

    $('.email-modal-login').modal("setting", {
        closable : false,
        onApprove: function () {
            return false;
        }
    })

    $(document.body).on("click", ".email-modal-login .approve", function () {
        $(".email-modal-login .field").removeClass("error");
        var email = $(".email-modal-login #input-email-login").val();

        if (typeof email !== 'undefined' && email.length > 0) {
            $.ajax({
                url     : '/user/send-mail-with-password',
                method  : 'POST',
                dataType: 'json',
                data    : {email: email},
                success : function (response) {
                    if (response.status == 'ok') {
                        $(".first-new-password").hide();
                        $(".second-new-password").show();
                    } else if (response.status == 'error'){
                        $(".email-modal-login .field").addClass("error");
                    }
                }
            });
        }
        else {
            $(".email-modal-login .field").addClass("error");
        }
    });

    $('.button.change-password-login').click(function () {
        if ($(".email-modal-login .field").hasClass("error")) {
            $(".email-modal-login .field").removeClass("error");
        }
        $(".first-new-password").show();
        $(".second-new-password").hide();
        $('.email-modal-login').modal("show");
        return false;
    });
    $('.button.second-new-password').click(function () {
        $(".email-modal-login #input-email-login").val('');
        $('.email-modal-login').modal("hide");
    });

    $('#show-password').prop('checked', false);

    $('#show-password').change(function (e) {
        var pass = $('#password');
        if ($('#show-password').prop('checked')) {
            pass.attr('type', 'text');
        } else {
            pass.attr('type', 'password');
        }
    });
});