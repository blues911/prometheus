function highlightFilters() {
    $('.filters .form').find('[name="start"]')
        .add($('.filters .form').find('[name="end"]'))
        .each(function () {
            processHighlight(this, 'datepicker');
        });

    $('#media_list_filter, #media_category_filter, #country_id, #business_filter')
        .add($('.filters .form').find('[name="region_id"]'))
        .add('#campaign_list_filter, #newsbreak_list_filter, #product_list_filter, #speaker_list_filter, #event_list_filter')
        .each(function () {
            processHighlight(this, 'dropdown');
        });

    $('#campaign_id, #news_id, #product_id, #speaker_id, #event_filter').each(function () {
        processHighlight(this);
    });
}

function loadTop() {
    var filter = getFilter();
    if (this.top && this.path && this.chart) {
        var path = window.location.hash.substr(2, window.location.hash.length);
        filter["top"] = this.top;
        if (path === 'dynamics') {
            filter['date_filter'] = $("#timeline_filter").val();
            this.chart.render(this.path, filter, true);
        }
        else {
            if (path === 'general_information') {
                filter['filter_by'] = $("#general_information_filter").val();
                this.chart.render(this.path, filter, true);
            }
            else if (path === 'executive_summary') {
                // keyMessagesTotal(filter);
                filter['filter_by'] = $("#general_information_filter").val();
                this.chart.render(this.path, filter, true);
            }
        }
    }
}

function getFilter() {
    var start          = $('.filters .form').find('[name="start"]').val(),
        end            = $('.filters .form').find('[name="end"]').val(),
        region_id      = $('.filters .form').find('[name="region_id"]').val(),
        subregion_id   = $('.filters .form').find('[name="subregion_id"]').val(),
        event          = $('#event_filter').val(),
        news_id        = $('#news_id').val(),
        product_id     = $('#product_id').val(),
        country_id     = $('#country_id').val(),
        campaign_id    = $('#campaign_id').val(),
        business       = $('#business_filter').val(),
        speaker_id     = $('#speaker_id').val(),
        media_list     = $('#media_list_filter').val(),
        media_category = $('#media_category_filter').val(),
        media_subcategory = $('#media_subcategory_filter').val(),
        filter         = {};

    if (null !== localStorage.getItem('report_filter')) {
        filter = JSON.parse(localStorage.getItem('report_filter'));
    }
    if (start.length > 0) {
        filter['start']          = start;
    }
    if (end.length > 0) {
        filter['end']            = end;
    }
    if (region_id.length > 0) {
        filter['region_id']      = region_id;
    }
    if (subregion_id.length > 0) {
        filter['subregion_id']   = subregion_id;
    }
    if (event.length > 0) {
        filter['event']          = event;
    }
    if (news_id.length > 0) {
        filter['newsbreak_id']   = news_id;
    }
    if (product_id.length > 0) {
        filter['product_id']     = product_id;
    }
    if (country_id.length > 0 && 'All' != country_id) {
        filter['country_id']     = country_id;
    }
    if (campaign_id.length > 0) {
        filter['campaign_id']    = campaign_id;
    }
    if (business.length > 0) {
        filter['business']       = business;
    }
    if (speaker_id.length > 0) {
        filter['speaker_id']     = speaker_id;
    }
    if (media_list.length > 0) {
        filter['media_list']     = media_list;
    }
    if (media_category.length > 0) {
        filter['media_category'] = media_category;
    }
    if (media_subcategory.length > 0) {
        filter['media_subcategory'] = media_subcategory;
    }

    return filter;
}

function isset(value) {
    return 'undefined' !== typeof value;
}

function clearStorageFilter() {
    localStorage.removeItem('report_filter');
}

function saveFilter(filters) {
    filters = filters || {}
    clearStorageFilter();
    var filter   = getFilter(),
        news     = $('#news_filter').val(),
        product  = $('#product_filter').val(),
        campaign = $('#campaign_filter').val(),
        business = $('#business_filter').val(),
        speaker  = $('#speaker_filter').val();

    $('#campaign_list_filter').next('input').val('');
    $('#newsbreak_list_filter').next('input').val('');
    $('#product_list_filter').next('input').val('');
    $('#speaker_list_filter').next('input').val('');
    $('#event_list_filter').next('input').val('');

    if (news.length > 0) {
        filter['news'] = news;
    }
    if (product.length > 0) {
        filter['product'] = product;
    }
    if (campaign.length > 0) {
        filter['campaign'] = campaign;
    }
    if (business.length > 0) {
        filter['business'] = business;
    }
    if (speaker.length > 0) {
        filter['speaker'] = speaker;
    }
    localStorage.setItem('report_filter', JSON.stringify(jQuery.extend(filter, filters)));
}

function fillFilter() {
    if (null !== localStorage.getItem('report_filter')) {
        var filter = JSON.parse(localStorage.getItem('report_filter'));
        if (isset(filter['start'])) {
            $('.filters .form').find('[name="start"]').datepicker('setDate', filter['start']);
        }
        if (isset(filter['end'])) {
            $('.filters .form').find('[name="end"]').datepicker('setDate', filter['end']);
        }
        if (isset(filter['region_id'])) {
            $('.ui.dropdown.region-filter').dropdown('set value', filter['region_id']).dropdown('set selected', filter['region_id']);
        }
        if (isset(filter['subregion_id'])) {
            $('.ui.dropdown.subregion-filter').dropdown('set value', filter['subregion_id']).dropdown('set selected', filter['subregion_id']);
        }
        if (isset(filter['country_id'])) {
            setTimeout(function() {
                $('.ui.dropdown.country-filter').dropdown('set value', filter['country_id']).dropdown('set selected', filter['country_id']);
            }, 1000);
        }
        if (isset(filter['event'])) {
            $('#event_filter').val(filter['event']);
        }
        if (isset(filter['newsbreak_id']) && isset(filter['news'])) {
            $('#news_id').val(filter['newsbreak_id']);
            $('#news_filter').val(filter['news']);
        }
        if (isset(filter['product_id']) && isset(filter['product'])) {
            $('#product_id').val(filter['product_id']);
            $('#product_filter').val(filter['product']);
        }
        if (isset(filter['campaign_id']) && isset(filter['campaign'])) {
            $('#campaign_id').val(filter['campaign_id']);
            $('#campaign_filter').val(filter['campaign']);
        }
        if (isset(filter['business'])) {
            $(".ui.dropdown.business").dropdown('set value', filter['business']).dropdown('set selected', filter['business']);
        }
        if (isset(filter['speaker_id']) && isset(filter['speaker'])) {
            $('#speaker_id').val(filter['speaker_id']);
            $('#speaker_filter').val(filter['speaker']);
        }
        if (isset(filter['media_list'])) {
            $(".ui.dropdown.media-list").dropdown('set value', filter['media_list']).dropdown('set selected', filter['media_list']);
        }
        if (isset(filter['media_category'])) {
            $(".ui.dropdown.media-category").dropdown('set value', filter['media_category']).dropdown('set selected', filter['media_category']);
        }
        if (isset(filter['media_subcategory'])) {
            $(".ui.dropdown.media-subcategory").dropdown('set value', filter['media_subcategory']).dropdown('set selected', filter['media_subcategory']);
        }

        setTimeout(function() {
            highlightFilters();
        }, 1200);
    }
}

getCovarageTotals = function(filter) {
    $.ajax({
        url     : '/stats/coverage-totals',
        method  : 'GET',
        dataType: 'json',
        data    : filter || null,
        success: function (data) {
            if (typeof data !== 'undefined') {
                $("#coverage_count").html(data['coverage_count']);
                $("#impact_index").html(data['impact_index']);
                $("#net_effect").html(data['net_effect']);
                $("#media_outreach").html(data['media_outreach']);
            }
        }
    });
};

function redirectByFilters(filter) {
    localStorage.removeItem('filter');
    window.location.href = window.location.origin + "/coverage?" + $.param(filter);
}

function getLocationFilterName() {
    var region = $('.filters .form').find('[name="region_id"]').val();
    var subregion = $('.filters .form').find('[name="subregion_id"]').val();
    var filter = '';
    if (region == '' && subregion == '') {
        filter = 'region_id';
    } else if (region != '' && subregion == '') {
        filter = 'subregion_id';
    } else {
        filter = 'country_id';
    }
    return filter;
}

function formatDate(date) {
    var year  = date.getFullYear();
    var month = date.getMonth() + 1;
    var day   = date.getDate();
    month = (1 === month.toString().length) ? '0' + month : month;
    day   = (1 === day.toString().length) ? '0' + day : day;
    return year + '-' + month + '-' + day;

}

function getFileName() {
    var filename = '';
    if (filters.country_id != undefined) {
        $.each(countries, function () {
            if(this.id == filters.country_id) {
                filename = filename + '_' + this.value;
                return false;
            }
        });
    } else if (filters.region_id != undefined) {
        $.each(regions, function () {
            if(this.id == filters.region_id) {
                filename = filename + '_' + this.name;
                return false;
            }
        });
    }
    if (filters.start != undefined) {
        filename = filename + '_from' + filters.start;
    }
    if (filters.end != undefined) {
        filename = filename + '_to' + filters.end;
    }

    return filename;
}

$(function () {
    "use strict";

    $('.ui.checkbox').checkbox('uncheck');

    $('.input-daterange').datepicker({
        format: "yyyy-mm-dd",
        weekStart: 1,
        endDate: "today",
        autoclose: true,
        todayHighlight: true
    });

    $('.ui.dropdown.country-filter').dropdown();
    $('.ui.dropdown.region-filter').dropdown({
        onChange: function (value) {
            // clear subregions
            $('#filters-menu').find($('[name="subregion"]')).val("");
            $(".ui.dropdown.subregion-filter").dropdown('set value', "").dropdown('set selected', "All subregions");
            $('.ui.dropdown.subregion-filter .menu').empty();
            $('.ui.dropdown.subregion-filter .text').text('All subregions');
            var subregionItem = $('<div>')
                .addClass('item')
                .addClass('active')
                .attr('data-value', '')
                .text('All subregions');
            $('.ui.dropdown.subregion-filter .menu').append(subregionItem);
            // clear countries
            $('#filters-menu').find($('[name="country"]')).val("");
            $(".ui.dropdown.country-filter").dropdown('set value', "").dropdown('set selected', "All countries");
            $('.ui.dropdown.country-filter .menu').empty();
            $('.ui.dropdown.country-filter .text').text('All countries');
            var countryItem = $('<div>')
                .addClass('item')
                .addClass('active')
                .attr('data-value', '')
                .text('All countries');
            $('.ui.dropdown.country-filter .menu').append(countryItem);
            if (value != '') {
                $.each(subregions, function(key, subregion) {
                    if (subregion['region_id'] == value) {
                        subregionItem = $('<div>')
                            .addClass('item')
                            .attr('data-value', subregion['id'])
                            .text(subregion['name']);
                        $('.ui.dropdown.subregion-filter .menu').append(subregionItem);
                    }
                });
                $.each(countries, function(key, country) {
                    if (country['region_id'] == value) {
                        countryItem = $('<div>')
                            .addClass('item')
                            .attr('data-value', country['id'])
                            .text(country['name']);
                        $('.ui.dropdown.country-filter .menu').append(countryItem);
                    }
                });
            } else {
                $.each(subregions, function(key, subregion) {
                    subregionItem = $('<div>')
                        .addClass('item')
                        .attr('data-value', subregion['id'])
                        .text(subregion['name']);
                    $('.ui.dropdown.subregion-filter .menu').append(subregionItem);
                });
                $.each(countries, function(key, country) {
                    countryItem = $('<div>')
                        .addClass('item')
                        .attr('data-value', country['id'])
                        .text(country['name']);
                    $('.ui.dropdown.country-filter .menu').append(countryItem);
                });
            }
        }
    });

    $('.ui.dropdown.subregion-filter').dropdown({
        onChange: function (value) {
            $('#filters-menu').find($('[name="country"]')).val("");
            $(".ui.dropdown.country-filter").dropdown('set value', "").dropdown('set selected', "All countries");
            $('.ui.dropdown.country-filter .menu').empty();
            $('.ui.dropdown.country-filter .text').text('All countries');
            var item = $('<div>')
                .addClass('item')
                .addClass('active')
                .attr('data-value', '')
                .text('All countries');
            $('.ui.dropdown.country-filter .menu').append(item);

            if (value != '') {
                $.each(countries, function(key, country) {
                    if (country['subregion_id'] == value) {
                        item = $('<div>')
                            .addClass('item')
                            .attr('data-value', country['id'])
                            .text(country['name']);
                        $('.ui.dropdown.country-filter .menu').append(item);
                    }
                });
          } else {
              var region_id = $('.ui.dropdown.region-filter').find($('[name="region_id"]')).val();
              if (region_id != '') {
                  $.each(countries, function(key, country) {
                      if (country['region_id'] == region_id) {
                          item = $('<div>')
                              .addClass('item')
                              .attr('data-value', country['id'])
                              .text(country['name']);
                          $('.ui.dropdown.country-filter .menu').append(item);
                      }
                  });
              } else {
                  $.each(countries, function(key, country) {
                      item = $('<div>')
                          .addClass('item')
                          .attr('data-value', country['id'])
                          .text(country['name']);
                      $('.ui.dropdown.country-filter .menu').append(item);
                  });
              }

          }
        }
    });

    if (typeof region !== 'undefined' && region.length > 0) {
        $(".region-filter").data().moduleDropdown.action.activate(undefined, region)
    }

    jQuery('#event_filter').autocomplete({
        'minLength':'2',
        'source'   : events
    });

    jQuery('#news_filter').autocomplete({
        'minLength' :'2',
        'change'    : function(event, ui) {
            // записываем полученный id в скрытое поле
            this.value = ui.item ? ui.item.value : null;
            $("#news_id").val(ui.item ? ui.item.id : null);
            $("#news_filter").change();
            return false;
        },
        'source'     : news
    });

    jQuery('#product_filter').autocomplete({
        'minLength' :'2',
        'change'    : function(event, ui) {
            // записываем полученный id в скрытое поле
            this.value = ui.item ? ui.item.value : null;
            $("#product_id").val(ui.item ? ui.item.id : null);
            $("#product_filter").change();
            return false;
        },
        'source'     : products
    });

    jQuery('#campaign_filter').autocomplete({
        'minLength' :'2',
        'change'    : function(event, ui) {
            // записываем полученный id в скрытое поле
            this.value = ui.item ? ui.item.value : null;
            $("#campaign_id").val(ui.item ? ui.item.id : null);
            $("#campaign_filter").change();
            return false;
        },
        'source'    : campaigns
    });

    jQuery('#speaker_filter').autocomplete({
        'minLength' :'2',
        'change'    : function(event, ui) {
            // записываем полученный id в скрытое поле
            this.value = ui.item ? ui.item.value : null;
            $("#speaker_id").val(ui.item ? ui.item.id : null);
            $("#speaker_filter").change();
            return false;
        },
        'source'    : speakers
    });

    $('.ui.dropdown.media-list').dropdown();

    $('.ui.dropdown.media-category').dropdown();

    $('.ui.dropdown.media-subcategory').dropdown();

    $('.ui.dropdown.business').dropdown();

    fillFilter();

    // Overwrite method to do padding for title
    Highcharts.Chart.prototype.layOutTitles = function (redraw) {
        var titleOffset = 0,
            title = this.title,
            subtitle = this.subtitle,
            options = this.options,
            titleOptions = options.title,
            subtitleOptions = options.subtitle,
            requiresDirtyBox,
            renderer = this.renderer,
            autoWidth = this.spacingBox.width - 100,
            PX = 'px';

        if (title) {
            title
                .css({ width: (titleOptions.width || autoWidth) + PX })
                .align(Highcharts.extend({
                    y: renderer.fontMetrics(titleOptions.style.fontSize, title).b - 3
                }, titleOptions), false, 'spacingBox');

            if (!titleOptions.floating && !titleOptions.verticalAlign) {
                titleOffset = title.getBBox().height;
            }
        }
        if (subtitle) {
            subtitle
                .css({ width: (subtitleOptions.width || autoWidth) + PX })
                .align(Highcharts.extend({
                    y: titleOffset + (titleOptions.margin - 13) + renderer.fontMetrics(titleOptions.style.fontSize, subtitle).b
                }, subtitleOptions), false, 'spacingBox');

            if (!subtitleOptions.floating && !subtitleOptions.verticalAlign) {
                titleOffset = Math.ceil(titleOffset + subtitle.getBBox().height);
            }
        }

        requiresDirtyBox = this.titleOffset !== titleOffset;
        this.titleOffset = titleOffset; // used in getMargins

        if (!this.isDirtyBox && requiresDirtyBox) {
            this.isDirtyBox = requiresDirtyBox;
            // Redraw if necessary (#2719, #2744)
            if (this.hasRendered && Highcharts.pick(redraw, true) && this.isDirtyBox) {
                this.redraw();
            }
        }
    };

    $(".chart_date_filter." + $("#timeline_filter").val()).addClass("teal");
    $(".information_filter." + $("#general_information_filter").val()).addClass("teal");

    function parseLocationId(obj) {
        return (undefined === obj.country_id) ? obj.region_id : obj.country_id
    }

    $(document.body).on("click", '.reports a.delete', function (e) {
        var b = $(e.currentTarget);

        if (b.hasClass('disabled')) {
            return false;
        }

        b.closest(".item").find('.edit-block').animate(
            {opacity: 0},
            {
                complete: function () {
                    b.closest(".item").find('.active-block').css('display', 'none');
                }
            }
        );
        b.closest(".item").find('.active-block').animate(
            {opacity: 0},
            {
                complete: function () {
                    b.closest(".item").find('.active-block').css('cssText', 'display: none !important');
                }
            }
        );

        b.animate({
            opacity: 0
        }, {
            complete: function () {
                b.css('cssText', 'display: none !important');
                b.parent().find('.approve').css({'display': 'inline'});
                b.parent().find('.approve').animate({
                    opacity: 1
                });
            }
        });

        return false;
    });

    $(document.body).on("click", '.reports .approve .no', function (e) {
        var a = $(e.currentTarget).parent('.approve');
        a.animate(
            {
                opacity: 0
            },
            {
                complete: function () {
                    a.css('display', 'none');
                    a.closest(".item").find('a.delete, .edit-block, .active-block').css('display', 'inline');
                    a.closest(".item").find('a.delete, .edit-block, .active-block').animate({
                        opacity: 1
                    });
                }
            }
        );

        return false;
    });

    $(document.body).on("click", '.reports .approve .yes', function (e) {
        var id = $(this).closest('.item').data('id');
        $.ajax({
            url     : '/report/' + id,
            type  : 'delete',
            dataType: 'json',
            success: function (data) {
                table.draw();
            }
        });
    });

    $(document.body).on("click", '.reports .menu .edit-report', function (e) {
        e.preventDefault();
        var title = $(this).attr("title");
        $.ajax({
            method  : 'GET',
            url     : '/report/get-edit-modal',
            dataType: 'json',
            data    : { id: $(this).closest('.item').data('id') },
            success : function (response) {
                $("#edit-report-modal .header").html(title);
                $("#edit-report-modal .content").html(response.html);
                               
                $("#edit-report-modal").modal("setting", {
                    autofocus: false,
                    closable: false,
                    onApprove: function () {
                        return false;
                    }
                }).modal("show");

            }
        });
    });

    $(document.body).on("click", ".save-edit", function(e) {
        e.preventDefault();
        var editData = {};

        editData['id'] = $('#edit-report-modal input[name="id"]').val();
        editData['name'] = $('#edit-report-modal input[name="name"]').val();
        if (editData['name'].length <= 0) {
            $('#edit-report-modal input[name="name"]').parent().addClass('error');
            return false;
        }

        $.ajax({
            url     : "/report",
            method  : "POST",
            dataType: "json",
            data    : editData,
            success: function (response) {
                if (!response.error) {
                    table.draw();
                    $("#edit-report-modal").modal("hide");
                }
            }
        });
    });

    $(document.body).on("click", '.accordion .title a:not(.link)', function () {
        return false;
    });

    $(document.body).on("click", ".chart_date_filter", function() {
        var filter = $(this).attr("data-value");
        $("#timeline_filter").val(filter);
        $(".chart_date_filter").removeClass("teal");
        $(".chart_date_filter." + filter).addClass("teal");
        $('.button.report-filter').click();
    });

    $(document.body).on("click", ".information_filter", function() {
        var filter = $(this).attr("data-value");
        $("#general_information_filter").val(filter);
        $(".information_filter").removeClass("teal");
        $(".information_filter." + filter).addClass("teal");
        $('.button.report-filter').click();
    });



    function getRegionIdByName(name) {
        var regionId = null;
        $.each(regions, function(key, region) {
            if (region.name === name) {
                regionId = region.id;
                return true;
            }
        });
        return regionId;
    }

    function redirectByFilters(filter) {
        localStorage.removeItem('filter');
        window.location.href = window.location.origin + "/coverage?" + $.param(filter);
    }

    var articlesPieChart,
        keyMessagesBarChart,
        //securityIntelligenceBarChart,
        reachByRegionChart,
        coverageByTypeChart,
        coverageByListChart,
        coverageByCategories,
        coverageByMainTonalityChart,
        netByRegionChart,
        speakerQuoteChart,
        thirdSpeakerQuoteChart,
        topProductsChart,
        mentionsChart,
        desiredArticleChart,
        netDynamicChart,
        reachDynamicChart,
        articlesDynamicChart,
        impactDynamicChart,
        speakersCountChart,
        topNewsbreakChart,
        topCampaignChart,
        topEventsChart,
        indexesComparison,
        impactIndex,
        subcategoriesChart,

        options = {
            articles: {
                chart: {
                    renderTo: 'chart-articles',
                    type    : 'pie'
                },
                title: {
                    text: 'Number of articles'
                },
                legend: {
                    layout: "horizontal",
                    width: 400,
                    itemWidth: 200,
                    x: 40,
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: false,
                        cursor          : 'pointer',
                        point           : {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    filter[getLocationFilterName()] = this.location_id;
                                    redirectByFilters(filter);
                                }
                            } 
                        }
                    }
                },
                subtitle: {
                    useHTML: true
                },
                series: [
                    {name: 'Number of articles'}
                ]
            },
            key_meesages: {
                chart: {
                    renderTo: 'key_meesages',
                    type    : 'bar'
                },
                colors: ['#cccccc', '#1abc9c'],
                title: {
                    text: 'KEY MESSAGES'
                },
                subtitle: {
                    text: 'Messages pick up analysis'
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ""
                    }
                },
                legend: {
                    reversed: true
                },
                tooltip: {
                    enabled: true,
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}{point.format}</b><br/>'
                },
                plotOptions: {
                    series: {
                        showInLegend: false,
                        stacking: 'percent',
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    filter['message_penetration'] = this.type;
                                    filter[getLocationFilterName()] = this.location_id;
                                    redirectByFilters(filter);
                                }
                            }
                        },
                        dataLabels: {
                            enabled: true,
                            format: null,
                            formatter: function() {
                                if (100000 < this.point.y) {
                                    return (this.point.y / 1000000).toFixed(2) + 'M';
                                } else {
                                    return this.point.y;
                                }
                            }
                        }
                    }
                },
                series: []
            },/*
            security_intelligence: {
                chart: {
                    renderTo: 'security_intelligence',
                    type    : 'bar'
                },
                colors: ['#cccccc', '#1abc9c'],
                title: {
                    text: 'Security Intelligence'
                },
                subtitle: {
                    text: ''
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ""
                    }
                },
                legend: {
                    reversed: true
                },
                tooltip: {
                    enabled: true,
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}{point.format}</b><br/>'
                },
                plotOptions: {
                    series: {
                        showInLegend: false,
                        stacking: 'percent',
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    filter['security_intelligence_penetration'] = this.type;
                                    filter[getLocationFilterName()] = this.location_id;
                                    redirectByFilters(filter);
                                }
                            }
                        },
                        dataLabels: {
                            enabled: true,
                            format: null,
                            formatter: function() {
                                if (100000 < this.point.y) {
                                    return (this.point.y / 1000000).toFixed(2) + 'M';
                                } else {
                                    return this.point.y;
                                }
                            }
                        }
                    }
                },
                series: []
            },*/
            reach: {
                chart: {
                    renderTo: 'chart-reach',
                    type    : 'pie'
                },
                title: {
                    text: 'Media Outreach'
                },
                subtitle: {
                    useHTML: true
                },
                legend: {
                    layout: "horizontal",
                    width: 400,
                    itemWidth: 200,
                    x: 40,
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null
                        },
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    filter[getLocationFilterName()] = this.location_id;
                                    redirectByFilters(filter);
                                }
                            } 
                        }
                    }
                },
                series: [
                    {name: 'Media outreach'}
                ]
            },
            types: {
                chart: {
                    renderTo: 'chart-media-type',
                    type    : 'pie'
                },
                title: {
                    text: 'Media types'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null
                        },
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    var map = {
                                        'Online'      : 'online',
                                        'Print'       : 'print',
                                        'Agency'      : 'agency',
                                        'TV'          : 'tv',
                                        'Not Present' : '',
                                        'Radio'       : 'radio'
                                    };
                                    filter['media_type'] = map[this.name];
                                    redirectByFilters(filter);
                                }
                            } 
                        }
                    }
                },
                series: [
                    {name: 'Media types'}
                ]
            },
            lists: {
                chart: {
                    renderTo: 'chart-media-list',
                    type    : 'pie'
                },
                title: {
                    text: 'Media lists'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null
                        },
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point : {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    var map = {
                                        'Diamond'  : '1',
                                        'Platinum' : '0.8',
                                        'Gold'     : '0.6'
                                    };
                                    filter['media_list'] = map[this.name];
                                    redirectByFilters(filter);
                                }
                            } 
                        }
                    }
                },
                series: [
                    {name: 'Media lists'}
                ]
            },
            categories: {
                chart: {
                    renderTo: 'chart-media-category',
                    type    : 'pie'
                },
                title: {
                    text: 'Media categories'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null
                        },
                        allowPointSelect: false,
                        cursor          : 'pointer',
                        point           : {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    var map = {
                                        'IT'       : 'it',
                                        'Vertical' : 'vertical',
                                        'General'  : 'general',
                                        'Business' : 'business',
                                        'Other'    : ''
                                    };
                                    filter['media_category'] = map[this.name];
                                    redirectByFilters(filter);
                                }
                            } 
                        }
                    }
                },
                series: [
                    {name: 'Media categories'}
                ]
            },

            
            subcategories: {
                chart: {
                    renderTo: 'chart-subcategories',
                    type    : 'pie'
                },
                title: {
                    text: 'Subcategories'
                },
                subtitle: {
                    useHTML: true
                },
                legend: {
                    layout: "horizontal",
                    width: 400,
                    itemWidth: 200,
                    x: 40,
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null,
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        },
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    if (this.location_id) {
                                        filter[getLocationFilterName()] = this.location_id;
                                    }
                                    filter['media_subcategory'] = event.currentTarget.options.id;
                                    redirectByFilters(filter);
                                }
                            } 
                        }
                    }
                },
                series: [
                    {name: "Subcategories"}
                ]
            },
            
            net: {
                chart: {
                    renderTo: 'chart-net-effect',
                    type    : 'pie'
                },
                title: {
                    text: 'Net Effect'
                },
                subtitle: {
                    useHTML: true
                },
                legend: {
                    layout: "horizontal",
                    width: 400,
                    itemWidth: 200,
                    x: 40,
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null,
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        },
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    filter[getLocationFilterName()] = this.location_id;
                                    redirectByFilters(filter);
                                }
                            } 
                        }
                    }
                },
                series: [
                    {name: "Net effect"}
                ]
            },
            main_tonality: {
                chart: {
                    renderTo: 'chart-main-tonality',
                    type    : 'pie'
                },
                title: {
                    text: 'Main tonality'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null
                        },
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point : {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    filter['main_tonality'] = this.name.toLowerCase();
                                    redirectByFilters(filter);
                                }
                            } 
                        }
                    }
                },
                series: [
                    {name: 'Main tonality'}
                ]
            },
            speaker_quote: {
                chart: {
                    renderTo: 'chart-speaker-quote',
                    type    : 'pie'
                },
                title: {
                    text: 'Speakers quoted'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null
                        },
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    var map = {
                                        'Present' : '1',
                                        'Absent'  : '0.8'
                                    };
                                    filter['speaker_quote'] = map[this.name];
                                    redirectByFilters(filter);
                                }
                            } 
                        }
                    }
                },
                series: [
                    {name: 'Speakers quoted'}
                ]
            },
            third_speaker_quote: {
                chart: {
                    renderTo: 'chart-third-speaker-quote',
                    type    : 'pie'
                },
                title: {
                    text: '3rd party Speakers'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null
                        },
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    var map = {
                                        'Present' : '1',
                                        'Absent'  : '0'
                                    };
                                    filter['third_speakers_quotes'] = map[this.name];
                                    redirectByFilters(filter);
                                }
                            } 
                        }
                    }
                },
                series: [
                    {name: 'Third-party speakers quoted'}
                ]
            },
            products: {
                chart: {
                    renderTo: 'chart-products',
                    type    : 'bar'
                },
                title: {
                    text: 'Products rating'
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: null,
                            formatter: function() {
                                if (100000 < this.point.y) {
                                    return (this.point.y / 1000000).toFixed(2) + 'M';
                                } else {
                                    return this.point.y;
                                }
                            }
                        },
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    filter['product_id'] = this.product_id;
                                    redirectByFilters(filter);
                                }
                            }
                        }
                    }
                },
                tooltip: {
                    enabled: true,
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}{point.format}</b><br/>'
                },
                yAxis: {
                    title: {
                        text: "Mentions"
                    }
                },
                series: [
                    {}
                ],
                exporting: {
                    buttons: {
                        topTen: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 10, chart: topProductsChart, path: '/stats/top-products' });
                                loading();
                            },
                            x: -156,
                            y: -73,
                            symbol: 'url(../img/10.png)',
                            _titleKey: "top10",
                            theme: {
                                'stroke-width': 0,
                                cursor: 'pointer',
                                states: {
                                    hover: {
                                        fill: 'transparent',
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                        top20: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 20, chart: topProductsChart, path: '/stats/top-products' });
                                loading();
                            },
                            x: -129,
                            y: -73,
                            symbol: 'url(../img/20.png)',
                            _titleKey: "top20",
                            theme: {
                                'stroke-width': 0,
                                states: {
                                    hover: {
                                        fill: 'transparent'
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                        top30: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 30, chart: topProductsChart, path: '/stats/top-products' });
                                loading();
                            },
                            x: -102,
                            y: -73,
                            symbol: 'url(../img/30.png)',
                            _titleKey: "top30",
                            theme: {
                                'stroke-width': 0,
                                states: {
                                    hover: {
                                        fill: 'transparent'
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                        top40: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 40, chart: topProductsChart, path: '/stats/top-products' });
                                loading();
                            },
                            x: -75,
                            y: -73,
                            symbol: 'url(../img/40.png)',
                            _titleKey: "top40",
                            theme: {
                                'stroke-width': 0,
                                states: {
                                    hover: {
                                        fill: 'transparent'
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                        top50: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 50, chart: topProductsChart, path: '/stats/top-products' });
                                loading();
                            },
                            x: -48,
                            y: -73,
                            symbol: 'url(../img/50.png)',
                            _titleKey: "top50",
                            theme: {
                                'stroke-width': 0,
                                states: {
                                    hover: {
                                        fill: 'transparent'
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                    }
                }
            },
            news_break: {
                chart: {
                    renderTo: 'chart-newsbreaks',
                    type    : 'bar'
                },
                title: {
                    text: 'Newsbreaks Rating',
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: null,
                            formatter: function() {
                                if (100000 < this.point.y) {
                                    return (this.point.y / 1000000).toFixed(2) + 'M';
                                } else {
                                    return this.point.y;
                                }
                            }
                        },
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    filter['newsbreak_id'] = this.newsbreak_id;
                                    redirectByFilters(filter);
                                }
                            }
                        }
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    enabled: true,
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}{point.format}</b><br/>'
                },
                yAxis: {
                    title: {
                        text: "Mentions"
                    }
                },
                series: [
                    {}
                ],
                exporting: {
                    buttons: {
                        topTen: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 10, chart: topNewsbreakChart, path: '/stats/top-newsbreaks' });
                                loading();
                            },
                            x: -156,
                            y: -73,
                            symbol: 'url(../img/10.png)',
                            _titleKey: "top10",
                            theme: {
                                'stroke-width': 0,
                                cursor: 'pointer',
                                states: {
                                    hover: {
                                        fill: 'transparent',
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                        top20: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 20, chart: topNewsbreakChart, path: '/stats/top-newsbreaks' });
                                loading();
                            },
                            x: -129,
                            y: -73,
                            symbol: 'url(../img/20.png)',
                            _titleKey: "top20",
                            theme: {
                                'stroke-width': 0,
                                states: {
                                    hover: {
                                        fill: 'transparent'
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                        top30: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 30, chart: topNewsbreakChart, path: '/stats/top-newsbreaks' });
                                loading();
                            },
                            x: -102,
                            y: -73,
                            symbol: 'url(../img/30.png)',
                            _titleKey: "top30",
                            theme: {
                                'stroke-width': 0,
                                states: {
                                    hover: {
                                        fill: 'transparent'
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                        top40: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 40, chart: topNewsbreakChart, path: '/stats/top-newsbreaks' });
                                loading();
                            },
                            x: -75,
                            y: -73,
                            symbol: 'url(../img/40.png)',
                            _titleKey: "top40",
                            theme: {
                                'stroke-width': 0,
                                states: {
                                    hover: {
                                        fill: 'transparent'
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                        top50: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 50, chart: topNewsbreakChart, path: '/stats/top-newsbreaks' });
                                loading();
                            },
                            x: -48,
                            y: -73,
                            symbol: 'url(../img/50.png)',
                            _titleKey: "top50",
                            theme: {
                                'stroke-width': 0,
                                states: {
                                    hover: {
                                        fill: 'transparent'
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                    }
                }
            },
            event: {
                chart: {
                    renderTo: 'chart-events',
                    type    : 'bar'
                },
                title: {
                    text: 'Events rating'
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: null,
                            formatter: function() {
                                if (100000 < this.point.y) {
                                    return (this.point.y / 1000000).toFixed(2) + 'M';
                                } else {
                                    return this.point.y;
                                }
                            }
                        },
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    filter['event'] = this.name;
                                    redirectByFilters(filter);
                                }
                            }
                        }
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    enabled: true,
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}{point.format}</b><br/>'
                },
                yAxis: {
                    title: {
                        text: "Mentions"
                    }
                },
                series: [
                    {}
                ]
            },
            campaign: {
                chart: {
                    renderTo: 'chart-campaign',
                    type    : 'bar'
                },
                title: {
                    text: 'Campaigns rating'
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: null,
                            formatter: function() {
                                if (100000 < this.point.y) {
                                    return (this.point.y / 1000000).toFixed(2) + 'M';
                                } else {
                                    return this.point.y;
                                }
                            }
                        },
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    filter['campaign_id'] = this.campaign_id;
                                    redirectByFilters(filter);
                                }
                            }
                        }
                    }
                },
                tooltip: {
                    enabled: true,
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}{point.format}</b><br/>'
                },
                yAxis: {
                    title: {
                        text: "Mentions"
                    }
                },
                series: [
                    {}
                ],
                exporting: {
                    buttons: {
                        topTen: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 10, chart: topCampaignChart, path: '/stats/top-campaigns' });
                                loading();
                            },
                            x: -156,
                            y: -73,
                            symbol: 'url(../img/10.png)',
                            _titleKey: "top10",
                            theme: {
                                'stroke-width': 0,
                                cursor: 'pointer',
                                states: {
                                    hover: {
                                        fill: 'transparent',
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                        top20: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 20, chart: topCampaignChart, path: '/stats/top-campaigns' });
                                loading();
                            },
                            x: -129,
                            y: -73,
                            symbol: 'url(../img/20.png)',
                            _titleKey: "top20",
                            theme: {
                                'stroke-width': 0,
                                states: {
                                    hover: {
                                        fill: 'transparent'
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                        top30: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 30, chart: topCampaignChart, path: '/stats/top-campaigns' });
                                loading();
                            },
                            x: -102,
                            y: -73,
                            symbol: 'url(../img/30.png)',
                            _titleKey: "top30",
                            theme: {
                                'stroke-width': 0,
                                states: {
                                    hover: {
                                        fill: 'transparent'
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                        top40: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 40, chart: topCampaignChart, path: '/stats/top-campaigns' });
                                loading();
                            },
                            x: -75,
                            y: -73,
                            symbol: 'url(../img/40.png)',
                            _titleKey: "top40",
                            theme: {
                                'stroke-width': 0,
                                states: {
                                    hover: {
                                        fill: 'transparent'
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                        top50: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 50, chart: topCampaignChart, path: '/stats/top-campaigns' });
                                loading();
                            },
                            x: -48,
                            y: -73,
                            symbol: 'url(../img/50.png)',
                            _titleKey: "top50",
                            theme: {
                                'stroke-width': 0,
                                states: {
                                    hover: {
                                        fill: 'transparent'
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                    }
                }
            },
            speakers_count: {
                chart: {
                    renderTo: 'chart-speakers-count',
                    type    : 'bar'
                },
                title: {
                    text: 'Speakers rating'
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: null,
                            formatter: function() {
                                if (100000 < this.point.y) {
                                    return (this.point.y / 1000000).toFixed(2) + 'M';
                                } else {
                                    return this.point.y;
                                }
                            }
                        },
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    filter['speaker_id'] = this.speaker_id;
                                    redirectByFilters(filter);
                                }
                            }
                        }
                    }
                },
                tooltip: {
                    enabled: true,
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}{point.format}</b><br/>'
                },
                yAxis: {
                    title: {
                        text: ""
                    }
                },
                series: [
                    {}
                ],
                exporting: {
                    buttons: {
                        topTen: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 10, chart: speakersCountChart, path: '/stats/top-speakers-rating' });
                                loading();
                            },
                            x: -156,
                            y: -73,
                            symbol: 'url(../img/10.png)',
                            _titleKey: "top10",
                            theme: {
                                'stroke-width': 0,
                                cursor: 'pointer',
                                states: {
                                    hover: {
                                        fill: 'transparent',
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                        top20: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 20, chart: speakersCountChart, path: '/stats/top-speakers-rating' });
                                loading();
                            },
                            x: -129,
                            y: -73,
                            symbol: 'url(../img/20.png)',
                            _titleKey: "top20",
                            theme: {
                                'stroke-width': 0,
                                states: {
                                    hover: {
                                        fill: 'transparent'
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                        top30: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 30, chart: speakersCountChart, path: '/stats/top-speakers-rating' });
                                loading();
                            },
                            x: -102,
                            y: -73,
                            symbol: 'url(../img/30.png)',
                            _titleKey: "top30",
                            theme: {
                                'stroke-width': 0,
                                states: {
                                    hover: {
                                        fill: 'transparent'
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                        top40: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 40, chart: speakersCountChart, path: '/stats/top-speakers-rating' });
                                loading();
                            },
                            x: -75,
                            y: -73,
                            symbol: 'url(../img/40.png)',
                            _titleKey: "top40",
                            theme: {
                                'stroke-width': 0,
                                states: {
                                    hover: {
                                        fill: 'transparent'
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                        top50: {
                            onclick: function() {
                                var loading = loadTop.bind({ top: 50, chart: speakersCountChart, path: '/stats/top-speakers-rating' });
                                loading();
                            },
                            x: -48,
                            y: -73,
                            symbol: 'url(../img/50.png)',
                            _titleKey: "top50",
                            theme: {
                                'stroke-width': 0,
                                states: {
                                    hover: {
                                        fill: 'transparent'
                                    },
                                    select: {
                                        fill: '#000'
                                    }
                                }
                            },
                        },
                    }
                }
            },
            impact_index: {
                chart: {
                    renderTo: 'impact-index',
                    type    : 'bar'
                },
                title: {
                    text: 'Impact index'
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    filter[getLocationFilterName()] = this.location_id;
                                    redirectByFilters(filter);
                                }
                            }
                        }
                    },
                    bar: {
                        colorByPoint: true
                    }
                },
                tooltip: {
                    enabled: true,
                },
                yAxis: {
                    title: {
                        text: ""
                    }
                },
                series: [
                    {}
                ]
            },
            mention: {
                chart: {
                    renderTo: 'chart-mention-relation',
                    type    : 'pie'
                },
                title: {
                    text: 'Products mentions'
                },
                legend: {
                    labelFormatter: function() {
                        if (100000 < this.y) {
                            return '<b>' + this.name + '</b>: ' + this.percentage.toFixed(1) + ' % (' + (this.y / 1000000).toFixed(1) + 'M) Impact Index: ' + this.avg_index;
                        } else {
                            return '<b>' + this.name + '</b>: ' + this.percentage.toFixed(1) + ' % (' + this.y + ') Impact Index: ' + this.avg_index;
                        }
                    },
                    layout: "vertical"
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null,
                        },
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    var map = {
                                        'With Product'    : '1',
                                        'Without Product' : '0'
                                    };
                                    filter['mention_article'] = map[this.name];
                                    redirectByFilters(filter);
                                }
                            } 
                        }
                    }
                },
                series: [
                    {name: 'Products mentions in article'}
                ]
            },

            desired: {
                chart: {
                    renderTo: 'chart-desired-article',
                    type    : 'pie'
                },
                title: {
                    text: 'Desired/Undesired articles'
                },
                legend: {
                    labelFormatter: function() {
                        if (100000 < this.y) {
                            return '<div><b>' + this.name + '</b>: ' + this.percentage.toFixed(1) + ' % (' + (this.y / 1000000).toFixed(1) + 'M)</div>';
                        } else {
                            return '<div><b>' + this.name + '</b>: ' + this.percentage.toFixed(1) + ' % (' + this.y + ')</div>';
                        }
                    },
                    layout: "vertical"
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false,
                            format: null
                        },
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    var map = {
                                        'Desired'   : '1',
                                        'Undesired' : '0'
                                    };
                                    filter['desired_article'] = map[this.name];
                                    redirectByFilters(filter);
                                }
                            } 
                        }
                    }
                },
                series: [
                    {name: 'Desired articles'}
                ]
            },

            articles_dynamic: {
                global: {
                    useUTC: false
                },
                colors: ['#00c4bc', '#d35400', '#f1c40f', '#9b59b6', '#27ae60', '#34495e', '#e74c3c'],
                chart: {
                    renderTo: 'chart-articles-dynamic',
                    type    : 'area'
                },
                title: {
                    text: 'Number of Articles Dynamics'
                },
                xAxis: {
                    gridLineWidth: 0.5,
                    gridZIndex: 10,
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        month: '%B %Y'
                    }
                },
                yAxis: {
                    gridLineWidth: 0.5,
                    gridZIndex: 10,
                    title: {
                        text: null
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    enabled: true,
                    pointFormatter: function () {
                        if (typeof this.newsbreak !== 'undefined' && this.newsbreak !== null) {
                            return this.newsbreak+'<b>: '+this.total+'</b> / <b>'+this.ne+'M</b><br>'+this.series.name+': <b>'+this.y+'</b>';
                        }
                        return 'No significant newsbreaks<br>' + this.series.name+': <b>'+this.y+'</b>';
                    }
                },
                plotOptions: {
                    area: {
                        fillOpacity: 0.5
                    }
                },
                series: [
                    {}
                ]
            },
            impact_dynamic: {
                global: {
                    useUTC: false
                },
                colors: ['#00c4bc', '#d35400', '#f1c40f', '#9b59b6', '#27ae60', '#34495e', '#e74c3c'],
                chart: {
                    renderTo: 'chart-impact-dynamic',
                    type    : 'area'
                },
                title: {
                    text: 'Impact Index Dynamics'
                },
                xAxis: {
                    gridLineWidth: 0.5,
                    gridZIndex: 10,
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        month: '%B %Y'
                    }
                },
                yAxis: {
                    gridLineWidth: 0.5,
                    gridZIndex: 10,
                    title: {
                        text: null
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    enabled: true,
                    pointFormatter: function () {
                        if (typeof this.newsbreak !== 'undefined' && this.newsbreak !== null) {
                            return this.newsbreak+'<b>: '+this.total+'</b> / <b>'+this.ne+'M</b><br>'+this.series.name+': <b>'+this.y+'</b>';
                        }
                        return 'No significant newsbreaks<br>' + this.series.name+': <b>'+this.y+'</b>';
                    }
                },
                plotOptions: {
                    area: {
                        fillOpacity: 0.5
                    }
                },
                series: [
                    {}
                ]
            },
            reach_dynamic: {
                global: {
                    useUTC: false
                },
                colors: ['#00c4bc', '#d35400', '#f1c40f', '#9b59b6', '#27ae60', '#34495e', '#e74c3c'],
                chart: {
                    renderTo: 'chart-reach-dynamic',
                    type    : 'area'
                },
                title: {
                    text: 'Media Outreach Dynamics'
                },
                xAxis: {
                    gridLineWidth: 0.5,
                    gridZIndex: 10,
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        month: '%B %Y'
                    }
                },
                yAxis: {
                    gridLineWidth: 0.5,
                    gridZIndex: 10,
                    title: {
                        text: null
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        fillOpacity: 0.5
                    }
                },
                tooltip: {
                    enabled: true,
                    pointFormatter: function () {
                        var reach = this.y / 1000000;
                        reach = Math.round(reach * 10) / 10;
                        if (typeof this.newsbreak !== 'undefined' && this.newsbreak !== null) {
                            return this.newsbreak+'<b>: '+this.total+'</b> / <b>'+this.ne+'M</b><br>'+this.series.name+': <b>'+reach+'M</b>';
                        }
                        return 'No significant newsbreaks<br>' + this.series.name+': <b>'+reach+'M</b>';
                    }
                },
                series: [
                    {}
                ]
            },
            net_dynamic: {
                global: {
                    useUTC: false
                },
                colors: ['#00c4bc', '#d35400', '#f1c40f', '#9b59b6', '#27ae60', '#34495e', '#e74c3c'],
                chart: {
                    renderTo: 'chart-net-dynamic',
                    type    : 'area'
                },
                title: {
                    text: 'Net Effect Dynamics'
                },
                xAxis: {
                    gridLineWidth: 0.5,
                    gridZIndex: 10,
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        month: '%B %Y'
                    }
                },
                yAxis: {
                    gridLineWidth: 0.5,
                    gridZIndex: 10,
                    title: {
                        text: null
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        fillOpacity: 0.5
                    }
                },
                tooltip: {
                    enabled: true,
                    pointFormatter: function () {
                        var net = this.y / 1000000;
                        net = Math.round(net * 10) / 10;
                        if (typeof this.newsbreak !== 'undefined' && this.newsbreak !== null) {
                            return this.newsbreak+'<b>: '+this.total+'</b> / <b>'+this.ne+'M</b><br>'+this.series.name+': <b>'+net+'M</b>';
                        }
                        return 'No significant newsbreaks<br>' + this.series.name+': <b>'+net+'M</b>';
                    }
                },
                series: [{}]
            },
            indexes_comparison: {
                chart: {
                    type     : 'bubble',
                    renderTo : 'chart-indexes-comparison'
                },
                plotOptions: {
                    bubble: {
                        animation: false,
                        threshold: 0,
                        dataLabels: {
                            enabled: true,
                            formatter:function() {
                                return this.point.title;
                            }
                        },
                        allowPointSelect: false,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function (event) {
                                    var filter = getFilter();
                                    filter[getLocationFilterName()] = this.location_id;
                                    redirectByFilters(filter);
                                }
                            }
                        }
                    }
                },
                xAxis: {
                    title: {
                        text: 'Impact Index'
                    }
                },
                yAxis: {
                    title: {
                        text: 'Media Outreach'
                    }
                },
                legend: {
                    enabled: false
                },
                title: {
                    text : '3 Indexes comparison',
                    margin: 35
                },
                tooltip: {
                    enabled: true,
                    pointFormatter: function () {
                        var net_effect     = this.z / 1000000;
                        var media_outreach = this.y / 1000000;
                        net_effect         = Math.round(net_effect * 10) / 10;
                        media_outreach     = Math.round(media_outreach * 10) / 10;
                        return 'Net Effect: <b>' + net_effect + 'M</b>' +
                            '<br/>Media Outreach: <b>' + media_outreach + 'M</b><br/>' +
                            'Impact Index: <b>' + this.x + '</b>';
                    }
                },
                subtitle: {
                    text: 'The size of bubbles is defined by Net Effect values'
                },
                exporting: {
                    buttons: {
                        xlsButton : {
                            onclick : function() {
                                var data = [["Region", "Net Effect", "Media Outreach", "Impact Index"]];
                                $.each(this.series, function(key, series) {
                                    data.push([series.data[0].title, series.data[0].z, series.data[0].y, series.data[0].x]);
                                });
                                this.exportChart({
                                    type     : Highcharts.exporting.MIME_TYPES.XLS,
                                    filename : this.options.title.text + getFileName()
                                }, {
                                    dataRows: data
                                });
                            }
                        },
                    }
                },
                series: [{}]
            }
        };
    var Vis = function (options, dataHandle, useLoading) {
        this.chartOptions = options || {};
        this.dataHandle   = dataHandle || null;
        this.useLoading   = useLoading || false;
        this.chart        = new Highcharts.Chart(options);
    };

    Vis.prototype = {
        render: function (url, params, reflow) {
            var vis = this;
            if (typeof vis.useLoading !== 'undefined' && vis.useLoading == true) {
                vis.chart.showLoading();
            }

            $.ajax({
                url: url,
                method: 'GET',
                dataType: 'json',
                data: params || null,
                success: function (data) {
                    if (vis.dataHandle) {
                        data = vis.dataHandle(data, vis.chart);
                    }

                    if (typeof data.series !== 'undefined') {
                        var seriesLength = vis.chart.series.length;
                        for(var i = seriesLength -1; i > -1; i--) {
                            vis.chart.series[i].remove(false);
                        }
                        if (typeof data.categories !== 'undefined' && data.categories.length > 0) {
                            vis.chart.xAxis[0].setCategories(data.categories);
                        }
                        $.each(data.series, function(key, seriesData) {
                            vis.chart.addSeries(seriesData);
                        });
                    }
                    else {
                        vis.chart.series[0].setData(data);
                    }
                    if (typeof vis.useLoading !== 'undefined' && vis.useLoading == true) {
                        vis.chart.hideLoading();
                    }

                    if (true === reflow) {
                        vis.chart.reflow();
                    }

                }
            });
            return this;
        }
    };

    // Highcharts default options
    Highcharts.setOptions({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false,
            style: { 'overflow': 'visible' }
        },
        credits: {
            enabled: false
        },
        lang: {
            numericSymbols: [ "k" , "M" , "B" , "T" , "P" , "E"],
            downloadPng: "Download PNG Image",
            downloadXls: "Download XLS File",
            top10: 'Top 10',
            top20: 'Top 20',
            top30: 'Top 30',
            top40: 'Top 40',
            top50: 'Top 50',
        },
        tooltip: {
            enabled: false
        },
        colors: ['#1abc9c', '#d35400', '#f1c40f', '#9b59b6', '#27ae60', '#34495e', '#e74c3c'],
        legend: {
            labelFormatter: function() {
                var percentage = "";
                if (typeof this.percentage !== 'undefined') {
                    percentage = this.percentage.toFixed(1) + '% ';
                }
                if (typeof this.y === 'undefined') {
                    return '<b>' + this.name + '</b> ' + percentage;
                }
                if (100000 < this.y) {
                    return '<b>' + this.name + '</b> ' + percentage + '(' + (this.y / 1000000).toFixed(1) + 'M)';
                } else {
                    return '<b>' + this.name + '</b> ' + percentage + '(' + this.y + ')';
                }
            },
            layout: "vertical",
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>:<br>{point.percentage:.1f} % ({point.y})',
                    formatter: function() {
                        if (100000 < this.point.y) {
                            return '<b>' + this.point.name + '</b>:<br>' + this.point.percentage.toFixed(1) + ' % (' + (this.point.y / 1000000).toFixed(2) + 'M)';
                        } else {
                            return '<b>' + this.point.name + '</b>:<br>' + this.point.percentage.toFixed(1) + ' % (' + this.point.y + ')';
                        }
                    },
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true,
            },
            bar: {
                dataLabels: {
                    enabled: true
                }
            },
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: '{point.x} cm, {point.y} kg'
                }
            },
            spline: {
                allowPointSelect: false,
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (event) {
                            var filter   = getFilter();
                            var dateFrom = new Date(this.x);
                            var dateTo   = dateFrom;
                            filter['start'] = formatDate(dateFrom);
                            if ('month' === $("#timeline_filter").val()) {
                                dateTo.setMonth(dateTo.getMonth() + 1);
                                // last day of current month
                                dateTo.setDate(dateTo.getDate() - 1);
                            } else {
                                dateTo.setDate(dateTo.getDate() + 6);
                            }
                            filter['end'] = formatDate(dateTo);
                            redirectByFilters(filter);
                        }
                    }
                }
            },
            area: {
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, 'rgba(0, 196, 188, 0.4)'],
                        [1, 'rgba(0, 196, 188, 0.1)'],
                    ]
                },
            }
        },
        exporting: {
            scale: 1,
            buttons: {
                contextButton: {
                    enabled: false
                },
                xlsButton: {
                    symbol: 'url(../img/xls-min.png)',
                    onclick: function () {
                        this.exportChart({
                            type: Highcharts.exporting.MIME_TYPES.XLS,
                            filename: this.options.title.text + getFileName()
                        });
                    },
                    _titleKey: "downloadXls",
                    theme: {
                        'stroke-width': 0,
                        states: {
                            hover: {
                                fill: 'transparent'
                            },
                            select: {
                                fill: '#000'
                            }
                        }
                    },
                    x: 0,
                    y: -70
                },
                pngButton: {
                    symbol: 'url(../img/png-min.png)',
                    onclick: function () {
                        this.exportChart({
                            type: Highcharts.exporting.MIME_TYPES.PNG,
                            filename: this.options.title.text + getFileName()
                        });
                    },
                    _titleKey: "downloadPng",
                    theme: {
                        'stroke-width': 0,
                        states: {
                            hover: {
                                fill: 'transparent'
                            },
                            select: {
                                fill: '#000'
                            }
                        }
                    },
                    x: -24,
                    y: -70
                },
            }
        },
        title: {
            style: {"color":  "#646388"}
        }
    });

    indexesComparison = new Vis(options.indexes_comparison, function (data, chart) {
        var result     = [];
        data.forEach(function (v, i, a) {
            var impact_index   = parseFloat(v.impact_index);
            var media_outreach = parseFloat(v.media_outreach);
            var net_effect     = parseFloat(v.net_effect);
            impact_index       = isNaN(impact_index) ? 0 : impact_index;
            media_outreach     = isNaN(media_outreach) ? 0 : media_outreach;
            net_effect         = isNaN(net_effect) ? 0 : net_effect;
            result.push({'name': v.name, 'data': [{ title: v.name, x: impact_index, y: media_outreach, z: net_effect, location_id: parseLocationId(v) }]});
        });

        return {'series': result};
    }, true);

    articlesPieChart = new Vis(options.articles, function (data, chart) {
        var count = 0;

        data.forEach(function (v, i, a) {
            a[i] = { name: v.name, y: parseFloat(v.total), location_id: parseLocationId(v) };
            count += parseFloat(v.total);
        });

        return data;
    });

    keyMessagesBarChart = new Vis(options.key_meesages, function (data, chart) {
        var result     = {'without': {'name': 'Without messages', 'data': []}, 'with': {'name': 'With messages', 'data': []}};
        var categories = [];
        data.forEach(function (v, i, a) {
            result['without'].data.push({ name: v.name, y: parseFloat(v.without_messages), type: 'without', location_id: parseLocationId(v) });
            result['with'].data.push({ name: v.name, y: parseFloat(v.with_messages), type: 'with', location_id: parseLocationId(v) });
            categories.push(v.name);
        });
        return {'series': result, 'categories': categories};
    });
    /*
    securityIntelligenceBarChart = new Vis(options.security_intelligence, function (data, chart) {
        var result     = {'without': {'name': 'Without Security Intelligence', 'data': []}, 'with': {'name': 'With Security Intelligence', 'data': []}};
        var categories = [];
        data.forEach(function (v, i, a) {
            result['without'].data.push({ name: v.name, y: parseFloat(v.without_security_intelligence), type: 'without', location_id: parseLocationId(v) });
            result['with'].data.push({ name: v.name, y: parseFloat(v.with_security_intelligence), type: 'with', location_id: parseLocationId(v) });
            categories.push(v.name);
        });
        return {'series': result, 'categories': categories};
    });*/

    reachByRegionChart = new Vis(options.reach, function (data, chart) {
        var count = 0;

        data.forEach(function (v, i, a) {
            a[i] = { name: v.name, y: parseFloat(v.reach), location_id: parseLocationId(v) };

            if (v.reach > 0) {
                count = parseFloat(v.reach + count);
            }
        });

        return data;
    });

    coverageByTypeChart = new Vis(options.types, function (data, chart) {
        data.forEach(function (v, i, a) {
            var map = {
                'online': 'Online',
                'print' : 'Print',
                'agency': 'Agency',
                'tv'    : 'TV',
                'other' : 'Not Present',
                'radio' : 'Radio'
            };

            if (v.media_type === null) {
                v.media_type = 'other';
            }

            a[i] = {
                name: map[v.media_type],
                y: parseFloat(v.total),
                format: v.format
            };
        });

        return data;
    });

    coverageByListChart = new Vis(options.lists, function (data, chart) {
        data.forEach(function (v, i, a) {
            var map = {
                1      : 'Diamond',
                0.8    : 'Platinum',
                0.6    : 'Gold',
                'other': 'Other'
            };
            if (v.media_list === null) {
                v.media_list = 'other';
            } else {
                v.media_list = parseFloat(v.media_list);
            }
            a[i] = { name: map[v.media_list], y: parseFloat(v.total), format: v.format };
        });

        return data;
    });

    coverageByCategories = new Vis(options.categories, function (data) {
        data.forEach(function (v, i, a) {
            var map = {
                'it'      : 'IT',
                'vertical': 'Vertical',
                'general' : 'General',
                'business': 'Business',
                'other'   : 'Other'
            };

            if (v.media_category === null) {
                v.media_category = 'other';
            }

            a[i] = {
                name: map[v.media_category],
                y: parseFloat(v.total),
                format: v.format
            };
        });

        return data;
    });

    netByRegionChart = new Vis(options.net, function (data, chart) {
        var count = 0;

        data.forEach(function (v, i, a) {
            a[i] = {  name: v.name, y: parseFloat(v.net), location_id: parseLocationId(v) };

            if (v.net > 0) {
                count = parseFloat(v.net + count);
            }
        });

        return data;
    });
    
    subcategoriesChart = new Vis(options.subcategories, function (data, chart) {

        data.forEach(function (v, i, a) {
            a[i] = {  name: v.title, y: parseFloat(v.coverage_cnt), id: v.id };
        });

        return data;
    });

    coverageByMainTonalityChart = new Vis(options.main_tonality, function (data) {
        data.forEach(function (v, i, a) {
            var map = {
                'positive': 'Positive',
                'negative': 'Negative',
                'neutral' : 'Neutral'
            };

            a[i] = { name: map[v.main_tonality], y: parseFloat(v.total)};
        });

        return data;
    });

    speakerQuoteChart = new Vis(options.speaker_quote, function (data) {
        data.forEach(function (v, i, a) {
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format};
        });

        return data;
    });

    thirdSpeakerQuoteChart = new Vis(options.third_speaker_quote, function (data) {
        data.forEach(function (v, i, a) {
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format };
        });

        return data;
    });

    topProductsChart = new Vis(options.products, function (data, chart) {
        var p = [];
        data.forEach(function (v, i, a) {
            p.push(v.name);
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format, product_id: v.product_id };
        });
        chart.xAxis[0].setCategories(p);
        chart.series[0].name = $("#general_information_filter").val() == 'net_effect' ? 'Net Effect' : 'Articles';

        return data;
    });

    topNewsbreakChart = new Vis(options.news_break, function (data, chart) {
        var p = [];
        data.forEach(function (v, i, a) {
            p.push(v.name);
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format, newsbreak_id: v.newsbreak_id };
        });
        chart.xAxis[0].setCategories(p);
        chart.series[0].name = $("#general_information_filter").val() == 'net_effect' ? 'Net Effect' : 'Articles';
        return data;
    });

    topEventsChart = new Vis(options.event, function (data, chart) {
        var p = [];
        data.forEach(function (v, i, a) {
            p.push(v.name);
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format };
        });
        chart.xAxis[0].setCategories(p);
        chart.series[0].name = $("#general_information_filter").val() == 'net_effect' ? 'Net Effect' : 'Articles';
        return data;
    });

    topCampaignChart = new Vis(options.campaign, function (data, chart) {
        var p = [];
        data.forEach(function (v, i, a) {
            p.push(v.name);
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format, campaign_id: v.campaign_id };
        });
        chart.xAxis[0].setCategories(p);
        chart.series[0].name = $("#general_information_filter").val() == 'net_effect' ? 'Net Effect' : 'Articles';
        return data;
    });

    mentionsChart = new Vis(options.mention, function (data, chart) {
        var avg = [];
        data.forEach(function (v, i, a) {
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format, avg_index: v.avg_index};
            avg.push(v.avg_index);
        });

        return data;
    });

    desiredArticleChart = new Vis(options.desired, function (data, chart) {
        data.forEach(function (v, i, a) {
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format };
        });
        return data;
    });

    speakersCountChart = new Vis(options.speakers_count, function (data, chart) {
        var p = [];
        data.forEach(function (v, i, a) {
            p.push(v.name);
            a[i] = { name: v.name, y: parseFloat(v.total), format: v.format, speaker_id: v.speaker_id };
        });
        chart.xAxis[0].setCategories(p);
        chart.series[0].name = $("#general_information_filter").val() == 'net_effect' ? 'Net Effect' : 'Articles';
        return data;
    });

    impactIndex = new Vis(options.impact_index, function (data, chart) {
        var p = [];
        data.by_region.forEach(function (v, i, a) {
            p.push(v.name);
            a[i] = { name: v.name, y: parseFloat(v.avg_index), location_id: parseLocationId(v) };
        });
        chart.xAxis[0].setCategories(p);
        chart.series[0].name = "Impact Index";
        chart.setTitle(null, { text: 'Average: ' + data.common});
        return data.by_region;
    });

    /** Dynamic charts **/
    netDynamicChart = new Vis(options.net_dynamic, function (data, chart) {
        data.forEach(function (v, i, a) {
            a[i] = { x: parseInt(v.date), y: parseFloat(v.monthly_net), total: parseFloat(v.total), newsbreak: v.name, ne: v.net_effect };
        });

        chart.series[0].name = 'Net Effect';
        return data;
    });

    reachDynamicChart = new Vis(options.reach_dynamic, function (data, chart) {
        data.forEach(function (v, i, a) {
            a[i] = { x: parseInt(v.date), y: parseFloat(v.monthly_reach), total: parseFloat(v.total), newsbreak: v.name, ne: v.net_effect };
        });

        chart.series[0].name = 'Reach';
        return data;
    });

    articlesDynamicChart = new Vis(options.articles_dynamic, function (data, chart) {
        data.forEach(function (v, i, a) {
            a[i] = { x: parseInt(v.date), y: parseFloat(v.count), total: parseFloat(v.total), newsbreak: v.name, ne: v.net_effect };
        });

        chart.series[0].name = 'Articles';
        return data;
    });

    impactDynamicChart = new Vis(options.impact_dynamic, function (data, chart) {
        data.forEach(function (v, i, a) {
            a[i] = { x: parseInt(v.date), y: parseFloat(v.monthly_impact_index), total: parseFloat(v.total), newsbreak: v.name, ne: v.net_effect };
        });

        chart.series[0].name = 'Impact Index';
        return data;
    });

    $('.menu.charts .item').tab({
        history: 'hash',
        onLoad: function (path) {
            var filter = getFilter();
            if (path === 'reports') {
                $('#filters-menu').hide();
                table.draw();
            } else {
                $('#filters-menu').show();
            }
            if (path === 'dynamics') {
                filter['date_filter'] = $("#timeline_filter").val();
                articlesDynamicChart.render('/stats/coverage-dynamic', filter, true);
                impactDynamicChart.render('/stats/impact-dynamic', filter, true);
                reachDynamicChart.render('/stats/reach-dynamic', filter, true);
                netDynamicChart.render('/stats/net-dynamic', filter, true);
            }
            else {
                if (path === 'general_information') {
                    filter['filter_by'] = $("#general_information_filter").val();
                    coverageByTypeChart.render('/stats/coverage-by-type', filter, true);
                    coverageByListChart.render('/stats/coverage-by-list', filter, true);
                    coverageByCategories.render('/stats/coverage-by-categories', filter, true);
                    mentionsChart.render('/stats/mention-product-relation', filter, true);
                    speakerQuoteChart.render('/stats/speaker-quote', filter, true);
                    thirdSpeakerQuoteChart.render('/stats/third-speaker-quote', filter, true);
                    topProductsChart.render('/stats/top-products', filter, true);
                    topNewsbreakChart.render('/stats/top-newsbreaks', filter, true);
                    topEventsChart.render('/stats/top-events', filter, true);
                    topCampaignChart.render('/stats/top-campaigns', filter, true);
                    desiredArticleChart.render('/stats/desired-articles', filter, true);
                    speakersCountChart.render('/stats/top-speakers-rating', filter, true);
                    subcategoriesChart.render('/stats/subcategories', filter, true);
                    //securityIntelligenceBarChart.render('/stats/security-intelligence', filter, true);
                }
                else if (path === 'executive_summary') {
                    keyMessagesTotal(filter);
                    articlesPieChart.render('/stats/coverage-by-region', filter, true);
                    impactIndex.render('/stats/avg-index-by-region', filter, true);
                    reachByRegionChart.render('/stats/reach-by-region', filter, true);
                    netByRegionChart.render('/stats/net-by-region', filter, true);
                    coverageByMainTonalityChart.render('/stats/coverage-by-main-tonality', filter, true);
                    filter['filter_by'] = $("#general_information_filter").val();
                    keyMessagesBarChart.render('/stats/key-messages', filter, true);
                    indexesComparison.render('/stats/indexes-comparison', filter, true);
                }
            }
        }
    });

    var keyMessagesTotal = function(filter) {
        $.ajax({
            url     : '/stats/total-key-messages',
            method  : 'GET',
            dataType: 'json',
            data    : filter || null,
            success: function (data) {
               if (typeof data !== 'undefined') {
                    $('#key_message_present').html(0);
                    $('#key_message_extent').html(0);
                    $('#key_message_counter').html(0);
                    $('#key_message_absent').html(0);

                    $.each(data, function(key, val) {
                        $('#key_message_' + key).html(val);
                    });
               }
            }
        });
    };

    var tab    = $('.menu.charts').tab('get path').substring(1);
    var filter = getFilter();
    
    keyMessagesTotal(filter);
    keyMessagesBarChart.render('/stats/key-messages', filter);
    articlesPieChart.render('/stats/coverage-by-region', filter);
    reachByRegionChart.render('/stats/reach-by-region', filter);
    netByRegionChart.render('/stats/net-by-region', filter);

    $('.input-daterange i.add-on').click(function (e) {
        $(e.currentTarget).prev().datepicker('show');
    });

    $('.button.report-filter').click(function () {
        var tab = $('.menu.charts').tab('get path').substring(1);
        var filter = getFilter();

        if (tab === 'general_information' || tab === 'executive_summary') {
            filter['filter_by'] = $("#general_information_filter").val();
            keyMessagesTotal(filter);
            keyMessagesBarChart.render('/stats/key-messages', filter);
            //securityIntelligenceBarChart.render('/stats/security-intelligence', filter);
            articlesPieChart.render('/stats/coverage-by-region', filter);
            reachByRegionChart.render('/stats/reach-by-region', filter);
            coverageByTypeChart.render('/stats/coverage-by-type', filter);
            coverageByListChart.render('/stats/coverage-by-list', filter);
            coverageByCategories.render('/stats/coverage-by-categories', filter);
            netByRegionChart.render('/stats/net-by-region', filter);
            coverageByMainTonalityChart.render('/stats/coverage-by-main-tonality', filter);
            speakerQuoteChart.render('/stats/speaker-quote', filter);
            thirdSpeakerQuoteChart.render('/stats/third-speaker-quote', filter);
            topProductsChart.render('/stats/top-products', filter);
            topNewsbreakChart.render('/stats/top-newsbreaks', filter);
            topEventsChart.render('/stats/top-events', filter);
            topCampaignChart.render('/stats/top-campaigns', filter);
            mentionsChart.render('/stats/mention-product-relation', filter);
            desiredArticleChart.render('/stats/desired-articles', filter);
            speakersCountChart.render('/stats/top-speakers-rating', filter);
            impactIndex.render('/stats/avg-index-by-region', filter);
            indexesComparison.render('/stats/indexes-comparison', filter);
            subcategoriesChart.render('/stats/subcategories', filter);
        } else if (tab === 'dynamics') {
            filter['date_filter'] = $("#timeline_filter").val();
            articlesDynamicChart.render('/stats/coverage-dynamic', filter);
            impactDynamicChart.render('/stats/impact-dynamic', filter);
            reachDynamicChart.render('/stats/reach-dynamic', filter);
            netDynamicChart.render('/stats/net-dynamic', filter);
        }

        highlightFilters();
    });

    $('.button.refresh-report-filter').click(function() {
        $('#filters-menu').find($('[name="start"]')).val("");
        $('#filters-menu').find($('[name="end"]')).val("");
        $('#filters-menu').find($('[name="country"]')).val("");
        $('#filters-menu').find($('[name="campaign"]')).val("");
        $('#filters-menu').find($('[name="news"]')).val("");
        $('#filters-menu').find($('[name="product"]')).val("");
        $('#filters-menu').find($('[name="speaker"]')).val("");
        $('#filters-menu').find($('[name="event"]')).val("");

        $('#filters-menu input[type="hidden"]').val('');

        $(".dropdown.region-filter").dropdown('set value', "").dropdown('set selected', "");
        $(".dropdown.subregion-filter").dropdown('set value', "").dropdown('set selected', "");
        $(".dropdown.country-filter").dropdown('set value', "").dropdown('set selected', "");
        $(".dropdown.media-list").dropdown('set value', "").dropdown('set selected', "");
        $(".dropdown.business").dropdown('set value', "").dropdown('set selected', "");
        $(".dropdown.media-category").dropdown('set value', "").dropdown('set selected', "");
        $(".dropdown.media-subcategory").dropdown('set value', "").dropdown('set selected', "");

        clearStorageFilter();

        // фильтруем после очистки фильтров
        $('.button.report-filter').click();

        // clear subregions
        $('#filters-menu').find($('[name="subregion"]')).val("");
        $(".ui.dropdown.subregion-filter").dropdown('set value', "").dropdown('set selected', "All subregions");
        $('.ui.dropdown.subregion-filter .menu').empty();
        $('.ui.dropdown.subregion-filter .text').text('All subregions');
        var subregionItem = $('<div>')
            .addClass('item')
            .addClass('active')
            .attr('data-value', '')
            .text('All subregions');
        $('.ui.dropdown.subregion-filter .menu').append(subregionItem);

        // clear countries
        $('#filters-menu').find($('[name="country"]')).val("");
        $(".ui.dropdown.country-filter").dropdown('set value', "").dropdown('set selected', "All countries");
        $('.ui.dropdown.country-filter .menu').empty();
        $('.ui.dropdown.country-filter .text').text('All countries');
        var countryItem = $('<div>')
            .addClass('item')
            .addClass('active')
            .attr('data-value', '')
            .text('All countries');
        $('.ui.dropdown.country-filter .menu').append(countryItem);

        $.each(subregions, function(key, subregion) {
            subregionItem = $('<div>')
                .addClass('item')
                .attr('data-value', subregion['id'])
                .text(subregion['name']);
            $('.ui.dropdown.subregion-filter .menu').append(subregionItem);
        });
        $.each(countries, function(key, country) {
            countryItem = $('<div>')
                .addClass('item')
                .attr('data-value', country['id'])
                .text(country['name']);
            $('.ui.dropdown.country-filter .menu').append(countryItem);
        });
    });

    $('.button.second-step').click(function () {
        $(".report-modal #report_name").val('');
        $(".report-modal #report_url").val('');
        $('.report-modal').modal("hide");
    });

    $(document).on('click', '#save_regional_statistics_xls', function(e) {
        e.stopPropagation();
        var filter = getFilter();
        filter['filename'] = 'Kaspersky Lab Regional Statistics' + getFileName();
        window.location.href = window.location.origin + "/stats/export/regional-kpi?" + $.param(filter);
    });

    $(document).on('click', '#save_product_mentions_xls', function(e) {
        e.stopPropagation();
        var filter = getFilter();
        filter['filename'] = 'Product Mentions' + getFileName();
        window.location.href = window.location.origin + "/stats/export/regional-product-mentions?" + $.param(filter);
    });
    $(document).on('click', '#save_message_penetration_xls', function(e) {
        e.stopPropagation();
        var filter = getFilter();
        filter['filename'] = 'Key Message Penetration' + getFileName();
        window.location.href = window.location.origin + "/stats/export/total-key-messages?" + $.param(filter);
    });

    $(document).on('mouseenter', '.fixed-first-column-table-block', function() {
        $('.fixed-column-table tbody tr').hover(function(){
            var index = $(this).index() + 1;
            $(this).parents('.fixed-first-column-table-block')
                .find('.original-table-block table tbody tr:nth-child('+index+')')
                .css('background', 'rgba(0, 0, 0, 0.05)')
                .css('color', 'rgba(0, 0, 0, 0.95)');
        }, function(){
            var index = $(this).index() + 1;
            $(this).parents('.fixed-first-column-table-block')
                .find('.original-table-block table tbody tr:nth-child('+index+')')
                .css('background', 'rgba(0, 0, 0, 0)')
                .css('color', 'rgba(0, 0, 0, 0.8)');
        });

        $('.original-table-block tbody tr').hover(function(){
            var index = $(this).index() + 1;
            $(this).parents('.fixed-first-column-table-block')
                .find('.fixed-column-table tbody tr:nth-child('+index+')')
                .css('background', 'rgba(0, 0, 0, 0.05)')
                .css('color', 'rgba(0, 0, 0, 0.95)');
        }, function(){
            var index = $(this).index() + 1;
            $(this).parents('.fixed-first-column-table-block')
                .find('.fixed-column-table tbody tr:nth-child('+index+')')
                .css('background', 'rgba(0, 0, 0, 0)')
                .css('color', 'rgba(0, 0, 0, 0.8)');
        });
    });

    var tableEl       = $('#charts-report-list-table'),
    scrollBody        = $('.dataTables_scrollBody'),
    selected_delete   = [],
    selected_to_draft = [],
    selected_approve  = [],
    tableColumns,
    recordsForDelete  = 0,
    table             = tableEl.DataTable({
        scrollCollapse: true,
        searching     : false,
        processing    : true,
        language: {
            processing: '<img src="/img/loading.gif"/>',
            paginate: {
                "sNext": '<i class="caret right icon"></i>',
                "sPrevious": '<i class="caret left icon"></i>'
            }
        },
        aoColumnDefs: [
            {
                "bSortable": false,
                "aTargets" : "_all"
            },
            {
                "targets": "col-reportname",
                "render" : function (value, action, row, settings) {
                    return '<a target="_blank" href="' + row.url + '" class="name">' + row.name + '</a>'
                }
            },
            {
                "targets": "col-actions",
                "render" : function (value, action, row, settings) {
                    return '<a title="Copy report url" href="#" class="ui icon yes button copy-btn basic green active-block" data-clipboard-text="' + row.url + '"><i class="copy icon"></i></a>'
                        + '<a href="#" class="ui icon button basic yellow edit-report active-block" title="Edit report title"><i class="icon-edit-yellow icon"></i></a>'
                        + '<a href="#" class="ui delete button red mini basic" title="Delete report" data-method="delete"><i class="delete icon"></i></a>'
                        + '<span class="approve"><a class="right floated ui yes button green">Yes</a>'
                        + '<a href="#" class="right floated ui no button red">No</a></span>'
                    ;
                }
            },
        ],
        columns: [
            { "data": "name" },
            { "data": "created_at", class: 'date-column' },
            { "data": "type", class: 'menu' },
        ],
        dom: '<"top"f>rt<"bottom"ilp><"clear">',
        serverSide: true,
        ajax: {
            url : "/charts/report-list",
            dataSrc: function ( json ) {
                return json.data;
            }
        },
        drawCallback: function(settings) {
            tableEl.find("thead").remove();
            $(".copy-btn").each(function(index, element) {
                $(element).on("click", function(event) {
                    clipboard.copy($(this).attr("data-clipboard-text"));
                });
            });
        }
    });

    table.on('draw.dt', function () {
        table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i, data) {
            $(cell).closest("tr").attr("id", "report-" + table.row(i).data().id);
            $(cell).closest("tr").attr("data-id", table.row(i).data().id);
            $(cell).closest("tr").addClass("item");
        });
    });

    $(document).on('click', '.copy-btn', function(e) {
        e.preventDefault();
        e.stopPropagation();
    });

});