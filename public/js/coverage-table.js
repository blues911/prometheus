$(document).ready(function () {
    $("#select-all-coverage").checkbox('uncheck');

    var recordsFiltered  = 0;
    var clearFilter      = false;
    var checkboxTemplate = '<div class="ui checkbox coverage-select"><input type="checkbox" name="select"><label></label></div>';

    var highlightFilters = function () {
        $('#start_range, #end_range').each(function () {
            processHighlight(this, 'datepicker');
        });
        $('#region-filter, #user-filter').each(function () {
            processHighlight(this, 'dropdown');
        });
    };

    "use strict";

    $('.ui.dropdown').dropdown();
    if (typeof regionId !== "undefined") {
        $("#region_dropdown_filter").dropdown('set value', regionId).dropdown('set selected', regionId);
    }

    $('.input-daterange').datepicker({
        format        : "yyyy-mm-dd",
        weekStart     : 1,
        autoclose     : true,
        todayHighlight: true
    });

    fillFilter();

    var table_headers = {
        business: "Business segment",
        campaign: "Themes / Campaigns",
        desired_article: "Desired",
        event_name: "Event name",
        file_id: "File",
        header_tonality: "Header ton.",
        image: "Image",
        impact_index: "Impact index",
        key_message_penetration: "Key mess. pen.",
        main_tonality: "Main ton.",
        media_category: "Cat.",
        media_list: "List",
        media_type: "Type",
        mention: "Brand / product mention",
        net_effect: "Net effect",
        news_break_global: "Newsbreaks Global",
        news_break_local: "Newsbreaks Local",
        products: "Mentioned products",
        speakers_quoted: "Speakers quoted",
        third_speakers_quoted: "3rd party speakers quoted",
        url: "URL",
        visibility: "Visibility"
    };

    var tableEl           = $('#fixed-table'),
        scrollBody        = $('.dataTables_scrollBody'),
        selected_delete   = [],
        selected_to_draft = [],
        selected_approve  = [],
        tableColumns,
        recordsForDelete  = 0,
        table             = tableEl.DataTable({
            scrollX       : (tableMode == "full"),
            scrollCollapse: true,
            searching     : true,
            oSearch       : {"sSearch": defaultSearch()},
            processing    : true,
            language: {
                processing: '<img src="/img/loading.gif"/>',
                paginate: {
                    "sNext": '<i class="caret right icon"></i>',
                    "sPrevious": '<i class="caret left icon"></i>'
                }
            },
            aoColumnDefs: [
                {
                    "bSortable": false,
                    "aTargets" : "_all"
                },
                {
                    "targets": "coverage-col-status",
                    "data"   : "status",
                    "render" : function (status) {
                        if (typeof status == 'undefined' || !status) {
                            return '';
                        }
                        info = {
                            'draft':    {class: 'write orange',   title: 'Draft'},
                            'complete': {class: 'wait blue',      title: 'Complete, waiting for approval'},
                            'approved': {class: 'checkmark teal', title: 'Approved'},
                        };
                        status = status.toLowerCase();
                        if (typeof info[status] !== 'undefined') {
                            return '<i class="status-icon icon ' + info[status].class + '" title="' + info[status].title + '" ></i>'
                        }
                    }
                },
                {
                    "targets": "coverage-col-id",
                    "render" : function (value, action, row, settings) {
                        return checkboxTemplate + '<span data-id="' + value + '">#' + (settings.row + 1) + "</span>";
                    }
                },
                {
                    "targets": "coverage-col-url",
                    "data"   : "url",
                    "render" : function (url) {
                        if (typeof url == 'undefined' || !url || !url.length) {
                            return "No";
                        }
                        if (!/^(https?):\/\//i.test(url) && 'https?://'.indexOf(url) === -1) {
                            return url;
                        }
                        return '<a href="' + url + '" target="_blank">' + (url.length > 30 ? url.substr(0, 30) + "..." : url) + '</a>';
                    }
                },
                {
                    "targets": "coverage-col-img",
                    "data"   : "image",
                    "render" : function (image) {
                        if (image != null && typeof image != 'undefined' && image.toLowerCase() == 'present') {
                            return '<span class="text-teal">Yes</span>';
                        }
                        return 'No';
                    }
                },
                {
                    "targets": "coverage-col-place",
                    "render" : function (value, action, row) {
                        return row.region + "&nbsp;&mdash;&nbsp;" + row.country;
                    }
                },
                {
                    "targets": ["coverage-col-mentioned-products"],
                    "render" : function (value) {
                        if (typeof value == 'undefined' || !value) {
                            return '';
                        }
                        return '<span class="products-list">' + value + '</span>';
                    }
                },
                {
                    "targets": ["coverage-col-speakers-quoted"],
                    "render" : function (value) {
                        if (typeof value == 'undefined' || !value) {
                            return '';
                        }
                        return '<span class="speakers-quoted">' + value + '</span>';
                    }
                },
                {
                    "targets": ["coverage-col-3rd-speakers-quoted"],
                    "render" : function (value) {
                        if (typeof value == 'undefined' || !value) {
                            return '';
                        }
                        return '<span class="3rd-speakers-quoted">' + value + '</span>';
                    }
                },
                {
                    "targets": ["coverage-col-negative"],
                    "render" : function (value) {
                        if (typeof value == 'undefined' || !value) {
                            return '';
                        }
                        return '<span class="negative-list">' + value + '</span>';
                    }
                },
                {
                    "targets": ["coverage-col-newsbreak-global"],
                    "render" : function (value) {
                        if (typeof value == 'undefined' || !value) {
                            return '';
                        }
                        return '<span class="newsbreak-list-global">' + value + '</span>';
                    }
                },
                {
                    "targets": ["coverage-col-newsbreak-local"],
                    "render" : function (value) {
                        if (typeof value == 'undefined' || !value) {
                            return '';
                        }
                        return '<span class="newsbreak-list-local">' + value + '</span>';
                    }
                },
                {
                    "targets": ["coverage-col-comments"],
                    "render" : function (value) {
                        if (typeof value == 'undefined' || !value) {
                            return '';
                        }
                        return '<span class="comments-list">' + value + '</span>';
                    }
                },
                {
                    "targets": ["coverage-col-campaign"],
                    "render" : function (value) {
                        if (typeof value == 'undefined' || !value) {
                            return '';
                        }
                        return '<span class="campaigns-list">' + value + '</span>';
                    }
                },
                {
                    "targets": ["coverage-col-business"],
                    "render" : function (value) {
                        if (typeof value == 'undefined' || !value) {
                            return '';
                        }
                        return '<span class="business-list">' + value + '</span>';
                    }
                },
                {
                    "targets": "coverage-col-media",
                    "render" : function (value, action, row) {
                        var media_column = '<div class="ui three column centered grid">';
                        ['media_list', 'media_type', 'media_category'].forEach(function (media) {
                            media_column += '<div class="field column text-center no-padding"><i class="custom-icon icon-' + row[media].toLowerCase() +'" title="' + row[media] + '"></i></div>';
                        });
                        media_column += "</div>";

                        if (row['media_subcategories']) {
                            var subCategories = row['media_subcategories'].split(',');
                            subCategories.forEach(function(categories) {
                                media_column += '<div class="field column text-center no-padding">';
                                media_column += '<div class="subcategories-column">';
                                media_column += categories;
                                media_column += '</div></div>';
                            });
                        }

                        return media_column;
                    }
                },
                {
                    "targets": "coverage-col-tonality",
                    "render" : function (value, action, row) {
                        var tonalit_column = '<div class="ui two column centered grid">';
                        ['header_tonality', 'main_tonality'].forEach(function (tonality) {
                            if (row[tonality] != null) {
                                tonalit_column += '<div class="field column text-center no-padding"><i class="custom-icon icon-' + row[tonality].toLowerCase() +'" title="' + row[tonality] + '"></i></div>';
                            }
                        });
                        return tonalit_column + "</div>";
                    }
                },
                {
                    "targets": "coverage-col-message-penetration",
                    "render" : function (data) {
                        if (!data || data === null) {
                            return data;
                        }
                        var icons = {
                            'present to some extent' : 'neutral',
                            'clearly present'        : 'positive',
                            'counter-message'        : 'dead',
                            'absent'                 : 'negative',
                        };
                        return '<div class="icon-' + icons[data.toLowerCase()] +'" title="' + data + '"></div>';
                    }
                },
                {
                    "targets": "coverage-col-desired-article",
                    "render" : function (data) {
                        if (parseInt(data)) {
                            return '<span class="text-teal">Yes</span>';
                        }
                        return 'No';
                    }
                },
                {
                    "targets": "coverage-col-file",
                    "render" : function (data) {
                        if (!data || data == null || !data.file || data.file == null) {
                            return '';
                        }
                        return '<a href="#" file-id="' + data.file_id + '"class="dot link-file">' + data.file + "</a><br/><p>" + data.file_created_at + '</p>';
                    }
                },
                {
                    "targets": "coverage-col-general-compact",
                    "render" : function (value, action, row, settings) {
                        info = {
                            'draft':    {class: 'text-orange', title: 'Draft'},
                            'complete': {class: 'text-blue',   title: 'Complete'},
                            'approved': {class: 'text-teal',   title: 'Approved'},
                        };
                        column = checkboxTemplate + getEditButton("general", row) + '<div class="column-block"><span class="sub-text">#' + (settings.row + 1) +
                            '</span>&nbsp;<span class="' + info[row.status].class + '">' + info[row.status].title + "</span>";
                        column += "<div>" + row.name + "</div></div>";
                        column += '<div class="column-block"><span class="sub-text">Owner</span><div>' + row.owner + "</div></div>";
                        column += '<div class="column-block"><span class="sub-text">Reach</span><div>' + row.reach + "</div></div>";
                        return column + '<div class="column-menu">' + getCoverageMenu(row.preference) + getComment(row.comments) + "</div>";
                    }
                },
                {
                    "targets": "coverage-col-publication-compact",
                    "render" : function (value, action, row) {
                        var column = getEditButton("publication", row);
                        ["date", "place", "author", "headline"].forEach(function (field) {
                            if (field == "place") {
                                row[field] = row.region + "&nbsp;&mdash;&nbsp;" + row.country;
                            }
                            if (typeof row[field] == 'undefined' || !row[field].length) {
                                return true;
                            }
                            column += '<div class="column-block dont-break-out publication-block"><span class="sub-text">' + field + '</span><div>' + row[field] + '</div></div>';
                        });
                        return column;
                    }
                },
                {
                    "targets": "coverage-col-media-compact",
                    "render" : function (value, action, row) {
                        var column = "";
                        ['media_list', 'media_type', 'media_category'].forEach(function (field) {
                            column += '<div class="column-block"><span class="sub-text">' + table_headers[field] + '</span>';
                            column += '<div><i class="custom-icon icon-' + row[field].toLowerCase() +'" title="' + row[field] + '"></i></div></div>';
                        });

                        if (row['media_subcategories']) {
                            var subCategories = row['media_subcategories'].split(',');
                            column += '<div class="sub-text">Subcat.</div>';
                            subCategories.forEach(function(categories) {                                
                                column += '<div class="field column text-center no-padding">';
                                column += '<div class="subcategories-column">';
                                column += categories;
                                column += '</div></div>';
                            });
                        }

                        return column;
                    }
                },
                {
                    "targets": ["coverage-col-analytics-compact"],
                    "render" : function (value, action, row) {
                        var column = getEditButton("analytics", row) + '<div class="ui four column centered grid analytics-block">';
                        ['desired_article', 'visibility', 'image'].forEach(function (field) {
                            column += '<div class="column text-center"><span class="sub-text">' + table_headers[field] + '</span><br>';
                            style = "";
                            value = "";
                            if (field == 'desired_article') {
                                value = parseInt(row[field]) ? 'Yes'       : 'No';
                                style = parseInt(row[field]) ? 'text-teal' : '';
                            }
                            else if (field == 'image') {
                                condition = row[field] != null && typeof row[field] != 'undefined' && row[field].toLowerCase() == 'present';
                                value = condition ? 'Yes'       : 'No';
                                style = condition ? 'text-teal' : '';
                            }
                            else {
                                value = row[field] == null ? "&mdash;" : row[field];
                            }
                            column += '<span class="' + style + ' ui basic label">' + value + '</span></div>';
                        });
                        column += '</div><div class="ui one column no-center grid" style="padding-left: 2%; padding-top: 10px;">';
                        ["file_id", "url"].forEach(function (field) {
                            if (typeof row[field] == 'undefined' || row[field] == null || !row[field].length) {
                                return true;
                            }
                            if (field == "url" && (/^(https?):\/\//i.test(row[field]) || 'https?://'.indexOf(row[field]) !== -1)) {
                                row[field] = '<a href="' + row[field] + '" target="_blank">' + row[field] + '</a>';
                            }
                            else if (field == "file_id") {
                                row[field] = '<a href="#" file-id="' + row.file.file_id + '"class="link-file">' + row.file.file + "</a>";
                            }
                            column += '<div class="column"><div class="column-block dont-break-out"><span class="sub-text">' + table_headers[field] + '</span><div>' + row[field] + '</div></div></div>';
                        });
                        column += '</div>';
                        return column;
                    }
                },
                {
                    "targets": "coverage-col-mentioned-compact",
                    "render" : function (value, action, row) {
                        var column = getEditButton("mentioned", row);
                        ['mention', 'products', 'speakers_quoted', 'third_speakers_quoted'].forEach(function (field) {
                            if (typeof row[field] == 'undefined' || row[field] == null || !row[field].length) {
                                return true;
                            }
                            column += '<div class="column-block"><span class="sub-text">' + table_headers[field] + '</span><div>' + row[field] + '</div></div>';
                        });
                        return column;
                    }
                },
                {
                    "targets": "coverage-col-tonality-compact",
                    "render" : function (value, action, row) {
                        var column = getEditButton("tonality", row);
                        var icons = {
                            'present to some extent' : 'neutral',
                            'clearly present'        : 'positive',
                            'counter-message'        : 'dead',
                            'absent'                 : 'negative',
                        };
                        ['header_tonality', 'main_tonality', 'key_message_penetration'].forEach(function (field) {
                            if (typeof row[field] == 'undefined' || row[field] == null || !row[field].length) {
                                return true;
                            }
                            column += '<div class="column-block"><span class="sub-text">' + table_headers[field] + '</span>';
                            icon   = field == "key_message_penetration" ? icons[row[field].toLowerCase()] : row[field].toLowerCase();
                            icon_span = '<div><i class="custom-icon icon-' + icon + ' ' + field + '" title="' + row[field] + '"></i></div>'
                            if ("main_tonality" == field && row[field].toLowerCase() == "negative" && row.negative_explanation.length) {
                                column += '<div class="tonality-popup"><span data-title="EXPLANATION OF NEGATIVE" data-position="left center" data-content="' + row.negative_explanation + '">' + icon_span + '</span></div></div>';
                            }
                            else {
                                column += icon_span + '</div>';
                            }
                        });
                        return column;
                    }
                },
                {
                    "targets": "coverage-col-events-compact",
                    "render" : function (value, action, row) {
                        var column = getEditButton("events", row);
                        ['event_name', 'news_break_global', 'news_break_local', 'campaign', 'business'].forEach(function (field) {
                            if (typeof row[field] == 'undefined' || !row[field].length) {
                                return true;
                            }
                            column += '<div class="column-block"><span class="sub-text">' + table_headers[field] + '</span><div>' + row[field] + '</div></div>';
                        });
                        return column;
                    }
                },
                {
                    "targets": "coverage-col-impact-compact",
                    "render" : function (value, action, row) {
                        var column = "";
                        ['impact_index', 'net_effect'].forEach(function (field) {
                            column += '<div class="column-block"><span class="sub-text">' + table_headers[field] + '</span>';
                            column += '<div><span class="ui basic label">' + row[field] + '</span></div></div>';
                        });
                        return column;
                    }
                },
                {
                    "targets": "coverage-col-actions",
                    "render" : function (data) {
                        return '<div class="item-' + data.id + ' hidden-column" style="display: none;">' + getCoverageMenu(data) + "</div>";
                    }
                }
            ],
            dom: '<"top"filp<"clear">>rt<"bottom"ilp<"clear">>',
            "order": [
                [ 0, "desc" ]
            ],
            serverSide: true,
            ajax: {
                url : "/coverage",
                data: function (params, test, t2, t3) {
                    params.start_range = $("#start_range").val();
                    params.end_range   = $("#end_range").val();
                    params.region_id   = $('#region-filter').val();
                    params.user_id     = $('#user-filter').val();
                    params.filter      = $("#other_filters").val();
                    info = getPageinfo();
                    if (info !== null) {
                        params.start = info['start'];
                        params.length = info['length'];
                    }
                    else if (null !== localStorage.getItem('filter')) {
                        var filter = JSON.parse(localStorage.getItem('filter'));
                        params.start = filter['start'];
                        params.length = filter['length'];
                    }
                },
                "dataSrc": function ( json ) {
                    recordsForDelete = json.recordsForDelete;
                    if (0 === recordsForDelete) {
                        $('.table.coverage .table-actions a.delete-all').addClass('disabled');
                    } else {
                        $('.table.coverage .table-actions a.delete-all').removeClass('disabled');
                    }

                    recordsFiltered = json.recordsFiltered;

                    return json.data;
                }
            },
            columns: getTableColumns(),
            "initComplete": function(settings, json) {
                jumpToPage();
                // $('#coverage-page .row.table-actions.ui.form .fields').append('<div class="field" id="top-pagging"></field>');
                // $('.bottom').clone(true).appendTo('#top-pagging');
            },
            "drawCallback": function(settings) {
                saveFilter();
                processHighlight($('.dataTables_filter input[type=search]'), 'search');
                var params_shorten = {
                    "showChars"    : 10,
                    "minHideChars" : 1,
                    "ellipsesText" : '',
                    "moreText"     : '<i class="caret right icon"></i>',
                    "lessText"     : 'Hide',
                };
                $(".products-list, .speakers-quoted, .3rd-speakers-quoted, .negative-list, .newsbreak-list-global, .newsbreak-list-local, .campaigns-list, .business-list").shorten(params_shorten);
                params_shorten["showChars"] = 35;
                $(".comments-list").shorten(params_shorten);
                $('.ui.checkbox').checkbox();
                $('.tonality-popup span').popup({ inline: true, distanceAway: 20 });
                $('.popup').popup({ inline: true});
                selected_delete   = [];
                selected_to_draft = [];
                selected_approve  = [];
                $('.table.coverage .table-actions a.delete, .table.coverage .table-actions a.approve, .table.coverage .table-actions a.return_to_draft').addClass('disabled');
            }
        });

    table.on('draw.dt', function () {
        table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i, data) {
            $(cell).closest("td").attr("data-id", $(cell).find("span").attr("data-id"));
            $(cell).closest("td").addClass("fixed-id-column");
        });
    });

    table.on( 'search.dt', function () {
        updateExportUrl();
    });

    function getEditButton(column, row) {
        var infoCol = {
            "general"     : { title: "Edit general info",                      type: 'edit-general' },
            "publication" : { title: "Edit publication info",                  type: 'edit-publication' },
            "media"       : { title: "Edit media info",                        type: 'edit-media' },
            "analytics"   : { title: "Edit analytics info",                    type: 'edit-analytics' },
            "mentioned"   : { title: "Edit mentions and speakers quoted info", type: 'edit-mentioned' },
            "tonality"    : { title: "Edit tonality and penertation info",     type: 'edit-tonality' },
            "events"      : { title: "Edit analytics info",                    type: 'edit-events' },
        };
        if (typeof infoCol[column] === 'undefined' || !row.preference.access_edit) {
            return '';
        }
        return '<div class="edit-column-block">'
            + '<a data-type="' + infoCol[column].type + '" data-id="' + row.id + '" title="'+ infoCol[column].title +'" class="ui basic yellow button skip-click icon edit-column-btn">'
            + '<i class="icon-edit-yellow icon"></i></a></div>'
    }

    function getPageinfo() {
        if (typeof table == 'undefined') {
            return null;
        }
        return table.page.info();
    }

    function getComment(comment) {
        if (comment == null || !comment.length) {
            return '';
        }
        var icon = '<i class="custom-icon icon-comment"></i>';
        return '<div class="comment-icon"><span data-title="COMMENT" class="popup" data-position="right center" data-content="' + comment + '">'+ icon +'</span></div>';
    }

    function getCoverageMenu(data) {

        var actions  = [];
        var menu     = '';
        var menu_actions = {
            'edit':               { access: data.access_edit },
            'no_action':          { access: !data.access_edit && tableMode == "full" },
            'complete':           { access: data.access_complete },
            'approve_and_return': { access: data.access_publish && tableMode == "full" && false }, // временно отключена
            'approve':            { access: data.access_publish },
            'return':             { access: data.access_publish },
            'delete':             { access: data.access_delete  }
        };

        $.each(menu_actions, function(action, row) {
            if (row.access) {
                actions.push(action);
            }
        });

        var info = {
            'edit':               { class: 'basic yellow', title: 'Edit coverage',                  icon: 'icon-edit-yellow' },
            'return':             { class: 'blue',         title: 'Return coverage to draft',       icon: 'reply' },
            'delete':             { class: 'basic red',    title: 'Delete coverage',                icon: 'remove' },
            'approve':            { class: 'green',        title: 'Approve coverage',               icon: 'checkmark' },
            'complete':           { class: 'basic blue',   title: 'Complete coverage',              icon: 'checkmark box' },
            'no_action':          { class: 'basic green',  title: 'Coverage is complete',           icon: 'checkmark' },
            'approve_and_return': { class: 'basic orange', title: 'Actions for completed coverage', icon: 'settings' }
        };

        actions.forEach(function (action) {
            menu += '<a href="#" class="ui icon button ' + info[action].class + ' ' + action +' item" title="' + info[action].title +'" data-id="' + data.id + '" data-action="' + action + '"><i class="' + info[action].icon +' icon"></i></a>';
        });

        menu = '<span class="menu_' + data.id + ' actions">' + menu + '</span>';

        actions.forEach(function (action) {
            if (action != 'approve_and_return') {
                menu += '<span class="actions action_' + action + '_' + data.id + ' '+ action +'" style="display: none;" data-action="' + action + '">'
                    + '<a href="#" class="ui icon button red no" data-id="' + data.id + '">No</a>'
                    + '<a href="#" class="ui icon button green yes" data-id="' + data.id + '">Yes</a></span>';
            }
            else {
                menu += '<span class="actions sub_menu_' + data.id + ' action_' + action + '_' + data.id + ' '+ action +'" style="display: none;" data-action="' + action + '">'
                    + '<a href="#" class="ui icon ' + info['approve'].class + ' button approve item" title="' + info['approve'].title + '" data-id="' + data.id + '" data-action="approve"><i class="' + info['approve'].icon +' icon"></i></a>'
                    + '<a href="#" class="ui icon ' + info['return'].class + ' button return item" title="' + info['return'].title + '" data-id="' + data.id + '" data-action="return"><i class="' + info['return'].icon +' icon"></i></a></span>';
                ['return', 'approve'].forEach(function (sub_action) {
                    menu += '<span class="actions action_' + sub_action + '_' + data.id + ' '+ sub_action +'" style="display: none;" data-action="' + sub_action + '">'
                         + '<a href="#" class="ui icon button red no" data-id="' + data.id + '">No</a>'
                        + '<a href="#" class="ui icon button green yes" data-id="' + data.id + '">Yes</a></span>';
                });
            }
        });

        return menu;
    }

    function saveModalForm(editData, id, pageNum) {
        $.ajax({
            url     : "/coverage/" + id,
            method  : "PUT",
            dataType: "json",
            data    : editData,
            success: function (response) {
                var modal = $("#edit-coverage-modal");
                modal.find('.field').removeClass('error');
                modal.find('.ui.label').remove();
                if (response.error) {
                    $.each(response.errors, function(key, block) {
                        var field = modal.find('input[name="'+key+'"]').closest('.field');
                        field.addClass('error');
                        field.append('<div class="ui red pointing prompt label transition visible">'+block+'</div>')
                    });
                } else {
                    table.page(pageNum).draw(false);
                    modal.modal("hide");
                }
            }
        });
    }

    if (tableMode == "full") {
        tableColumns = new $.fn.dataTable.FixedColumns(table, {
            leftColumns: 1,
            rightColumns: 1
        });
    }

    $(document.body).on("click", ".morelink:not(.less)", function() {
        setTimeout(function() {
            tableEl.find('tbody tr').css('width', 'auto');
            tableEl.find('tbody tr').css('height', 'auto');
            $('#fixed-table').resize();
            tableColumns.fnRedrawLayout();
        });
        var $element = $(this).closest(".shortened");
        if (typeof $element !== 'undefined' && $element.length > 0) {
            $element[0].className.split(/\s+/).forEach(function (class_name) {
                var shortened_classes = ["products-list", "speakers-quoted", "3rd-speakers-quoted", "negative-list", "newsbreak-list-global", "newsbreak-list-local", "campaigns-list", "comments-list"];
                if ($.inArray(class_name, shortened_classes) !== -1) {
                    setTimeout(function() {
                        $("." + class_name + " .morelink:not(.morelink.less)").trigger("click");
                    });
                }
            });
        }
    });

    $(document.body).on("click", ".morelink.less", function() {
        setTimeout(function() {
            tableEl.find('tbody tr').css('width', 'auto');
            tableEl.find('tbody tr').css('height', 'auto');
            tableColumns.fnRedrawLayout();
        });
    });

    $('.input-daterange').on('changeDate', function (ev) {
        if (clearFilter) {
            return;
        }
        table.draw();
        updateExportUrl();
        highlightFilters();
    });


    $('.input-daterange i.add-on').click(function (e) {
        $(e.currentTarget).prev().datepicker('show');
    });

    $('.dataTables_filter label').addClass('ui transparent icon input');
    $('.dataTables_filter input').after('<i class="search icon"></i>');
    $('.dataTables_paginate').addClass('ui borderless pagination menu');
    $('.dataTables_paginate a').addClass('item');

    $('.dataTables_filter').click(function (e) {
        $('.dataTables_filter').css('border-color', 'rgba(39, 41, 43, 0.3)');
        $(this).find('input').animate({ width: '150px' }, 500);
    });

    $("body").click(function (e) {
        if ($(e.target).closest(".dataTables_filter").length === 0) {
            $('.dataTables_filter').css('border-color', 'rgba(39, 41, 43, 0.15)');
            $('.dataTables_filter input').animate({ width: '0px' }, 500);
        }
    });
    
    $('.table-actions .toggle .circle').click(function (e) {
        if (!$(this).hasClass('.active')) {
            $(this).parent().find('.active').removeClass('teal active').addClass('grey');
            $(this).removeClass('grey').addClass('teal active');
        }
    });

    function getTableColumns() {
        if (tableMode == "full") {
            return [
                { "data": "id"},
                { "data": "name" },
                { "data": "status", class: 'small' },
                { "data": "owner" },
                { "data": "reach", class: 'small' },
                { "data": "date", class: 'small' },
                { "data": "country", class: 'small' },
                { "data": "author" },
                { "data": "headline" },
                { "data": "media_list" },
                { "data": "key_message_penetration", class: 'small' },
                { "data": "visibility", class: 'small' },
                { "data": "desired_article", class: 'small' },
                { "data": "image", class: 'small' },
                { "data": "url" },
                { "data": "mention" },
                { "data": "products" },
                { "data": "speakers_quoted" },
                { "data": "third_speakers_quoted" },
                { "data": "header_tonality" },
                { "data": "negative_explanation" },
                { "data": "event_name" },
                { "data": "news_break_global" },
                { "data": "news_break_local" },
                { "data": "campaign" },
                { "data": "business", class: "middle" },
                { "data": "impact_index", class: 'small' },
                { "data": "net_effect", class: 'small' },
                { "data": "file" },
                { "data": "comments" },
                { "data": "preference", orderable: false}
            ];
        }
        return [
            { "data": "id"},
            { "data": "owner"},
            { "data": "media", class: 'small' },
            { "data": "analytics", class: "middle" },
            { "data": "mention"},
            { "data": "header_tonality", class: 'small' },
            { "data": "event_name"},
            { "data": "impact_index", class: 'small' }
        ];
    }


    scrollBody.css('height', 'auto');

    tableEl.find('tbody').on('click', 'tr', function (e) {
        if (typeof table.row(this).data() === 'undefined' || $(e.target).closest(".morelink, .actions, .skip-click").length || $(e.target).hasClass("skip-click")) {
            return;
        }
        var id                     = table.row(this).data().id;
        var access_delete          = table.row(this).data().preference.access_delete;
        var access_approve         = table.row(this).data().preference.access_publish;
        var access_return_to_draft = table.row(this).data().preference.access_return_to_draft;

        $(this).toggleClass('selected');

        if ($(this).hasClass('selected')) {
            $(this).find('.coverage-select').checkbox('check');
            if (!$(".dataTables_scrollBody .coverage-select:not(.checked)").length) {
              $("#select-all-coverage").checkbox('check');
            }
            if (access_approve) {
                selected_approve.push(id);
            }
            if (access_delete) {
                selected_delete.push(id);
            }
            if (access_return_to_draft) {
                selected_to_draft.push(id);
            }
        } else {
            $(this).find('.coverage-select').checkbox('uncheck');
            if ($("#select-all-coverage").checkbox("is checked")) {
               $("#select-all-coverage").checkbox('uncheck');
            }
            delete selected_approve[$.inArray(id, selected_approve)];
            delete selected_delete[$.inArray(id, selected_delete)];
            delete selected_to_draft[$.inArray(id, selected_to_draft)];
        }

        if ($.isEmptyObject(selected_delete)) {
            $('.table.coverage .table-actions a.delete').addClass('disabled');
        } else {
            $('.table.coverage .table-actions a.delete').removeClass('disabled');
        }

        if ($.isEmptyObject(selected_approve)) {
            $('.table.coverage .table-actions a.approve').addClass('disabled');
        } else {
            $('.table.coverage .table-actions a.approve').removeClass('disabled');
        }

        if ($.isEmptyObject(selected_to_draft)) {
            $('.table.coverage .table-actions a.return_to_draft').addClass('disabled');
        } else {
            $('.table.coverage .table-actions a.return_to_draft').removeClass('disabled');
        }
    });

    tableEl.find('tbody').on('mouseover', 'tr', function (e) {
        $("." + $(this).attr("id")).show();
    });

    tableEl.find('tbody').on('mouseleave', 'tr', function (e) {
        $("." + $(this).attr("id")).hide();
    });

    $(document.body).on('mouseover', '.DTFC_RightBodyLiner td', function (e) {
        $(this).find(".hidden-column").show();
    });

    $(document.body).on('mouseleave', '.DTFC_RightBodyLiner td', function (e) {
        $(this).find(".hidden-column").hide();
    });

    $(document.body).on('mouseover', '.fixed-id-column', function (e) {
        $(".item-" + $(this).attr("data-id")).show();
    });

    $(document.body).on('mouseleave', '.fixed-id-column', function (e) {
        $(".item-" + $(this).attr("data-id")).hide();
    });

    $(document.body).on("click", "#select-all-coverage", function() {
        var selector = ".coverage-select.checked"
        if ($(this).checkbox("is checked")) {
            selector = ".coverage-select:not(.checked)"
        }
        $(selector).each(function(key, element) {
            $(element).closest("tr").trigger("click");
        });
    });

    $(document.body).on("click", ".save-edit", function(e) {
        e.preventDefault();

        var form       = $('#edit-coverage-modal .form'),
            editData   = {},
            id         = "",
            validators = {},
            paginationLinks = $('.dataTables_paginate').find('span').children(),
            pageNum = 0;

        validators.third_speakers_quoted = {
            identifier: 'third_speakers_quoted',
            rules: [
                {
                    type  : 'not_in',
                    prompt: 'Field has an invalid value (no, none, n/a, na, not available, no name, no speaker)'
                },
                {
                    type  : 'third_speakers_format',
                    prompt: 'Please, use ony letters and comma sign.'
                }
            ]
        };

        validators.date = {
            identifier: 'date',
            rules: [
                {
                    type  : 'empty',
                    prompt: 'Empty field is not allowed'
                },
                {
                    type: 'is_coding_completed',
                    prompt: 'Coverage coding process in this month was completed'
                }
            ]
        };

        $.each(paginationLinks, function(index, item) {
            if ($(item).hasClass('current')) {
                pageNum = parseInt($(item).text()) - 1;
            }
        });

        $("#edit-coverage-modal input, #edit-coverage-modal textarea").each(function(key, element) {
            if ($(element).attr("name") == null || !$(element).attr("name").length) {
                return true;
            }
            if ($(element).attr("name") == "id") {
                id = $(element).val();
                return true;
            }
            if ($(element).attr('type') == "checkbox") {
                if ($(element).attr("data-model") != null && typeof editData[$(element).attr("data-model") + "_state"] == 'undefined') {
                    editData[$(element).attr("data-model") + "_state"] = 0;
                }
                if ($(element).parent().hasClass('checked')) {
                    editData[$(element).attr("data-model") + "_state"] = 1;
                    editData[$(element).attr("name")] = $(element).val();
                }
            } else {
                editData[$(element).attr("name")] = $.trim($(element).val());
            }            
        });

        editData['speaker_quote'] = editData['speaker_quote_state'] = $('#speakers_quoted').val() != '[]' ? 1 : 0.8;
        editData['third_speakers_quotes'] = editData['third_speakers_quotes_state'] = $('#third_speakers_quoted').val() ? 1 : 0;

        if (editData['media_subcategories'] === '') {
            editData['media_subcategories'] = '0';
        }

        if ('edit-publication' === $('#edit-coverage-modal').attr('data-type')) {
            form.form(validators, {
                rules: {
                    is_coding_completed: function(value) {
                        var result = true;
                        var format = value.split('-');
                        var isAdmin = ($("meta[name=is_admin]").attr('content') == 'yes') ? true : false;
                        format.pop();
                        format = format.join('-');
            
                        if (isAdmin == false && Object.keys(coding_closed_months).length > 0) {
                            $.each(coding_closed_months, function(k, v){
                                if (format == v) {
                                    result = false;
                                    return true;
                                }
                            });
                        }
            
                        return result;
                    }
                },
                inline: true,
                onSuccess: function () {
                    saveModalForm(editData, id, pageNum);
                }
            });
            form.form('validate form');
        }

        if ('edit-events' === $('#edit-coverage-modal').attr('data-type')) {
            if ($('#edit-coverage-modal .business input:checked').length < 1) {
                $('#edit-coverage-modal .business .ui.label').removeClass('hidden');
                return false;
            } else {
                $('#edit-coverage-modal .business .ui.label').addClass('hidden');
            }
        }

        if ('edit-mentioned' === $('#edit-coverage-modal').attr('data-type')) {
            form.form(validators, {
                rules: {
                    third_speakers_format: function (value) {
                        return (0 < value.length) ? /^[^_0-9!@#$%№;:?*.^<>~\[\]\-_='"\\|\/{}()\+]+$/ig.test(value) : true;
                    },
                    not_in: function (value) {
                        return !($.inArray(value.toLowerCase(), ['no', 'none', 'n/a', 'na', 'not available', 'no name', 'no speaker']) !== -1);
                    }
                },
                inline: true,
                onSuccess: function () {
                    saveModalForm(editData, id, pageNum);
                }
            });
            form.form('validate form');
        } else {
            console.log(editData);
            saveModalForm(editData, id, pageNum);
        }
    });

    $(document.body).on("click", ".edit-column-btn", function(e) {
        e.preventDefault();

        var title = $(this).attr("title"),
            type = $(this).attr("data-type"),
            coverageId = $(this).attr('data-id');

        $.ajax({
            method: 'POST',
            url: '/coverage/check-coding-process',
            dataType: 'json',
            data: { method: 'edit', coverage_id: coverageId},
            success: function(r) {
                if (r.error == true) {
                    var closedMonth = $("#coding-process-notify-modal").find('#closed-month-name');
                    closedMonth.empty();
                    closedMonth.text(r.month);
                    showCodingProcessNotifyModal();
                    return false;
                } else {
                    $.ajax({
                      method  : 'GET',
                      url     : '/coverage/get-edit-modal',
                      dataType: 'json',
                      data    : { id: coverageId, type: type },
                      success : function (response) {
                          $("#edit-coverage-modal .header").html(title);
                          $("#edit-coverage-modal .content").html(response.html);
                          $("#edit-coverage-modal").attr('data-type', type);
                          $('.ui.checkbox').checkbox();
                          $('.ui.dropdown').dropdown();
                          $('.input.date input').datepicker({
                              format   : "yyyy-mm-dd",
                              weekStart: 1,
                              autoclose: true,
                              endDate  : "today",
                              todayHighlight: true
                          });
          
                          $('.ui.dropdown.region-list').dropdown({
                              onChange: function (value) {
                                  if (!value) {
                                      return false;
                                  }
          
                                  $.get('/country', {
                                      region_id: value
                                  }, function (response) {
                                      var defaultText = 'Choose country';
                                      $('.ui.dropdown.countries-list .menu').empty();
                                      $('.ui.dropdown.countries-list .text').text(defaultText);
          
                                      if (!$.isEmptyObject(response.countries)) {
                                          response.countries.forEach(function (country) {
                                              var item = $('<div>')
                                                  .addClass('item')
                                                  .attr('data-value', country.id)
                                                  .text(country.name);
                                              $('.ui.dropdown.countries-list .menu').append(item);
                                          });
                                      }
                                  });
                              }
                          });
                          
                          if ($('#edit-coverage-modal .edit-tonality input[name="main_tonality"]').val() != 'negative') {
                              $('#edit-coverage-modal .edit-tonality .comment').hide();
                          }
                          // init Newsbreak
                          $("#news_break_control").tagit({
                              allowNewTags           : true,
                              triggerKeys            : ['enter', 'tab'],
                              select                 : true,
                              editable               : false,
                              sortable               : false,
                              tagSource              : news,
                              callbackOnInitialTagAdd: false,
                              placeholder            : '',
                              autocompleteMinLength  : 2,
                              tagsChanged: function(tagValue, action, element) {
                                  var tags = [];
                                  $.each($("#news_break_control").tagit("tags"), function(k, v) {
                                      tags.push(v.value);
                                  });

                                  $("#news_break").val(JSON.stringify(tags));
                                  if (action == 'added' || action == 'popped') {
                                      $("#news_break").change();
                                  }
                              }
                          });
                          var tagit_control_data = [];
                          $.each(news_breaks, function(k, v) {
                            tagit_control_data.push({label: v, value: v});
                          });
          
                          $("#news_break_control").tagit("fill", tagit_control_data);
                          
                          // init Speakers Quoted
                          $("#speakers_quoted_control").tagit({
                              allowNewTags           : false,
                              triggerKeys            : ['enter', 'comma', 'tab'],
                              select                 : true,
                              editable               : false,
                              sortable               : false,
                              tagSource              : speakers,
                              callbackOnInitialTagAdd: false,
                              placeholder            : '',
                              autocompleteMinLength  : 2,
                              tagsChanged: function(tagValue, action, element) {
                                  var tags = [];
                                  if (action == 'added' && !/^([a-zA-Z]{2,}(-[a-zA-Z]{2,})*)(\s[a-zA-Z]{2,}(-[a-zA-Z]{2,})*)+$/.test(tagValue)){
                                      $('#speakers_quoted_control').tagit("remove", 'tag', tagValue);
                                      return;
                                  }
          
                                  $.each($("#speakers_quoted_control").tagit("tags"), function(k, v) {
                                      tags.push(v.value);
                                  });
          
                                  $("#speakers_quoted").val(JSON.stringify(tags));
                                  if (action == 'added' || action == 'popped') {
                                      $("#speakers_quoted").change();
                                  }
                              }
                          });
          
                          tagit_control_data = [];
                          $.each(speakers_quoted, function(k, v) {
                            tagit_control_data.push({label: v, value: v});
                          });
          
                          $("#speakers_quoted_control").tagit("fill", tagit_control_data);
          
                          var subcategories = $('.ui.dropdown.media').find('.item.active.selected').attr('data-subcategories');
                          if (subcategories == undefined) {
                              subcategories = ''
                          } else {
                              subcategories = subcategories.split(',');
                          }
                          $('.ui.dropdown.media-subcategories').dropdown('set exactly', subcategories).dropdown('initialize');
          
                          $('.ui.dropdown.media').dropdown({
                              onChange: function (value, text, choice) {
                                  if (!value) {
                                      return false;
                                  }
          
                                  var mediaData = $.parseJSON($(choice).attr('data-media'));
                                  var subcategories = $(choice).attr('data-subcategories');
                                  if (subcategories == undefined) {
                                      subcategories = ''
                                  } else {
                                      subcategories = subcategories.split(',');
                                  }
                                  $('.ui.dropdown.media-subcategories').dropdown('set exactly', subcategories).dropdown('initialize');
          
                                  $('.edit-general-form input[name="media_list"]').val(mediaData.list);
                                  $('.edit-general-form input[name="media_category"]').val(mediaData.category);
                                  $('.edit-general-form input[name="media_type"]').val(mediaData.type);
                                  $('.edit-general-form input[name="reach"]').val(mediaData.reach);
                              }
                          });
          
                          $("#edit-coverage-modal").modal("setting", {
                              autofocus: false,
                              closable: false,
                              onApprove: function () {
                                  return false;
                              },
                              close: '.button.cancel',
                          }).modal("show");
                      }
                    });
                }
            }
        });

    });
    
    $(document.body).on("change", "#edit-coverage-modal .edit-tonality input[name='main_tonality']", function(e) {
        e.preventDefault();
        if ($(this).val() == 'negative') {
            $('#edit-coverage-modal .edit-tonality .comment').show();
        } else {
            $('#edit-coverage-modal .edit-tonality .comment').hide();
        }
    });

    function saveFilter()
    {
        var params = getParams();
        if (params['start_range'] === undefined) {
            params['start_range'] = '';
        }
        if (params['end_range'] === undefined) {
            params['end_range'] = '';
        }
        var search = $('#fixed-table_filter input').val();
        if (typeof search !== undefined && '' !== search) {
            params['search'] = search;
        }
        params['page']   = table.page();
        var info         = table.page.info();
        params['start']  = info.start;
        params['length'] = info.length;
        localStorage.setItem('filter', JSON.stringify(params));
    }

    $(document.body).on('click', 'a.add', function (e) {
        saveFilter();
    });

    tableEl.find('tbody').on('click', 'a.edit', function (e) {
        e.preventDefault();
        var coverageId = $(this).attr('data-id');
        $.ajax({
            method: 'POST',
            url: '/coverage/check-coding-process',
            dataType: 'json',
            data: { method: 'edit', coverage_id: coverageId},
            success: function(r) {
                if (r.error == true) {
                    var closedMonth = $("#coding-process-notify-modal").find('#closed-month-name');
                    closedMonth.empty();
                    closedMonth.text(r.month);
                    showCodingProcessNotifyModal();
                    return false;
                } else {
                    var paginationLinks = $('.dataTables_paginate').find('span').children();
                    var pageNum = 0;
                    saveFilter();
                    $.each(paginationLinks, function(index, item) {
                        if ($(item).hasClass('current')) {
                            pageNum = parseInt($(item).text()) - 1;
                        }
                    });
                    localStorage.setItem('table_page', pageNum);
                    window.location.replace("/coverage/" + coverageId + "/edit");
                }
            }
        });
    });

    tableEl.find('tbody').on('click', 'a.complete, a.delete, a.approve_and_return', function (e) {
        e.preventDefault();
        var id     = $(e.currentTarget).attr('data-id');
        var action = $(e.currentTarget).attr('data-action');
        $(".menu_" + id).fadeOut("slow", function() {$(".action_" + action + "_" + id).fadeIn();});
    });

    tableEl.find('tbody').on('click', 'a.approve, a.return', function (e) {
        e.preventDefault();
        var id     = $(e.currentTarget).attr('data-id');
        var action = $(e.currentTarget).attr('data-action');
        var selector = ".menu_" + id;
        if (tableMode == "full") {
            selector = ".sub_menu_" + id;
        }
        $(selector).fadeOut("slow", function() {$(".action_" + action + "_" + id).fadeIn();});
    });

    tableEl.find('tbody').on("click", 'span.actions a.no', function (e) {
        e.preventDefault();
        var id     = $(e.currentTarget).attr('data-id');
        var action = $(e.currentTarget).parent().attr('data-action');
        $(".action_" + action + "_" + id).fadeOut("slow", function() {$(".menu_" + id).fadeIn();});
    });

    tableEl.find('tbody').on('click', 'span.approve a.yes', function (e) {
        e.preventDefault();
        changeStatus('/coverage/approve/', $(e.currentTarget).attr('data-id'));
    });

    tableEl.find('tbody').on('click', 'span.complete a.yes', function (e) {
        e.preventDefault();
        changeStatus('/coverage/complete/', $(e.currentTarget).attr('data-id'));
    });

    tableEl.find('tbody').on('click', 'span.return a.yes', function (e) {
        e.preventDefault();
        changeStatus('/coverage/return-to-draft/', $(e.currentTarget).attr('data-id'));
    });

    tableEl.find('tbody').on('click', 'span.delete a.yes', function (e) {
        e.preventDefault();
        changeStatus('/coverage/' + $(e.currentTarget).attr('data-id'), null, "DELETE");
    });

    tableEl.find('tbody').on('click', 'a.link-file', function (e) {
        e.preventDefault();
        var other_filters = JSON.parse($('#other_filters').val());
        other_filters.file_id = $(this).attr('file-id');
        $('#other_filters').val(JSON.stringify(other_filters));
        if (baseFilters == undefined) {
            baseFilters = {};
        }
        baseFilters['file_id'] = other_filters.file_id;
        table.draw();
        updateExportUrl();
    });

    function changeStatus(url, id, method) {
        $.ajax({
            method  : method || 'GET',
            url     : url,
            dataType: 'json',
            data    : {id: id},
            success : function () {
                $(".action_" + id).fadeOut("slow", function() {$(".menu_" + id).fadeIn();});
                $('#notification').dimmer('show');
                setTimeout(function () {
                    $('#notification').dimmer('hide');
                    window.location.reload();
                }, 1000);
            }
        });
    }


    $(document.body).on("click", "#table-switcher div", function (e) {
        $.ajax({
            method  : 'POST',
            url     : "/coverage/set-table-mode",
            dataType: 'json',
            data    : {mode: $(this).attr("data-table-mode")},
            success : function () {
                $('#notification').dimmer('show');
                setTimeout(function () {
                    $('#notification').dimmer('hide');
                    window.location.reload();
                }, 1000);
            }
        });
    });

    tableEl.find('tbody').on('dblclick', 'tr', function (e) {
        if ($(e.target).closest(".morelink, .actions, .skip-click, .checkbox").length || $(e.target).hasClass("skip-click") || $(e.target).hasClass("checkbox")) {
            return;
        }
        var id = table.row(this).data().id;
        saveFilter();
        window.location.replace("/coverage/" + id + "/edit");
    });

    $(document.body).on("click", "#delete-modal .delete", function (e) {

        if ($('.table.coverage .table-actions a.delete').hasClass('disabled')) {
            return false;
        }

        selected_delete.forEach(function (v) {
            $.ajax({
                url     : "/coverage/" + v,
                method  : "DELETE",
                dataType: "json",
                success : function () {
                    delete selected_delete[$.inArray(v, selected_delete)];
                    table.row('#item-' + v).remove();
                    $('#delete-modal').modal('hide');
                }
            });
        });

        table.draw();
        $('.table.coverage .table-actions a.delete').addClass('disabled');
        $('.table.coverage .table-actions a.approve').addClass('disabled');
        return false;
    });

    $('.table.coverage .table-actions a.delete-all').click(function (e) {
        var modal = $('#delete-all-modal');
        modal.find('.content b').text(recordsForDelete);
        modal.modal('show');
        return false;
    });

    $('.table.coverage .table-actions a.delete').click(function (e) {
        $('#delete-modal').modal('show');
        return false;
    });

    $(document.body).on("click", "#delete-all-modal .approve", function () {
        $.ajax({
            url     : '/coverage/delete-all',
            method  : 'DELETE',
            dataType: 'json',
            data    : {
                start_range: $("#start_range").val(),
                end_range  : $("#end_range").val(),
                region_id  : $('#region-filter').val(),
                user_id    : $('#user-filter').val(),
                search     : $('#fixed-table_filter input').val(),
                filter     : $("#other_filters").val()
            },
            success: function (response) {
                $('#notification').dimmer('show');
                setTimeout(function () {
                    $('#notification').dimmer('hide');
                    window.location.reload();
                }, 1000);
            }
        });
    });

    $('.table.coverage .table-actions a.approve').click(function (e) {
        return changeStatusSelected(e, 'approve');
    });

    $('.table.coverage .table-actions a.return_to_draft').click(function (e) {
        return changeStatusSelected(e, 'draft');
    });

    function changeStatusSelected(e, status) {
        var info = {
            'approve': {list: selected_approve,  url: '/coverage/approve/'},
            'draft'  : {list: selected_to_draft, url: '/coverage/return-to-draft/'},
        };
        if ($(e.currentTarget).hasClass('disabled') || typeof info[status] === 'undefined') {
            return false;
        }

        var records = info[status];

        records['list'].forEach(function (v) {
            $.ajax({
                method  : 'GET',
                url     : records['url'],
                dataType: 'json',
                data    : {id: v},
                success: function () {
                    delete records['list'][$.inArray(v, records['list'])];
                }
            });
        });

        $('#notification').dimmer('show');
        setTimeout(function () {
            $('#notification').dimmer('hide');
            window.location.reload();
        }, 1000);

        return false;
    };

    $('.table.coverage .table-actions a.approve_all').click(function (e) {

        if ($(e.currentTarget).hasClass('disabled')) {
            return false;
        }

        $.ajax({
            method  : 'GET',
            url     : '/coverage/approve-all/',
            dataType: 'json',
            data    : getParams(),
            success: function () {
                $('#notification').dimmer('show');
                setTimeout(function () {
                    $('#notification').dimmer('hide');
                    window.location.reload();
                }, 1000);
            }
        });


        return false;
    });

    var region = $('#region-filter').val(),
        user   = $('#user-filter').val(),
        baseFilters;
    if (typeof region !== undefined && parseInt(region) > 0 || typeof user !== undefined && parseInt(user) > 0) {
        table.draw();
    }

    $('#region-filter').change(function (e) {
        region = $('#region-filter').val();
        if (!clearFilter) {
            table.draw();
            updateExportUrl();
        }
    });

    $('#user-filter').change(function (e) {
        user = $(e.currentTarget).val();
        if (!clearFilter) {
            table.draw();
            updateExportUrl();
        }
    });

    $('#region-filter, #user-filter').on('change', function () {
        highlightFilters();
    });

    function getParams() {
        var start_range = $("#start_range").val();
        var end_range   = $("#end_range").val();
        var params      = {};
        var filter      = JSON.parse($('#other_filters').val());

        if (baseFilters !== undefined) {
            params['filter']      = JSON.stringify(baseFilters);
        }
        if (region !== undefined && parseInt(region) > 0) {
            params['region']      = region;
        }
        if (user !== undefined && parseInt(user) > 0) {
            params['user']        = user;
        }
        if (typeof start_range !== undefined && start_range.length > 0) {
            params['start_range'] = start_range;
        }
        if (typeof end_range !== undefined && end_range.length > 0) {
            params['end_range'] = end_range;
        }
        if (Object.keys(filter).length > 0) {
            params['filter'] = $('#other_filters').val();
        }

        return params;
    }

    function updateExportUrl() {
        var params = getParams();
        var search = $('#fixed-table_filter input').val();
        if (typeof search !== undefined && '' !== search) {
            params['search'] = search;
        }
        $(".export_btn").attr("href", "/coverage/export?" + $.param(params));
    }

    updateExportUrl();

    function isset(value) {
        return 'undefined' !== typeof value;
    }

    function fillFilter() {
        if (null !== localStorage.getItem('filter')) {
            var filter = JSON.parse(localStorage.getItem('filter'));
            if (isset(filter['start_range'])) {
                $("#start_range").datepicker('setDate', filter['start_range']);
            }
            if (isset(filter['end_range'])) {
                $("#end_range").datepicker('setDate', filter['end_range']);
            }
            if (isset(filter['region'])) {
                $("#region_dropdown_filter").dropdown('set value', filter['region']).dropdown('set selected', filter['region']);
            }
            if (isset(filter['user'])) {
                $("#user_dropdown_filter").dropdown('set value', filter['user']).dropdown('set selected', filter['user']);
            }
            if (isset(filter['filter'])) {
                $("#other_filters").val(filter['filter']);
            }
        }
        highlightFilters();
    }

    function defaultSearch() {
        if (null !== localStorage.getItem('filter')) {
            var filter = JSON.parse(localStorage.getItem('filter'));
            if (isset(filter['search'])) {
                return filter['search'];
            }
        }
    }

    function jumpToPage()
    {
        var filter = JSON.parse(localStorage.getItem('filter'));
        var page = filter['page'];
        var tablePage = localStorage.getItem('table_page');
        table.page.len(filter['length']);
        if (null !== tablePage) {
            table.page(parseInt(tablePage)).draw(false);
            localStorage.removeItem('table_page');
        } else {
            table.page(filter['page']).draw(false);
        }
    }

    jQuery.fn.dataTableExt.oApi.fnClearSearch = function (oSettings) {
        $('input', this.fnSettings().aanFeatures.f).val('');
        processHighlight(this, 'search');
        return this;
    };

    $('.clear-filters').click(function ()
    {
        clearFilter = true;

        baseFilters = undefined;
        region = undefined;
        user = undefined;

        $('#other_filters').val('{}');
        $('#start_range').datepicker('setDate', defaultDateFrom);
        $('#end_range').datepicker('setDate', defaultDateTo);
        $("#user_dropdown_filter").dropdown('set value', "").dropdown('set selected', "All");
        $("#region_dropdown_filter").dropdown('set value', "").dropdown('set selected', "All");

        localStorage.removeItem('filter');
        updateExportUrl();

        clearFilter = false;
        $('#fixed-table').dataTable().fnClearSearch();
        $('#fixed-table').dataTable().fnFilter('');
    });

    $(".export_btn").click(function (event) {
        if (recordsFiltered > exportCount) {
            $.ajax({
                method  : 'GET',
                url     : $(this).attr('href'),
                success : function () {
                    $('#export-modal').modal('show');
                }
            });
            event.preventDefault();
        }
    });

    /* Coverage coding process */

    $('a.add').click(function(){
        $.ajax({
            method: 'POST',
            url: '/coverage/check-coding-process',
            dataType: 'json',
            data: { method: 'create'},
            success: function(r) {
                if (r.error == true) {
                    var closedMonth = $("#coding-process-notify-modal").find('#closed-month-name');
                    closedMonth.empty();
                    closedMonth.text(r.month);
                    showCodingProcessNotifyModal();
                    return false;
                } else {
                    window.location.replace("/coverage/create");
                }
            }
        });
    });

    $("#coding-process").click(function(){
        $.ajax({
            method: 'GET',
            url: '/coverage/get-coding-process-modal',
            dataType: 'json',
            data: null,
            success: function(r) {
                $("#coding-process-modal").find('.content').html(r.html);
                $("#coding-process-modal").modal("setting", {
                    autofocus: false,
                    closable: false,
                    onApprove: function () {
                        return false;
                    },
                    close: '.button.cancel',
                }).modal("show");
                $('.input.coding-year input').datepicker({
                    format: "yyyy",
                    viewMode: "years", 
                    minViewMode: "years",
                    autoclose: true,
                    todayHighlight: true
                });
                $('.ui.dimmer.visible').css({'overflow-y': 'scroll'});
            }
        });

    });

    $("#coding-process-modal").on('click', '.save', function(){
        var form = $("#coding-process-modal form");
        $.ajax({
            method: 'POST',
            url: '/coverage/change-coding-process',
            dataType: 'json',
            data: form.serialize(),
            success: function(r) {
                $("#coding-process-modal").modal("hide");
            }
        });
    });
  
    $("#coding-process-modal").on('click', '.cancel', function(){
        $("#coding-process-modal").modal("hide");
    });

    $(document.body).on('click', '#coding-process-form .add-coding-year', function(e){
        e.preventDefault();
        var form = $('#coding-process-form');
        var yearInput = $('#coding-year');
        var activeYears = JSON.parse(form.attr('data-active-years'));
        var codingYearsList = $('#coding-years-list');
        var template = _.template($('#coding-process-template').html());

        yearInput.parents('.field.column.no-margin-bottom').removeClass('error');
        if (yearInput.val() == '') {
            yearInput.parents('.field.column.no-margin-bottom').addClass('error');
            return false;
        }
        if (Object.keys(activeYears).length > 0 && _.contains(activeYears, yearInput.val())) {
            yearInput.parents('.field.column.no-margin-bottom').addClass('error');
            return false;
        }

        activeYears.push(yearInput.val());
        form.attr('data-active-years', JSON.stringify(activeYears));
        codingYearsList.append(template({year: yearInput.val()}));
    });

    $(document.body).on('click', '.delete-coding-year', function(e){
        e.preventDefault();
        var form = $('#coding-process-form');
        var elem = $(this).parents('.coding-year-form');
        var elemYear = elem.attr('data-year');
        var inputYear = elem.find('.coding-date input').val();
        var activeYears = JSON.parse(form.attr('data-active-years'));
        var deletedYears = JSON.parse(form.find('input[name="deleted_years"]').val());

        activeYears = activeYears.filter(function(el){
            return el != inputYear; 
        });

        if (elemYear != '') {
            deletedYears.push(inputYear);
            $('input[name="deleted_years"]').val(JSON.stringify(deletedYears));
        }

        form.attr('data-active-years', JSON.stringify(activeYears));
        elem.remove();
    });

    $(document.body).on('click', '.handle-coding-year', function(e){
        e.preventDefault();
        var form = $('#coding-process-form');
        var elem = $(this).parents('.coding-year-form');
        elem.find('.coding-months').toggle();
    });

    function showCodingProcessNotifyModal()
    {
        $("#coding-process-notify-modal").modal("setting", {
            autofocus: false,
            closable: false,
            onApprove: function () {
                return false;
            },
            close: '.button.close',
        }).modal("show");
    }
});