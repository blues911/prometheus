app.factory('api', function ($rootScope, $http) {

  api_run = function (url, params, callback, method) {
      ajaxCallback = function(response) {
          callback(response);
      };

      if (method == "POST") {
          $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
          ajax = $http.post(url, params || {});
      }
      else {
          ajax = $http.get(url, {params: params || {}});
      }
      ajax.success(ajaxCallback).error(ajaxCallback);
  };

  return {
    top_stories: function (params, callback) {
      api_run('/stats/top-stories', params, callback, "GET");
    },
    comparison: function (params, callback) {
      api_run('/stats/comparison', params, callback, "GET");
    },
    message_penetrations: function (params, callback) {
        api_run('/stats/total-key-messages', params, callback, "GET");
    },
    regional_kpi: function (params, callback) {
      api_run('/stats/regional-kpi', params, callback, "GET");
    },
    product_mentions: function (params, callback) {
      api_run('/stats/regional-product-mentions', params, callback, "GET");
    },
    create_report: function (params, callback) {
      api_run('/report/create', params, callback, "GET");
    },
    save_report_pdf: function (params, callback) {
      api_run('/charts/save-pdf', params, callback, "GET");
    },
    delete_report: function (params, callback) {
      api_run('/report/' + params['id'], params, callback, "POST");
    },
    edit_report: function (params, callback) {
      api_run('/report', params, callback, "POST");
    },
    top_filter: function (params, callback) {
      api_run('/stats/comparison-top-filter', params, callback, "GET");
    }
  };

});