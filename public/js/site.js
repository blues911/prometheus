function processHighlight(input, type) {
    var inputVal = $(input).val(),
        active   = undefined !== inputVal.length && inputVal.length > 0,
        element;

    switch (type) {
        case 'datepicker':
            element = $('.input-daterange .ui.labeled.icon.input');
            active = ($('.input-daterange [name="start"]').val().length > 0 ||
            $('.input-daterange [name="end"]').val().length > 0);
            break;
        case 'dropdown':
            active = ('' !== inputVal && 0 != inputVal && 'All' !== inputVal);
            element = $(input).closest('.ui.selection.dropdown');
            break;
        case 'search':
            element = $(input).closest('div.dataTables_filter');
            break;
        default:
            element = $(input).closest('.ui.labeled.icon.input');
            break;
    }

    if (active) {
        element.addClass('highlight');
    } else {
        element.removeClass('highlight');
    }
}

$(document).ready(function(){
    $('.ui.dropdown.settings-user').dropdown();

    $('.email-modal-random-password').modal("setting", {
        closable : false,
        onApprove: function () {
            return false;
        }
    });
    $(document.body).on("click", "#send-mail-random-password", function () {
        $(".email-modal-random-password .field").removeClass("error");
        var email = $(".email-modal-random-password #input-email-random-password").val();

        if (typeof email !== 'undefined' && email.length > 0) {
            $.ajax({
                url     : '/user/send-mail-for-confirm-password',
                method  : 'POST',
                dataType: 'json',
                data    : {email: email},
                success : function (response) {
                    if (response.status == 'ok') {
                        $(".first-random-password").hide();
                        $(".second-random-password").show();
                    } else if (response.status == 'error') {
                        $(".email-modal-random-password .field").addClass("error");
                    }
                }
            });
        }
        else {
            $(".email-modal-random-password .field").addClass("error");
        }
    });

    $('#change-password').click(function () {
        if ($(".email-modal-random-password .field").hasClass("error")) {
            $(".email-modal-random-password .field").removeClass("error");
        }
        $(".first-random-password").show();
        $(".second-random-password").hide();
        $('.email-modal-random-password').modal("show");
        return false;
    });

    $('.button.second-random-password').click(function () {
        $(".email-modal-random-password #input-email-random-password").val('');
        $('.email-modal-random-password').modal("hide");
    });
});