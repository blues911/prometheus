$(function () {

    var fileId = null;
    $('.help-row').hide();

    $('.import-file').click(function (e) {
        fileId = e.currentTarget.getAttribute('file-id');
        $('#import-files').find('input[type="file"]').fileupload({
            url: '/help/import/id/' + fileId,
            dataType: 'json',
            add: function (e, data) {
                data.submit();
            },
            done: function (e, data) {
                if (!!data.result.error) {
                    $('.help-row .message').removeClass("positive").addClass("negative");
                    var errors = '';
                    $.each(data.result.validationErrors, function(index, value) {
                        errors += value.join(", ");
                    });
                    $('.help-row #message-text').html(errors);
                }
                else {
                    $('.help-row .message').removeClass("negative").addClass("positive");
                    $('.help-row #message-text').html("File upload");
                    $('.help-row').show();
                }
            }
        });
        $(e.currentTarget).closest('.help-files').find('input[type="file"]').click();
    });

    $('.close-title').click(function (e) {
        $('.help-row').hide();
    });

});