$(function () {
    $('.upload-add').click(function (e) {
        $(e.currentTarget).parent().find('input[type="file"]').click();
    });

    $('.ui.checkbox').checkbox();

    function highlightFilters() {
        processHighlight($('#type-filter'), 'dropdown');
        processHighlight($('#start_range'), 'datepicker');
        processHighlight($('.dataTables_filter input[type=search]'), 'search');
    }

    function checkStatusImport(importSection, dataJson) {
        var type = importSection.attr("id").replace("import-", "");
        var errorLink = importSection.find('a.error-link');
        var interval = setInterval(function() {
                $.ajax({
                    url     : '/' + type + '/import/status',
                    method  : 'GET',
                    dataType: 'json',
                    data    : dataJson,
                    success: function (response) {
                        if (typeof response.progress !== 'undefined') {
                            if (response.progress > 0) {
                                importSection.find('.import-progress').removeClass('start');
                            }
                            importSection.find('.import-progress .value').text(response.progress);
                            if (response.progress == 100) {
                                importSection.find(".status").show();
                                if (response.status == "ok") {
                                    importSection.find(".status .value").text('import success!').addClass('teal');
                                } else {
                                    importSection.find(".status .value").text('import failed!').addClass('red');
                                    if (response.error_file) {
                                        errorLink.attr('href', response.error_file);
                                        errorLink.show();
                                    }
                                }
                                clearInterval(interval);
                                $(".re-import").removeClass('disabled');
                            }
                        }
                        else {
                            $('<i/>').addClass('checkmark circular icon');
                            clearInterval(interval);
                            $(".re-import").removeClass('disabled');
                        }
                    }
                });
            }, 5000
        );
    };

    function initImport(importSection) {
        var errorLink = importSection.find('a.error-link');
        errorLink.hide();
        importSection.find('input[type="file"]').fileupload({
            dataType: 'json',
            dropZone: importSection.find('.drop'),

            add: function (e, data) {
                var drop = importSection.find('.drop');
                drop.find('.tip').text(data.files[0].name.substr(0, 20) + '...');
                importSection.find('.upload-add').hide();
                data.context = $('<a>')
                    .addClass('ui button teal')
                    .text('Start import')
                    .appendTo(drop)
                    .click(function () {
                        drop.find('.tip').hide();
                        drop.find('.upload-progress').show();
                        drop.find('.import-progress').show();
                        $(this).hide();
                        data.submit();
                    });
            },

            done: function (e, data) {
                var drop = importSection.find('.drop');
                drop.find('.upload-progress .value').text(100);

                $(".re-import").addClass('disabled');
                checkStatusImport(importSection, {"file": data.result.file});
            },

            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                importSection.find('.upload-progress .value').text(progress);
            }
        });
    }

    initImport($('#import-coverage'));
    initImport($('#import-media'));
    $('.accordion').accordion();

    $(document.body).bind('drop dragover', function (e) {
        e.preventDefault();
    });

    $(document.body).bind('dragover', function (e) {
        var dropZone = $('.drop'),
            foundDropzone,
            timeout  = window.dropZoneTimeout,
            found    = false,
            node     = e.target;

        if (!timeout) {
            dropZone.addClass('in');
        } else {
            clearTimeout(timeout);
        }

        do {
            if ($(node).hasClass('drop')) {
                found = true;
                foundDropzone = $(node);
                break;
            }

            node = node.parentNode;
        } while (node !== null);

        dropZone.removeClass('in hover');

        if (found) {
            foundDropzone.addClass('hover');
        }

        window.dropZoneTimeout = setTimeout(function () {
            window.dropZoneTimeout = null;
            dropZone.removeClass('in hover');
        }, 100);
    });

    var tableEl       = $('#import-history-table'),
    scrollBody        = $('.dataTables_scrollBody'),
    selected_delete   = [],
    selected_to_draft = [],
    selected_approve  = [],
    tableColumns,
    recordsForDelete  = 0,
    table             = tableEl.DataTable({
        scrollCollapse: true,
        searching     : true,
        processing    : true,
        language: {
            processing: '<img src="/img/loading.gif"/>',
            paginate: {
                "sNext": '<i class="caret right icon"></i>',
                "sPrevious": '<i class="caret left icon"></i>'
            }
        },
        aoColumnDefs: [
            {
                "bSortable": false,
                "aTargets" : "_all"
            },
            {
                "targets": "col-file-type",
                "render" : function (value, action, row, settings) {
                    info = {
                        'import_coverage': 'icon-coverage',
                        'import_media'   : 'icon-media',
                    };
                    return '<i class="' + info[row.type] + ' square outline icon"></i>'
                }
            },
            {
                "targets": "col-filename",
                "render" : function (value, action, row, settings) {
                    if (row.type == 'import_coverage') {
                        return '<a href="/coverage?file_id=' + row.id + '" class="name">' + row.name + '</a>'
                    }
                    return row.name;
                }
            },
            {
                "targets": "col-actions",
                "render" : function (value, action, row, settings) {
                    return '<a href="/import/repeat/' + row.id + '" class="right active-block re-import ui undo button teal mini basic" title="Re import the file"><i class="undo icon"></i></a>'
                    + '<a href="/import/file/' + row.id + '" class="right active-block ui download button blue mini basic" title="Download file"><i class="download icon"></i></a>'
                    + '<a href="#" class="right ui delete button red mini basic" title="Delete import file"><i class="delete icon"></i></a>'
                    + '<span class="approve">'
                        + '<a href="#" class="ui no button red mini">No</a>'
                        + '<a href="/import/delete/' + row.id + '" class="ui yes button green mini">Yes</a>'
                    + '</span>';
                }
            },
        ],
        columns: [
            { "data": "id", class: 'ui center aligned no-padding icon-column'},
            { "data": "name" },
            { "data": "created_at", class: 'date-column' },
            { "data": "type", class: 'menu' },
        ],
        dom: '<"top"f>rt<"bottom"ilp><"clear">',
        serverSide: true,
        ajax: {
            url : "/import/history",
            data: function (params) {
                params.start_range = $("#start_range").val();
                params.end_range   = $("#end_range").val();
                params.type        = $('#type-filter').val();
            },
            dataSrc: function ( json ) {
                return json.data;
            }
        },
        drawCallback: function(settings) {
            tableEl.find("thead").remove();
            highlightFilters();
        }
    });

    table.on('draw.dt', function () {
        table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i, data) {
            $(cell).closest("tr").attr("id", "import-history-" + table.row(i).data().id);
            $(cell).closest("tr").addClass("item");
        });
    });

    $('.input-daterange').datepicker({
        format        : "yyyy-mm-dd",
        weekStart     : 1,
        autoclose     : true,
        todayHighlight: true
    }).on('changeDate', function (ev) {
        table.draw();
    });

    $('#type-filter').change(function (e) {
        table.draw();
    });


    $('.ui.dropdown').dropdown();

    $('#import-history-table').resize();

    $('.dataTables_filter label').addClass('ui transparent icon input');
    $('.dataTables_filter input').after('<i class="search icon"></i>');
    $('.dataTables_paginate').addClass('ui borderless pagination menu');
    $('.dataTables_paginate a').addClass('item');

    $('.dataTables_filter').click(function (e) {
        $('.dataTables_filter').css('border-color', 'rgba(39, 41, 43, 0.3)')
        $(this).find('input').animate({ width: '150px' }, 500);
    });

    $("body").click(function(e) {
        if ($(e.target).closest(".dataTables_filter").length === 0) {
            $('.dataTables_filter').css('border-color', 'rgba(39, 41, 43, 0.15)');
            $('.dataTables_filter input').animate({ width: '0px' }, 500);
        }
    });

    $(document.body).on("click", ".item.import-errors", function(e) {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).next('.item-error').hide();
        } else {
            $(this).addClass('active');
            $(this).next('.item-error').show();
            
        }
    });

    $('.menu.charts .item').tab({
        onTabLoad: function (path) {
            $('#import-history-table').resize();
        }
    });

    $(document.body).on("click", ".import-history a.delete, .import a.delete", function(e) {
        var b = $(e.currentTarget);

        if (b.hasClass('disabled')) {
            return false;
        }

        b.closest(".item").find('.active-block').animate(
            {opacity: 0},
            {
                complete: function () {
                    b.closest(".item").find('.active-block').css('display', 'none');
                }
            }
        );

        b.animate({
            opacity: 0
        }, {
            complete: function () {
                b.parent().addClass("active-item-menu");
                b.css('display', 'none');
                b.parent().find('.approve').css({'display': 'inline'});
                b.parent().find('.approve').animate({
                    opacity: 1
                });
            }
        });

        return false;
    });

    $(document.body).on("click", ".import-history .no, .import .no", function(e) {
        var a = $(e.currentTarget).parent('.approve');
        a.animate(
            {
                opacity: 0
            },
            {
                complete: function () {
                    a.parent().removeClass("active-item-menu");
                    a.css('display', 'none');
                    a.closest(".item").find('a.delete, .active-block').css('display', 'inline');
                    a.closest(".item").find('a.delete, .active-block').animate({
                        opacity: 1
                    });
                }
            }
        );
        return false;
    });

    $(document.body).on("click", ".import-history .yes", function(e) {
        var a = $(e.currentTarget);
        $.ajax({
            url     : a.attr('href'),
            method  : 'POST',
            dataType: 'json',
            success : function (response) {
                if (typeof response.id !== 'undefined') {
                    $("#import-history-" + response.id).remove();
                }
            }
        });

        return false;
    });

    $(document.body).on("click", ".import .yes", function(e) {
        e.preventDefault();
        var item = $(this).closest('.item');
        $.post('/import/read-message', {id: item.attr('data-msg-id')}, function (data) {
            $('.label.unread').text(data.unread_count);
        });
        item.closest(".import-errors").next().remove();
        item.remove();
    });

    $(document.body).on("click", ".re-import", function(e) {
        var a = $(e.currentTarget);
        if ($(this).hasClass('disabled')) {
            return;
        }
        $(".re-import").addClass('disabled');
        $.ajax({
            url     : a.attr('href'),
            method  : 'POST',
            dataType: 'json',
            success : function (data) {
                if ('undefined' !== typeof data.file) {
                    var selector = '#' + data.type.replace("_", "-");
                    $('.menu.charts .item').tab('change tab', "main_import");
                    $(selector).find('.upload-add').hide();
                    $(selector).find('.drop').find('.tip').hide();
                    $(selector).find('.drop').find('.upload-progress').show();
                    $(selector).find('.drop').find('.upload-progress .value').text(100);
                    $(selector).find('.drop').find('.import-progress').show();

                    checkStatusImport($(selector), {"file": data.file});
                }
            }
        });
        return false;
    });

});