$(document).ready(function(){
	var table = $('#fixed-table').DataTable( {
        scrollY:        '500px',
        scrollX:        true,
        scrollCollapse: true,
        paging:         true,
        columnDefs: [
        	{ width: '20%', targets: 0 }
        ],
        dom: '<"top"f>rt<"bottom"lip><"clear">',
				"aoColumnDefs": [ 
					{ "bSortable": false, "aTargets": [ 0 ] } 
				],
				"order": [[ 0, "asc" ]]
    } );        

    
		new $.fn.dataTable.FixedColumns( table, {
        leftColumns: 1
    } );  
    
    $('.dataTables_filter input').attr("placeholder", "Search...");
    $('.dataTables_paginate').addClass('ui borderless pagination menu');
    $('.dataTables_paginate a').addClass('item');
    
		var heightfix = $('.dataTables_scrollBody').outerHeight() - 15;
		$('.dataTables_scrollBody').css('height', 'auto');
		
		$('#fixed-table tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
    } );
});    