$(document).ready(function(){

	$('.menu .item').tab({
		history: true,
    historyType: 'state'
  });
  
  $('#goto-stage2').click(function(){
		$('.menu .item').tab('change tab', 'analytical-pt1'); 
		$('.menu .item').tab({ history: true, historyType: 'state'}); 
  });
  
  $('#goto-stage3').click(function(){
		$('.menu .item').tab('change tab', 'analytical-pt2'); 
		$('.menu .item').tab({ history: true, historyType: 'state'}); 
  });
  
  $('#goto-stage4').click(function(){
		$('.menu .item').tab('change tab', 'speakers'); 
		$('.menu .item').tab({ history: true, historyType: 'state'}); 
  });
  
  $('#goto-stage5').click(function(){
		$('.menu .item').tab('change tab', 'impact'); 
		$('.menu .item').tab({ history: true, historyType: 'state'});
		$('.global-submit').css('opacity',1); 
  });      
  
  $('.ui.dropdown').dropdown();
  $('.ui.modal').modal();  
  
  $('.adduser').click(function(){
	  $('.add-user').toggleClass('visible');
	  $('.add-toggle').toggleClass('invisible');		
	  $('.add-toggle-header').fadeIn().toggleClass('visible');
  });
  
  $('#save-user').click(function(){
	  $('.add-user').toggleClass('visible');
	  $('.add-toggle').toggleClass('invisible');
	  $('.add-toggle-header').toggleClass('visible').fadeOut();	  
  });  
  
	$('#publication-date').datepicker({
	    format: "dd.mm.yyyy",
	    weekStart: 1,
	    endDate: "today",
	    todayBtn: "linked"
	});  

  
});