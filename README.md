# Prometheus
### Service for media coverage monitoring and management

### Get started

1) Clone project from github

```
git clone https://github.com/Fahrenheit-451/prometheus ./prometheus
```

2) Need download all vendors from composer
```
composer install
```
3) Next need copy .env.example.php and rename to .env.php. After rename you need open him and add your app configuration.
Default .env.php contains

```
  'APP_DEBUG'          => 'false',
  'APP_URL'            => 'http://prometheus.lo',
  'APP_KEY'            => 'SomeRandomString',
  'APP_ASSETS_VERSION' => '',
  'DB_HOST'            => 'localhost',
  'DB_DATABASE'        => 'dbname',
  'DB_USERNAME'        => 'dbusername',
  'DB_PASSWORD'        => '',
```

4) After adding configuration in .env.php you need execute all migration

```
cd /you_base_app_path/
php artisan migrate
```
